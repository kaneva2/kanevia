/******************************************************************************
 collisionmanager.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "collidable.h"
#include "bvsphere.h"
#include "bvellipsoid.h"
#include "octnode.h"
#include "worldobject.h"
#include "collisionmanager.h"
#include "collision.h"

#include "renderstate.h"
#include "meshrenderer.h"
#include "meshnode.h"
#include "meshfactory.h"
#include "mesh.h"
#include "device.h"

#include <algorithm>
#include <vector>

namespace wkg
{

namespace engine
{


void 
CollisionManager::Collide(Collidable *c1, Collidable *c2, uint32 ms)
{
	// collide two collidables
	Collision *c = c1->Collide(c2, ms);
	if(c)
	{
		t_collision tc;
		tc.c1 = c1;
		tc.c2 = c2;
		tc.c = c;
		m_collisions.push_back(tc);
	}
}

void
CollisionManager::Callbacks()
{
	//SortCollisions();

	int32 nc = m_collisions.size();
//	DEBUGMSG("C: %d", nc);

	for(int32 i = 0; i < m_collisions.size(); i++)
	{

		Collidable *c1 = m_collisions[i].c1;
		Collidable *c2 = m_collisions[i].c2;

		Collision *c = m_collisions[i].c;
		if(c)
		{
			if(Find(c1) >= 0)
			{
				c1->OnCollide(c2, c);
			}

			if(Find(c2) >= 0)
			{
				c->m_n = -c->m_n;
				c2->OnCollide(c1, c);
			}
	
			delete c;
		}
	}
	m_collisions.clear();

	nc = m_unsorted_collisions.size();
	for(i = 0; i < m_unsorted_collisions.size(); i++)
	{

		Collidable *c1 = m_unsorted_collisions[i].c1;
		Collidable *c2 = m_unsorted_collisions[i].c2;

		Collision *c = m_unsorted_collisions[i].c;
		if(c)
		{
			if(Find(c1) >= 0)
			{
				c1->OnCollide(c2, c);
			}

			if(Find(c2) >= 0)
			{
				c->m_n = -c->m_n;
				c2->OnCollide(c1, c);
			}
	
			delete c;
		}
	}
	m_unsorted_collisions.clear();


	std::set<Collidable *>::iterator itor = m_to_delete.begin();
	for(; itor != m_to_delete.end(); itor++)
	{
		delete *itor;
	}

	m_to_delete.clear();
}

class sortit
{
public:
	bool operator () (t_collision c1, t_collision c2)
	{
//		ASSERT(c1.c->m_t >= 0.0f && c1.c->m_t <= 1.0f);
//		ASSERT(c2.c->m_t >= 0.0f && c1.c->m_t <= 1.0f);

		if(c1.c && c2.c)
			return c1.c->m_t < c2.c->m_t;
		else
			return true;
	}
};

void
CollisionManager::SortCollisions()
{
	std::sort(m_collisions.begin(), m_collisions.end(), sortit());
}

void 
CollisionManager::Collide(uint32 ms)
{
	if(m_collidables.size() < 2) return;

	for(int32 i = 0; i < m_collidables.size(); i++)
	{
		for(int32 j = i+1; j < m_collidables.size(); j++)
		{
			Collide(m_collidables[i], m_collidables[j], ms);
		}
	}
}

void
CollisionManager::Render()
{
	RenderState rs;

	rs.Set(D3DRS_LIGHTING, FALSE);
	rs.Set(D3DRS_CULLMODE, D3DCULL_NONE);
	rs.Set(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	rs.Set(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
	rs.Set(D3DRS_FOGENABLE, FALSE);
	rs.Set(D3DRS_FILLMODE,  D3DFILL_WIREFRAME);

	Matrix44 ident; ident.Identity();
	Device::Instance()->SetTransform(D3DTS_WORLD, &ident);
	Device::Instance()->SetRenderState(rs);
	Device::Instance()->SetTexture(0, (Texture*)0);
	Device::Instance()->SetPixelShader(0);

	MeshDescriptor msd;
	Mesh *mesh;
	MeshNode *mn;

	msd.filename = "unit_sphere.wbm";
	msd.type = MeshDescriptor::STANDARD_MESH;
	mesh = MeshFactory::Instance()->Get(msd);

	mn = new MeshNode();
	mn->SetMesh(mesh);

	mn->SetRenderer(new MeshRenderer());

	// render each bvsphere
	for(int32 i = 0; i < m_collidables.size(); i++)
	{
		BVSphere *bvs;
		bvs = dynamic_cast<BVSphere *>(m_collidables[i]->GetBoundingVolume());
		if(bvs)
		{
			Matrix44 sm;
			sm.Scale(bvs->GetRadius()/1.0f, bvs->GetRadius()/1.0f, bvs->GetRadius()/1.0f);

			Vector3 pos = bvs->GetWorldObject()->GetPosition();
			pos += bvs->GetWorldObject()->GetBVOffset();
			Matrix44 tm;
			tm.Translate(pos);

			Matrix44 m = sm * tm;

			mn->SetMatrix(m);

			m.Identity();

			mn->DFSUpdate(m);
			mn->DFSRender();
		}

		BVEllipsoid *bve;
		bve = dynamic_cast<BVEllipsoid *>(m_collidables[i]->GetBoundingVolume());
		if(bve)
		{
			Matrix44 sm;
			sm.Scale(bve->GetRadius().x, bve->GetRadius().y, bve->GetRadius().z);

			Vector3 pos = bve->GetWorldObject()->GetPosition();
			pos += bve->GetWorldObject()->GetBVOffset();
			Matrix44 tm;
			tm.Translate(pos);

			Matrix44 m = sm * tm;

			mn->SetMatrix(m);

			m.Identity();

			mn->DFSUpdate(m);
			mn->DFSRender();
		}
	}

	delete mn;
	mesh->Release();

	RenderCollisions();
}

void 
CollisionManager::RenderCollision(const Vector3 &c, uint32 color)
{
	RenderState rs;

	rs.Set(D3DRS_LIGHTING, FALSE);
	rs.Set(D3DRS_CULLMODE, D3DCULL_NONE);
	rs.Set(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	rs.Set(0, D3DTSS_COLORARG1, D3DTA_TFACTOR);
	rs.Set(D3DRS_TEXTUREFACTOR, color);
	rs.Set(D3DRS_FOGENABLE, FALSE);
	rs.Set(D3DRS_FILLMODE,  D3DFILL_WIREFRAME);

	Matrix44 ident; ident.Identity();
	Device::Instance()->SetTransform(D3DTS_WORLD, &ident);
	Device::Instance()->SetRenderState(rs);
	Device::Instance()->SetTexture(0, (Texture*)0);
	Device::Instance()->SetPixelShader(0);

	MeshDescriptor msd;
	Mesh *mesh;
	MeshNode *mn;

	msd.filename = "unit_sphere.wbm";
	msd.type = MeshDescriptor::STANDARD_MESH;
	mesh = MeshFactory::Instance()->Get(msd);

	mn = new MeshNode();
	mn->SetMesh(mesh);

	mn->SetRenderer(new MeshRenderer());

	Matrix44 sm;
	sm.Scale(1.0f, 1.0f, 1.0f);

	Vector3 pos = c;

	Matrix44 tm;
	tm.Translate(pos);

	Matrix44 m = sm * tm;

	mn->SetMatrix(m);

	m.Identity();

	mn->DFSUpdate(m);
	mn->DFSRender();

	delete mn;

//	mesh->Release();
}

void 
CollisionManager::RenderCollision(Vector3 c, Vector3 size, Vector3 axis[3])
{
	RenderState rs;

	rs.Set(D3DRS_LIGHTING, FALSE);
	rs.Set(D3DRS_CULLMODE, D3DCULL_NONE);
	rs.Set(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	rs.Set(0, D3DTSS_COLORARG1, D3DTA_TFACTOR);
	rs.Set(D3DRS_TEXTUREFACTOR, 0xffffffff);
	rs.Set(D3DRS_FOGENABLE, FALSE);
	rs.Set(D3DRS_FILLMODE,  D3DFILL_WIREFRAME);

	Matrix44 ident; ident.Identity();
	Device::Instance()->SetTransform(D3DTS_WORLD, &ident);
	Device::Instance()->SetRenderState(rs);
	Device::Instance()->SetTexture(0, (Texture*)0);
	Device::Instance()->SetPixelShader(0);

	MeshDescriptor msd;
	Mesh *mesh;
	MeshNode *mn;

	msd.filename = "unit_box.wbm";
	msd.type = MeshDescriptor::STANDARD_MESH;
	mesh = MeshFactory::Instance()->Get(msd);

	mn = new MeshNode();
	mn->SetMesh(mesh);

	mn->SetRenderer(new MeshRenderer());

	Matrix44 rm;
	rm._11 = axis[0].x; rm._12 = axis[0].y; rm._13 = axis[0].z; rm._14 = 0.0f;
	rm._21 = axis[1].x; rm._22 = axis[1].y; rm._23 = axis[1].z; rm._24 = 0.0f;
	rm._31 = axis[2].x; rm._32 = axis[2].y; rm._33 = axis[2].z; rm._34 = 0.0f;
	rm._41 = 0.0f;      rm._42 = 0.0f;      rm._43 = 0.0f;      rm._44 = 1.0f;

	Matrix44 sm;
	sm.Scale(size.x, size.y, size.z);

	Vector3 pos = c;

	Matrix44 tm;
	tm.Translate(pos);

	Matrix44 m = sm * rm * tm;

	mn->SetMatrix(m);

	m.Identity();

	mn->DFSUpdate(m);
	mn->DFSRender();

	delete mn;

//	mesh->Release();
}

void
CollisionManager::RenderCollisions()
{
	for(int32 i = 0; i < m_collisions_to_render.size(); i++)
	{
		RenderCollision(m_collisions_to_render[i], m_colors[i]);
	}
	ClearRenderCollisions();

	for(i = 0; i < m_box_collisions_to_render.size(); i++)
	{
		RenderCollision(m_box_collisions_to_render[i].c, m_box_collisions_to_render[i].s, m_box_collisions_to_render[i].a);
	}
}

void 
CollisionManager::AddRenderCollision(Vector3 c, uint32 color)
{
	m_collisions_to_render.push_back(c);
	m_colors.push_back(color);
}

void 
CollisionManager::ClearRenderCollisions()
{
	m_collisions_to_render.clear();
	m_colors.clear();
}

}

}



