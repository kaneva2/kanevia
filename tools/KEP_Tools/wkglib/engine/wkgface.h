/******************************************************************************
 wkgface.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

class WKGFace
{
public:
	uint32 v1, v2, v3;
	uint8 material;
};


}