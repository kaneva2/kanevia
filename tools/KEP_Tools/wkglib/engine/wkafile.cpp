/******************************************************************************
 wkafile.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "wkafile.h"
#include "meshtypes.h"
#include "filestream.h"
#include <string>

namespace wkg
{

WKAFile::WKAFile() :
	m_file_layout(0),
	m_file_size(0)
{
}

WKAFile::~WKAFile()
{
	Reset();
}

bool
WKAFile::Reset()
{
	delete [] m_file_layout;
	m_file_layout = 0;
	m_file_size = 0;

	return true;
}

void 
WKAFile::SetSizes(uint32 num_keyframes, uint32 num_bone_weights)
{
	Reset();

	m_file_size = 
		sizeof(WKAHeaderLayout) + 
		sizeof(WKGKeyframe) * num_keyframes +
		sizeof(WKGBoneWeight) * num_bone_weights;

	m_file_layout = 
		(WKAFileLayout *)(new uint8[m_file_size]);
	ASSERT(m_file_layout);

	memset(m_file_layout, 0, m_file_size);

	m_file_layout->m_header.m_type = 'WKGA';
	m_file_layout->m_header.m_version = WKA_VERSION;

	m_file_layout->m_header.m_num_keyframes = num_keyframes;
	m_file_layout->m_header.m_keyframe_offset = sizeof(WKAHeaderLayout);

	m_file_layout->m_header.m_num_bone_weights = num_bone_weights;
	m_file_layout->m_header.m_bone_weight_offset = 
		m_file_layout->m_header.m_keyframe_offset + 
		num_keyframes * sizeof(WKGKeyframe);
}

void 
WKAFile::SetKeyframes(WKGKeyframe *keyframes)
{
	uint8 *dst = ((uint8 *)m_file_layout) + m_file_layout->m_header.m_keyframe_offset;
	memcpy(dst, keyframes, m_file_layout->m_header.m_num_keyframes * sizeof(WKGKeyframe));
}

void 
WKAFile::SetBoneWeights(WKGBoneWeight *weights)
{
	uint8 *dst = ((uint8 *)m_file_layout) + m_file_layout->m_header.m_bone_weight_offset;
	memcpy(dst, weights, m_file_layout->m_header.m_num_bone_weights * sizeof(WKGBoneWeight));
}

bool 
WKAFile::Load(const char *filename)
{
	FileStream fs;
	if(fs.Open(filename, FileStream::ReadOnly))
	{
		// read in the header
		WKAHeaderLayout header;
		fs.Read((uint8 *)(&header), sizeof(WKAHeaderLayout));

		if(header.m_type != 'WKGA' || header.m_version != WKA_VERSION)
		{
			DEBUGMSG("Invalid WKAFile type or version");
			fs.Close();
			return false;
		}

		SetSizes(header.m_num_keyframes, header.m_num_bone_weights);

		if (!m_file_layout)
		{
			ASSERT(0);
			fs.Close();
			return false;
		}

		memcpy(m_file_layout, &header, sizeof(WKAHeaderLayout));

		uint32 toRead = m_file_size - sizeof(WKAHeaderLayout);

		if (toRead != fs.Read(((uint8 *)m_file_layout) + sizeof(WKAHeaderLayout), toRead))
		{
			ASSERT(0);
			fs.Close();
			return false;
		}

		fs.Close();

		return true;
	}
	else
	{
		return false;
	}
}

bool 
WKAFile::Save(const char *filename)
{
	ASSERT(m_file_layout);

	FileStream fs;
	if(fs.Open(filename, FileStream::CreateAlways))
	{
		fs.Write((uint8 *)m_file_layout, m_file_size);

		fs.Close();

		return true;
	}
	else
	{
		return false;
	}
}

void 
WKAFile::FlipHands()
{
	uint32 num_kfs = GetNumKeyframes();
	WKGKeyframe *keyframes = GetKeyframes();
	for(uint32 kf = 0; kf < num_kfs; kf++)
	{
		keyframes[kf].m_position.z = -keyframes[kf].m_position.z;
		keyframes[kf].m_orientation.x = -keyframes[kf].m_orientation.x;
		keyframes[kf].m_orientation.y = -keyframes[kf].m_orientation.y;
	}
}

}




