/******************************************************************************
 Font.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"
#include <map>

namespace wkg {

class BMP;

namespace engine {

class Texture;
class VertexShader;
class DynIVB;

class Font
{
	protected:
		struct DrawState;

	public:
		Font();
		~Font();

		bool Load(const TCHAR* filename);

		void Measure(const TCHAR* text, int32* wid, int32* hei) const;	// in screen-space pixels or world-space units
		uint32 GetAscent() const;										// in screen-space pixels or world-space units
		uint32 GetDescent() const;										// in screen-space pixels or world-space units
		uint32 GetHeight() const;

		void SetVBIB(LPDIRECT3DVERTEXBUFFER9, LPDIRECT3DINDEXBUFFER9);
		void SetColor(uint8 a, uint8 r, uint8 g, uint8 b);
		void SetZPlane(real);	// 0.0 is front-most, 1.0 is back-most (and not visible)...only used when drawing screen-space text

		void Draw(int32 x, int32 yBaseline, const TCHAR* text) const;						// x,y screen coords...text is drawn in screen-space
		void DrawRotated(int32 x, int32 yBaseline, real degrees, const TCHAR* text) const;	// x,y screen coords...text is drawn in screen-space

		void Draw(const TCHAR* text) const;	// 3D text at current world transform

	protected:
		void Draw(const TCHAR*, void (*)(uint8*, int32, const DrawState&, real, real, real, real, real)) const;
		static void AddScreenVert(uint8*, int32, const DrawState&, real, real, real, real, real);
		static void AddWorldVert(uint8*, int32, const DrawState&, real, real, real, real, real);

		void ExtractFontMetrics(const BMP*);
		void ExtractGlyphMetrics(const BMP*);
		int32 ExtractWhiteCount(const BMP*, int32, int32, int32, int32, int32) const;
		int32 ExtractPosNeg(const BMP*, int32, int32, uint32, uint32) const;
		int32 ExtractByte(const BMP*, int32, int32, int32, int32) const;

	protected:
		uint32 cellSizeX, cellSizeY;
		uint32 ascent;
		uint32 numRows, numCols, rowHei, numGlyphs;
		engine::Texture* glyphTex;

		struct GlyphMetric
		{
			int32 a, b, c;			// ABC widths
			real tl, tt, tr, tb;	// tex coords of left, top, right, bottom of glyph
		};

		typedef std::map<uint16, GlyphMetric> GlyphMap;
		typedef GlyphMap::const_iterator GMCIter;

		GlyphMap glyphMap;

		struct DrawState
		{
			uint32					color;
			mutable Vector3			origin;
			mutable real			cosAng, sinAng;
			mutable uint32			vertSize;
			VertexShader*			screenVS;
			VertexShader*			worldVS;
			LPDIRECT3DVERTEXBUFFER9	pVB;
			LPDIRECT3DINDEXBUFFER9	pIB;
		};

		DrawState state;
};

inline uint32 
Font::GetAscent() const
{
	return ascent;
}

inline uint32 
Font::GetDescent() const
{
	return cellSizeY - ascent;
}

inline uint32 
Font::GetHeight() const
{
	return GetAscent() + GetDescent();
}

inline void 
Font::SetColor(uint8 a, uint8 r, uint8 g, uint8 b)
{
	state.color = D3DCOLOR_ARGB(a, r, g, b);
}

inline void 
Font::SetZPlane(real newZ)
{
	state.origin.z = CLAMP(newZ, 0.0f, 1.0f);
}

inline void 
Font::SetVBIB(LPDIRECT3DVERTEXBUFFER9 vb, LPDIRECT3DINDEXBUFFER9 ib)
{
	state.pVB = vb;
	state.pIB = ib;
}

}

}

