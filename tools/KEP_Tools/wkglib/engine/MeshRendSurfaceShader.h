/******************************************************************************
 MeshRendSurfaceShader.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "MeshRenderer.h"

namespace wkg {
namespace engine {
		
class MeshRendSurfaceShader : public MeshRenderer
{
	public:
		virtual void Render(Mesh*);
};

}}
