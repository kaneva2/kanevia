/******************************************************************************
 FontVert.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {
namespace engine {

class FontScreenVert
{
	public:
		enum Constants
		{
			FVF = D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0)
		};

	public:
		Vector3 pos;
		real rhw;
		uint32 diffuse;
		real tu, tv;
};

class FontWorldVert
{
	public:
		enum Constants
		{
			FVF = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0)
		};

	public:
		Vector3 pos;
		uint32 diffuse;
		real tu, tv;
};

}}