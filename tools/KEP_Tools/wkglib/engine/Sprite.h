/******************************************************************************
 Sprite.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <vector>

namespace wkg {
namespace engine {

class Texture;
class DynIVB;

class Sprite
{
    public:
        Sprite(const char* szFilename);
        Sprite(Texture* pTex);
        Sprite();
        ~Sprite();

        void Draw(int32 x, int32 y,
				  RECT* srcRect = NULL,
				  real scaleX = 1.0f,
				  real scaleY = 1.0f, 
				  real radiansCCW = 0.0f,
				  int32 rotCenterX = 0,
				  int32 rotCenterY = 0,
				  uint32 color = 0xFFFFFFFF);	// queues sprite for later rendering by FlushDrawQueue()

        void Draw();							// queues sprite for later rendering by FlushDrawQueue()

		void DrawImmediate();					// renders sprite (i.e. not queued)

		void Set(int32 x, int32 y,
	 			 RECT* srcRect,
				 real scaleX, real scaleY, 
				 real radiansCCW, int32 rotCenterX, int32 rotCenterY,
				 uint32 color);

		void SetTexture(Texture*);
		void SetTexture(const char*);
		void SetPosition(int32 x, int32 y);
		void SetSrcRect(RECT*);
		void SetScale(real x, real y);
		void SetRotation(real radiansCCW);
		void SetRotationCenter(int32 x, int32 y);
		void SetColorAdjust(uint32 color);

		Texture* GetTexture() const;

		int32 GetTextureWidth() const;	// todo: kill this when we make all sprite textures powers of 2
		int32 GetTextureHeight() const;	// todo: kill this...replace all 4 of these with GetWidth/GetHeight

		int32 GetImageWidth() const;	// todo: kill this
		int32 GetImageHeight() const;	// todo: kill this

		int32 GetPosX() const;
		int32 GetPosY() const;
		void GetSrcRect(RECT*) const;	// null pointer uses the entire texture
		real GetScaleX() const;
		real GetScaleY() const;
		real GetRotation() const;		// radians (pos is CCW)
		int32 GetRotCenterX() const;
		int32 GetRotCenterY() const;
		uint32 GetColorAdjust() const;

		static void FlushDrawQueue();

		static void SetVBIB(LPDIRECT3DVERTEXBUFFER9, LPDIRECT3DINDEXBUFFER9);	// once for app

	protected:
		static void PrepareToRender(DynIVB*);
		void Render(DynIVB*);
		static void FlushBatch(DynIVB*);

    protected:
        Texture*             m_pTexture;
		int32				 m_x, m_y;
		int32				 m_texWidth, m_texHeight;	// todo: kill this
		RECT				 m_srcRect;
		real				 m_scaleX;		// scale factors with respect to the original
		real				 m_scaleY;		// image, not the texture size (which is a power of 2)
		real				 m_rotCCW;		// counterclockwise rotation
		int32				 m_rotCenterX;	// x screen coord to rotate around
		int32				 m_rotCenterY;	// y screen coord to rotate around
		uint32				 m_colorAdjust;

		typedef std::vector<Sprite*> SpriteVector;
		typedef SpriteVector::iterator SpriteIterator;

		static LPDIRECT3DVERTEXBUFFER9	s_vb;
		static LPDIRECT3DINDEXBUFFER9	s_ib;
		static Texture*					s_curTex;
		static SpriteVector				s_spritesToDraw;
};

inline void 
Sprite::SetPosition(int32 x, int32 y)
{
	m_x = x;
	m_y = y;
}

inline void 
Sprite::SetSrcRect(RECT* src)
{
	if (src)
		m_srcRect = *src;
	else
	{
		m_srcRect.left = m_srcRect.top = 0;
		m_srcRect.right = GetImageWidth();
		m_srcRect.bottom = GetImageHeight();
	}
}

inline void 
Sprite::SetScale(real x, real y)
{
	m_scaleX = x;
	m_scaleY = y;
}

inline void 
Sprite::SetRotation(real radiansCCW)
{
	m_rotCCW = radiansCCW;
}

inline void 
Sprite::SetRotationCenter(int32 x, int32 y)
{
	m_rotCenterX = x;
	m_rotCenterY = y;
}

inline void 
Sprite::SetColorAdjust(uint32 color)
{ 
	m_colorAdjust = color;
}

inline Texture* 
Sprite::GetTexture() const 
{
	return m_pTexture;
}

inline int32 
Sprite::GetTextureWidth() const 
{
	return m_texWidth; 
}

inline int32 
Sprite::GetTextureHeight() const 
{ 
	return m_texHeight; 
}

inline int32 
Sprite::GetPosX() const
{
	return m_x;
}

inline int32 
Sprite::GetPosY() const
{
	return m_y;
}

inline void 
Sprite::GetSrcRect(RECT* result) const
{
	ASSERT(result);

	*result = m_srcRect;
}

inline real 
Sprite::GetScaleX() const
{
	return m_scaleX;
}

inline real 
Sprite::GetScaleY() const
{
	return m_scaleY;
}

inline real 
Sprite::GetRotation() const
{
	return m_rotCCW;
}

inline int32 
Sprite::GetRotCenterX() const
{
	return m_rotCenterX;
}

inline int32 
Sprite::GetRotCenterY() const
{
	return m_rotCenterY;
}

inline uint32 
Sprite::GetColorAdjust() const
{
	return	m_colorAdjust;
}

inline void 
Sprite::Set(int32 x, int32 y,
			RECT* pSrcRect, 
			real scaleX, real scaleY,
			real radiansCCW, int32 rotCenterX, int32 rotCenterY, 
			uint32 color)
{
	SetPosition(x, y);
	SetSrcRect(pSrcRect);
	SetScale(scaleX, scaleY);
	SetRotation(radiansCCW);
	SetRotationCenter(rotCenterX, rotCenterY);
	SetColorAdjust(color);
}

inline void 
Sprite::Draw(int32 x, int32 y,
			 RECT* pSrcRect,
			 real scaleX, real scaleY,
			 real rotationCCW, int32 rotCenterX, int32 rotCenterY, 
			 uint32 color)
{
	Set(x, y, pSrcRect, scaleX, scaleY, rotationCCW, rotCenterX, rotCenterY, color);
	Draw();
}

}}
