/******************************************************************************
 MeshRendGeomOnly.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "MeshRendGeomOnly.h"
#include "MeshTypes.h"
#include "Mesh.h"
#include "MeshSection.h"
#include "Device.h"

namespace wkg {
namespace engine {

void MeshRendGeomOnly::Render(Mesh* mesh)
{
	MeshSection* section;
	uint32 i;

	Device::Instance()->SetStreamSource(0, mesh->GetVB(), 0, sizeof(WKGVertex));

	for (i = 0; i < mesh->GetNumSections(); i++)
	{
		section = mesh->GetSections()[i];
		if (section)
			section->Render();
	}
}

}}
