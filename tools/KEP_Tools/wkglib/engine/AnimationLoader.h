/******************************************************************************
 AnimationLoader.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "singleton.h"

#pragma warning (disable : 4503)

namespace wkg
{

class XMLDocument;
class XMLNode;

namespace engine
{
class AnimatedMesh;
}

namespace game
{

class AnimationLoader : public Singleton<AnimationLoader>
{
public:
	AnimationLoader();
	virtual ~AnimationLoader();

	bool Load(std::string filename);

	bool ApplySetToAnimatedMesh(std::string set, engine::AnimatedMesh *mesh);
	bool ApplyAllToAnimatedMesh(engine::AnimatedMesh *mesh);

protected:
	void DumpSet(std::string set);
	void Dump();

protected:
	typedef std::map<std::string, std::string> NameAnimMap;
	typedef std::map<std::string, NameAnimMap> AnimSetMap;

	XMLDocument* CreateXMLDoc(std::string filename);
	void Load(XMLNode* curr, NameAnimMap &name_anim_map);

	AnimSetMap m_anim_set_map;

};

}

}

