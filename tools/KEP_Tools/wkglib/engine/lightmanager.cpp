/******************************************************************************
 lightmanager.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "xmldocument.h"
#include "xmlnode.h"
#include "xmlnodeattributes.h"
#include "vector3.h"
#include "light.h"
#include "matrix44.h"
#include "vector4.h"
#include "lightmanager.h"

#include <algorithm>

namespace wkg
{

namespace engine
{


LightManager::LightManager() : 
	Singleton<LightManager>(), 
	TimedUpdatable()
{
}

LightManager::~LightManager()
{
}

int32 
LightManager::GetNumLights()
{
	return m_lights.size();
}

void 
LightManager::AddLight(Light *light)
{
	m_lights.push_back(light);
}

Light *
LightManager::GetLight(int32 idx)
{
	return m_lights[idx];
}

Light *
LightManager::GetClosestLight(const Vector3 &pos)
{
	if(m_lights.size() > 0)
	{
		real closest_dist = (pos - m_lights[0]->GetPosition()).LengthSqr();
		Light *closest_light = m_lights[0];

		for(int32 i = 1; i < m_lights.size(); i++)
		{
			real dist = (pos - m_lights[i]->GetPosition()).LengthSqr();
			if(dist < closest_dist)
			{
				closest_dist = dist;
				closest_light = m_lights[i];
			}
		}

		return closest_light;
	}
	else
	{
		return 0;
	}
}

class LightPredicate
{
public:
	LightPredicate(const Vector3 &pos) : m_pos(pos) {}

	bool operator () (Light *l1, Light *l2)
	{
		real d1 = (l1->GetPosition() - m_pos).Length();
		real d2 = (l2->GetPosition() - m_pos).Length();
		return d1 < d2;
	}

	Vector3 m_pos;
};

int32 
LightManager::GetClosestLights(const Vector3 &pos, Light **lights, int32 max_lights)
{
	std::vector<Light *> sorted_lights = m_lights;
	std::sort(sorted_lights.begin(), sorted_lights.end(), LightPredicate(pos));

	if(sorted_lights.size() < max_lights)
		max_lights = sorted_lights.size();

	for(int32 i = 0; i < max_lights; i++)
	{
		lights[i] = sorted_lights[i];
	}
	return max_lights;

}

int32 
LightManager::GetLightsForMeshSection(MeshSection *ms, Light **lights, int32 max_lights)
{
	if(m_lights.size() < max_lights)
		max_lights = m_lights.size();

	for(int32 i = 0; i < max_lights; i++)
	{
		lights[i] = m_lights[i];
	}
	return max_lights;
}

bool 
LightManager::Load(const char *lightfile)
{
	XMLDocument *doc = XMLDocument::CreateFromFile(lightfile);
	if(doc)
	{
		return Load(doc);
	}

//	ASSERT(0);
	return false;
}

bool 
LightManager::Load(XMLDocument *xml_doc)
{
	XMLNode *curr = xml_doc->GetRootNode();
	if(curr)
	{
		curr = curr->GetFirstChild();
		curr = curr->GetFirstChild();

		return LoadLights(curr);
	}

	return false;
}

bool 
LightManager::LoadLights(XMLNode *node)
{
	if(node->GetName() == "Lights")
	{
		XMLNode *curr = node->GetFirstChild();
		while(curr)
		{
			std::string x = curr->GetAttributes()->FindAttributeValue("x");
			std::string y = curr->GetAttributes()->FindAttributeValue("y");
			std::string z = curr->GetAttributes()->FindAttributeValue("z");

			Light *l = new Light();
			l->SetPosition(
				Vector3(
					atof(x.c_str()), 
					atof(y.c_str()), 
					atof(z.c_str())));

			AddLight(l);

			curr = curr->GetNextSibling();
		}
	}
	return false;
}

void 
LightManager::TimedUpdate(int32 mspf)
{
#if 0
	Matrix44 m;
	m.RotateY((1.0f * mspf) / 1000.0f);

	Vector3 p = m_lights[0]->GetPosition();
	Vector4 p4;
	p4.x = p.x; 
	p4.y = p.y; 
	p4.z = p.z; 
	p4.w = 1.0f;

	p4 *= m;

	p.x = p4.x; p.y = p4.y; p.z = p4.z;

	m_lights[0]->SetPosition(p);
#endif
}

}

}

