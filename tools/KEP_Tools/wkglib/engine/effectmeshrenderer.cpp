/******************************************************************************
 effectmeshrenderer.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "vector4.h"
#include "meshtypes.h"
#include "meshsection.h"
#include "Mesh.h"
#include "TangentBasis.h"
#include "texturefactory.h"
#include "effectshader.h"
#include "lightmanager.h"
#include "light.h"
#include "effectmeshrenderer.h"

namespace wkg 
{

namespace engine 
{

EffectMeshRenderer::EffectMeshRenderer(EffectShader *es) : m_shader(es)
{
    D3DVERTEXELEMENT9 decl[] =
    {
        { 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,		0 },
        { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,		0 },
        { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,		0 },
        { 1,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT,		0 },
        { 1, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT,		1 },
        { 1, 24, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL,		0 },
        D3DDECL_END()
    };

	HRESULT hres = Device::Instance()->CreateVertexDeclaration(decl, &m_decl);

	m_phong_map = TextureFactory::Instance()->Get("textures/phong.dds");
	m_cube_map = TextureFactory::Instance()->GetCubeTexture("textures/cube.dds");
	m_half_angle_map = TextureFactory::Instance()->Get("textures/ctHalf.dds");
	m_normal_angle_map = TextureFactory::Instance()->Get("textures/ctNorm.dds");
	m_fresnel_map = TextureFactory::Instance()->Get("textures/fresnel.dds");

//	m_phong_map = 0;
//	m_cube_map = 0;
//	m_half_angle_map = 0;
//	m_normal_angle_map = 0;
}

void 
EffectMeshRenderer::OnLostDevice()
{
	m_shader->OnLostDevice();
}

void 
EffectMeshRenderer::OnResetDevice()
{
	m_shader->OnResetDevice();
}

void 
EffectMeshRenderer::Render(Mesh *mesh)
{
	if(!m_shader)
	{
		ASSERT(0);
		return;
	}

	int32 num_passes = m_shader->Begin();
	for(int32 i = 0; i < num_passes; i++)
	{
		m_shader->BeginPass(i);
		m_shader->Prepare(mesh);

		RenderAllSections(mesh);

		m_shader->EndPass();
	}

	m_shader->End();
}

void 
EffectMeshRenderer::RenderAllSections(Mesh *mesh)
{
	Device::Instance()->SetVertexDeclaration(m_decl);
	Device::Instance()->SetStreamSource(0, mesh->GetVB(), 0, sizeof(WKGVertex));
	Device::Instance()->SetStreamSource(1, mesh->GetBumpVB(), 0, sizeof(TangentBasis::Vert));

	const real delta = 0.3f;
	Vector4 color(0.0f, 0.0f, 0.0f, 1.0f);

	Matrix44 view;
	Device::Instance()->GetTransform(D3DTS_VIEW, &view);
	view.Inverse();
	Vector3 cam_pos(view._41, view._42, view._43);

	for (uint32 i = 0; i < mesh->GetNumSections(); i++)
	{
		MeshSection *section = mesh->GetSections()[i];

		Vector3 min = section->GetMin();
		Vector3 max = section->GetMax();

		real dist = 0.0f;

		if(cam_pos.x < min.x)
			if(min.x - cam_pos.x > dist) dist = min.x - cam_pos.x;

		if(cam_pos.y < min.y)
			if(min.y - cam_pos.y > dist) dist = min.y - cam_pos.y;

		if(cam_pos.z < min.z)
			if(min.z - cam_pos.z > dist) dist = min.z - cam_pos.z;

		if(cam_pos.x > max.x)
			if(cam_pos.x - max.x > dist) dist = cam_pos.x - max.x;

		if(cam_pos.y > max.y)
			if(cam_pos.y - max.y > dist) dist = cam_pos.y - max.y;

		if(cam_pos.z > max.z)
			if(cam_pos.z - max.z > dist) dist = cam_pos.z - max.z;


//		if(dist > 1200.0f) continue;


		if(section)
		{
			m_shader->PrepareSection(section);

			// set the texture
			ASSERT(section->m_material >= 0 && section->m_material < (int32)mesh->GetNumMaterials());

			WKGMaterial *mesh_material = mesh->GetMaterials() + section->m_material;

			WKGColor ambient = LightManager::Instance()->GetAmbient();

			WKGColor material_diffuse = mesh_material->diffuse;
			WKGColor material_ambient = mesh_material->ambient;
			WKGColor material_specular = mesh_material->specular;
			real material_power = mesh_material->power;
			if(EQUAL(0.0f, material_power))
			{
				material_specular.r = material_specular.g = material_specular.b = 0.0f;
			}
			else
			{
				material_power = 1.0f / material_power;
			}

			ASSERT(section->m_material < (int32)mesh->GetNumTextures());

			m_shader->SetColor("ambientColor", ambient);
			m_shader->SetColor("diffuseColor", material_diffuse);
			m_shader->SetColor("materialDiffuse", material_diffuse);
			m_shader->SetColor("specularColor", material_specular);
			m_shader->SetFloat("shininess", material_power);

#if 0
			Light *lights[8];
//			int32 n = LightManager::Instance()->GetLightsForMeshSection(section, lights, 4);

			int32 n = LightManager::Instance()->GetClosestLights(cam_pos, lights, 8);

			if(n > 0)
				m_shader->SetVector("lightPos", lights[0]->GetPosition());
			else
				m_shader->SetVector("lightPos", Vector4(0.0f, 24.0f, 0.0f));

			if(n > 1)
				m_shader->SetVector("lightPos2", lights[1]->GetPosition());
			else
				m_shader->SetVector("lightPos2", Vector4(0.0f, 24.0f, 0.0f));

			if(n > 2)
				m_shader->SetVector("lightPos3", lights[2]->GetPosition());
			else
				m_shader->SetVector("lightPos3", Vector4(0.0f, 24.0f, 0.0f));

			if(n > 3)
				m_shader->SetVector("lightPos4", lights[3]->GetPosition());
			else
				m_shader->SetVector("lightPos4", Vector4(0.0f, 24.0f, 0.0f));

			if(n > 4)
				m_shader->SetVector("lightPos5", lights[3]->GetPosition());
			else
				m_shader->SetVector("lightPos5", Vector4(0.0f, 24.0f, 0.0f));

			if(n > 5)
				m_shader->SetVector("lightPos6", lights[3]->GetPosition());
			else
				m_shader->SetVector("lightPos6", Vector4(0.0f, 24.0f, 0.0f));

			if(n > 6)
				m_shader->SetVector("lightPos7", lights[3]->GetPosition());
			else
				m_shader->SetVector("lightPos7", Vector4(0.0f, 24.0f, 0.0f));

			if(n > 7)
				m_shader->SetVector("lightPos8", lights[3]->GetPosition());
			else
				m_shader->SetVector("lightPos8", Vector4(0.0f, 24.0f, 0.0f));
#endif

			m_shader->SetTexture("diffuseTexture", mesh->GetTextures()[section->m_material]);

			if(mesh->GetBumpTextures()[section->m_material]) 
			{
				m_shader->SetTexture("normalMap", mesh->GetBumpTextures()[section->m_material]);
			}
			else
			{
				m_shader->SetTexture("normalMap", (Texture *)0);
			}

			m_shader->SetTexture("phongMap", m_phong_map);
			m_shader->SetTexture("cubeMap", m_cube_map);
			m_shader->SetTexture("halfAngleMap", m_half_angle_map);
			m_shader->SetTexture("normalAngleMap", m_normal_angle_map);
			m_shader->SetTexture("fresnelTex", m_fresnel_map);

			m_shader->CommitChanges();
			section->Render();
		}
	}
}

}

}
