/******************************************************************************
 meshopts.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

// if you change this, you have to change the vertex shader contstants
//  in skinnedvsconst.h
#define MAX_BONES_PER_SECTION (24)


