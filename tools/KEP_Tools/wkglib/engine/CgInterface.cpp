/******************************************************************************
 CgInterface.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "CGInterface.h"
#include "Device.h"

namespace wkg {
namespace engine {

CG::~CG()
{
	// note: must release all cg programs and binditerators prior to this call!
	//
	context = 0;	// note: can't delete a cgContext without corrupting the cg object...must delete the cg object and create another cgDirect3D (yuck!)

	cgCleanup();
}

bool CG::Init()
{
	context = CreateContextContainer(Device::Instance()->GetD3DDevicePointer());

	return 0 != context;
}

}}
