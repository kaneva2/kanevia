/******************************************************************************
 wkgkeyframe.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "quaternion.h"
#include <vector>

namespace wkg
{

class WKGPositionKeyframe
{
public:
	int32 m_frame;
	Vector3 m_position;
};
typedef std::vector<WKGPositionKeyframe *> WKGPositionKeyframeVector;
typedef WKGPositionKeyframeVector::iterator WKGPositionKeyframeIterator;

class WKGOrientationKeyframe
{
public:
	int32 m_frame;
	Quaternion m_orientation;
};
typedef std::vector<WKGOrientationKeyframe *> WKGOrientationKeyframeVector;
typedef WKGOrientationKeyframeVector::iterator WKGOrientationKeyframeIterator;

class WKGKeyframe
{
public:
	int32 m_bone;
	char m_name[255];
	int32 m_frame;
	Vector3 m_position;
	Quaternion m_orientation;
};
typedef std::vector<WKGKeyframe *> WKGKeyframeVector;
typedef WKGKeyframeVector::iterator WKGKeyframeIterator;

}




