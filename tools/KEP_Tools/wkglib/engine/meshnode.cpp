/******************************************************************************
 meshnode.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "vector3.h"
#include "vector4.h"
#include "matrix44.h"
#include "mesh.h"
#include "meshnode.h"
#include "MeshRenderer.h"

namespace wkg
{

namespace engine
{

MeshNode::MeshNode() : m_mesh(0)
{
}

MeshNode::~MeshNode()
{
	if(m_mesh)
		m_mesh->Release();
	m_mesh = (Mesh *)0;

	ClearRenderers();
}

void 
MeshNode::OnRender() const
{
	for(uint32 i = 0; i < m_renderers.size(); i++)
	{
		if(m_renderers[i] && m_mesh)
		{
			XFormNode::OnRender();
			Device::Instance()->SetTransform(D3DTS_WORLD, &m_local_to_world);
			m_renderers[i]->Render(m_mesh);
		}
	}
}

void 
MeshNode::SetMesh(Mesh *mesh)
{ 
	if (m_mesh)
		m_mesh->Release();

	m_mesh = mesh; 

	if (m_mesh)
		m_mesh->AddRef();
}

void
MeshNode::AddRenderer(MeshRenderer *renderer)
{
	m_renderers.push_back(renderer);
}

void 
MeshNode::SetRenderer(MeshRenderer *renderer)
{
	ClearRenderers();

	AddRenderer(renderer);
}

void 
MeshNode::ClearRenderers()
{
	for(uint32 i = 0; i < m_renderers.size(); i++)
	{
		if(m_renderers[i])
			m_renderers[i]->Release();
	}
	m_renderers.clear();
}

}

}
