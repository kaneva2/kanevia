/******************************************************************************
 meshfactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>

#include "singleton.h"
#include "factory.h"
#include "mesh.h"

namespace wkg
{

namespace engine
{

class MeshDescriptor
{
public:
	MeshDescriptor()
	{
	}

	enum Type { STANDARD_MESH, SKINNED_MESH, ANIMATED_MESH};

	MeshDescriptor(std::string Filename, Type MeshType = STANDARD_MESH)
	{
		filename = Filename;
		type = MeshType;
	}

	std::string filename;
	Type type;

	bool operator==(MeshDescriptor &md) const
	{
		return (filename == md.filename);
	}
};

class MeshFactory : 
	public Singleton<MeshFactory>, 
	public Factory<Mesh, MeshDescriptor>
{
public:
	MeshFactory();
	virtual ~MeshFactory();

protected:
	virtual Mesh *Create(const MeshDescriptor &desc) const;

};

}

}

