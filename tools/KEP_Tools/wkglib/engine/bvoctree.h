/******************************************************************************
 bvoctree.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "boundingvolume.h"

#pragma once

namespace wkg
{

namespace engine
{

class OctNode;

class BVOctree : public BoundingVolume
{
public:
	BVOctree();
	virtual ~BVOctree();

	virtual Collision *Collide(BoundingVolume *other, uint32 ms);
	void SetOctree(OctNode *on) { m_octree = on; }

protected:
	OctNode *m_octree;
};

}

}

