/******************************************************************************
 coloredvertex.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _COLORED_VERTEX_H_
#define _COLORED_VERTEX_H_

#include "vertex.h"

#define FVF_COLORED_VERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE)

namespace wkg
{

namespace engine
{

class ColoredVertex : public SimpleVertex
{
	public:
		uint32 diffuse;
};

}

}

#endif