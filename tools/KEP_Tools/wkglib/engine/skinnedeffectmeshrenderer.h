/******************************************************************************
 skinnedeffectmeshrenderer.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "effectmeshrenderer.h"

namespace wkg 
{

namespace engine 
{

class Mesh;

class EffectShader;

class SkinnedEffectMeshRenderer : public EffectMeshRenderer
{
public:
	SkinnedEffectMeshRenderer(const char *fxfile = 0);

protected:
	virtual void RenderAllSections(Mesh *mesh);

};

}

}
