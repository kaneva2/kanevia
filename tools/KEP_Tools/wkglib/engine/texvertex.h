/******************************************************************************
 texvertex.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _TEX_VERTEX_H_
#define _TEX_VERTEX_H_

#include "vertex.h"

#define FVF_TEX_VERTEX (FVF_CUSTOM_VERTEX | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0))

namespace wkg
{

namespace engine
{

class TexVertex : public Vertex
{
	public:
		real tu, tv;
};

}

}

#endif