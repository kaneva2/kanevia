/******************************************************************************
 vsfactory.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/


#include "wkgtypes.h"
#include "vsfactory.h"
#include "vertexshader.h"

namespace wkg
{

namespace engine
{

VSFactory::VSFactory()
{
}

VSFactory::~VSFactory()
{
	ShaderIterator itor = m_shaders.begin();
	ShaderIterator end = m_shaders.end();
	for(; itor < end; itor++)
	{
		delete itor->m_shader;
	}

	m_shaders.clear();
}

void 
VSFactory::RegisterShader(VertexShader *vs, std::string name)
{
	ShaderEntry entry;
	entry.m_name = name;
	entry.m_shader = vs;
	m_shaders.push_back(entry);
}

VertexShader *
VSFactory::GetShader(std::string name)
{
	VertexShader *shader = (VertexShader *)0;

	ShaderIterator itor = m_shaders.begin();
	ShaderIterator end = m_shaders.end();
	for(; itor < end; itor++)
	{
		if(itor->m_name == name)
		{
			shader = itor->m_shader; 
			break;
		}
	}

	ASSERT(shader);
	return shader;
}


}

}
