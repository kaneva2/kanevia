/******************************************************************************
 octnode.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "node.h"
#include "aabb.h"

namespace wkg
{

class Capsule;

namespace engine
{

class Mesh;

class OctNode : public Node, public AABB
{
public:
	OctNode();
	~OctNode();

	bool AddMesh(Mesh *mesh, const Matrix44 &xform);
	bool AddGeometry(std::vector<Vector3> &verts);
	bool AddGeometryHelper(std::vector<Vector3> &verts, std::vector<uint32> &poly_ids);

	std::vector<Vector3> *GetVerts() { return &m_verts; }

	virtual void DFSRender();
	virtual void DFSUpdate(Matrix44 &m);

	bool HasChildren();

	SERIALIZABLE

	bool CollideSphere(Vector3 &dst, real r, Vector3 *n = 0, Vector3 *poc = 0);
	bool CollideCapsule(Capsule &c, Vector3 *n = 0);


	bool CollideDynamicSphere(Vector3 &src, Vector3 &dst, 
		real r, Vector3 *n = 0, Vector3 *poc = 0, int32 recurse = 5);

	bool CollideDynamicEllipsoid(Vector3 &src, Vector3 &dst, 
		Vector3 r, Vector3 *n = 0, Vector3 *poc = 0, int32 recurse = 5);

protected:

	bool CollideDynamicEllipsoidHelper(const Vector3 &src, Vector3 &dst, Vector3 r, 
		Vector3 *n = 0, Vector3 *poc = 0);

	void CollideDynamicEllipsoidHelperHelper(const Vector3 &src, const Vector3 &veclocity, const Vector3 &r, 
		bool &found_collision, double &nearest_dist_to_collision, Vector3 &nearest_point, double &nearest_t);

	bool PolygonContains(Vector3 &p, Vector3 &v1, Vector3 &v2, Vector3 &v3);

	void GetChildBounds(int32 child, Vector3 &min, Vector3 &max);

protected:
	OctNode *m_children[8];

	std::vector<Vector3> m_verts;
	std::vector<uint32> m_poly_ids;

	enum { LEFT = 1, RIGHT = 2, LOWER = 4, UPPER = 8, BACK = 16, FRONT = 32 };
	static uint32 ms_child_flags[8];

};

}

}
