/******************************************************************************
 skinnedmeshblureffect.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "skinnedmeshblureffect.h"

namespace wkg
{

namespace engine
{

SkinnedMeshBlurEffect::SkinnedMeshBlurEffect() : SkinnedMeshEffect()
{
}

SkinnedMeshBlurEffect::~SkinnedMeshBlurEffect()
{
}

void 
SkinnedMeshBlurEffect::Prepare(Mesh *mesh)
{
	SkinnedMeshEffect::Prepare(mesh);
}

void 
SkinnedMeshBlurEffect::PrepareSection(MeshSection *section)
{
	SkinnedMeshEffect::PrepareSection(section);
}


}

}

