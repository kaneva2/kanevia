/******************************************************************************
 rigidbody.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "worldobject.h"
#include "ode.h"

namespace wkg
{

namespace engine
{

class RigidBody : public WorldObject
{
public:

	friend class Physics;

	RigidBody();
	virtual ~RigidBody();

	virtual void SetPosition(const Vector3 &position);
	virtual void SetOrientation(const Quaternion &orientation);
	virtual void SetVelocity(const Vector3 &velocity);
	virtual void SetMass(real mass);
	virtual void SetAngularVelocity(Vector3 &v);
	virtual void SetBVOffset(Vector3 &bvoffset);
	virtual void SetBoundingVolume(BoundingVolume *volume);
	virtual void SetMu(real mu) { m_mu = mu; }

	virtual void AddToSim();
	virtual bool IsInSim();
	virtual void RemoveFromSim();

	void SetImmovable(bool im = true);

	void AddForce(Vector3 &f);
	void AddForceAtPos(const Vector3 &pos, Vector3 &force);

	void AddImpulse(Vector3 &i)
	{
		m_impulses += i;
	}

	void ApplyImpulsesToForce(int32 ms)
	{
		if(ms > 0)
		{
//			m_impulses *= ((real)ms / 1000.0f);
			m_impulses *= 100.0f;
			AddForce(m_impulses);
		}

		m_impulses = Vector3(0.0f, 0.0f, 0.0f);
	}

	virtual void UpdateStateFromSim();

	void AddBallJoint(Vector3 bpos, Vector3 wpos);

	bool m_on_ground;

protected:
	void AdjustDMass();

protected:
	dBodyID m_body;
	std::vector<dGeomID> m_geom;

	real m_cor;
	real m_mu;

	Vector3 m_impulses;
};

}

}

