/******************************************************************************
 factories.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "texture.h"
#include "texturefactory.h"
#include "meshfactory.h"
#include "psfactory.h"
#include "vsfactory.h"
#include "vertexshader.h"
#include "pixelshader.h"
#include "texvertex.h"
#include "factories.h"

namespace wkg
{

namespace engine
{

Factories::Factories()
{
	m_tex_fact = new TextureFactory();
	m_mesh_fact = new MeshFactory();
	m_ps_fact = new PSFactory();
	m_vs_fact = new VSFactory();

	VertexShader *texvertfvf = new VertexShader();
	texvertfvf->Init(FVF_TEX_VERTEX);
	m_vs_fact->RegisterShader(texvertfvf, "TexVertFVF");

	VertexShader *xyzdiffusefvf = new VertexShader();
	xyzdiffusefvf->Init(FVF_TEX_VERTEX);
	m_vs_fact->RegisterShader(xyzdiffusefvf, "XYZDiffuseFVF");

	m_ps_fact->RegisterShader(new PixelShader(), "Fixed");
}

Factories::~Factories()
{
	delete m_vs_fact;
	m_vs_fact = 0;

	delete m_ps_fact;
	m_ps_fact = 0;

	delete m_mesh_fact;
	m_mesh_fact = 0;

	delete m_tex_fact;
	m_tex_fact = 0;
}

}


}