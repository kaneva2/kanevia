/******************************************************************************
 MeshRendSurfaceShader.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "MeshRendSurfaceShader.h"
#include "MeshTypes.h"
#include "Mesh.h"
#include "MeshSection.h"
#include "Device.h"
#include "SurfaceDB.h"
#include "Surface.h"
#include "SurfaceShader.h"
#include "TangentBasis.h"
#include "RenderState.h"

namespace wkg 
{

namespace engine 
{

void MeshRendSurfaceShader::Render(Mesh* mesh)
{
	MeshSection* section;
	uint32 i;

	RenderState rs(RenderState::DELTA);
	rs.Set(D3DRS_FOGENABLE, false);
	rs.Set(D3DRS_ALPHABLENDENABLE, false);
	Device::Instance()->SetRenderState(rs);

	Device::Instance()->SetStreamSource(0, mesh->GetVB(), 0, sizeof(WKGVertex));
	Device::Instance()->SetStreamSource(1, mesh->GetColorVB(), 0, sizeof(WKGVertexColor));
	Device::Instance()->SetStreamSource(2, mesh->GetBumpVB(), 0, sizeof(TangentBasis::Vert));

	for (i = 0; i < mesh->GetNumSections(); i++)
	{
		section = mesh->GetSections()[i];
		if (section)
		{
			const Surface* surf = SurfaceDB::Instance()->GetSurface(uint16(section->m_material & 0xffff));
			bool useBumpStream = false;

			if (surf && surf->GetShader(Surface::SHADER_SURFACE))
				surf->GetShader(Surface::SHADER_SURFACE)->Prepare(surf, &useBumpStream);

			section->Render();
		}
	}

	Device::Instance()->SetStreamSource(1, 0, 0, 0);
	Device::Instance()->SetStreamSource(2, 0, 0, 0);
}

}

}


