/******************************************************************************
 effectshader.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "vector3.h"
#include "matrix44.h"
#include "vector4.h"
#include "camera.h"
#include "effectshader.h"
#include "camera.h"
#include "cameramanager.h"
#include "wkgcolor.h"

namespace wkg
{

namespace engine
{

EffectShader::EffectShader() :
	m_effect(0)
{
	memset(&m_technique, 0, sizeof(D3DXHANDLE));
}

EffectShader::~EffectShader()
{
}

bool 
EffectShader::Create(const char *filename)
{
	m_filename = filename;

	Device *dev = Device::Instance();
	ASSERT(dev);

	ID3DXBuffer *errors = 0;

	const char *fn = m_filename.c_str();

	HRESULT hr;
	hr = D3DXCreateEffectFromFile(
		dev, 
		fn, 
		0, 
		0, 
		0,
		0,
		&m_effect, 
		&errors);

    if(FAILED(hr))
	{
		if(errors)
		{
			DEBUGMSG("%s", (char *)errors->GetBufferPointer());
			errors->Release();
			errors = 0;
		}
		else
		{
			DEBUGMSG("Generic error compiling effect %s", filename);
		}
		ASSERT(0);
        return false;
	}

	if(m_effect)
	{
		// might as well find a valid technique for this shader
		hr = m_effect->FindNextValidTechnique(0, &m_technique);

		if(FAILED(hr))
		{
			DEBUGMSG("Unable to find valid technique for effect %s", filename);
			m_effect->Release();
			m_effect = 0;

			return false;
		}
 
	    m_effect->SetTechnique(m_technique);
	}

	return true;
}

void
EffectShader::PrepareSection(MeshSection *section)
{

}

void 
EffectShader::Prepare(Mesh *mesh)
{
	Matrix44 world, view, proj, wvp;

	Device::Instance()->GetTransform(D3DTS_WORLD, &world);
	Device::Instance()->GetTransform(D3DTS_VIEW, &view);
	Device::Instance()->GetTransform(D3DTS_PROJECTION, &proj);

	wvp = world * view * proj;
	SetMatrix("WorldViewProj", wvp);
	SetMatrix("worldViewProj", wvp);
	SetMatrix("wvpMatrix", wvp);
	SetMatrix("mvpMatrix", wvp);


	Matrix44 wv = world * view;
	SetMatrix("worldViewMatrix", wvp);

	wv.Inverse();
	SetMatrix("worldViewMatrixI", wv);


	SetMatrix("viewMatrix", view);

	view.Inverse();
	SetMatrix("viewInverse", view);
	SetMatrix("viewIMatrix", view);
	SetMatrix("viewInverseMatrix", view);

	SetMatrix("world", world);
	SetMatrix("worldMatrix", world);

	world.Inverse();
	SetMatrix("worldIMatrix", world);
	SetMatrix("worldInverse", world);

	world.Transpose();
	SetMatrix("worldInverseTranspose", world);
}

int32 
EffectShader::Begin()
{
	UINT passes;

	if(m_effect)
	{
		m_effect->Begin(&passes, 0 );
		return passes;
	}
	else
	{
		return 0;
	}
}

void 
EffectShader::BeginPass(int32 pass)
{
	if(m_effect)
		m_effect->BeginPass(pass);
}

void
EffectShader::EndPass()
{
	if(m_effect)
		m_effect->EndPass();
}

void 
EffectShader::End()
{
	if(m_effect)
		m_effect->End();
}

void
EffectShader::CommitChanges()
{
	if(m_effect)
		m_effect->CommitChanges();
}

bool 
EffectShader::SetMatrix(const char *parameter, Matrix44 &m)
{
	D3DXMATRIX d3dm;

	d3dm._11 = m._11; d3dm._12 = m._12; d3dm._13 = m._13; d3dm._14 = m._14;
	d3dm._21 = m._21; d3dm._22 = m._22; d3dm._23 = m._23; d3dm._24 = m._24;
	d3dm._31 = m._31; d3dm._32 = m._32; d3dm._33 = m._33; d3dm._34 = m._34;
	d3dm._41 = m._41; d3dm._42 = m._42; d3dm._43 = m._43; d3dm._44 = m._44;

	HRESULT hr = m_effect->SetMatrix(parameter, &d3dm);
	return SUCCEEDED(hr);
}

bool 
EffectShader::SetVector(const char *parameter, Vector4 &v)
{
//	D3DXHANDLE h = m_effect->GetParameterByName(0, parameter);
//	ASSERT(h);

	D3DXVECTOR4 v4;
	v4.x = v.x; v4.y = v.y; v4.z = v.z; v4.w = v.w;
	HRESULT hr = m_effect->SetVector(parameter, &v4);
	return SUCCEEDED(hr);
}

bool 
EffectShader::SetVector(const char *parameter, Vector3 &v)
{
	return SetVector(parameter, Vector4(v.x, v.y, v.z, 1.0f));
}

bool 
EffectShader::SetFloat(const char *parameter,  real f)
{
	HRESULT hr = m_effect->SetFloat(parameter, f);
	return SUCCEEDED(hr);
}

bool 
EffectShader::SetInt(const char *parameter,  uint32 dw)
{
	HRESULT hr = m_effect->SetInt(parameter, dw);
	return SUCCEEDED(hr);
}

bool 
EffectShader::SetTexture(const char *parameter,  Texture *t)
{
	HRESULT hr;
	if(t)
		hr = m_effect->SetTexture(parameter, t->Interface());
	else
		hr = m_effect->SetTexture(parameter, 0);

	return SUCCEEDED(hr);
}

bool 
EffectShader::SetTexture(const char *parameter,  LPDIRECT3DTEXTURE9 t)
{
	HRESULT hr = m_effect->SetTexture(parameter, t);
	return SUCCEEDED(hr);
}

bool 
EffectShader::SetColor(const char *parameter, WKGColor &color)
{
	return SetVector(parameter, Vector4(color.r, color.g, color.b, color.a));
}

void 
EffectShader::OnLostDevice()
{
	if ( m_effect )
		m_effect->OnLostDevice();
}

void 
EffectShader::OnResetDevice()
{
	if ( m_effect )
		m_effect->OnResetDevice();
}



}

}



