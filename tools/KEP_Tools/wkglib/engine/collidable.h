/******************************************************************************
 collidable.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg 
{

class Vector3;

namespace engine
{

class BoundingVolume;
class Collision;

class Collidable
{
public:
	Collidable();
	virtual ~Collidable();

	virtual void OnCollide(Collidable *other, Collision *c);
	virtual void EnableCollisions(bool e = true);
	virtual void EnableSliding(bool s = true)
	{
		m_slide = s;
	}
	bool ShouldSlide() { return m_slide; }

	virtual Collision *Collide(Collidable *other, uint32 ms);

	virtual void SetBoundingVolume(BoundingVolume *volume);
	BoundingVolume *GetBoundingVolume()						{ return m_volume;   }

	real GetBVSphereRadius();

protected:
	BoundingVolume *m_volume;

	bool m_slide;
};

}

}


