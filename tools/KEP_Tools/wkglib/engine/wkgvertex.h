/******************************************************************************
 wkgvertex.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

class WKGVertex
{
public:
	float x, y, z;
	float nx, ny, nz;
	float tu, tv;
};

class WKGVertexWeights
{
public:
	uint16 m_bones[2];
	real m_weights[2];
};

class WKGWeightedVertex
{
public:
	union
	{
		struct
		{
			WKGVertex m_vertex;
			WKGVertexWeights m_vertex_weights;
		};

		struct
		{
			float  x, y, z;
			float  nx, ny, nz;
			float  tu, tv;
			uint16 m_bones[2];
			real   m_weights[2];
		};
	};
};


}

