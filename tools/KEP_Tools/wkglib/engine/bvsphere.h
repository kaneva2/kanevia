/******************************************************************************
 bvsphere.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "boundingvolume.h"
#include "sphere.h"

namespace wkg
{

namespace engine
{

class WorldObject;

class BVSphere : public BoundingVolume
{
public:
	BVSphere(WorldObject *wo, real radius);
	virtual ~BVSphere();

	real GetRadius()				{ return m_radius; }

	virtual Collision *Collide(BoundingVolume *other, uint32 ms);

	virtual real GetBVSphereRadius() 
	{ 
		return m_radius;
	}


protected:
	real m_radius;
};

}

}

