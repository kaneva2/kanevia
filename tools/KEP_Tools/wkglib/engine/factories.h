/******************************************************************************
 factories.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "singleton.h"

namespace wkg
{

namespace engine
{

class TextureFactory;
class MeshFactory;
class VSFactory;
class PSFactory;

class Factories : public Singleton<Factories>
{
	public:
		Factories();
		~Factories();

	protected:
		TextureFactory *m_tex_fact;
		MeshFactory *m_mesh_fact;
		PSFactory *m_ps_fact;
		VSFactory *m_vs_fact;
};

}

}

