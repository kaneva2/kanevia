/******************************************************************************
 wkgbone.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "matrix44.h"
#include "vector3.h"
#include "quaternion.h"

namespace wkg
{

class WKGBone
{
public:
	int32 m_parent;
	Vector3 m_position;
	Quaternion m_orientation;
	char m_name[255];
};

}