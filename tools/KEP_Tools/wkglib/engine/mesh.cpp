/******************************************************************************
 mesh.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "meshtypes.h"
#include "wkbfile.h"
#include "meshsection.h"
#include "device.h"
#include "texturefactory.h"
#include "vector3.h"
#include "aabb.h"
#include "mesh.h"
#include "tangentbasis.h"

int32 g_vert_data = 0;

namespace wkg
{

namespace engine
{

Mesh::Mesh() : 
	RefCounter<Mesh>(),
	m_num_sections(0),
	m_sections(0),
	m_num_solid_sections(0),
	m_num_materials(0),
	m_materials(0),
	m_num_textures(0),
	m_textures(0),
	m_bump_textures(0),
	m_num_verts(0),
	m_vb(0),
	m_color_vb(0),
	m_bumpVB(0),
	m_verts(0),
	m_num_faces(0),
	m_faces(0)
{
}

Mesh::~Mesh()
{
	delete [] m_materials;
	m_materials = 0;
	m_num_materials = 0;

	for(uint32 i = 0; i < m_num_sections; i++)
		delete m_sections[i];

	delete [] m_sections;

	m_sections = 0;
	m_num_sections = 0;

	for(uint32 i = 0; i < m_num_textures; i++)
	{
		if(m_textures[i])
			m_textures[i]->Release();
		if(m_bump_textures[i])
			m_bump_textures[i]->Release();
	}

	delete [] m_textures;
	delete [] m_bump_textures;
	m_num_textures = 0;

	delete [] m_verts;
	m_verts = 0;
	m_num_verts = 0;

	delete [] m_faces;
	m_faces = 0;
	m_num_faces = 0;

	if(m_vb)
		m_vb->Release();
	m_vb = 0;

	if(m_color_vb)
		m_color_vb->Release();
	m_color_vb = 0;
	
	if(m_bumpVB)
		m_bumpVB->Release();
	m_bumpVB = 0;
}

bool 
Mesh::Load(const char *filename)
{
	WKBFile wkbfile;
	if(!wkbfile.Load(filename))
		return false;

	return Load(wkbfile)
		&& ComputeTangentBasis(sizeof(WKGVertex));
}

bool 
Mesh::Load(WKBFile &wkbfile)
{
	uint32 num_materials = wkbfile.GetNumMaterials();
	WKGMaterial *materials = wkbfile.GetMaterials();

	m_num_verts = wkbfile.GetNumVerts();
	WKGWeightedVertex *weighted_verts = wkbfile.GetVerts();

	WKGVertex *verts = new WKGVertex[m_num_verts];
	for(uint32 v = 0; v < m_num_verts; v++)
	{
		verts[v].x = weighted_verts[v].x;
		verts[v].y = weighted_verts[v].y;
		verts[v].z = weighted_verts[v].z;

		verts[v].nx = weighted_verts[v].nx;
		verts[v].ny = weighted_verts[v].ny;
		verts[v].nz = weighted_verts[v].nz;

		verts[v].tu = weighted_verts[v].tu;
		verts[v].tv = weighted_verts[v].tv;

	}

	uint32 num_faces = wkbfile.GetNumFaces();
	WKGFace *faces = wkbfile.GetFaces();

	m_faces = new WKGFace[num_faces];
	m_num_faces = num_faces;
	memcpy(m_faces, faces, sizeof(WKGFace) * num_faces);

	if(m_num_verts && num_faces)
	{
		// save off the materials
		if(num_materials)
		{
			m_materials = new WKGMaterial[num_materials];
			memcpy(m_materials, materials, sizeof(WKGMaterial) * num_materials);
		}
		else
		{
			m_materials = new WKGMaterial[1];
			m_materials[0].ambient.r = 
			m_materials[0].ambient.g = 
			m_materials[0].ambient.b = 
			m_materials[0].ambient.a = 1.0f;

			m_materials[0].diffuse.r = 
			m_materials[0].diffuse.g = 
			m_materials[0].diffuse.b = 
			m_materials[0].diffuse.a = 1.0f;

			m_materials[0].specular.r = 
			m_materials[0].specular.g = 
			m_materials[0].specular.b = 
			m_materials[0].specular.a = 1.0f;

			m_materials[0].power = 1.0f;

			m_materials[0].texture[0] = 0;

			num_materials = 1;
		}

		m_num_materials = num_materials;
		m_num_textures = num_materials;
		m_textures = new Texture *[m_num_textures];
		m_bump_textures = new Texture *[m_num_textures];
		for(uint32 i = 0; i < m_num_textures; i++)
		{
			if(m_materials[i].texture[0])
				m_textures[i] = TextureFactory::Instance()->Get(m_materials[i].texture);
			else
				m_textures[i] = 0;

			if(m_materials[i].bump_texture[0])
				m_bump_textures[i] = TextureFactory::Instance()->Get(m_materials[i].bump_texture);
			else
				m_bump_textures[i] = 0;
		}

		// save off the verts (create our vert buffer)
		SetVerts(m_num_verts, verts);

		delete [] verts;

		// setup our vertex color vb
		WKGVertexColor *vert_colors = wkbfile.GetVertColors();
		SetVertColors(m_num_verts, vert_colors);

#ifndef USE_32BIT_INDICES
		ASSERT(m_num_verts <= 65535);
#endif

		// find out how many unique materials we have for this mesh
		m_num_sections = wkbfile.GetNumSections();

		WKGMeshSection *sections = wkbfile.GetSections();

		m_sections = new MeshSection *[m_num_sections];
		m_num_solid_sections = wkbfile.GetNumSolidSections();

		for(uint32 i = 0; i < m_num_sections; i++)
		{
			m_sections[i] = new MeshSection();
			Vector3 min, max;
			wkbfile.FindAABBForSection(i, min, max);
			m_sections[i]->SetBounds(min, max);

			ASSERT(m_sections[i]);

			if (sections[i].m_material >= (int32)num_materials)
				sections[i].m_material = 0;

			m_sections[i]->m_material = sections[i].m_material;

			if (wkbfile.GetNumBones() > 0)
			{
				for(int32 b = 0; b < MAX_BONES_PER_SECTION; b++)
				{
					ASSERT((int32)sections[i].m_bones[b] >= 0 && (int32)sections[i].m_bones[b] < (int32)wkbfile.GetNumBones());

					m_sections[i]->m_bones.push_back(sections[i].m_bones[b]);
				}
			}

			uint32 num_idxs = sections[i].m_num_faces * 3;
			if (num_idxs)
			{
				WKGIndex *idxs = new WKGIndex[num_idxs];

				if (idxs)
				{
					ASSERT(sections[i].m_first_face >= 0 && 
						sections[i].m_first_face < m_num_faces &&
						sections[i].m_first_face + sections[i].m_num_faces <= m_num_faces);

					int32 num_tris = sections[i].m_num_faces;
					WKGIndex *ci = idxs;
					WKGFace *face = &(m_faces[sections[i].m_first_face]);
					while(num_tris)
					{
						*ci++ = face->v1;
						*ci++ = face->v2;
						*ci++ = face->v3;

						num_tris--;
						face++;
					}

					m_sections[i]->SetIndices(num_idxs, idxs);

					delete [] idxs;
				}
			}
		}
	}
	else
	{
		ASSERT(0);
		return false;
	}

	return true;
}

void 
Mesh::SetVerts(uint32 num_verts, WKGVertex *v)
{
	HRESULT hr = Device::Instance()->CreateVertexBuffer(
		sizeof(WKGVertex) * num_verts, 
		D3DUSAGE_WRITEONLY, 0, 
		D3DPOOL_MANAGED,
		&m_vb, 0);

	g_vert_data += sizeof(WKGVertex) * num_verts;
	DEBUGMSG("vert data: %d", g_vert_data);

	if(FAILED(hr))
	{
		ASSERT(0);
	}

	void *dst;
	hr = m_vb->Lock(0, 0, &dst, 0);
	ASSERT(SUCCEEDED(hr));

	memcpy(dst, v, sizeof(WKGVertex) * num_verts);

	hr = m_vb->Unlock();
	ASSERT(SUCCEEDED(hr));

	if(m_verts)
		delete [] m_verts;
	m_verts = new WKGVertex[num_verts];
	ASSERT(m_verts);
	memcpy(m_verts, v, sizeof(WKGVertex) * num_verts);
}


void 
Mesh::SetVertColors(uint32 num_verts, WKGVertexColor *colors)
{
	HRESULT hr = Device::Instance()->CreateVertexBuffer(
		sizeof(WKGVertexColor) * num_verts, 
		D3DUSAGE_WRITEONLY, 0, 
		D3DPOOL_MANAGED,
		&m_color_vb, 0);

	g_vert_data += sizeof(WKGVertexColor) * num_verts;
	DEBUGMSG("vert data: %d", g_vert_data);


	if(FAILED(hr))
	{
		ASSERT(0);
	}

	void *dst;
	hr = m_color_vb->Lock(0, 0, &dst, 0);
	ASSERT(SUCCEEDED(hr));

	memcpy(dst, colors, sizeof(WKGVertexColor) * num_verts);

	hr = m_color_vb->Unlock();
	ASSERT(SUCCEEDED(hr));
}

void 
Mesh::GetAABB(AABB &aabb)
{
	WKGVertex *verts = m_verts;

	Vector3 min, max;
	min.x = max.x = verts[0].x;
	min.y = max.y = verts[0].y;
	min.z = max.z = verts[0].z;
	for(uint32 i = 0; i < m_num_verts; i++)
	{
		if(verts[i].x < min.x)
			min.x = verts[i].x;
		if(verts[i].x > max.x)
			max.x = verts[i].x;

		if(verts[i].y < min.y)
			min.y = verts[i].y;
		if(verts[i].y > max.y)
			max.y = verts[i].y;

		if(verts[i].z < min.z)
			min.z = verts[i].z;
		if(verts[i].z > max.z)
			max.z = verts[i].z;
	}

	aabb.SetMin(min);
	aabb.SetMax(max);
}

void
Mesh::VecsFromVerts(std::vector<Vector3> &vecs, const Matrix44 &xform)
{
	WKGVertex *verts = GetVerts();

	WKGFace *faces = GetFaces();

	for(uint32 i = 0; i < m_num_faces; i++)
	{
		Vector3 v;
		v = Vector3(verts[faces[i].v1].x, verts[faces[i].v1].y, verts[faces[i].v1].z);
		v.PtMultEqProject(xform);
		vecs.push_back(v);

		v = Vector3(verts[faces[i].v2].x, verts[faces[i].v2].y, verts[faces[i].v2].z);
		v.PtMultEqProject(xform);
		vecs.push_back(v);

		v = Vector3(verts[faces[i].v3].x, verts[faces[i].v3].y, verts[faces[i].v3].z);
		v.PtMultEqProject(xform);
		vecs.push_back(v);

	}
}

bool 
Mesh::ComputeTangentBasis(uint32 VertStride)
{
	TangentBasis tanBasis;

	ASSERT(!m_bumpVB);

	if (tanBasis.BeginBuild(GetVB(), GetNumVerts(), VertStride, D3DPOOL_MANAGED))
	{
		MeshSection* section;
		uint32 i;

		for (i = 0; i < GetNumSections(); i++)
		{
			section = GetSections()[i];
			if (section)
				tanBasis.ComputeMeshSection(section->GetIB(), section->GetNumTris());
		}

		m_bumpVB = tanBasis.EndBuild();
	}

	return 0 != m_bumpVB;
}

}

}