/******************************************************************************
 shadowmapmanager.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "vector4.h"
#include "device.h"
#include "matrix44.h"
#include "scenegraph.h"
#include "vector3.h"
#include "light.h"
#include "texturefactory.h"
#include "texture.h"
#include "lightmanager.h"
#include "shadowmapmanager.h"

#define SHADOWMAP_SIZE (512)

namespace wkg
{

namespace engine
{

ShadowMapManager::ShadowMapManager() : Singleton<ShadowMapManager>()
{
}

ShadowMapManager::~ShadowMapManager()
{
}

void
ShadowMapManager::SaveRenderState() const
{
	// save the render target
	Device::Instance()->GetRenderTarget(0, &m_old_target);

	// save off our original viewport
	Device::Instance()->GetViewport(&m_old_viewport);

	// and the projection matrix
	Device::Instance()->GetTransform(D3DTS_PROJECTION, &m_old_proj);
	Device::Instance()->GetTransform(D3DTS_VIEW, &m_old_view);
}

void 
ShadowMapManager::RestoreRenderState() const
{
	// restore the old render target and zbuffer
	Device::Instance()->SetRenderTarget(0, m_old_target);

	// restore the old viewport
	Device::Instance()->SetViewport(&m_old_viewport);

	Device::Instance()->SetTransform(D3DTS_PROJECTION, &m_old_proj);
	Device::Instance()->SetTransform(D3DTS_VIEW, &m_old_view);

	// and reduce the refcount on the old rt
	m_old_target->Release();

	Device::Instance()->Clear(
		0, 0, 
		D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 
		0x0, 
		1.0f, 0);


}

void 
ShadowMapManager::SetRenderState(Light *l, int32 direction) const
{
	Texture *t = (m_shadowmaps[l])[direction];
	if(!t)
	{
		CreateShadowMap(l);
		t = (m_shadowmaps[l])[direction];
		if(!t) 
		{
			ASSERT(0);
			return;
		}
	}

	// set our new render target
	Texture *shadowmap = t;
	LPDIRECT3DTEXTURE9 d3dtex = shadowmap->Interface();
	LPDIRECT3DSURFACE9 surf;

	d3dtex->GetSurfaceLevel(0, &surf);

	Device::Instance()->SetRenderTarget(0, surf);
	surf->Release();

	Device::Instance()->Clear(0, 0, 
		D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 
		0xffffffff, 1.0f, 0);


	Matrix44 view = GetViewTransform(l, direction);
	Matrix44 proj = GetProjection(l);

//	Device::Instance()->SetTransform(D3DTS_WORLD, &Matrix44::IDENTITY);
	Device::Instance()->SetTransform(D3DTS_VIEW, &view);
	Device::Instance()->SetTransform(D3DTS_PROJECTION, &proj);

	m_curr_light_range = GetLightRange(l);
}

Matrix44 
ShadowMapManager::GetTextureProjection(Light *l, int32 direction) const
{
	// this is the view matrix times the projection matrix times a cool special matrix
	//  that inverts y and halves the axes then moves things over half a pixel

	Matrix44 view = GetViewTransform(l, direction);
	Matrix44 proj = GetProjection(l);

	Matrix44 tpm;
	tpm.Identity();
	tpm.Scale(0.5f, -0.5f, 1.0f);
	tpm._41 = tpm._42 = 0.5f;

	Matrix44 final = view * proj * tpm;

	return final;
}

Matrix44 
ShadowMapManager::GetProjection(Light *l) const
{
	// create the texture projection matrix
	Matrix44 mat;
	real hither = 1.0f;
	real yon    = 1000.0f;
	D3DXMatrixPerspectiveFovLH(&mat, WKG_PI/2.0f, 1.0f, hither, yon);
	return mat;
}

Matrix44 
ShadowMapManager::GetViewTransform(Light *l, int32 direction) const
{

	D3DXVECTOR3 dir;
	D3DXVECTOR3 up;
	
	switch(direction)
	{
	case SM_DOWN:
		dir = D3DXVECTOR3(0.0f, -1.0f, 0.0f);
		up  = D3DXVECTOR3 (0.0f, 0.0f, 1.0f);
		break;

	case SM_LEFT:
		dir = D3DXVECTOR3(-1.0f, 0.0f, 0.0f);
		up  = D3DXVECTOR3 (0.0f, 1.0f, 0.0f);
		break;

	case SM_RIGHT:
		dir = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
		up  = D3DXVECTOR3 (0.0f, 1.0f, 0.0f);
		break;

	case SM_BACK:
		dir = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
		up  = D3DXVECTOR3 (0.0f, 1.0f, 0.0f);
		break;

	case SM_FORWARD:
		dir = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
		up  = D3DXVECTOR3 (0.0f, 1.0f, 0.0f);
		break;

	default:
		ASSERT(0);

	}

	D3DXMATRIX d3dview;
	D3DXVECTOR3 eye(l->GetPosition().x, l->GetPosition().y, l->GetPosition().z);
	D3DXVECTOR3 at(eye + (dir * 24.0f));
	D3DXMatrixLookAtLH(&d3dview, &eye, &at, &up);

	Matrix44 view;
	view = d3dview;

	return view;
}

Vector4 
ShadowMapManager::GetLightRange(Light *l) const
{
	real hither = 1.0f;
	real yon    = 400.0f;
	return Vector4(1.0f / (yon - hither), -hither / (yon - hither), 0.0f, 0.5f);
}

Vector4 
ShadowMapManager::GetCurrLightRange()
{
	return m_curr_light_range;
}

void 
ShadowMapManager::DFSRender() const
{
	if(m_flags & NO_RENDER) return;

	SaveRenderState();

	int32 num_lights = LightManager::Instance()->GetNumLights();
	for(int32 i = 0; i < num_lights; i++)
	{
		int32 d = SM_DOWN;
//		for(int32 d = SM_DOWN; d <= SM_FORWARD; d++)
		{
			SetRenderState(LightManager::Instance()->GetLight(i), d);

			// if we have children, recurse
			ConstNodeIterator itor;
			ConstNodeIterator endChild = m_children.end();

			for(itor = m_children.begin(); itor != endChild; itor++)
			{
				ASSERT(*itor);
				(*itor)->DFSRender();
			}
		}
	}

	RestoreRenderState();
}

Texture *
ShadowMapManager::GetShadowMap(Light *l, int32 direction) const
{
	return (m_shadowmaps[l])[direction];
}

Texture *
ShadowMapManager::CreateShadowMap() const
{
	Texture *t = new Texture();
	bool b = t->Create(SHADOWMAP_SIZE, SHADOWMAP_SIZE, D3DFMT_A8R8G8B8, D3DUSAGE_RENDERTARGET, 1, D3DPOOL_DEFAULT);
	if(!b) 
		return 0;
	else
		return t;
}

void 
ShadowMapManager::CreateShadowMap(Light *l) const
{
	int32 d = SM_DOWN;
//	for(int32 d = SM_DOWN; d <= SM_FORWARD; d++)
	{
		Texture *t = CreateShadowMap();
		(m_shadowmaps[l])[d] = t;
	}
}

}

}
