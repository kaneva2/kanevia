/******************************************************************************
 Sprite.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Sprite.h"

#include "Device.h"
#include "Texture.h"
#include "TextureFactory.h"
#include "VSFactory.h"
#include "SpriteVS.h"
#include "RenderState.h"
#include "DynIVB.h"

namespace wkg {
namespace engine {

LPDIRECT3DVERTEXBUFFER9	Sprite::s_vb = 0;
LPDIRECT3DINDEXBUFFER9	Sprite::s_ib = 0;
Texture*				Sprite::s_curTex = 0;
Sprite::SpriteVector	Sprite::s_spritesToDraw;

struct SpriteVert 
{
	real index; 
	real srcLeft, srcTop, srcRight, srcBottom; 
	real scaleX, scaleY; 
	real rotCenterX, rotCenterY; 
	real rot; 
	real transX, transY; 
	uint32 color; 
};

void 
Sprite::SetVBIB(LPDIRECT3DVERTEXBUFFER9 vb, LPDIRECT3DINDEXBUFFER9 ib)
{
	s_vb = vb;
	s_ib = ib;
}

Sprite::Sprite(const char* szFilename)
:	m_pTexture(NULL),
	m_x(0),
	m_y(0),
	m_texWidth(0),
	m_texHeight(0),
	m_scaleX(1.0f),
	m_scaleY(1.0f),
	m_rotCCW(0.0f),
	m_rotCenterX(0),
	m_rotCenterY(0),
	m_colorAdjust(0xFFFFFFFF)
{
	if (szFilename)
		SetTexture(TextureFactory::Instance()->Get(szFilename));

	// SetTexture will increment the ref count also...adjust for that behavior
	//
	if (m_pTexture)
		m_pTexture->Release();

	SetSrcRect(0);
}


Sprite::Sprite(Texture* pTex)
:   m_pTexture(NULL),
	m_x(0),
	m_y(0),
	m_texWidth(0),
	m_texHeight(0),
	m_scaleX(1.0f),
	m_scaleY(1.0f),
	m_rotCCW(0.0f),
	m_rotCenterX(0),
	m_rotCenterY(0),
	m_colorAdjust(0xFFFFFFFF)
{
	SetTexture(pTex);
	SetSrcRect(0);
}


Sprite::Sprite()
:	m_pTexture(NULL),
	m_x(0),
	m_y(0),
	m_texWidth(0),
	m_texHeight(0),
	m_scaleX(1.0f),
	m_scaleY(1.0f),
	m_rotCCW(0.0f),
	m_rotCenterX(0),
	m_rotCenterY(0),
	m_colorAdjust(0xFFFFFFFF)
{
	SetSrcRect(0);
}


Sprite::~Sprite()
{
	if (m_pTexture)
	{
		m_pTexture->Release();
		m_pTexture = 0;
	}
}

int32 
Sprite::GetImageWidth() const
{
	return m_pTexture ? m_pTexture->GetSourceImageWidth() : 0;
}

int32 
Sprite::GetImageHeight() const
{
	return m_pTexture ? m_pTexture->GetSourceImageHeight() : 0;
}

void 
Sprite::SetTexture(Texture* pTex)
{
	if (m_pTexture)
		m_pTexture->Release();

	m_pTexture = TextureFactory::Instance()->Get(pTex);

	if (m_pTexture && m_pTexture->Interface())
	{
		D3DSURFACE_DESC desc;
		m_pTexture->Interface()->GetLevelDesc(0,&desc);
		m_texWidth = desc.Width;
		m_texHeight = desc.Height;
	}
	else
	{
		m_texWidth = m_texHeight = 1;	// 1 (instead of 0) to avoid divide-by-zero (see SetScale(x,y))
	}
}


void 
Sprite::SetTexture(const char* szFilename)
{
	if (m_pTexture)
		m_pTexture->Release();

	m_pTexture = TextureFactory::Instance()->Get(szFilename);

	if (m_pTexture && m_pTexture->Interface())
	{
		D3DSURFACE_DESC desc;
		m_pTexture->Interface()->GetLevelDesc(0,&desc);
		m_texWidth = desc.Width;
		m_texHeight = desc.Height;
	}
	else
	{
		m_texWidth = m_texHeight = 1;	// 1 (instead of 0) to avoid divide-by-zero (see SetScale(x,y))
	}
}

void 
Sprite::Draw()
{
	s_spritesToDraw.push_back(this);   // queue to draw in one batch
}

void 
Sprite::FlushDrawQueue()
{
	SpriteIterator iSprite = s_spritesToDraw.begin();
	SpriteIterator endSprite = s_spritesToDraw.end();

	if (iSprite != endSprite)
	{
		DynIVB divb;

		PrepareToRender(&divb);

		while (iSprite != endSprite)
		{
			(*iSprite)->Render(&divb);
			++iSprite;
		}

		FlushBatch(&divb);

		s_spritesToDraw.clear();
	}
}

void 
Sprite::DrawImmediate()
{
	if (m_pTexture && m_pTexture->Interface())
	{
		DynIVB divb;

		PrepareToRender(&divb);
		Render(&divb);
		FlushBatch(&divb);
	}
}

void
Sprite::PrepareToRender(DynIVB* divb)
{
	RenderState rs;
	VertexShader* vs;

	rs.Set(D3DRS_LIGHTING, FALSE);
	rs.Set(D3DRS_COLORVERTEX, FALSE);
	rs.Set(D3DRS_CULLMODE, D3DCULL_NONE);
	rs.Set(D3DRS_ZWRITEENABLE, FALSE);
	rs.Set(D3DRS_ZENABLE, FALSE);
	rs.Set(D3DRS_WRAP0, 0);
	rs.Set(D3DRS_ALPHABLENDENABLE, TRUE);
	rs.Set(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	rs.Set(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	rs.Set(D3DRS_FOGENABLE, FALSE);
	rs.Set(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	rs.Set(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
	rs.Set(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);
	rs.Set(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	rs.Set(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
	rs.Set(0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
	rs.SSet(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	rs.SSet(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	rs.SSet(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	rs.SSet(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	rs.SSet(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	rs.Set(1, D3DTSS_COLOROP, D3DTOP_DISABLE);

	vs = VSFactory::Instance()->GetShader("Sprite");

	divb->Prepare(s_vb, sizeof(SpriteVert), s_ib);

	Device::Instance()->SetRenderState(rs);
	vs->Activate();
	Device::Instance()->SetPixelShader(0);
	Device::Instance()->SetStreamSource(0, divb->GetVB(), 0, divb->GetVertSize());

	vs->LoadConstants();

	s_curTex = 0;
}

void
Sprite::Render(DynIVB* divb)
{
	if (m_pTexture && m_pTexture->Interface())
	{
		SpriteVert* pSpriteVert;
		uint16 *piSpriteVert, ivNext;
		int32 i;

		if (m_pTexture != s_curTex)
		{
			SpriteVS* vs;

			FlushBatch(divb);

			Device::Instance()->SetTexture(0, m_pTexture);

			vs = (SpriteVS*)VSFactory::Instance()->GetShader("Sprite");
			vs->SetTexture(m_pTexture);

			s_curTex = m_pTexture;
		}

		if (divb->CanAppend(4, 6, (uint8**)&pSpriteVert, &piSpriteVert, &ivNext))
		{
			for (i = 0; i < 4; i++)
			{
				pSpriteVert[i].index		= i;
				pSpriteVert[i].srcLeft		= m_srcRect.left;
				pSpriteVert[i].srcTop		= m_srcRect.top;
				pSpriteVert[i].srcRight		= m_srcRect.right;
				pSpriteVert[i].srcBottom	= m_srcRect.bottom;
				pSpriteVert[i].scaleX		= m_scaleX;
				pSpriteVert[i].scaleY		= m_scaleY;
				pSpriteVert[i].rotCenterX	= m_rotCenterX;
				pSpriteVert[i].rotCenterY	= m_rotCenterY;
				pSpriteVert[i].rot			= -m_rotCCW;
				pSpriteVert[i].transX		= m_x;
				pSpriteVert[i].transY		= m_y;
				pSpriteVert[i].color		= m_colorAdjust;
			}

			piSpriteVert[0] = ivNext + 0;
			piSpriteVert[1] = ivNext + 1;
			piSpriteVert[2] = ivNext + 2;
			piSpriteVert[3] = ivNext + 0;
			piSpriteVert[4] = ivNext + 2;
			piSpriteVert[5] = ivNext + 3;
		}
	}
}

void 
Sprite::FlushBatch(DynIVB* divb)
{
	int32 vCount, vStart;
	uint16 ivCount, ivStart;

	if (divb->BatchReady(&vStart, &vCount, &ivStart, &ivCount))
	{
		Device::Instance()->SetIndices(divb->GetIB());
		Device::Instance()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, vStart, 0, vCount, ivStart, ivCount / 3);
	}
}

}

}
