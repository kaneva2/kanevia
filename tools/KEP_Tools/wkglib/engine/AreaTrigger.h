/******************************************************************************
 AreaTrigger.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "WorldObject.h"

namespace wkg 
{

namespace engine
{

class AreaTrigger : public WorldObject
{
public:
	AreaTrigger(std::string name, int32 waitingFor);
	virtual ~AreaTrigger();

	int32 GetWaitingFor()						{	return m_waitingFor;		}
	void SetWaitingFor(int32 waitingFor)		{	m_waitingFor = waitingFor;	}

	void OnCollide(Collidable *other, Collision *c)	{}
	virtual void OnUpdate(uint32 ms);

	virtual WorldObject* CollisionDetected()				{	return 0;				}

protected:
	// string representing the name of the worldobject that this trigger is waiting to collide with
	int32			m_waitingFor;
	std::string		m_name;

};

}

}