/******************************************************************************
 Copy of octnode.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "mesh.h"
#include "vector3.h"
#include "meshtypes.h"
#include "intersect.h"
#include "distance.h"
#include "sphere.h"
#include "triangle.h"
#include "octnode.h"
#include "capsule.h"
#include "units.h"
#include "FileStream.h"

// #define CONSTRAIN_TRIS

#define NUM_TRIS (256)
#define MIN_VOLUME (120000.0f * 5.0f)

namespace wkg
{

namespace engine
{

uint32 OctNode::ms_child_flags[8] =
{
	LEFT  | LOWER | BACK,
	LEFT  | LOWER | FRONT,
	LEFT  | UPPER | BACK,
	LEFT  | UPPER | FRONT,
	RIGHT | LOWER | BACK,
	RIGHT | LOWER | FRONT,
	RIGHT | UPPER | BACK,
	RIGHT | UPPER | FRONT,
};

OctNode::OctNode()
{
	memzero(m_children, sizeof(OctNode *) * 8);
}

OctNode::~OctNode()
{
	for(int32 i = 0; i < 8; i++)
		delete m_children[i];

	m_children[i] = 0;
}

bool 
OctNode::AddMesh(Mesh *mesh, const Matrix44 &xform)
{
	std::vector<Vector3> vecs;
	mesh->VecsFromVerts(vecs, xform);

	return AddGeometry(vecs);
}

void 
OctNode::GetChildBounds(int32 child_idx, Vector3 &min, Vector3 &max)
{
	uint32 child = ms_child_flags[child_idx];

	if(child & LEFT) 
	{
		min.x = this->min.x;
		max.x = this->min.x + (this->max.x - this->min.x) / 2.0f;
	}
	else if(child & RIGHT)
	{
		min.x = this->min.x + (this->max.x - this->min.x) / 2.0f;
		max.x = this->max.x;
	}

	if(child & LOWER)
	{
		min.y = this->min.y;
		max.y = this->min.y + (this->max.y - this->min.y) / 2.0f;
	}
	else if(child & UPPER)
	{
		min.y = this->min.y + (this->max.y - this->min.y) / 2.0f;
		max.y = this->max.y;
	}

	if(child & BACK)
	{
		min.z = this->min.z;
		max.z = this->min.z + (this->max.z - this->min.z) / 2.0f;
	}
	else if(child & FRONT)
	{
		min.z = this->min.z + (this->max.z - this->min.z) / 2.0f;
		max.z = this->max.z;
	}
}

bool
OctNode::AddGeometry(std::vector<Vector3> &verts)
{
	// this function assumes nothing about the input verts, so pick
	//  out only those we're interested in

	std::vector<Vector3>::iterator vi = verts.begin();
	std::vector<Vector3>::iterator ve = verts.end();
	for(; vi < ve; vi += 3)
	{
		if(Intersect::AABBTri(*this, Triangle(&vi[0])))
		{
			m_verts.push_back(*vi);
			m_verts.push_back(*(vi + 1));
			m_verts.push_back(*(vi + 2));
		}
	}

	if(!m_verts.size()) 
		return false;

	// if our target triangles are within our range, then add them
	//  otherwise, push them down
#ifdef CONSTRAIN_TRIS
	if(m_verts.size() < NUM_TRIS * 3)
#else 
	if(max.x - min.x < MIN_VOLUME &&
	   max.y - min.y < MIN_VOLUME &&
	   max.z - min.z < MIN_VOLUME)
#endif
	{
		// we're in business - we're within our target
		return true;
	}
	else
	{
		// the new verts, plus our current verts (if any) are too many
		//  for us, so push them further down the tree.

		for(int32 i = 0; i < 8; i++)
		{
			if(!m_children[i])
			{
				m_children[i] = new OctNode();
				Vector3 min, max;
				GetChildBounds(i, min, max);
				m_children[i]->Set(min, max);
			}
			if(!m_children[i]->AddGeometry(m_verts))
			{
				if(!m_children[i]->HasChildren())
				{
					delete m_children[i];
					m_children[i] = 0;
				}
			}
		}

		m_verts.clear();
	}

	return true;
}

bool 
OctNode::HasChildren()
{
	return 
	   (m_children[0] ||
		m_children[1] ||
		m_children[2] ||
		m_children[3] ||
		m_children[4] ||
		m_children[5] ||
		m_children[6] ||
		m_children[7]);
}

void 
OctNode::DFSRender()
{
	Node::DFSRender();

	for(int32 i = 0; i < 8; i++)
	{
		if(m_children[i])
			m_children[i]->DFSRender();
	}
}

void 
OctNode::DFSUpdate(Matrix44 &m)
{
	Node::DFSUpdate(m);

	for(int32 i = 0; i < 8; i++)
	{
		if(m_children[i])
			m_children[i]->DFSUpdate(m);
	}
}

bool 
OctNode::CollideCapsule(Capsule &c, Vector3 *n)
{
	if(Intersect::CapsuleAABB(c, *this))
	{
		if(m_verts.size())
		{
			// test our geometry
			for(uint32 v = 0; v < m_verts.size() / 3; v++)
			{
				Triangle tri(&m_verts[v * 3]);
				if(Intersect::CapsuleTri(c, tri))
				{
					if(n)
						*n = tri.GetNormal().Normalize();
					return true;
				}
			}
		}
		else
		{
			for(int32 i = 0; i < 8; i++)
			{
				if(m_children[i])
				{
					if(m_children[i]->CollideCapsule(c, n))
						return true;
				}
			}
		}
	}

	return false;
}

bool 
OctNode::CollideSphereExhaustive(Vector3 &dst, real r, Vector3 *n)
{
	// DWD TODO:
	DEBUGMSG("Warning: CollideSphereExhaustive called, not implemented yet.");
	DEBUGMSG("Defaulting to non-exhaustive behavior");

	return CollideSphere(dst, r, n);
}

bool 
OctNode::CollideSphere(Vector3 &dst, real r, Vector3 *n, Vector3 *poc)
{
	Sphere sphere(dst, r);
	if(Intersect::AABBSphere(*this, sphere))
	{
		if(m_verts.size())
		{
			// test our geometry
			for(uint32 v = 0; v < m_verts.size() / 3; v++)
			{
				Triangle tri(&m_verts[v * 3]);
				if(Intersect::SphereTri(sphere, tri))
				{
					if(n)
					{
						*n = tri.GetNormal().Normalize();
					}

					if(poc)
					{
						// get the distance from the tri to the center
						//  of the sphere
						real dist = Distance::SqrTriPoint(tri, dst);
						dist = wkg::sqrt(dist);
						dist -= r;
						(*poc) = dst + tri.GetNormal().Normalize() * dist;
					}

					return true;
				}
			}
		}
		else
		{
			for(int32 i = 0; i < 8; i++)
			{
				if(m_children[i])
				{
					if(m_children[i]->CollideSphere(dst, r, n))
						return true;
				}
			}
		}
	}

	return false;
}

bool
OctNode::SweptCircleEdgeVertexIntersect(
		Vector3 &in_v1, Vector3 &in_v2, Vector3 &in_v3,
		Vector3 &begin, Vector3 &delta, 
		real a, real b, real c, 
		Vector3 &out_point, float &out_fraction)
{
	real upper_bound = 1.0f;
	bool collision = false;

	for(int32 i = 0; i < 3; i++)
	{
		Vector3 v1, v2;
		switch(i)
		{
		case 0:
			v1 = in_v1;
			v2 = in_v2;
			break;

		case 1:
			v1 = in_v2;
			v2 = in_v3;
			break;

		case 2:
			v1 = in_v3;
			v2 = in_v1;
			break;

		default:
			ASSERT(0);
			break;
		}

		real t;

		Vector3 bv1 = v1 - begin;
		real a1 = a - delta.LengthSqr();
		real b1 = b + 2.0f * delta.Dot(bv1);
		real c1 = c - bv1.LengthSqr();

		if(FindLowestRootInInterval(a1, b1, c1, upper_bound, t))
		{
			// we have a collision
			collision = true;
			upper_bound = t;
			out_point = v1;
		}

		// check if circle hits edge
		Vector3 v1v2 = v2 - v1;
		real v1v2_dot_delta = v1v2.Dot(delta);
		real v1v2_dot_bv1 = v1v2.Dot(bv1);
		real v1v2_len_sq = v1v2.LengthSqr();

		real a2 = v1v2_len_sq * a1 + v1v2_dot_delta * v1v2_dot_delta;
		real b2 = v1v2_len_sq * b1 - 2.0f * v1v2_dot_bv1 * v1v2_dot_delta;
		real c2 = v1v2_len_sq * c1 + v1v2_dot_bv1 * v1v2_dot_bv1;
		if(FindLowestRootInInterval(a2, b2, c2, upper_bound, t))
		{
			// check if intersection point is on edge
			real f = t * v1v2_dot_delta - v1v2_dot_bv1;
			if(f >= 0.0f && f < v1v2_len_sq)
			{
#if 1
				// we have a collision
				collision = true;
				upper_bound = t;
				out_point = v1 + v1v2 * (f / v1v2_len_sq);
#endif
			}
		}
	}

	if(!collision)
		return false;

	out_fraction = upper_bound;
	return true;
}

bool
OctNode::FindLowestRootInInterval(real a, real b, real c, real upper_bound, real &outx)
{
	// check if solution exists
	real determinant = b * b - 4.0f * a * c;
	if(determinant < 0.0f)
		return false;

	// solve
	real q = -0.5f * (b + (b < 0.0f ? -1.0f : 1.0f) * sqrt(determinant));

	// start with highest possible solution
	real x = upper_bound;
	bool solution = false;

	// solution one
	if(a >= 0.0f)
	{
		if(0.0f <= a && a < x * q)
		{
			x = q / a;
			solution = true;
		}
	}
	else
	{
		if(0.0f >= a && a  >= x * q)
		{
			x = q / a;
			solution = true;
		}
	}

	// solution two
	if(q >= 0.0f)
	{
		if(0.0f <= c && c <= x * q)
		{
			x = c / q;
			solution = true;
		}
	}
	else
	{
		if(0.0f >= c && c >= x * q)
		{
			x = c / q;
			solution = true;
		}
	}

	// store if within [0, upper_bound]
	if(!solution)
		return false;

	outx = x;
	return true;
}

bool
OctNode::PolygonCircleIntersect(
		Vector3 &in_v1, Vector3 &in_v2, Vector3 &in_v3, 
		Vector3 &center, real radius_sq, Vector3 &out_point)
{
	if(PolygonContains(center, in_v1, in_v2, in_v3))
	{
		out_point = center;

		return true;
	}

	// loop through edges
	bool collision = false;

	for(int32 i = 0; i < 3; i++)
	{
		Vector3 v1, v2;
		switch(i)
		{
		case 0:
			v1 = in_v1;
			v2 = in_v2;
			break;

		case 1:
			v1 = in_v2;
			v2 = in_v3;
			break;

		case 2:
			v1 = in_v3;
			v2 = in_v1;
			break;

		default:
			ASSERT(0);
			break;
		}

		Vector3 v1_v2 = v2 - v1;
		Vector3 v1_center = center - v1;
		real fraction = v1_center.Dot(v1_v2);
		if(fraction < 0.0f)
		{
			// closest point is v1
			real dist_sq = v1_center.LengthSqr();
			if(dist_sq < radius_sq)
			{
				collision = true;
				out_point = v1;
				radius_sq = dist_sq;
			}
		}
		else
		{
			real v1_v2_len_sq = v1_v2.LengthSqr();
			if(fraction <= v1_v2_len_sq)
			{
				// closest point on line segment
				Vector3 point = v1 + v1_v2 * (fraction / v1_v2_len_sq);
				real dist_sq = (point - center).LengthSqr();
				if(dist_sq <= radius_sq)
				{
					collision = true;
					out_point = point;
					radius_sq = dist_sq;
				}
			}
		}
	}

	return collision;
}

bool
OctNode::PlaneSweptSphereIntersection(
		Vector3 &n, Vector3 &p,
		Vector3 begin, Vector3 &delta, real radius, 
		float &t1, float &t2, real &dist_to_plane)
{
	real n_dot_d = n.Dot(delta);
	real dist_to_b = n.Dot(begin - p);
	if(EQUAL(n_dot_d, 0.0f))
	{
//		if(EQUAL(dist_to_b, radius))
//		{
//			ASSERT(0);
//		}
		if(ABS(dist_to_b) > radius)
		{
			return false;
		}

		// intersects entire range
		t1 = 0.0f;
		t2 = 1.0f;

		dist_to_plane = dist_to_b;

		return true;
	}
	else
	{
		dist_to_plane = 0.0f;
		// determine interval of intersetion
		t1 = (radius - dist_to_b)/n_dot_d;
		t2 = (-radius - dist_to_b)/n_dot_d;

		// order
		if(t1 > t2)
		{
			real temp = t1;
			t1 = t2;
			t2 = temp;
		}

		// early out if no hit possible
		if(t1 > 1.0f || t2 < 0.0f)
			return false;

		// clamp
//		if(t1 < 0.0f) t1 = 0.0f;
//		if(t2 > 1.0f) t2 = 1.0f;
	}

	return true;
}

bool
OctNode::PolygonContains(Vector3 &p, Vector3 &v1, Vector3 &v2, Vector3 &v3)
{
	// if the point is outside any edge, it's outside the poly
	// edge 1
	Vector3 v1_v2 = v2 - v1;
	Vector3 v1_p = p - v1;
	if(v1_v2.x * v1_p.y - v1_p.x * v1_v2.y < 0.0f)
		return false;

	// edge 2
	v1_v2 = v3 - v2;
	v1_p = p - v2;
	if(v1_v2.x * v1_p.y - v1_p.x * v1_v2.y < 0.0f)
		return false;

	// edge 3
	v1_v2 = v1 - v3;
	v1_p = p - v3;
	if(v1_v2.x * v1_p.y - v1_p.x * v1_v2.y < 0.0f)
		return false;


	return true;
}

void
GetBasisVectors(Vector3 &u, Vector3 &v, Vector3 &n)
{
	// given n, find u and v for the orthonormal basis
	real nzsq = n.z * n.z;

	if(n.x > n.y)
	{
		real nxsq = n.x * n.x;
		real d1 = sqrt(nxsq + nzsq);
		u.x = n.z / d1;
		u.y = 0.0f;
		u.z = -n.x / d1;
	}
	else
	{
		real nysq = n.y * n.y;
		real d2 = sqrt(nysq + nzsq);
		u.x = 0.0f;
		u.y = n.z / d2;
		u.z = -n.y / d2;
	}

	v = n.Cross(u);
}

Vector3
ConvertWorldToPlane(Vector3 &u, Vector3 &v, Vector3 &n, Vector3 &p)
{
	Vector3 outp;

	outp.x = p.Dot(u);
	outp.y = p.Dot(v);
	outp.z = 0.0f;

	return outp;
}

Vector3
ConvertPlaneToWorld(Vector3 &u, Vector3 &v, Vector3 &n, real c, Vector3 &p)
{
	Vector3 outp;
	outp = u * p.x + v * p.y - n * c;

	return outp;
}

bool
OctNode::PolygonSweptSphereIntersect(
		Vector3 &v1_three, Vector3 &v2_three, Vector3 &v3_three,
		Vector3 &begin, Vector3 &delta, 
		real radius, 
		Vector3 &out_point,
		real &out_fraction,
		real &distance_to_plane)
{
	Vector3 n = (v2_three - v1_three).Cross(v3_three - v1_three);
	n.Normalize();

	Vector3 plane_p = v1_three;

	// get basis
	Vector3 u, v;
	GetBasisVectors(u, v, n);

	// now project our three points onto the plane
	Vector3 v1 = ConvertWorldToPlane(u, v, n, v1_three);
	Vector3 v2 = ConvertWorldToPlane(u, v, n, v2_three);
	Vector3 v3 = ConvertWorldToPlane(u, v, n, v3_three);

	real plane_c = - n.Dot(plane_p);

	// determine range sphere intersects over plane
	real t1, t2;
	if(!PlaneSweptSphereIntersection(n, plane_p, begin, delta, radius, t1, t2, distance_to_plane))
	{
		return false;
	}

	real n_dot_d = n.Dot(delta);
	real dist_to_b = n.Dot(begin - plane_p);
	real a = -n_dot_d * n_dot_d;
	real b = -2.0f * n_dot_d * dist_to_b;
	real c = radius * radius - dist_to_b * dist_to_b;

	// get begin and delta in plane space
	Vector3 pbegin = ConvertWorldToPlane(u, v, n, begin);
	Vector3 pdelta = ConvertWorldToPlane(u, v, n, delta);

	// test if sphere intersects at t1
	Vector3 p;
	if(PolygonCircleIntersect(v1, v2, v3, pbegin + pdelta * t1, a * t1 * t1 + b * t1 + c, p))
	{
		out_fraction = t1;
//		ASSERT(out_fraction > 0.0f);
		out_point = ConvertPlaneToWorld(u, v, n, plane_c, p);
		return true;
	}

	// test if sphere intersects with one of the edges or verts
	if(SweptCircleEdgeVertexIntersect(v1, v2, v3, pbegin, pdelta, a, b, c, p, out_fraction))
	{
//		ASSERT(out_fraction > 0.0f);
		out_point = ConvertPlaneToWorld(u, v, n, plane_c, p);
		return true;
	}

	return false;
}

bool
OctNode::CollideDynamicEllipsoid(Vector3 &in_src, Vector3 &in_out_dst, real r, real yscale, Vector3 *out_n)
{
	static int32 count = 0;
	DEBUGMSG("--------------------");
	DEBUGMSG("%d", count++);

	Vector3 pofc;
	Vector3 n;

	Vector3 delta = in_out_dst - in_src;
	delta.Normalize();
//	Vector3 src = in_src - delta * r;
	Vector3 src = in_src;
	Vector3 dst = in_out_dst;

	CollisionType ct = CollideDynamicEllipsoidHelper(src, dst, r, yscale, &n, &pofc);

	switch(ct)
	{
	case ONC_NONE:
		DEBUGMSG("ONC_NONE");
		break;
	case ONC_TOUCHING:
		DEBUGMSG("ONC_TOUCHING");
		break;
	case ONC_COLLIDING:
		DEBUGMSG("ONC_COLLIDING");
		break;
	case ONC_EMBEDDED:
		DEBUGMSG("ONC_EMBEDDED");
		break;
	}

	// don't do anything if there was no collision or just moving along a plane
	if(ONC_NONE != ct && ONC_TOUCHING != ct)
	{
		n.Normalize();

		if(ONC_EMBEDDED == ct)
		{
			// penetrating this polygon, so push us back along the normal
			dst = pofc + n;
		}

		if(out_n)
			(*out_n) = n;

		in_out_dst = dst;

		return true;
	}
	else
	{
		return false;
	}
}

OctNode::CollisionType
OctNode::CollideDynamicEllipsoidHelper(
	Vector3 &in_src, 
	Vector3 &in_out_dst, 
	real r, 
	real yscale, 
	Vector3 *out_n, 
	Vector3 *pofc,
	std::vector<OctNodeCollision> *primitives)
{
	LineSegment seg;
	seg.SetEndPoints(in_src, in_out_dst);

	real cap_radius = r;
	if(yscale > 1.0f)
		cap_radius *= yscale;

	Capsule c(seg, cap_radius);

	// static test using a capsule with the larget radius as the capsule radius
	Vector3 capsule_n;
	if(CollideCapsule(c, &capsule_n))
	{
		Vector3 src = in_src;

		src.y /= yscale;

		Vector3 out_dst = in_out_dst;
		out_dst.y /= yscale;

		if(m_verts.size())
		{
			CollisionType out_ct = ONC_NONE;

			// test our geometry
			for(uint32 v = 0; v < m_verts.size(); v += 3)
			{
				Vector3 v1 = m_verts[v + 0];
				v1.y /= yscale;
				Vector3 v2 = m_verts[v + 1];
				v2.y /= yscale;
				Vector3 v3 = m_verts[v + 2];
				v3.y /= yscale;

				if(v1 == v2 || v2 == v3 || v1 == v3) continue;

				real distance_to_plane;
				real fraction;
				Vector3 out_p;
				bool b = 
					PolygonSweptSphereIntersect(
					v1, v2, v3, 
					src, (out_dst - src), 
					r, 
					out_p, 
					fraction,
					distance_to_plane);

//				DEBUGMSG("**%.4f", distance_to_plane);

				// only count collisions, not if we're moving
				//  away from the polygon or along the surface
				if(b)
				{
					Vector3 n = (v2 - v1).Cross(v3 - v1);
					n.y *= yscale;
					n.Normalize();

					// given the normal, dot the motion vector
					real dotp = n.Dot((in_out_dst - in_src).Normalize());

					if(dotp < 0.0f)
					{
						if(pofc)
						{
							*pofc = in_src + (in_out_dst - in_src) * fraction;
						}

						if(fraction < 0.0f)
						{
							out_ct = ONC_EMBEDDED;
						}
						else
						{
							out_ct = ONC_COLLIDING;
						}

						if(fraction > 1.0f) fraction = 1.0f;
						if(fraction < 0.0f) fraction = 0.0f;

						in_out_dst = in_src + (in_out_dst - in_src) * fraction;

						if(out_n)
							*out_n = n;
					}
					else if(EQUAL(0.0f, dotp))
					{
//						DEBUGMSG("-----------------------------");
						out_ct = ONC_TOUCHING;
						DEBUGMSG("Normal:");
						n.Dump();
//						DEBUGMSG("%.4f", distance_to_plane);
//						DEBUGMSG("-----------------------------");
					}
				}
			}

			return out_ct;
		}
		else
		{
			CollisionType total_ct = ONC_NONE;
			for(int32 i = 0; i < 8; i++)
			{
				if(m_children[i])
				{
					CollisionType ct = m_children[i]->CollideDynamicEllipsoidHelper(in_src, in_out_dst, r, yscale, out_n);
					switch(total_ct)
					{
					case ONC_NONE:
						// accept it no matter what - it can't be any less serious than nothing
						total_ct = ct;
						break;

					case ONC_TOUCHING:
						// use it only if it's not nothing
						if(ONC_NONE != ct)
							total_ct = ct;
						break;

					case ONC_COLLIDING:
						// use it if it's anything other than none, or touching
						if(ONC_NONE != ct && ONC_TOUCHING != ct)
							total_ct = ct;
						break;

					case ONC_EMBEDDED:
						// just don't use it - we're already embedded, so it can't be any
						//  more serious
						break;
					}
				}
			}
			return total_ct;
		}

		return ONC_NONE;
	}

	return ONC_NONE;
}

bool
OctNode::CollideDynamicSphere(Vector3 &src, Vector3 &out_dst, real r, Vector3 *out_n)
{
	Vector3 n;
	bool b = CollideDynamicSphereHelper(src, out_dst, r, &n);

	if(b)
	{
		out_dst = out_dst + n * 1.0f;
		if(out_n)
			(*out_n) = n;
	}

	return b;
}

bool 
OctNode::CollideDynamicSphereHelper(Vector3 &src, Vector3 &out_dst, real r, Vector3 *out_n)
{
	LineSegment seg;
	seg.SetEndPoints(src, out_dst);

	Capsule c(seg, r);
	// static test
	if(CollideCapsule(c, out_n))
	{
#if 1
		if(m_verts.size())
		{
			bool out_b = false;

			// test our geometry
			for(uint32 v = 0; v < m_verts.size(); v += 3)
			{
				Vector3 v1 = m_verts[v + 0];
				Vector3 v2 = m_verts[v + 1];
				Vector3 v3 = m_verts[v + 2];

				if(v1 == v2 || v2 == v3 || v1 == v3) continue;

				real fraction;
				Vector3 out_p;
				real dist_to_b;
				bool b = 
					PolygonSweptSphereIntersect(
					v1, v2, v3, 
					src, (out_dst - src), 
					r, 
					out_p, fraction, dist_to_b);

				if(fraction > 1.0f) fraction = 1.0f;
				if(fraction < 0.0f) fraction = 0.0f;

				ASSERT(fraction <= 1.0f);

				if(b)
				{
					Vector3 n = (v2 - v1).Cross(v3 - v1);
					n.Normalize();

					if(out_n)
					{
						*out_n = n;
//						*out_n = (out_dst - src).Normalize();
					}

					out_b = true;

					out_dst = src + (out_dst - src) * fraction;
#if 0
					if((out_dst - src).Length() > 1.5f)
						out_dst -= (out_dst - src).Normalize() * 1.5f;
					else
						out_dst = src;
#endif

					if(out_dst == src) break;
				}
			}

			return out_b;
		}
		else
		{
			bool b = false;
			for(int32 i = 0; i < 8; i++)
			{
				if(m_children[i])
				{
					b = b | (m_children[i]->CollideDynamicSphereHelper(src, out_dst, r, out_n));
				}
			}
			return b;
		}

		return false;
#else
		Vector3 dir = (out_dst - src);
		real lensqr = dir.LengthSqr();
		dir.Normalize();

		Vector3 v_step;
//		if((r > 0.0f) && (lensqr > r*r))
//		{
//			// 1/4 radius precision for capsules with large steps
//			v_step = dir * (r / 4.0f);
//		}

		if(lensqr < 1.0f)
		{
			// already within one inch
			out_dst = src;
			return true;
		}

		if(Units::FromFeet(1.0f / 12.0f) < r)
		{
			// once inch precision for line segments and capsules with small steps
			v_step = dir * Units::FromFeet(1.0f / 12.0f);
		}
		else
		{
			v_step = dir * r;
		}

		Vector3 delta(0.0f, 0.0f, 0.0f);
		Vector3 curr = src;

		while(delta.LengthSqr() < lensqr)
		{
			Vector3 dst = src + delta;
			if(CollideSphere(dst, r, out_n))
			{
				out_dst = curr;
				return true;
			}
			else
			{
				curr = dst;
			}

			delta += v_step;
			if(delta.LengthSqr() > lensqr)
			{
				out_dst = curr;
				return false;
			}
		}
#endif
	}

	return false;
}


}

}

#ifdef USE_SERIALIZATION
#include "Stream.h"
namespace wkg {
namespace engine {

bool 
OctNode::Load(Stream* stream)
{
	uint32 i;
	uint8 children;
	uint32 numVerts;
	Vector3 vert;
	bool result;

	result = stream->Read(&children)
		&& AABB::Load(stream)
		&& stream->Read(&numVerts);

	for (i = 0; result && i < numVerts; i++)
	{
		result = vert.Load(stream);
		if (result)
			m_verts.push_back(vert);
	}

	for (i = 0; result && i < 8; i++)
	{
		if (children & (1 << i))
		{
			m_children[i] = new OctNode();
			if (m_children[i])
				result = m_children[i]->Load(stream);
			else
				result = false;
		}
	}

	return result;
}

bool 
OctNode::Save(Stream* stream) const
{
	uint32 i;
	uint8 children;
	uint32 numVerts;
	bool result;

	children = 0;

	for (i = 0; i < 8; i++)
		if (m_children[i])
			children |= (1 << i);

	numVerts = m_verts.size();

	result = stream->Write(children)
		&& AABB::Save(stream)
		&& stream->Write(numVerts);

	for (i = 0; result && i < numVerts; i++)
		result = m_verts[i].Save(stream);

	for (i = 0; result && i < 8; i++)
		if (m_children[i])
			result = m_children[i]->Save(stream);

	return result;
}

}}

#endif

