/******************************************************************************
 meshnode.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "sgraph/xformnode.h"

namespace wkg
{

namespace engine
{

class Device;
class Mesh;
class MeshRenderer;

class MeshNode : public XFormNode
{
    public:
	    MeshNode();
	    virtual ~MeshNode();

	    virtual void OnRender() const;

	    void SetMesh(Mesh *mesh);

		// replaces all the mesh renderers with this one
		void SetRenderer(MeshRenderer *renderer);

		// used for adding effects on top of meshes
		void AddRenderer(MeshRenderer *renderer);

	    Mesh *GetMesh() { return m_mesh; }

    protected:
		void ClearRenderers();

    protected:
	    Mesh *m_mesh;
		std::vector<MeshRenderer *> m_renderers;
};

}

}

