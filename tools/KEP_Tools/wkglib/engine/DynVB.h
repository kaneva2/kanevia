/******************************************************************************
 DynVB.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg 
{

namespace engine 
{

class DynVB
{
	public:
		DynVB();
		~DynVB();

		LPDIRECT3DVERTEXBUFFER9 GetVB();
		uint8 GetVertSize();

		bool Prepare(LPDIRECT3DVERTEXBUFFER9 pVB, uint8 vertSize, int32 numBatches = 4);

		bool CanAppend(int32 numVerts, uint8** ppVert);			// returns false if no room currently available (i.e. needs BatchReady())
		bool BatchReady(int32* pStartVert, int32* pNumVerts);

	protected:
		LPDIRECT3DVERTEXBUFFER9 pVB;
		uint8 vertSize;
		int32 maxVerts, maxBatches, vertsPerBatch;
		int32 startByte, vbNext;
		uint8* pBatch;
};

inline LPDIRECT3DVERTEXBUFFER9
DynVB::GetVB()
{
	return pVB;
}

inline uint8 
DynVB::GetVertSize()
{
	return vertSize;
}

}

}

