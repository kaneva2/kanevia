/******************************************************************************
 orientmplexcontroller.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "controllable.h"
#include "controller.h"
#include "timedupdatable.h"

namespace wkg
{

class Quaternion;

namespace engine
{

class OrientMPlexController : public Controllable, public Controller, TimedUpdatable
{
public:
	OrientMPlexController();
	virtual ~OrientMPlexController();

	virtual void ControllableUpdate();

	void SetTarget(Quaternion *q) { m_target = q; }

	void SetPlaying(uint32 idx, bool playing) 
	{ 
		if(idx < m_playing.size())
			m_playing[idx] = playing; 
	}

	int32 AddEntry(int32 priority);
	Quaternion *GetEntry(int32 idx);

	void BeginTransition(real time) 
	{ 
		if(m_target && !m_first)
		{
			m_transition_begin = *m_target; 
			m_transitioning = true;
			m_transition_time_left = m_transition_time = time;
		}
		else if(m_first)
		{
			m_first = false;
		}
	}

	virtual void TimedUpdate(int32 mspf);

protected:

	std::vector<Quaternion *> m_orientations;
	std::vector<int32> m_priority;
	std::vector<bool> m_playing;

	Quaternion *m_target;
	Quaternion m_transition_begin;
	real m_transition_time;
	real m_transition_time_left;
	bool m_transitioning;
	bool m_first;
};

}

}