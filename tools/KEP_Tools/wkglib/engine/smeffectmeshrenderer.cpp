/******************************************************************************
 smeffectmeshrenderer.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "vector4.h"
#include "meshtypes.h"
#include "meshsection.h"
#include "Mesh.h"
#include "TangentBasis.h"
#include "texturefactory.h"
#include "effectshader.h"
#include "lightmanager.h"
#include "light.h"
#include "shadowmapmanager.h"
#include "smeffectmeshrenderer.h"

namespace wkg 
{

namespace engine 
{

SMEffectMeshRenderer::SMEffectMeshRenderer(EffectShader *es) : m_shader(es)
{
    D3DVERTEXELEMENT9 decl[] =
    {
        { 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,		0 },
        { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,		0 },
        { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,		0 },
        { 1,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT,		0 },
        { 1, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT,		1 },
        { 1, 24, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL,		0 },
        D3DDECL_END()
    };

	HRESULT hres = Device::Instance()->CreateVertexDeclaration(decl, &m_decl);

	m_spot = TextureFactory::Instance()->Get("textures/spot.dds");
}

void 
SMEffectMeshRenderer::Render(Mesh *mesh)
{
	if(!m_shader)
	{
		ASSERT(0);
		return;
	}

	int32 num_passes = m_shader->Begin();
	for(int32 i = 0; i < num_passes; i++)
	{
		m_shader->Pass(i);
		m_shader->Prepare(mesh);

		RenderAllSections(mesh);
	}

	m_shader->End();
}

void 
SMEffectMeshRenderer::RenderAllSections(Mesh *mesh)
{
	Device::Instance()->SetVertexDeclaration(m_decl);
	Device::Instance()->SetStreamSource(0, mesh->GetVB(), 0, sizeof(WKGVertex));

	Matrix44 view;
	Device::Instance()->GetTransform(D3DTS_VIEW, &view);
	view.Inverse();
	Vector3 cam_pos(view._41, view._42, view._43);

	for (uint32 i = 0; i < mesh->GetNumSections(); i++)
	{
		MeshSection *section = mesh->GetSections()[i];

		Vector3 min = section->GetMin();
		Vector3 max = section->GetMax();

		real dist = 0.0f;

		if(cam_pos.x < min.x)
			if(min.x - cam_pos.x > dist) dist = min.x - cam_pos.x;

		if(cam_pos.y < min.y)
			if(min.y - cam_pos.y > dist) dist = min.y - cam_pos.y;

		if(cam_pos.z < min.z)
			if(min.z - cam_pos.z > dist) dist = min.z - cam_pos.z;

		if(cam_pos.x > max.x)
			if(cam_pos.x - max.x > dist) dist = cam_pos.x - max.x;

		if(cam_pos.y > max.y)
			if(cam_pos.y - max.y > dist) dist = cam_pos.y - max.y;

		if(cam_pos.z > max.z)
			if(cam_pos.z - max.z > dist) dist = cam_pos.z - max.z;


		if(dist > 1200.0f) continue;


		if(section)
		{
			m_shader->PrepareSection(section);

			// set the texture
			ASSERT(section->m_material >= 0 && section->m_material < mesh->GetNumMaterials());

			WKGMaterial *mesh_material = mesh->GetMaterials() + section->m_material;

			WKGColor ambient = LightManager::Instance()->GetAmbient();

			WKGColor material_diffuse = mesh_material->diffuse;
			WKGColor material_ambient = mesh_material->ambient;
			WKGColor material_specular = mesh_material->specular;
			real material_power = mesh_material->power;
			if(!EQUAL(0.0f, material_power))
			{
				material_specular.r = material_specular.g = material_specular.b = 0.0f;
			}
			else
			{
				material_power = 1.0f / material_power;
			}

			ASSERT(section->m_material < mesh->GetNumTextures());

			Light *lights[4];
//			int32 n = LightManager::Instance()->GetLightsForMeshSection(section, lights, 4);
			int32 n = LightManager::Instance()->GetClosestLights(cam_pos, lights, 4);
//			int32 n = 1;
//			lights[0] = LightManager::Instance()->GetLight(0);

			for(int32 l = 0; l < n; l++)
			{
//				for(int32 d = ShadowMapManager::SM_DOWN; d <= ShadowMapManager::SM_FORWARD; d++)
				int32 d = ShadowMapManager::SM_DOWN;
				{
					// get the shadowmap for this light
					Texture *sm = ShadowMapManager::Instance()->GetShadowMap(lights[l], d);
					m_shader->SetTexture("ShadowMap", sm);

					// get the lightrange for this light
					Vector4 range = ShadowMapManager::Instance()->GetLightRange(lights[l]);
					m_shader->SetVector("LightRange", range);

					// now setup the texture projection matrix for this light
					Matrix44 tpm = ShadowMapManager::Instance()->GetTextureProjection(lights[l], d);
					m_shader->SetMatrix("TextureProjection", tpm);

					// now setup the projection matrix that the verts went through for the light
					Matrix44 lp_view = ShadowMapManager::Instance()->GetViewTransform(lights[l], d);
					Matrix44 lp_proj = ShadowMapManager::Instance()->GetProjection(lights[l]);
					Matrix44 lpm = lp_view * lp_proj;
					m_shader->SetMatrix("LightProjection", tpm);

					m_shader->SetTexture("LightMap", m_spot);

					// use lights[l] to project the shadowmap
					section->Render();
				}
			}
		}
	}
}

}

}
