/******************************************************************************
 watermeshrenderer.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "effectmeshrenderer.h"

namespace wkg
{

namespace engine
{

class EffectShader;

class WaterMeshRenderer : public EffectMeshRenderer
{
public:
	WaterMeshRenderer(EffectShader *shader);

protected:

};

}

}

