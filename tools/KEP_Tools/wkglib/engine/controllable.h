/******************************************************************************
 controllable.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <vector>

namespace wkg
{

namespace engine
{

class Controllable
{
public:
	virtual void ControllableUpdate() = 0;
};

typedef std::vector<Controllable *> ControllableVector;
typedef ControllableVector::iterator ControllableIterator;

}

}