/******************************************************************************
 positionkeyframecontroller.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "vector3.h"
#include "controllable.h"
#include "controller.h"
#include "wkgkeyframe.h"

namespace wkg
{

namespace engine
{

class PositionKeyframeController : public Controllable, public Controller
{
public:
	PositionKeyframeController();
	virtual ~PositionKeyframeController();

	virtual void ControllableUpdate();
	void SetTarget(Vector3 *pos) 
	{ 
		m_position = pos; 
	}

	void BeginTransition() 
	{ 
		if(m_position)
			m_begin_transition = *m_position; 
	}

	void AddPositionKeyframe(WKGPositionKeyframe *poskf)
	{
		m_position_keyframes.push_back(poskf);
	}

	// public to be controlled by the keyframe controller
	int32 m_frame;
	int32 m_next_frame;
	real m_partial_frame;

protected:
	WKGPositionKeyframeVector m_position_keyframes;

	Vector3 *m_position;
	Vector3 m_begin_transition;
};

}

}