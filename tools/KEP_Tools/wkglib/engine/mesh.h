/******************************************************************************
 mesh.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <vector>
#include "vector3.h"
#include "refcounter.h"

namespace wkg
{

class WKGMaterial;
class WKGVertex;
class MeshSection;
class WKGFace;
class AABB;
class Matrix44;
class WKBFile;
class WKGColor;
class WKGVertexColor;

namespace engine
{

class Texture;
class MeshSection;

class Mesh : public RefCounter<Mesh>
{
public:
	Mesh();
	virtual ~Mesh();

	virtual bool Load(WKBFile &wkbfile);
	virtual bool Load(const char *filename);

	void GetAABB(AABB &aabb);

	WKGVertex *GetVerts() { return m_verts; }
	uint32 GetNumVerts() { return m_num_verts; }

	WKGFace *GetFaces() { return m_faces; }
	uint32 GetNumFaces() { return m_num_faces; }

	uint32 GetNumSections() { return m_num_sections; }
	MeshSection **GetSections() { return m_sections; }

	uint32 GetNumSolidSections() { return m_num_solid_sections; }
	MeshSection **GetSolidSections() { return GetSections(); }

	uint32 GetNumTransparentSections() { return GetNumSections() - GetNumSolidSections(); }
	MeshSection **GetTransparentSections() { return m_sections + GetNumSolidSections(); }

	uint32 GetNumMaterials() { return m_num_materials; }
	WKGMaterial *GetMaterials() { return m_materials; }

	uint32 GetNumTextures() { return m_num_textures; }
	Texture **GetTextures() { return m_textures; }
	Texture **GetBumpTextures() { return m_bump_textures; }

	LPDIRECT3DVERTEXBUFFER9 GetVB()			{ return m_vb; }
	LPDIRECT3DVERTEXBUFFER9 GetColorVB()	{ return m_color_vb; }
	LPDIRECT3DVERTEXBUFFER9 GetBumpVB()		{ return m_bumpVB; }

	void VecsFromVerts(std::vector<Vector3> &vecs, const Matrix44 &xform);

	bool ComputeTangentBasis(uint32 VertStride);

protected:
	// constructs the vertex buffer
	void SetVerts(uint32 num_verts, WKGVertex *v);

	// constructs the vertex buffer for the colors
	void SetVertColors(uint32 num_verts, WKGVertexColor *colors);


protected:
	uint32 m_num_sections;
	MeshSection **m_sections;		// sections are sorted: solid sections followed by transparent sections; the order within each group is arbitrary
	uint32 m_num_solid_sections;

	uint32 m_num_materials;
	WKGMaterial *m_materials;

	uint32 m_num_textures;
	Texture **m_textures;
	Texture **m_bump_textures;

	uint32 m_num_verts;
	// system memory copy in case we need it
	WKGVertex *m_verts;
	// or vram vertex buffer
	LPDIRECT3DVERTEXBUFFER9 m_vb;
	LPDIRECT3DVERTEXBUFFER9 m_color_vb;
	LPDIRECT3DVERTEXBUFFER9 m_bumpVB;

	WKGFace *m_faces;
	uint32 m_num_faces;
};

} 

}

