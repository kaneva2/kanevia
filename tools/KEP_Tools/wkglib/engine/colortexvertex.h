/******************************************************************************
 colortexvertex.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _COLORTEXVERTEX_H_
#define _COLORTEXVERTEX_H_

#include "simplevertex.h"

#define FVF_COLORTEX_VERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0))

namespace wkg
{

namespace engine
{

class ColorTexVertex : public SimpleVertex
{
	public:
		uint32 diffuse;
		float tu, tv;
};

}

}

#endif