/******************************************************************************
 wkbfile.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "meshtypes.h"
#include "wkgmaterial.h"
#include "filestream.h"
#include <string>
#include "wkbfile.h"

namespace wkg
{

WKBFile::WKBFile() :
	m_file_layout(0),
	m_file_size(0),
	m_new_mats(0)
{
}

WKBFile::~WKBFile()
{
	Reset();
}

bool
WKBFile::Reset()
{
	delete [] m_file_layout;
	m_file_layout = 0;
	m_file_size = 0;
	delete [] m_new_mats;
	m_new_mats = 0;

	return true;
}

void 
WKBFile::SetSizes(uint32 num_verts, 
				  uint32 num_faces, 
				  uint32 num_sections,
				  uint32 num_solid_sections,
				  uint32 num_mats, 
				  uint32 num_bones,
				  bool reset)
{
	if(reset)
		Reset();

	if(WKB_OLD_VERSION == m_file_version)
	{
		m_file_size = 
			sizeof(WKBHeaderLayout) + 
			num_verts * sizeof(WKGWeightedVertex) +
			num_verts * sizeof(WKGVertexColor) + 
			num_faces * sizeof(WKGFace) +
			num_sections * sizeof(WKGMeshSection) +
			num_bones * sizeof(WKGBone) + 
			num_mats * sizeof(WKGMaterial131);
	}
	else
	{
		m_file_size = 
			sizeof(WKBHeaderLayout) + 
			num_verts * sizeof(WKGWeightedVertex) +
			num_verts * sizeof(WKGVertexColor) + 
			num_faces * sizeof(WKGFace) +
			num_sections * sizeof(WKGMeshSection) +
			num_bones * sizeof(WKGBone) + 
			num_mats * sizeof(WKGMaterial);
	}

	m_file_layout = 
		(WKBFileLayout *)(new uint8[m_file_size]);

	memset(m_file_layout, 0, m_file_size);

	m_file_layout->m_header.m_type = 'WKGB';
	m_file_layout->m_header.m_version = WKB_VERSION;

	m_file_layout->m_header.m_num_verts = num_verts;
	m_file_layout->m_header.m_vert_offset = 
		sizeof(WKBHeaderLayout);
	m_file_layout->m_header.m_vert_color_offset = 
		m_file_layout->m_header.m_vert_offset + 
		num_verts * sizeof(WKGWeightedVertex);

	m_file_layout->m_header.m_num_faces = num_faces;
	m_file_layout->m_header.m_face_offset = 
		m_file_layout->m_header.m_vert_color_offset + 
		num_verts * sizeof(WKGVertexColor);

	m_file_layout->m_header.m_num_sections = num_sections;
	m_file_layout->m_header.m_section_offset = 
		m_file_layout->m_header.m_face_offset +
		num_faces * sizeof(WKGFace);
	m_file_layout->m_header.m_num_solid_sections = num_solid_sections;

	m_file_layout->m_header.m_num_materials = num_mats;
	m_file_layout->m_header.m_material_offset = 
		m_file_layout->m_header.m_section_offset +
		num_sections * sizeof(WKGMeshSection);

	if(WKB_OLD_VERSION == m_file_version)
	{
		m_file_layout->m_header.m_num_bones = num_bones;
		m_file_layout->m_header.m_bone_offset =
			m_file_layout->m_header.m_material_offset +
			num_mats * sizeof(WKGMaterial131);
	}
	else
	{
		m_file_layout->m_header.m_num_bones = num_bones;
		m_file_layout->m_header.m_bone_offset =
			m_file_layout->m_header.m_material_offset +
			num_mats * sizeof(WKGMaterial);
	}
}

void 
WKBFile::SetVerts(WKGWeightedVertex *verts)
{
	uint8 *dst = ((uint8 *)m_file_layout) + m_file_layout->m_header.m_vert_offset;
	memcpy(dst, verts, m_file_layout->m_header.m_num_verts * sizeof(WKGWeightedVertex));
}

void 
WKBFile::SetVertColors(WKGVertexColor *colors)
{
	uint8 *dst = ((uint8 *)m_file_layout) + m_file_layout->m_header.m_vert_color_offset;
	memcpy(dst, colors, m_file_layout->m_header.m_num_verts * sizeof(WKGVertexColor));
}

void 
WKBFile::SetFaces(WKGFace *face)
{
	uint8 *dst = ((uint8 *)m_file_layout) + m_file_layout->m_header.m_face_offset;
	memcpy(dst, face, m_file_layout->m_header.m_num_faces * sizeof(WKGFace));
}

void 
WKBFile::SetSections(WKGMeshSection *section)
{
	uint8 *dst = ((uint8 *)m_file_layout) + m_file_layout->m_header.m_section_offset;
	memcpy(dst, section, m_file_layout->m_header.m_num_sections * sizeof(WKGMeshSection));
}

void 
WKBFile::SetMaterials(WKGMaterial *mats)
{
	if(WKB_OLD_VERSION == m_file_version)
	{
		ASSERT(0);
	}

	uint8 *dst = ((uint8 *)m_file_layout) + m_file_layout->m_header.m_material_offset;
	memcpy(dst, mats, m_file_layout->m_header.m_num_materials * sizeof(WKGMaterial));
}

void 
WKBFile::SetBones(WKGBone *bones)
{
	uint8 *dst = ((uint8 *)m_file_layout) + m_file_layout->m_header.m_bone_offset;
	memcpy(dst, bones, m_file_layout->m_header.m_num_bones * sizeof(WKGBone));
}

bool 
WKBFile::Load(const char *filename)
{
	FileStream fs;
	if(fs.Open(filename, FileStream::ReadOnly))
	{
		// read in the header
		WKBHeaderLayout header;
		fs.Read((uint8 *)(&header), sizeof(WKBHeaderLayout));

		if(header.m_type != 'WKGB' || header.m_version != WKB_VERSION)
		{
			if(WKB_OLD_VERSION == header.m_version)
			{
				m_file_version = WKB_OLD_VERSION;
			}
			else
			{
				DEBUGMSG("Invalid WKBFile type or version");
				fs.Close();
				return false;
			}
		}
		else
		{
			m_file_version = WKB_VERSION;
		}

		SetSizes(
			header.m_num_verts, 
			header.m_num_faces, 
			header.m_num_sections, 
			header.m_num_solid_sections,
			header.m_num_materials,
			header.m_num_bones);

		if (!m_file_layout)
		{
			ASSERT(0);
			fs.Close();
			return false;
		}

		memcpy(m_file_layout, &header, sizeof(WKBHeaderLayout));

		uint32 toRead = m_file_size - sizeof(WKBHeaderLayout);
		
		if (toRead != fs.Read(((uint8 *)m_file_layout) + sizeof(WKBHeaderLayout), toRead))
		{
			fs.Close();
			return false;
		}

		fs.Close();

		return true;
	}
	else
	{
		return false;
	}
}

bool 
WKBFile::Save(const char *filename)
{
	ASSERT(m_file_layout);

	FileStream fs;
	if(fs.Open(filename, FileStream::CreateAlways))
	{
		fs.Write((uint8 *)m_file_layout, m_file_size);

		fs.Close();

		return true;
	}
	else
	{
		return false;
	}
}

void
WKBFile::CalcNormals()
{
	uint32 num_verts = GetNumVerts();
	WKGWeightedVertex *verts = GetVerts();

	std::vector<Vector3> normals;
	std::vector<int32> normal_count;
	for(uint32 n = 0; n < num_verts; n++)
	{
		normals.push_back(Vector3(0.0f, 0.0f, 0.0f));
		normal_count.push_back(0);
	}

	uint32 num_faces = GetNumFaces();
	WKGFace *faces = GetFaces();
	for(uint32 f = 0; f < num_faces; f++)
	{
		Vector3 v1, v2, v3;
		v1.x = verts[faces[f].v1].x;
		v1.y = verts[faces[f].v1].y;
		v1.z = verts[faces[f].v1].z;

		v2.x = verts[faces[f].v2].x;
		v2.y = verts[faces[f].v2].y;
		v2.z = verts[faces[f].v2].z;

		v3.x = verts[faces[f].v3].x;
		v3.y = verts[faces[f].v3].y;
		v3.z = verts[faces[f].v3].z;

		Vector3 e0 = v2 - v1;
		Vector3 e1 = v3 - v1;

		Vector3 n = e0.Cross(e1);
		n.Normalize();

		normal_count[faces[f].v1]++;
		normal_count[faces[f].v2]++;
		normal_count[faces[f].v3]++;

		normals[faces[f].v1] += n;
		normals[faces[f].v2] += n;
		normals[faces[f].v3] += n;
	}

	for(uint32 n = 0; n < num_verts; n++)
	{
		normals[n] /= (real)normal_count[n];
		verts[n].nx = normals[n].x;
		verts[n].ny = normals[n].y;
		verts[n].nz = normals[n].z;
	}
}

void 
WKBFile::FlipHands()
{
	uint32 num_verts = GetNumVerts();
	WKGWeightedVertex *verts = GetVerts();
	for(uint32 v = 0; v < num_verts; v++)
	{
		verts[v].z = -verts[v].z;
		verts[v].nz = -verts[v].nz;
	}

	uint32 num_faces = GetNumFaces();
	WKGFace *faces = GetFaces();
	for(uint32 f = 0; f < num_faces; f++)
	{
		uint32 v1 = faces[f].v1;
		faces[f].v1 = faces[f].v3;
		faces[f].v3 = v1;
	}

	uint32 num_bones = GetNumBones();
	WKGBone *bones = GetBones();
	for(uint32 b = 0; b < num_bones; b++)
	{
		bones[b].m_position.z = -bones[b].m_position.z;
		bones[b].m_orientation.x = -bones[b].m_orientation.x;
		bones[b].m_orientation.y = -bones[b].m_orientation.y;
	}
}

WKGMaterial *
WKBFile::GetMaterials()
{
	if(!GetNumMaterials()) return 0;

	if(WKB_OLD_VERSION == m_file_version)
	{
		if(!m_new_mats)
		{
			WKGMaterial131 *old_mats = 
				(WKGMaterial131 *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_material_offset);

			m_new_mats = new WKGMaterial[GetNumMaterials()];
			for(int32 i = 0; i < (int32)GetNumMaterials(); i++)
			{
				memcpy(&m_new_mats[i], &old_mats[i], sizeof(WKGMaterial131));
				strcpy(m_new_mats[i].bump_texture, "");
			}
		}
		return m_new_mats;
	}
	else
	{
		return (WKGMaterial *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_material_offset);
	}
}

void 
WKBFile::FindMinSphere(Vector3 &center, real &radius)
{
	Vector3 min, max;
	FindAABB(min, max);
	center = Vector3(
		(max.x + min.x) / 2.0f, 
		(max.y + min.y) / 2.0f, 
		(max.z + min.z) / 2.0f);
	radius = (Vector3(min.x, min.y, min.z) - center).Length();
}

void 
WKBFile::FindAABB(Vector3 &min, Vector3 &max)
{
	real minx, maxx;
	real miny, maxy;
	real minz, maxz;

	uint32 num_verts = GetNumVerts();
	WKGWeightedVertex *verts = GetVerts();

	minx = maxx = verts[0].x;
	miny = maxy = verts[0].y;
	minz = maxz = verts[0].z;

	for(uint32 v = 1; v < num_verts; v++)
	{
		if(verts[v].x < minx) minx = verts[v].x;
		if(verts[v].y < miny) miny = verts[v].y;
		if(verts[v].z < minz) minz = verts[v].z;

		if(verts[v].x > maxx) maxx = verts[v].x;
		if(verts[v].y > maxy) maxy = verts[v].y;
		if(verts[v].z > maxz) maxz = verts[v].z;
	}

	min.x = minx;
	min.y = miny;
	min.z = minz;

	max.x = maxx;
	max.y = maxy;
	max.z = maxz;
}

void 
WKBFile::FindAABBForSection(int32 section_num, Vector3 &min, Vector3 &max)
{
	WKGMeshSection *curr_sections = GetSections();
	WKGMeshSection section = curr_sections[section_num];

	uint32 num_verts = GetNumVerts();
	WKGWeightedVertex *verts = GetVerts();

	int32 num_faces= section.m_num_faces;
	WKGFace *faces = GetFaces();

	bool first_time = true;

	// for each face
	for(uint32 f = section.m_first_face; f < section.m_first_face+num_faces; f++)
	{
		// for each vert of the face
		WKGWeightedVertex v1 = verts[faces[f].v1];
		if(first_time || v1.x < min.x) min.x = v1.x;
		if(first_time || v1.y < min.y) min.y = v1.y;
		if(first_time || v1.z < min.z) min.z = v1.z;

		if(first_time || v1.x > max.x) max.x = v1.x;
		if(first_time || v1.y > max.y) max.y = v1.y;
		if(first_time || v1.z > max.z) max.z = v1.z;

		first_time = false;

		WKGWeightedVertex v2 = verts[faces[f].v2];
		if(v2.x < min.x) min.x = v2.x;
		if(v2.y < min.y) min.y = v2.y;
		if(v2.z < min.z) min.z = v2.z;

		if(v2.x > max.x) max.x = v2.x;
		if(v2.y > max.y) max.y = v2.y;
		if(v2.z > max.z) max.z = v2.z;

		WKGWeightedVertex v3 = verts[faces[f].v3];
		if(v3.x < min.x) min.x = v3.x;
		if(v3.y < min.y) min.y = v3.y;
		if(v3.z < min.z) min.z = v3.z;

		if(v3.x > max.x) max.x = v3.x;
		if(v3.y > max.y) max.y = v3.y;
		if(v3.z > max.z) max.z = v3.z;
	}
}

void
WKBFile::PartitionSection(real max_size, int32 i, std::vector<WKGMeshSection> &resultant)
{
	// first, an early out
	Vector3 max, min;
	FindAABBForSection(i, min, max);
//	if((max.x - min.x < max_size) && (max.y - min.y < max_size) && (max.z - min.z < max_size))
//		return;

	// for each axis that's too large, partition it down to be the correct size
	// create a working list of faces
	WKGMeshSection *sections = GetSections();
	WKGMeshSection section = sections[i];

	int32 num_faces = section.m_num_faces;
	WKGFace *faces = GetFaces();

	uint32 num_verts = GetNumVerts();
	WKGWeightedVertex *verts = GetVerts();

	// we're going to rearrange the faces, but not create any new ones or delete old ones
	std::vector<WKGFace> old_faces;
	std::vector<WKGFace> new_faces;

	for(uint32 f = section.m_first_face; f < section.m_first_face+num_faces; f++)
	{
		old_faces.push_back(faces[f]);
	}

	real x_size = max.x - min.x;
	real y_size = max.y - min.y;
	real z_size = max.z - min.z;

	real curr_x = min.x;
	real curr_y = min.y;
	real curr_z = min.z;

	while(curr_z < max.z)
	{
		// find all the faces from the old list that fall within this range
		//  and add them to the new list

		WKGMeshSection new_section;
		memcpy(&new_section, &section, sizeof(WKGMeshSection));
		new_section.m_first_face = section.m_first_face + new_faces.size();

		std::vector<WKGFace>::iterator itor = old_faces.begin();
		while(itor != old_faces.end())
		{
			// get the three vert positions
			WKGWeightedVertex v1 = verts[(*itor).v1];
			WKGWeightedVertex v2 = verts[(*itor).v2];
			WKGWeightedVertex v3 = verts[(*itor).v3];

			if( (v1.x > curr_x || EQUAL(v1.x, curr_x)) && (v1.x < curr_x + max_size || EQUAL(v1.x, curr_x + max_size)) &&
				(v1.y > curr_y || EQUAL(v1.y, curr_y)) && (v1.y < curr_y + max_size || EQUAL(v1.y, curr_y + max_size)) &&
				(v1.z > curr_z || EQUAL(v1.z, curr_z)) && (v1.z < curr_z + max_size || EQUAL(v1.z, curr_z + max_size)))
			{
				// this face falls within it
				new_faces.push_back((*itor));
				itor = old_faces.erase(itor);
				continue;
			}

			if( (v2.x > curr_x || EQUAL(v2.x, curr_x)) && (v2.x < curr_x + max_size || EQUAL(v2.x, curr_x + max_size)) &&
				(v2.y > curr_y || EQUAL(v2.x, curr_y)) && (v2.y < curr_y + max_size || EQUAL(v2.y, curr_y + max_size)) &&
				(v2.z > curr_z || EQUAL(v2.x, curr_z)) && (v2.z < curr_z + max_size || EQUAL(v2.z, curr_z + max_size)))
			{
				// this face falls within it
				new_faces.push_back((*itor));
				itor = old_faces.erase(itor);
				continue;
			}

			if( (v3.x > curr_x || EQUAL(v3.x, curr_x)) && (v3.x < curr_x + max_size || EQUAL(v3.x, curr_x + max_size)) &&
				(v3.y > curr_y || EQUAL(v3.y, curr_y)) && (v3.y < curr_y + max_size || EQUAL(v3.y, curr_y + max_size)) &&
				(v3.z > curr_z || EQUAL(v3.z, curr_z))  && (v3.z < curr_z + max_size || EQUAL(v3.z, curr_z + max_size)))
			{
				// this face falls within it
				new_faces.push_back((*itor));
				itor = old_faces.erase(itor);
				continue;
			}

			// didn't delete any, so safe to increment our iterator
			itor++;
		}

		new_section.m_num_faces = (int32)new_faces.size() - (int32)(new_section.m_first_face - section.m_first_face);
		if(new_section.m_num_faces)
		{
			// store this resultant section
			resultant.push_back(new_section);
		}

		curr_x += max_size;
		if(curr_x > max.x)
		{
			curr_x = min.x;
			curr_y += max_size;
		}

		if(curr_y > max.y)
		{
			curr_y = min.y;
			curr_z += max_size;
		}
	}

	ASSERT(new_faces.size() == num_faces);

	// now fill in the face data
	memcpy(faces + section.m_first_face, &(*new_faces.begin()), num_faces * sizeof(WKGFace));
}

void 
WKBFile::PartitionSections(real max_size)
{
	int32 num_sections = GetNumSections();
	WKGMeshSection *curr_setcions = GetSections();

	// create a vector of sections to partition
	std::vector<WKGMeshSection> final_sections;
	for(int32 i = 0; i < num_sections; i++)
	{
		std::vector<WKGMeshSection> resultant_sections;
		resultant_sections.clear();
		PartitionSection(max_size, i, resultant_sections);

		final_sections.insert(final_sections.end(), resultant_sections.begin(), resultant_sections.end());
	}

	// pain in the butt
	//  gotta save off all the old data, then reset the size, then rest all the old data

	WKBFileLayout *old_file_layout = m_file_layout;

	uint32 nv = GetNumVerts();
	uint32 nf = GetNumFaces();
	uint32 ns = GetNumSections();
	uint32 nss = GetNumSolidSections();
	uint32 nts = GetNumTransparentSections();
	uint32 nb = GetNumBones();
	uint32 nm = GetNumMaterials();

	WKGWeightedVertex *verts = GetVerts();
	WKGVertexColor *vcolors = GetVertColors();
	WKGFace *faces = GetFaces();
	WKGMeshSection *sections = GetSections();
	WKGBone *bones = GetBones();
	WKGMaterial *materials = GetMaterials();

	uint32 nfs = (uint32)final_sections.size();

	SetSizes(nv, nf, nfs, 0, nm, nb, false);

	SetVerts(verts);
	SetVertColors(vcolors);
	SetFaces(faces);
	SetSections(&(*final_sections.begin()));
//	SetSections(sections);
	SetBones(bones);
	SetMaterials(materials);

	delete [] old_file_layout;
}

}




