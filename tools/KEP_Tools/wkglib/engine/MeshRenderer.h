/******************************************************************************
 MeshRenderer.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "RefCounter.h"

namespace wkg 
{
namespace engine 
{

class Mesh;

class MeshRenderer : public RefCounter<MeshRenderer>
{
	public:
		MeshRenderer();
		virtual ~MeshRenderer();

		virtual void Render(Mesh*);
};

inline MeshRenderer::MeshRenderer()
{
}

inline MeshRenderer::~MeshRenderer()
{
}

}
}


