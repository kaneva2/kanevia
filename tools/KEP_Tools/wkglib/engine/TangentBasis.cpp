/******************************************************************************
 TangentBasis.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "TangentBasis.h"
#include "Device.h"

extern int32 g_vert_data;

namespace wkg 
{

namespace engine 
{

TangentBasis::TangentBasis()
	: srcvb(0), tbvb(0), numVerts(0), srcStride(0), numIncident(0)
{
}

TangentBasis::~TangentBasis()
{
	Reset();
}

void TangentBasis::Reset()
{
	if (tbvb)
	{
		tbvb->Release();
		tbvb = 0;
	}

	delete[] numIncident;
	numIncident = 0;

	srcvb = 0;
	numVerts = 0;
	srcStride = 0;
}

bool TangentBasis::BeginBuild(LPDIRECT3DVERTEXBUFFER9 vb, uint32 NumVerts, uint32 SrcStride, D3DPOOL Pool)
{
	bool result = false;
	Vert* dest;

	if (!vb || NumVerts < 3 || SrcStride < sizeof(SrcVert))
		return result;

	Reset();

	if (D3D_OK != Device::Instance()->CreateVertexBuffer(
		NumVerts * sizeof(Vert), D3DUSAGE_WRITEONLY, 0, 
		Pool, &tbvb, 0))
	{
		tbvb = 0;
		return result;
	}

	g_vert_data += NumVerts * sizeof(Vert);
//	DEBUGMSG("vert data: %d", g_vert_data);


	numIncident = new uint32[NumVerts];
	if (!numIncident)
	{
		tbvb->Release();
		tbvb = 0;
		return result;
	}

	if (D3D_OK != tbvb->Lock(0, 0, (void**)&dest, 0))
	{
		tbvb->Release();
		tbvb = 0;
		delete[] numIncident;
		numIncident = 0;
		return result;
	}

	srcvb = vb;
	numVerts = NumVerts;
	srcStride = SrcStride;

	memset(dest,		0, numVerts * sizeof(Vert));
	memset(numIncident,	0, numVerts * sizeof(uint32));

	tbvb->Unlock();

	return true;
}

LPDIRECT3DVERTEXBUFFER9 TangentBasis::EndBuild()
{
	LPDIRECT3DVERTEXBUFFER9 result;

	result = tbvb;

	tbvb = 0;

#if 1
	if (result && numVerts && numIncident)
	{
		Vert* dest;
		real lenSqr;
		uint32 i;

		result->Lock(0, 0, (void**)&dest, 0);

		for (i = 0; i < numVerts; i++)
		{
			if (numIncident[i] > 1)		// if == 1, then already avg and norm
			{
				dest[i].T /= real(numIncident[i]);
				dest[i].B /= real(numIncident[i]);
				dest[i].N /= real(numIncident[i]);

				lenSqr = dest[i].T.LengthSqr();
				if (lenSqr > 0.0f && lenSqr != 1.0f)
					dest[i].T /= real(sqrt(lenSqr));
				lenSqr = dest[i].B.LengthSqr();
				if (lenSqr > 0.0f && lenSqr != 1.0f)
					dest[i].B /= real(sqrt(lenSqr));
				lenSqr = dest[i].N.LengthSqr();
				if (lenSqr > 0.0f && lenSqr != 1.0f)
					dest[i].N /= real(sqrt(lenSqr));
			}
		}

		result->Unlock();
	}
#endif

	Reset();

	return result;
}

bool TangentBasis::ComputeMeshSection(LPDIRECT3DINDEXBUFFER9 ib, uint32 nTris)
{
#if 0
	SrcVert *src_verts;

#ifdef USE_32BIT_INDICES
	uint32 *src_idxs;
#else
	uint16 *src_idxs;
#endif
		
	LPD3DXMESH dxmesh;

	HRESULT hr = D3DXCreateMeshFVF(
		nTris,
		numVerts,
		D3DXMESH_32BIT /*| D3DXMESH_SYSTEMMEM | D3DXMESH_SOFTWAREPROCESSING*/,
		D3DFVF_XYZ | D3DFVF_NORMAL | 
		D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0) | 
		D3DFVF_TEX2 | D3DFVF_TEXCOORDSIZE2(0),
		Device::Instance(),
		&dxmesh);

	struct xmesh_vert
	{
	public:
		Vector3 pos;
		Vector3 normal;
		real t0u, t0v;
		real t1u, t1v;
	};

	xmesh_vert *dest_verts;

	// fill in our verts
	srcvb->Lock(0, 0, (void **)&src_verts, 0);

	hr = dxmesh->GetVertexBuffer(dest_vb);
	dest_vb->Lock(0, 0, (void **)&dest_verts, 0);

	for(uint32 v = 0; v < numVerts; v++)
	{
		dest_verts[v].pos = src_verts.pos;
		dest_verts[v].norm = src_verts.norm;
		dest_verts[v].t0u = src_verts.tu;
		dest_verts[v].t0v = src_verts.tv;
		dest_verts[v].t1u = 0.0f;
		dest_verts[v].t1v = 0.0f;
	}

	dest_vb->Unlock();
	dest_bv->Release();
	srcvb->Unlock();


	// and now fill in our idxs

	ib->Lock(0, 0, (uint8**)&src_idxs, 0);

	LPDIRECT3DINDEXBUFFER8 dest_idxs;
	hr = dxmesh->GetIndexBuffer(&dest_idxs);
	dest_idxs->Lock();
	dest_idxs->Unlock();
	dest-idxs->Release();



	ib->Unlock();

	DWORD adj;

	hr = D3DXComputeTangent(dxmesh, 0, dxmesh, 1, 1, 0, &adj);

	// and copy over our basis vectors
	Vert *dest;
	tbvb->Lock(0, 0, (uint8**)&dest, 0);
	tbvb->Unlock();

	return true;

#else

#if 0
	SrcVert *src_verts;

#ifdef USE_32BIT_INDICES
	uint32 *src_idxs;
#else
	uint16 *src_idxs;
#endif

	Vert *dest;

	srcvb->Lock(0, 0, (void **)&src_verts, 0);
	ib->Lock(0, 0, (void**)&src_idxs, 0);
	tbvb->Lock(0, 0, (void**)&dest, 0);

	for(uint32 t = 0; t < nTris; t++)
	{
		// get our three verts
		Vector3 v1 = src_verts[src_idxs[t * 3 + 0]].pos;
		Vector3 v2 = src_verts[src_idxs[t * 3 + 1]].pos;
		Vector3 v3 = src_verts[src_idxs[t * 3 + 2]].pos;

		Vector3 v1n = src_verts[src_idxs[t * 3 + 0]].norm;
		Vector3 v2n = src_verts[src_idxs[t * 3 + 1]].norm;
		Vector3 v3n = src_verts[src_idxs[t * 3 + 2]].norm;

		real v1tu = src_verts[src_idxs[t * 3 + 0]].tu;
		real v1tv = src_verts[src_idxs[t * 3 + 0]].tv;

		real v2tu = src_verts[src_idxs[t * 3 + 1]].tu;
		real v2tv = src_verts[src_idxs[t * 3 + 1]].tv;

		real v3tu = src_verts[src_idxs[t * 3 + 2]].tu;
		real v3tv = src_verts[src_idxs[t * 3 + 2]].tv;

		Vector3 vec1 = v3 - v2;
		Vector3 vec2 = v1 - v2;

		real delta_u1 = v3tu - v2tu;
		real delta_u2 = v1tu - v2tu;

		Vector3 direction_v = vec1 * delta_u2  - vec2 * delta_u1;
		direction_v.Normalize();

		Vector3 direction_u1 = direction_v.Cross(v1n);
		direction_u1.Normalize();
		Vector3 direction_u2 = direction_v.Cross(v2n);
		direction_u2.Normalize();
		Vector3 direction_u3 = direction_v.Cross(v3n);
		direction_u3.Normalize();

		Vector3 direction_w1 = direction_u1.Cross(direction_v);
		direction_w1.Normalize();
		Vector3 direction_w2 = direction_u2.Cross(direction_v);
		direction_w2.Normalize();
		Vector3 direction_w3 = direction_u3.Cross(direction_v);
		direction_w3.Normalize();

		dest[src_idxs[t * 3 + 0]].T = direction_u1;
		dest[src_idxs[t * 3 + 1]].T = direction_u2;
		dest[src_idxs[t * 3 + 2]].T = direction_u3;

		dest[src_idxs[t * 3 + 0]].B = direction_v;
		dest[src_idxs[t * 3 + 1]].B = direction_v;
		dest[src_idxs[t * 3 + 2]].B = direction_v;

		dest[src_idxs[t * 3 + 0]].N = direction_w1;
		dest[src_idxs[t * 3 + 1]].N = direction_w2;
		dest[src_idxs[t * 3 + 2]].N = direction_w3;

		/*
		Vec1 = Vertex3 - Vertex2
		Vec2 = Vertex1 - Vertex2
		DeltaU1 = Vertex3.u - Vertex2.u
		DeltaU2 = Vertex1.u - Vertex2.u
		DirectionV = |DeltaU2*Vec1-DeltaU1*Vec2|
		DirectionU = |DirectionV x Vertex.N|
		DirectionW = |DirectionU x DirectionV|
		*/
	}

	tbvb->Unlock();
	ib->Unlock();
	srcvb->Unlock();

	return true;

#else

	bool result;
	uint8* src;
	SrcVert *p, *q, *r;
	Vert* dest;
	Vector3 v01, v02, vup;
	Vector3 T, B, N;
	uint32 i, tri;
	real lenSqr;
	D3DINDEXBUFFER_DESC ibDesc;
	uint16* idx16;
	uint32* idx32;
#	define IDX(which)	(ibDesc.Format == D3DFMT_INDEX16 ? idx16[which] : idx32[which])
#	define IDXPTR		(ibDesc.Format == D3DFMT_INDEX16 ? (void*)&idx16 : (void*)&idx32)

	result = false;

	if (!tbvb || !ib || !nTris)
		return result;

	ASSERT(srcvb && numVerts && numIncident && srcStride >= sizeof(SrcVert));

	if (D3D_OK != ib->GetDesc(&ibDesc))
		return result;

	srcvb->Lock(0, 0, (void**)&src, 0);
	ib->Lock(0, 0, (void**)IDXPTR, 0);
	tbvb->Lock(0, 0, (void**)&dest, 0);

	for (tri = 0; tri < nTris; tri++)
	{
		p = (SrcVert*)(src + srcStride * IDX(tri * 3 + 0));
		q = (SrcVert*)(src + srcStride * IDX(tri * 3 + 1));
		r = (SrcVert*)(src + srcStride * IDX(tri * 3 + 2));

		// make sure tri is not degenerate (all points non-collinear)
		//
		if (((EQUAL(p->pos.x, q->pos.x) && EQUAL(p->pos.y, q->pos.y) && EQUAL(p->pos.z, q->pos.z))) ||
			((EQUAL(p->pos.x, r->pos.x) && EQUAL(p->pos.y, r->pos.y) && EQUAL(p->pos.z, r->pos.z))) ||
			((EQUAL(q->pos.x, r->pos.x) && EQUAL(q->pos.y, r->pos.y) && EQUAL(q->pos.z, r->pos.z))))
			continue;

		T.Zero();
		B.Zero();

		// find dx/du & dx/dv
		//
		v01.Set(q->pos.x - p->pos.x, q->tu - p->tu, q->tv - p->tv);
		v02.Set(r->pos.x - p->pos.x, r->tu - p->tu, r->tv - p->tv);
		vup = v01.Cross(v02);

		if (!EQUAL(vup.x, 0.0f))
		{
			T.x = -vup.y / vup.x;
			B.x = -vup.z / vup.x;
		}

		// find dy/du & dy/dv
		//
		v01.x = q->pos.y - p->pos.y;
		v02.x = r->pos.y - p->pos.y;
		vup = v01.Cross(v02);

		if (!EQUAL(vup.x, 0.0f))
		{
			T.y = -vup.y / vup.x;
			B.y = -vup.z / vup.x;
		}

		// find dz/du & dz/dv
		//
		v01.x = q->pos.z - p->pos.z;
		v02.x = r->pos.z - p->pos.z;
		vup = v01.Cross(v02);

		if (!EQUAL(vup.x, 0.0f))
		{
			T.z = -vup.y / vup.x;
			B.z = -vup.z / vup.x;
		}

		lenSqr = T.LengthSqr();
		if (lenSqr > 0.0f)
			T /= real(sqrt(lenSqr));
		lenSqr = B.LengthSqr();
		if (lenSqr > 0.0f)
			B /= real(sqrt(lenSqr));
		N = T.Cross(B);

		// Gram-Schmidt orthogonalization for B
		//
		B = N.Cross(T);

		// make sure N points in the same direction as the triangle's normal
		//
		v01.Set(q->pos.x - p->pos.x, q->pos.y - p->pos.y, q->pos.z - p->pos.z);
		v02.Set(r->pos.x - p->pos.x, r->pos.y - p->pos.y, r->pos.z - p->pos.z);
		vup = v01.Cross(v02);

		if (N.Dot(vup) < 0.0)
			N.Set(-N.x, -N.y, -N.z);

		// note: this may need to change to use smoothing groups...for now it averages all incident
		//
		for (i = 0; i < 3; i++)
		{
			++numIncident[IDX(tri * 3 + i)];
			dest[IDX(tri * 3 + i)].T += T;
			dest[IDX(tri * 3 + i)].B += B;
			dest[IDX(tri * 3 + i)].N += N;
		}
	}

	tbvb->Unlock();
	ib->Unlock();
	srcvb->Unlock();

	return result;
#endif
#endif

}

}

}
