/******************************************************************************
 animation.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

// aids in the creation of a controller graph to control one
//  matrix44 based on position and orientation keyframes

#include "keyframenotify.h"

#include <string>
#include <vector>

namespace wkg
{

class Quaternion;

namespace engine
{

class KeyframeController;
class PosOrientToMatrix44Controller;
class PositionKeyframeController;
class OrientationKeyframeController;
class AnimNotify;

class Animation : public KeyframeNotify
{
	public:
		Animation();
		virtual ~Animation();

		void Create(int32 frame_start, int32 frame_end);

		enum { PLAY_NONE, PLAY_LOOPING, PLAY_ONCE };
		virtual void Start(int32 mode, bool reset = true);
		virtual void Stop();

		void AddPositionKeyframe(int32 frame, Vector3 &position);
		void AddOrientationKeyframe(int32 frame, Quaternion &orientation);

		void SetName(const char *name) { m_name = name; }
		const char *GetName() { return m_name.c_str(); }

		bool IsPlaying();
		void SetLooping(bool bEnable = true);

		PositionKeyframeController *GetPositionKeyframeController() 
		{
			return m_pkfc; 
		}

		OrientationKeyframeController *GetOrientationkeyframeController()
		{
			return m_okfc;
		}

		virtual void AnimDone(KeyframeController *which);
		void AddAnimNotify(AnimNotify *an);
		void RemoveAnimNotify(AnimNotify *an);

		int32 GetCurrFrame();

		void BeginTransition(real transition_time);

	protected:
		// the name of the bone this animation is meant to affect
		std::string m_name;

		// keyframed position controller
		KeyframeController *m_kfcp;
		// keyframed orientation controller
		KeyframeController *m_kfco;

		PositionKeyframeController *m_pkfc;
		OrientationKeyframeController *m_okfc;

		int32 m_mode;

		std::vector<AnimNotify *> m_an;
};

}

}

