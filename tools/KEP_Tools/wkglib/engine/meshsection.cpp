/******************************************************************************
 meshsection.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "meshtypes.h"
#include "device.h"
#include "meshsection.h"

namespace wkg
{

namespace engine
{

MeshSection::MeshSection() : 
	m_num_idxs(0),
	m_ib(0), 
	m_material(0),
	m_min_vert(0),
	m_num_verts(0),
	m_min(0.0f, 0.0f, 0.0f),
	m_max(0.0f, 0.0f, 0.0f)
{
}

MeshSection::~MeshSection()
{
	if(m_ib)
		m_ib->Release();
	m_ib = 0;
	m_num_idxs = 0;
}

void 
MeshSection::SetIndices(uint32 num_idxs, WKGIndex *i)
{
	ASSERT((num_idxs % 3) == 0);

	HRESULT hr = Device::Instance()->CreateIndexBuffer(
		sizeof(WKGIndex) * num_idxs,
		D3DUSAGE_WRITEONLY, 
#ifdef USE_32BIT_INDICES
		D3DFMT_INDEX32, 
#else
		D3DFMT_INDEX16, 
#endif
		D3DPOOL_MANAGED,
		&m_ib, 
		0);

	if(FAILED(hr))
	{
		ASSERT(0);
	}

	void *dst;
	hr = m_ib->Lock(0, 0, &dst, 0);
	ASSERT(SUCCEEDED(hr));

	memcpy(dst, i, sizeof(WKGIndex) * num_idxs);

	hr = m_ib->Unlock();
	ASSERT(SUCCEEDED(hr));

	m_num_idxs = num_idxs;

	uint32 min_vert = 0xffffffff;
	uint32 max_vert = 0x0;
	for(uint32 idx = 0; idx < num_idxs; idx++)
	{
		if(i[idx] > max_vert)
			max_vert = i[idx];
		if(i[idx] < min_vert)
			min_vert = i[idx];
	}

	ASSERT(min_vert != 0xffffffff);
	m_min_vert = min_vert;
	m_num_verts = max_vert - min_vert + 1;
}

void 
MeshSection::Render()
{
	Device::Instance()->SetIndices(m_ib);

	Device::Instance()->DrawIndexedPrimitive(
		D3DPT_TRIANGLELIST,
		0,
		m_min_vert, 
		m_num_verts, 
		0, 
		m_num_idxs / 3);
}

}

}

