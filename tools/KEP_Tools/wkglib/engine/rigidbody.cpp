/******************************************************************************
 rigidbody.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "physics.h"
#include "bvsphere.h"
#include "bvellipsoid.h"
#include "bvbox.h"
#include "units.h"
#include "worldobjectlist.h"
#include "rigidbody.h"

namespace wkg
{

namespace engine
{

RigidBody::RigidBody() :
	WorldObject(),
	m_cor(0.0f),
	m_mu(65535.0f), 
	m_body(0),
	m_impulses(0.0f, 0.0f, 0.0f)
{
	SetPosition(m_position);
	SetOrientation(m_orientation);
	SetVelocity(m_velocity);
	SetMass(m_mass);
	SetAngularVelocity(m_omega);
}

RigidBody::~RigidBody()
{
	if(m_body)
	{
#if 0
		engine::RigidBody *rb = 0;

		int32 num_wo = engine::WorldObjectList::Instance()->GetNum();
		for(int32 b = 0; b < num_wo; b++)
		{
			engine::WorldObject *wo = engine::WorldObjectList::Instance()->Get(b);

			if(wo == this) continue;
			if(!wo->IsInSim()) continue;

			engine::RigidBody *b = dynamic_cast<engine::RigidBody *>(wo);
			if(b)
			{
				if(dAreConnected(m_body, b->m_body))
					dBodyEnable(b->m_body);
			}
		}
#endif
		dBodyDestroy(m_body);
	}


	if(m_geom.size())
	{
		for(int32 i = 0; i < m_geom.size(); i++)
			dGeomDestroy(m_geom[i]);
	}
	m_geom.clear();
}

void 
RigidBody::AddToSim()
{
	m_body = dBodyCreate(Physics::Instance()->m_world);
	dBodySetData(m_body, this);
	AdjustDMass();

	dBodySetAutoDisableDefaults(m_body);


	Physics::Instance()->AddRigidBody(this);
}

bool
RigidBody::IsInSim()
{
	return (m_body != 0);
}

void 
RigidBody::RemoveFromSim()
{
	dBodySetData(m_body, 0);
	dBodyDestroy(m_body);
	m_body = 0;

	Physics::Instance()->RemoveRigidBody(this);
}

void 
RigidBody::SetImmovable(bool im)
{
	if(!im && m_body)
	{
		dBodyDestroy(m_body);
		m_body = 0;
	}
	else if(im && !m_body)
	{
		m_body = dBodyCreate(Physics::Instance()->m_world);
		dBodySetData(m_body, this);

		SetPosition(m_position);
		SetOrientation(m_orientation);
		SetVelocity(m_velocity);
		SetMass(m_mass);
		SetAngularVelocity(m_omega);
	}
}

void 
RigidBody::SetPosition(const Vector3 &position)
{
	WorldObject::SetPosition(position);

	Vector3 pos = position + m_bv_offset;

	if(m_body)
		dBodySetPosition(m_body, pos.x, pos.y, pos.z);
}

void 
RigidBody::SetOrientation(const Quaternion &orientation)
{
	WorldObject::SetOrientation(orientation);

	if(m_body)
	{
		dQuaternion q;
		q[1] = orientation.x;
		q[2] = orientation.y;
		q[3] = orientation.z;
		q[0] = orientation.w;

		dBodySetQuaternion(m_body, q);
	}
}

void 
RigidBody::SetVelocity(const Vector3 &velocity)
{
	WorldObject::SetVelocity(velocity);

	if(m_body)
		dBodySetLinearVel(m_body, velocity.x, velocity.y, velocity.z);
}

void 
RigidBody::SetAngularVelocity(Vector3 &v)
{
	WorldObject::SetAngularVelocity(v);
	if(m_body)
		dBodySetAngularVel(m_body, v.x, v.y, v.z);
}

void 
RigidBody::SetMass(real mass)
{
	WorldObject::SetMass(mass);
	AdjustDMass();
}

void 
RigidBody::SetBVOffset(Vector3 &bvoffset)
{
	WorldObject::SetBVOffset(bvoffset);
	AdjustDMass();
}

void 
RigidBody::SetBoundingVolume(BoundingVolume *volume)
{
	WorldObject::SetBoundingVolume(volume);
	AdjustDMass();
}

void 
RigidBody::AdjustDMass()
{
	if(!m_body) return;
	dMass dm;

	if(!m_volume) return;

	if(m_geom.size())
	{
		for(int32 i = 0; i < m_geom.size(); i++)
			dGeomDestroy(m_geom[i]);
	}
	m_geom.clear();

	BVSphere *bvs = dynamic_cast<BVSphere *>(m_volume);
	if(bvs)
	{
		real radius = 1.0f;
		radius = bvs->GetRadius();

		// default to sphere, anyways
		dMassSetSphere(&dm, 1.0f, radius);
		
		dMassAdjust(&dm, m_mass);

		dBodySetMass(m_body, &dm);

		m_geom.push_back(dCreateSphere(Physics::Instance()->m_space, radius));

		for(int32 i = 0; i < m_geom.size(); i++)
		{
			dGeomSetCategoryBits(m_geom[i], 0x02);
			dGeomSetBody(m_geom[i], m_body);
			dGeomSetCollideBits(m_geom[i], 0xff);
		}
	}

	BVBox *bvb = dynamic_cast<BVBox *>(m_volume);
	if(bvb)
	{
		Vector3 size = bvb->GetSize();

		dMassSetBox(&dm, 1.0f, size.x, size.y, size.z);
		
		dMassAdjust(&dm, m_mass);

		dBodySetMass(m_body, &dm);

		m_geom.push_back(dCreateBox(Physics::Instance()->m_space, size.x, size.y, size.z));

		for(int32 i = 0; i < m_geom.size(); i++)
		{
			dGeomSetCategoryBits(m_geom[i], 0x02);
			dGeomSetBody(m_geom[i], m_body);
			dGeomSetCollideBits(m_geom[i], 0xff);
		}
	}

	BVEllipsoid *bve = dynamic_cast<BVEllipsoid *>(m_volume);
	if(bve)
	{
		Vector3 size = bve->GetRadius();

		ASSERT(EQUAL(size.x, size.z));
		ASSERT(size.x < size.y);

		dMassSetSphere(&dm, 1.0f, size.x);
		
		dMassAdjust(&dm, m_mass);

		dBodySetMass(m_body, &dm);

		int32 num = size.y / size.x;
		num++;

		real base_offset = GetBVOffset().y;
		base_offset = -base_offset + size.x;

		real delta = ((size.y * 2.0f - size.x) - size.x) / (real)(num - 1);


		for(int32 i = 0; i < num; i++)
		{
			m_geom.push_back(dCreateSphere(0, size.x));
			dGeomSetPosition(m_geom[i], 0.0f, base_offset, 0.0f); 

			base_offset += delta;
		}

		for(i = 0; i < num; i++)
		{
			dGeomID geomid = dCreateGeomTransform(Physics::Instance()->m_space); 
			dGeomTransformSetGeom(geomid, m_geom[i]);
			m_geom.push_back(geomid);

			dGeomSetCategoryBits(geomid, 0x04);
			dGeomSetCollideBits(geomid, 0xfe);
			dGeomSetBody(geomid, m_body);
		}
	}

}

dJointGroupID hackgid = 0;

void 
RigidBody::UpdateStateFromSim()
{
	if(!m_body) return;

	// update our position, orientation, and velocities
	const dReal *dp = dBodyGetPosition(m_body);
	const dReal *dq = dBodyGetQuaternion(m_body);
	const dReal *dl = dBodyGetLinearVel(m_body);
	const dReal *da = dBodyGetAngularVel(m_body);

	Vector3 odep(dp[0], dp[1], dp[2]);
	Vector3 p = odep;

	p -= m_bv_offset;

	SetPosition(p);

	Quaternion q(dq[1], dq[2], dq[3], dq[0]);
	SetOrientation(q);

	Vector3 odel(dl[0], dl[1], dl[2]);
	Vector3 l = odel;
	SetVelocity(l);

	Vector3 a(da[0], da[1], da[2]);
	SetAngularVelocity(a);

	if(hackgid)
	{
		dJointGroupEmpty(hackgid);
	}
}

void
RigidBody::AddForce(Vector3 &f)
{
	if(!m_body) return;
	dBodyAddForce(m_body, f.x, f.y, f.z);
}

void
RigidBody::AddForceAtPos(const Vector3 &p, Vector3 &f)
{
	if(!m_body) return;
	dBodyAddForceAtPos(m_body, 
		f.x, f.y, f.z,
		p.x, p.y, p.z);
}

void 
RigidBody::AddBallJoint(Vector3 bpos, Vector3 wpos)
{
	if(!hackgid)
	{
		hackgid = dJointGroupCreate(0);
	}

	dJointID joint = dJointCreateBall(Physics::Instance()->m_world, hackgid); 
	dJointAttach(joint, m_body, 0); 

	ASSERT(0);
//	dJointSetBallAnchor1(joint, bpos.x, bpos.y, bpos.z); 
//	dJointSetBallAnchor2(joint, wpos.x, wpos.y, wpos.z); 

}

}

}

