/******************************************************************************
 wkgmaterial.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

class WKGMaterial131
{
public:
	WKGColor diffuse;
	WKGColor ambient;
	WKGColor specular;
	float power;

	char texture[255];
};

class WKGMaterial
{
public:
	WKGColor diffuse;
	WKGColor ambient;
	WKGColor specular;
	float power;

	char texture[255];
	char bump_texture[255];
};

}