/******************************************************************************
 meshfactory.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "meshtypes.h"
#include "mesh.h"
#include "skinnedmesh.h"
#include "meshfactory.h"
#include "animatedmesh.h"

namespace wkg
{

namespace engine
{

MeshFactory::MeshFactory() : 
    Singleton <MeshFactory>(), 
    Factory<Mesh, MeshDescriptor>()
{
}

MeshFactory::~MeshFactory()
{

}

Mesh *
MeshFactory::Create(const MeshDescriptor &desc) const
{
	Mesh *mesh;
	switch(desc.type)
	{
	case MeshDescriptor::STANDARD_MESH:
		mesh = new Mesh();
		break;

	case MeshDescriptor::SKINNED_MESH:
		mesh = new SkinnedMesh();
		break;

	case MeshDescriptor::ANIMATED_MESH:
		mesh = new AnimatedMesh();
		break;
	}

	mesh->Load(desc.filename.c_str());
	return mesh;
}

}

}