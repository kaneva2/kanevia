/******************************************************************************
 SkinnedMeshRenderer.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "SkinnedMeshRenderer.h"
#include "MeshTypes.h"
#include "SkinnedMesh.h"
#include "MeshSection.h"
#include "Matrix44.h"
#include "Vector4.h"
#include "XFormNode.h"
#include "MatrixPaletteNode.h"
#include "Device.h"
#include "VSFactory.h"
#include "VertexShader.h"
#include "SkinnedVSConst.h"
#include "RenderState.h"

namespace wkg {
namespace engine {

void SkinnedMeshRenderer::Render(Mesh* sm)
{
	SkinnedMesh* mesh = (SkinnedMesh*)sm;
	if (!mesh)
		return;

	RenderState rs(RenderState::DELTA);
	rs.Set(D3DRS_FOGENABLE, false);
	rs.Set(D3DRS_ALPHABLENDENABLE, false);
	Device::Instance()->SetRenderState(rs);

	// set our vertex shader
	VertexShader *vs = VSFactory::Instance()->GetShader("weighted");
	vs->Activate();
	Device::Instance()->SetPixelShader(0);

	// now render the actual sections

	// load the common constants
	Vector4 v(0.5f, 1.0f, BONEMATRIX0, 3.0f);
	Device::Instance()->SetVertexShaderConstantF(CONSTS, v, 1);

	Vector4 l(1.0f, 1.0f, 0.0f, 0.4f);
	l.Normalize();
	Device::Instance()->SetVertexShaderConstantF(LIGHT_POS, l, 1);

	// get the view matrix
	Matrix44 view;
	Device::Instance()->GetTransform(D3DTS_VIEW, &view);

	Matrix44 proj;
	Device::Instance()->GetTransform(D3DTS_PROJECTION, &proj);

	// and load the transpose of the view-proj matrix
	Matrix44 m;
	m = view * proj;
	m.Transpose();
	Device::Instance()->SetVertexShaderConstantF(VIEW_PROJ, m, 4);

	MeshSection* section;
	WKGMaterial* meshMat;
	D3DMATERIAL9 mat;

	Device::Instance()->SetStreamSource(0, mesh->GetVB(), 0, sizeof(WKGVertex));
	Device::Instance()->SetStreamSource(1, mesh->GetWeightVB(), 0, sizeof(WKGVertexWeights));

	// now render each section
	for(uint32 i = 0; i < mesh->GetNumSections(); i++)
	{
		section = mesh->GetSections()[i];
		if (section)
		{
			// set the texture

			ASSERT(section->m_material >= 0 && section->m_material < mesh->GetNumMaterials());
			meshMat = mesh->GetMaterials() + section->m_material;
			memcpy(&(mat.Diffuse), &(meshMat->diffuse), sizeof(WKGColor));
			memcpy(&(mat.Ambient), &(meshMat->ambient), sizeof(WKGColor));
			memcpy(&(mat.Specular), &(meshMat->specular), sizeof(WKGColor));
			memset(&(mat.Emissive), 0, sizeof(WKGColor));
			mat.Power = meshMat->power;
			Device::Instance()->SetMaterial(&mat);

			ASSERT(section->m_material < mesh->GetNumTextures());
			Device::Instance()->SetTexture(0, mesh->GetTextures()[section->m_material]);
			Device::Instance()->SetTexture(1, mesh->GetBumpTextures()[section->m_material]);

			// load the matrix palette
			for(int32 b = 0; b < MAX_BONES_PER_SECTION; b++)
			{
				m = mesh->GetMatrixPalette()->GetMatrix(section->m_bones[b]);

				m.Transpose();
				Device::Instance()->SetVertexShaderConstantF(BONEMATRIX0 + b * 3, m, 3);
			}

			section->Render();
		}
	}

	Device::Instance()->SetStreamSource(1, 0, 0, 0);
}

}

}
