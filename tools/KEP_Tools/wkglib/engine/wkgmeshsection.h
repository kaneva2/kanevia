/******************************************************************************
 wkgmeshsection.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "meshopts.h"

namespace wkg
{

class WKGMeshSection
{
public:
	uint32 m_first_face;
	uint32 m_num_faces;

	int32 m_bones[MAX_BONES_PER_SECTION];
	int32 m_material;
};

}

