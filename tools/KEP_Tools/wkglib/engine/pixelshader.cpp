/******************************************************************************
 pixelshader.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "pixelshader.h"
#include "FileStream.h"

namespace wkg
{

namespace engine
{

PixelShader::PixelShader() : m_function(0), m_handle(0)
{
}

PixelShader::~PixelShader()
{
	delete [] m_function;
	
	if (m_handle)
	{
		m_handle->Release();
		m_handle = 0;
	}
}

bool PixelShader::Init(const char* psoFilename)
{
	return Load(psoFilename) && CreateShader();
}

bool PixelShader::Load(const char* psoFilename)
{
	FileStream in;

	if (in.Open(psoFilename, FileStream::ReadOnly))
	{
		uint32 len = in.GetFileSize();

		if (len > 0)
		{
			m_function = new uint32[(len + 3) / 4];
			if (m_function)
			{
				if (len != in.Read((uint8*)m_function, len))
				{
					delete m_function;
					m_function = 0;
				}
			}
		}

		in.Close();
	}

	return 0 != m_function;
}

void 
PixelShader::LoadConstants()
{
}

bool 
PixelShader::CompileShader(const std::string function)
{
	LPD3DXBUFFER ps_code;
	LPD3DXBUFFER errors;

	HRESULT hres = D3DXAssembleShader(
	  function.c_str(),
	  function.size(),
	  0,
	  0,
	  0,
	  &ps_code,
	  &errors);

    if(FAILED(hres)) 
    {
        DEBUGMSG(static_cast<char *>(errors->GetBufferPointer()));
		if (errors)
			errors->Release();

        return false;
    }

	m_function = new uint32[(ps_code->GetBufferSize() + 3) / 4];
	if(!m_function) return false;
	memcpy(m_function, ps_code->GetBufferPointer(), ps_code->GetBufferSize());

	ps_code->Release();
	if (errors)
		errors->Release();

    return true;
}

bool 
PixelShader::CreateShader()
{
    HRESULT hres = 
		Device::Instance()->CreatePixelShader(m_function, &m_handle);

	if(FAILED(hres)) 
		return false;
	else
		return true;
}

}

}