/******************************************************************************
 NameValueSet.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "NameValueSet.h"

namespace wkg {

NameValueSet::NameValueSet()
	: src(0)
{
}

NameValueSet::~NameValueSet()
{
	delete [] src;
	src = 0;
	nvList.clear();
}

bool 
NameValueSet::Parse(const char* nameValSrc)
{
	delete [] src;
	src = 0;

	nvList.clear();

	if (nameValSrc)
	{
		int32 len;

		len = strlen(nameValSrc);
		src = new char[len + 1];
		if (!src)
			return false;

		memcpy(src, nameValSrc, len + 1);

		ParsePairs();
	}
	return true;
}

void
NameValueSet::ParsePairs()
{
	nv pair;
	char *str, *end;

	str = src;
	while (str)
	{
		pair.name = str;
		while (*pair.name && (*pair.name == ' ' || *pair.name == '\t'))
			++pair.name;

		end = strchr(pair.name, '=');
		if (!end)
			break;

		pair.value = end + 1;

		while (*pair.value && (*pair.value == ' ' || *pair.value == '\t'))
			++pair.value;

		str = strchr(pair.value, ';');

		while (end > pair.name && (end[-1] == ' ' || end[-1] == '\t'))
			--end;

		*end = '\0';

		if (str)
			end = str++;
		else
			end = pair.value + strlen(pair.value);

		while (end > pair.value && (end[-1] == ' ' || end[-1] == '\t'))
			--end;

		*end = '\0';

		nvList.push_back(pair);
	}
}

}