/******************************************************************************
 MessageSubscription.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "MessageSubscription.h"

namespace wkg
{

MessageSubscription::MessageSubscription(int32 to, int32 from) : 
	m_to(to),
	m_from(from)
{
}

MessageSubscription::~MessageSubscription()
{
}

}