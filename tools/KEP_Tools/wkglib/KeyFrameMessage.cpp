/******************************************************************************
 KeyFrameMessage.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "KeyFrameMessage.h"

namespace wkg
{

namespace state_machine
{

KeyFrameMessage::KeyFrameMessage(std::string msg, int32 frame) : 
	m_message(msg),
	m_frame(frame)
{
}

KeyFrameMessage::~KeyFrameMessage()
{

}

}
}