/******************************************************************************
 TargetCamera.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Camera.h"

namespace wkg 
{

class TargetCamera : public Camera
{
	public:
		TargetCamera();		// "target" in the sense that the target controls the direction of the camera (NOT a camera that is attached to an object target)

		virtual void SetTarget(const Vector3&);		// in wkg::Units

		virtual const Vector3& GetTarget() const;	// in wkg::Units

		//
		// note: OffsetPosition() for a TargetCamera will change the camera's direction if the target is
		//		 left untouched (contrary to the Camera::OffsetPosition() documentation).  This behavior is
		//		 by design as it gives the most flexibility.  For example, to get the 
		//		 Camera::OffsetPosition() documented behavior (i.e. no direction change), just
		//		 call both OffsetPosition() and OffsetTarget() with the same value.
		//

		virtual void OffsetTarget(const Vector3& offsetUnits);	// move the target a relative amount in an arbitrary direction

		// 
		// note: this class remains pure-virtual since it does not (by itself) have a
		//		 well-defined notion of "up."  For example, using the up hint direction of <0, 1, 0>
		//		 will cause a roll as the camera direction approaches parallel to the y-axis.
		// 
		// Subclasses MUST enforce a well-defined "up" direction (i.e. one that prevents unintended rolls).
		//

	protected:
		Vector3 target;
};

inline TargetCamera::TargetCamera()
	: target(0.0f, 0.0f, 0.0f)
{
	pos.z -= 1.0f;
}

inline void TargetCamera::SetTarget(const Vector3& Target)
{
	target = Target;
}

inline const Vector3& TargetCamera::GetTarget() const
{
	return target;
}

inline void TargetCamera::OffsetTarget(const Vector3& offsetUnits)
{
	target += offsetUnits;
}

}
