/******************************************************************************
 dbprint.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include <stdarg.h>
#include <stdio.h>

#ifndef OutputDebugString
extern "C" __declspec(dllimport) void __stdcall OutputDebugStringA(char*);
#endif

void
dbprint(const char* fmt, ...)
{
	va_list args;
	static char buf[1024];

	va_start(args, fmt);

	vsprintf(buf, fmt, args);
	OutputDebugStringA(buf);

	va_end(args);
}

void
dbprintln(const char* fmt, ...)
{
	va_list args;
	static char buf[1024];

	va_start(args, fmt);

	vsprintf(buf, fmt, args);
	OutputDebugStringA(buf);
	OutputDebugStringA("\r\n");

	va_end(args);
}
