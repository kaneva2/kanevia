/******************************************************************************
 wkgfunc.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"

//
// The purpose of this file is to (initially) avoid including any C run-time library
// headers in the app code.
//
// Eventually, this will let us replace these functions with faster versions without
// the hassle and bug-potential of going through all the app code.
//

#include <math.h>
#include <string.h>
#include <stdlib.h>

namespace wkg {

real 
sqrt(real arg)
{
	return static_cast<real>(::sqrt(static_cast<double>(arg)));
}

double
sqrt(double arg)
{
	return (::sqrt(static_cast<double>(arg)));
}

real
cos(real arg)
{
	return static_cast<real>(::cos(static_cast<double>(arg)));
}

real
acos(real arg)
{
	return static_cast<real>(::acos(static_cast<double>(arg)));
}

real
sin(real arg)
{
	return static_cast<real>(::sin(static_cast<double>(arg)));
}

real
asin(real arg)
{
	return static_cast<real>(::asin(static_cast<double>(arg)));
}

real
tan(real arg)
{
	return static_cast<real>(::tan(static_cast<double>(arg)));
}

real
atan(real arg)
{
	return static_cast<real>(::atan(static_cast<double>(arg)));
}

void 
memcpy(void* dest, const void* src, uint32 amt)
{
	::memcpy(dest, src, amt);
}

void 
memset(void* dest, int32 val, uint32 amt)
{
	::memset(dest, val, amt);
}

void 
memmove(void* dest, const void* src, uint32 amt)
{
	::memmove(dest, src, amt);
}

void 
memzero(void* dest, uint32 amt)
{
	::memset(dest, 0, amt);
}

int32 
memcmp(void* dest, const void* src, uint32 amt)
{
	return ::memcmp(dest, src, amt);
}

#ifndef NO_WKG_STRING

int32 
strlen(const char* src)
{
	return (uint32)::strlen(src);
}

char*
strcpy(char* dest, const char* src)
{
	return ::strcpy(dest, src);
}

int32 
strcmp(const char* s1, const char* s2)
{
	return ::strcmp(s1, s2);
}

int32
stricmp(const char* s1, const char* s2)
{
	return ::stricmp(s1, s2);
}

#endif

int32 
atoi(const char* str)
{
	return ::atoi(str);
}

real 
atof(const char* str)
{
	return static_cast<real>(::atof(str));
}

}
