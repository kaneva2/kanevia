/******************************************************************************
 StateFactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

class XMLNode;

namespace state_machine
{

class State;
class StateMachine;

class StateFactory
{
public:
	static State* Create(XMLNode* curNode, StateMachine* parent_sm);

protected:
	
};

}

}


