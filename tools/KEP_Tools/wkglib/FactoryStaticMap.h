/******************************************************************************
 FactoryStaticMap.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
//
//  Implements a factory pattern where the objects in the factory are created once
//  then retrieved.  No reference counting is done, since allocation and deallocation
//  of the objects is done by the factory;
// A typical use of this template would be:
//
//  class PSFactory : public FactoryStaticMap<PixelShader, std::string> { ... }
//
// Where a PixelShader pointer is retrieved based on it's name.
//
// Object type header file (in this example, pixelshader.h) must be included
// in factory header otherwise error will occur about deleting pointer to incomplete
// data type.
//


#ifndef _FACTORYSTATICMAP_H_
#define _FACTORYSTATICMAP_H_

#include <map>

#ifdef _DEBUG
#   include "typeinfo.h"
#endif

template <typename T, typename D> class FactoryStaticMap
{
    public:
        FactoryStaticMap();
        virtual ~FactoryStaticMap();

        T *GetObj(const D& desc);


    protected:
        typedef std::map<D, T*> NodeMap;
        typedef NodeMap::iterator NodeIterator;

        bool AddObject(const D& desc, T *data);

		mutable NodeMap m_nodes;
};

template <typename T, typename D> 
FactoryStaticMap<T, D>::FactoryStaticMap()
{
}

template <typename T, typename D> 
FactoryStaticMap<T, D>::~FactoryStaticMap()
{
    NodeIterator itor;
    for(itor = m_nodes.begin(); itor != m_nodes.end(); itor++)
    {
        delete itor->second;
    }

	m_nodes.clear();
}


template <typename T, typename D> 
T *FactoryStaticMap<T, D>::GetObj(const D& desc)
{
	NodeIterator found = m_nodes.find(desc);
	if (found != m_nodes.end())
	{
		return found->second;
	}
	else
	{
		return NULL;
	}
}


template <typename T, typename D>
bool FactoryStaticMap<T, D>::AddObject(const D& desc, T *data)
{
	m_nodes.insert(NodeMap::value_type(desc, data));
    return true;
}

#endif
