/******************************************************************************
 factoryrelease.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

// This goes along hand in hand with the factory template and the refcounter
//  it's purpose is to inform a factory that constructed this object that we
//  no longer exist, when using reference counting.

namespace wkg
{

template <typename T> class FactoryRelease
{
public:
	virtual void Release(const T *data) = 0;
};

}


