/******************************************************************************
 SkillStateMachine.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "XMLNode.h"
#include "XMLDocument.h"
#include "XMLNodeAttributes.h"
#include "FileStream.h"
#include "Actor.h"
#include "Skill.h"
#include "SkillState.h"
#include "SkillFactory.h"
#include "StateFactory.h"
#include "SkillChangeWeapon.h"
#include "SkillDrawWeapon.h"
#include "SkillHolsterWeapon.h"
#include "SkillStateMachine.h"

namespace wkg
{

using namespace state_machine;

namespace game
{


SkillStateMachine::SkillStateMachine(Actor* owner) :
	StateMachine(),
	m_actor(owner)
{
}

SkillStateMachine::~SkillStateMachine()
{
}

bool
SkillStateMachine::Load(XMLDocument *doc)
{
	XMLNode *curr = doc->GetRootNode();

	if(curr)
	{
		curr = curr->GetFirstChild()->GetFirstChild();
		m_name = curr->GetAttributes()->FindAttributeValue("name");
		//DEBUGMSG("Creating StateMachine: %s", m_name.c_str());
		GetStates(curr->GetFirstChild());
		AssignPointers();
		m_curr_state = *(m_states.begin());

		return true;
	}

	return false;
}

void
SkillStateMachine::GetStates(XMLNode *curNode)
{
	// Create state, attach its transitions, and add it to state vector
	SkillState* newState = dynamic_cast<SkillState*>(StateFactory::Create(curNode, this));

	newState->SetOwner(m_actor);

	m_map[newState->GetName()] = newState;

	std::string name = curNode->GetAttributes()->FindAttributeValue("name");
	int32 countdown = atoi(curNode->GetAttributes()->FindAttributeValue("countdown").c_str());

	CreateTransitions(newState, curNode->GetFirstChild());

	AddState(newState);

	// Recurse down to next state in XML tree
	if(curNode->GetNextSibling())
	{
		GetStates(curNode->GetNextSibling());
	}
}

}
}