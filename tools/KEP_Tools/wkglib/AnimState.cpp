/******************************************************************************
 AnimState.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "animstate.h"
#include "AnimatedMesh.h"
#include "Message.h"
#include "messagequeue.h"
#include "SkeletalAnimation.h"
#include "AnimStateMachine.h"

namespace wkg
{

using namespace engine;

namespace state_machine
{

AnimState::AnimState(std::string name, StateMachine* parent_sm) :
	State(name, parent_sm),
	m_transition_time(100.0f),
	m_play_mode(AS_ONCE)
{
}

AnimState::~AnimState()
{
}

void
AnimState::OnEnter()
{
	int32 mode = Animation::PLAY_LOOPING;
	if(AS_ONCE == m_play_mode)
		mode = Animation::PLAY_ONCE;

	//DEBUGMSG("Anim name: %s", GetName().c_str());
	GetAnimatedMesh()->PlayAnimation(
		GetName().c_str(), mode, m_transition_time);
}

void
AnimState::OnExit()
{
	GetAnimatedMesh()->StopAnimation(GetName().c_str(), m_transition_time);
}

void 
AnimState::OnUpdate(uint32 ms)
{
	SkeletalAnimation *sa = GetAnimatedMesh()->GetAnimation(GetName().c_str());
	if(sa && !sa->IsPlaying())
	{
		for(int32 i = 0; i < m_anim_done_messages.size(); i++)
		{
			MessageQueue::Instance()->QueueMessage(m_anim_done_messages[i]);
		}
	}
}

engine::AnimatedMesh*
AnimState::GetAnimatedMesh()
{
	AnimStateMachine* anim_sm = dynamic_cast<AnimStateMachine*>(m_parentSM);
	return anim_sm->GetAnimatedMesh();
}

}

}
