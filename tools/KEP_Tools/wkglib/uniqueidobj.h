/******************************************************************************
 uniqueidobj.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

// Derive from this class to automatically have a unique ID assigned

#pragma once

namespace wkg
{

class UniqueIDObj
{
public:
	UniqueIDObj();

	int32 GetUID() { return m_id; }

protected:
	uint32 m_id;
	static uint32 m_curr_id;

};

}


