/******************************************************************************
 refcounter.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

// Dervie from this class and use the Release() function instead of 
//  deleting it directly.
// If used in conjunction with a factory, use a FactoryRelease to
//  automatically inform the factory when this class' ref counter
//  reaches zero and is deleted.

#pragma once

#include "factoryrelease.h"

namespace wkg
{

template <typename T> class RefCounter
{
	public:
		RefCounter() : m_ref_count(1), m_factory_release(0)
		{
		};

		virtual void Release() 
		{
			m_ref_count--;
			ASSERT(m_ref_count >= 0);
			if(!m_ref_count)
			{
				// tell our factory we don't exist anymore
				if(m_factory_release) 
				{
					m_factory_release->Release((T*)this);
				}

				delete this;
			}
		}

		virtual void AddRef() 
		{ 
			m_ref_count++; 
		}

		virtual void SetFactoryRelease(FactoryRelease<T> *fr) 
		{ 
			m_factory_release = fr; 
		}

	protected:
		int32 m_ref_count;
		FactoryRelease<T> *m_factory_release;	

	protected:
		// can only be called from Release()
		virtual ~RefCounter() 
		{
			// change this to a warning instead
			if(m_ref_count)
				DEBUGMSG("Warning: Refcounter being deleted before reaching 0 references");
//			ASSERT(!m_ref_count);
		};

};

}



