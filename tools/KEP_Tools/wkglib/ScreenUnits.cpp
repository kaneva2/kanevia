/******************************************************************************
 ScreenUnits.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "screenunits.h"


namespace wkg {

ScreenUnits::Resolution ScreenUnits::s_res = ScreenUnits::RES640x480;


void ScreenUnits::SetResolution(Resolution res)
{
	s_res = res;
}


real ScreenUnits::GetResolutionMultiplier(Resolution res)
{
	real multiplier = 1.0f;

	if (res != s_res)
	{
		if (RES640x480 == res) 
		{
			if (RES800x600 == s_res)
			{
				multiplier = 1.25f;
			}
			else if (RES1024x768 == s_res)
			{
				multiplier = 1.6f;
			}
			else
			{
				ASSERT(!"Bad Resolution in ScreenUnits::GetResolutionMultiplier");
			}
		}
		else if (RES800x600 == res) 
		{
			if (RES640x480 == s_res)
			{
				multiplier = 0.8f;
			}
			else if (RES1024x768 == s_res)
			{
				multiplier = 1.28f;
			}
			else
			{
				ASSERT(!"Bad Resolution in ScreenUnits::GetResolutionMultiplier");
			}
		}
		else if (RES1024x768 == res) 
		{
			if (RES640x480 == s_res)
			{
				multiplier = 0.625f;
			}
			else if (RES800x600 == s_res)
			{
				multiplier = 0.78125f;
			}
			else
			{
				ASSERT(!"Bad Resolution in ScreenUnits::GetResolutionMultiplier");
			}
		}
	}

	return multiplier;
}

int32 ScreenUnits::GetCurrentScreenUnit(int32 unit, Resolution res)
{
	return (int32)(GetResolutionMultiplier(res) * ((real)unit));
}


int32 ScreenUnits::GetCurrentResolutionWidth()
{
	switch (s_res)
	{
		case RES640x480:
			return 640;
		case RES800x600:
			return 800;
		case RES1024x768:
			return 1024;
		default:
			ASSERT(!"Bad Resolution");
			return 0;
	}
}


int32 ScreenUnits::GetCurrentResolutionHeight()
{
	switch (s_res)
	{
		case RES640x480:
			return 480;
		case RES800x600:
			return 600;
		case RES1024x768:
			return 768;
		default:
			ASSERT(!"Bad Resolution");
			return 0;
	}
}


}