/******************************************************************************
 scenegraph.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "node.h"
#include "matrix44.h"
#include "scenegraph.h"

namespace wkg
{

SceneGraph::SceneGraph() 
{ 
    m_root = new Node();
    ASSERT(m_root);
}

SceneGraph::~SceneGraph()
{
    delete m_root;
    m_root = (Node *)0;
}

Node *
SceneGraph::GetParent(Node *child)
{
    ASSERT(m_root);
	return GetParentHelper(m_root, child);
}

Node *
SceneGraph::GetParentHelper(Node *curr, Node *target)
{
    ASSERT(curr);
    ASSERT(target);
	for(int32 i = 0; i < curr->GetNumChildren(); i++)
	{
		if(curr->GetChild(i) == target)
			return curr;

		Node *res;
		res = GetParentHelper(curr->GetChild(i), target);
		if(res) 
			return res;
	}

	return (Node *)0;
}

void 
SceneGraph::Update() 
{ 
	m_root->DFSUpdate(Matrix44::IDENTITY); 
}

void 
SceneGraph::Render() 
{ 
	m_root->DFSRender(); 
}

Node *
SceneGraph::FindByName(char *name)
{
	return m_root->FindByName(name);
}

}

