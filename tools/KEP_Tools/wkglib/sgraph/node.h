/******************************************************************************
 node.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <vector>
#include <string>

namespace wkg
{

class Matrix44;

class Node
{
    public:
	    Node();
	    virtual ~Node();

	    void AddChild(Node *node);
	    bool RemoveChild(Node *child);

	    // the id is automagically assigned contiguously starting from zero
	    uint32 GetID() const { return m_id; }

	    int32 GetNumChildren() const;
	    Node *GetChild(int32 idx) const;

	    // perform a dfs of the tree
	    virtual void DFSRender() const;

	    // called during a dfs of the tree
	    virtual void OnRender() const {}

	    // called prior to the render
	    //  uses the matrix and renderstate stack.  saves current renderstate/
	    //  matrix for the rendering pass
	    virtual void DFSUpdate(const Matrix44 &parent_mat);

	    // overloaded to do something during the update DFS
	    virtual void OnUpdate() {}

		void SetName(const char *name) { m_name = name; }
		const char *GetName() { return m_name.c_str(); }

		Node *FindByName(const char *name);

		enum 
		{ 
			NO_RENDER  = 0x01,
			NO_UPDATE  = 0x02,
		};

		void SetFlags(uint32 flags) { m_flags= flags; }
		uint32 GetFlags() { return m_flags; }

    protected:
		typedef std::vector<Node *> NodeVector;
	    typedef NodeVector::iterator NodeIterator;
	    typedef NodeVector::const_iterator ConstNodeIterator;

	    static uint32 m_current_node_id;

	    NodeVector m_children;

		std::string m_name;

	    uint32 m_id;

		uint32 m_flags;
};

}


