/******************************************************************************
 xformnode.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "matrix44.h"
#include "device.h"
#include "xformnode.h"

namespace wkg
{

XFormNode::XFormNode()
{
	m_matrix.Identity();
	m_local_to_world.Identity();
}

XFormNode::~XFormNode()
{
}

void 
XFormNode::DFSUpdate(const Matrix44 &parent_mat)
{
	Matrix44 old_mat = parent_mat;
	m_local_to_world = m_matrix * old_mat;

	OnUpdate();

	// if we have children, recurse
	if(m_children.size())
	{
		NodeIterator itor;
		for(itor = m_children.begin(); itor < m_children.end(); itor++)
		{
			(*itor)->DFSUpdate(m_local_to_world);
		}
	}
}

}
