/******************************************************************************
 node.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "node.h"

namespace wkg
{

uint32 Node::m_current_node_id = 0;

Node::Node() :
	m_flags(0)
{
	// assign the id
	m_id = m_current_node_id++;
}

Node::~Node()
{
	NodeIterator itor;
	NodeIterator endChild = m_children.end();

	for(itor = m_children.begin(); itor != endChild; itor++)
	{
		delete (*itor);
	}
}

int32 
Node::GetNumChildren() const
{
	return (int32)m_children.size();
}

Node *
Node::GetChild(int32 idx) const
{
	return m_children[idx];
}

bool
Node::RemoveChild(Node *child)
{
    ASSERT(child);
	NodeIterator itor;
	NodeIterator endChild = m_children.end();

	for(itor = m_children.begin(); itor < endChild; itor++)
	{
		if(child == *itor)
		{
			m_children.erase(itor);
			return true;
		}
	}
    return false;
}

void 
Node::AddChild(Node *node)
{
    ASSERT(node);
	m_children.push_back(node);
}

void 
Node::DFSRender() const
{
	if(m_flags & NO_RENDER) return;

	OnRender();

	// if we have children, recurse
	ConstNodeIterator itor;
	ConstNodeIterator endChild = m_children.end();

	for(itor = m_children.begin(); itor != endChild; itor++)
	{
        ASSERT(*itor);
		(*itor)->DFSRender();
	}
}

void 
Node::DFSUpdate(const Matrix44 &parent_mat)
{
	if(m_flags & NO_UPDATE) return;

	OnUpdate();

	// if we have children, recurse
	NodeIterator itor;
	NodeIterator endChild = m_children.end();

	for(itor = m_children.begin(); itor != endChild; itor++)
	{
        ASSERT(*itor);
		(*itor)->DFSUpdate(parent_mat);
	}
}

Node *
Node::FindByName(const char *name)
{
	if(m_name == name) 
		return this;

	NodeIterator itor = m_children.begin();
	NodeIterator iend = m_children.end();

	for(; itor < iend; itor++)
	{
		Node *n = (*itor)->FindByName(name);
		if(n) return n;
	}

	return 0;
}

}






