/******************************************************************************
 xformnode.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "node.h"
#include "matrix44.h"

namespace wkg
{

class XFormNode : public Node
{
    public:
	    XFormNode();
	    virtual ~XFormNode();

	    void SetMatrix(const Matrix44 &mat) { m_matrix = mat;  }
	    const Matrix44 &GetMatrix() const   { return m_matrix; }
		const Matrix44 &GetLocalToWorld() const { return m_local_to_world; }

	    virtual void DFSUpdate(const Matrix44 &parent_mat);

 	    Matrix44 m_matrix;
   protected:
	    Matrix44 m_local_to_world;

};

}




