/******************************************************************************
 scenegraph.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "singleton.h"

namespace wkg
{

class Node;

class SceneGraph : public Singleton<SceneGraph>
{
    public:
	    SceneGraph();
        virtual ~SceneGraph();

	    Node *GetRoot() { return m_root; }
	    Node *GetParent(Node *child);

		void Update();
		void Render();

		Node *FindByName(char *name);

    protected:
	    Node *GetParentHelper(Node *curr, Node *target);

    protected:
	    Node *m_root;
};

}
