/******************************************************************************
 Capsule.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "LineSeg.h"

namespace wkg {

class Capsule
{
	public:
		// uninitialized
		Capsule();

		Capsule(const LineSegment &seg, real radius);

		void Set(const LineSegment &seg, real radius);
		void SetSpan(const LineSegment &seg);
		void SetRadius(real r);

		const LineSegment &GetSpan() const;
		real GetRadius() const;

	public:
		LineSegment seg;
		real radius;
};

inline
Capsule::Capsule()
{
	// uninitialized
}

inline void 
Capsule::SetSpan(const LineSegment &seg)
{
	this->seg = seg;
}

inline void 
Capsule::SetRadius(real radius)
{
	this->radius = radius;
}

inline void 
Capsule::Set(const LineSegment &seg, real radius)
{
	SetSpan(seg);
	SetRadius(radius);
}

inline
Capsule::Capsule(const LineSegment &seg, real radius)
{
	Set(seg, radius);
}

inline const LineSegment & 
Capsule::GetSpan() const
{
	return seg;
}

inline real 
Capsule::GetRadius() const
{
	return radius;
}

}
