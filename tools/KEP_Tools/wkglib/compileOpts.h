/******************************************************************************
 compileOpts.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#define SINGLEPRECISION
#define MANAGED_TEXTURES
#define USE_32BIT_INDICES
#define USE_SERIALIZATION
