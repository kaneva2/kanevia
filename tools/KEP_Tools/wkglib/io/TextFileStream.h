/******************************************************************************
 TextFileStream.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "FileStream.h"

namespace wkg 
{

class TextFileStream : public FileStream
{
	public:
		TextFileStream();
		~TextFileStream();

		void SetBufSize(uint32);	// use before Open()

		bool Open(const char* filename, OpenMode);

		char* GetNextLine(uint32* lineLen);		// result is null terminated

	protected:
		bool FindLineEnd();

	protected:
		uint32	bufSize;
		char*	buf;
		char	*start, *end;
		uint32	nRead;
};

}
