/******************************************************************************
 FileStream.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "FileStream.h"

#include <stdio.h>

namespace wkg {

bool 
FileStream::Open(const char* filename, OpenMode flags)
{
	ASSERT(!fp);

	if (flags & CreateAlways)
		fp = fopen(filename, "w+b");
	else if (flags & ReadOnly)
		fp = fopen(filename, "rb");
	else
		fp = fopen(filename, "r+b");

	return fp != 0;
}

void 
FileStream::Close()
{
	if (fp)
	{
		fclose(fp);
		fp = 0;
	}
}

bool
FileStream::IsEOF()
{
	return feof(fp) != 0;
}

uint32 
FileStream::GetFileSize()
{
	if (fp)
	{
		uint32 result;
		int32 start;

		start = ftell(fp);
		if (0 == fseek(fp, 0, SEEK_END))
		{
			result = (uint32)ftell(fp);
			fseek(fp, start, SEEK_SET);
			return result;
		}
	}
	return 0;
}

uint32 
FileStream::Read(uint8* dest, uint32 destMax)
{
	return (uint32)fread(dest, 1, destMax, fp);
}

uint32 
FileStream::Write(uint8* src, uint32 srcAmt)
{
	return (uint32)fwrite(src, 1, srcAmt, fp);
}

}
