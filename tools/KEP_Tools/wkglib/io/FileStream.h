/******************************************************************************
 FileStream.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// Generic file IO

#include "Stream.h"

struct _iobuf;

namespace wkg 
{

class FileStream : public Stream
{
	public:
		FileStream();		// init
		~FileStream();

		enum OpenMode
		{
			CreateAlways	= 0x00000001,		// truncates existing file (can Read & Write)
			ReadOnly		= 0x00000002		// file must exist (no writing)
			// otherwise...........................file must exist (can Read & Write)
		};

		bool Open(const char* filename, OpenMode);
		void Close();

		bool IsEOF();

		uint32 GetFileSize();

		uint32 Read(uint8* dest, uint32 destMax);		// returns num Read
		uint32 Write(uint8* src, uint32 srcAmt);		// returns num written

		bool Read(real*);		// always single precision in the stream
		bool Read(int32*);
		bool Read(uint32*);
		bool Read(int16*);
		bool Read(uint16*);
		bool Read(uint8*);
		bool Read(int8*);

		bool Write(real);	// always single precision in the stream
		bool Write(int32);
		bool Write(uint32);
		bool Write(int16);
		bool Write(uint16);
		bool Write(uint8);
		bool Write(int8);

	protected:
		struct _iobuf* fp;
};

inline
FileStream::FileStream() 
{
	fp = 0;
}

inline
FileStream::~FileStream() 
{
	Close();
}

inline bool 
FileStream::Read(real* pVal)
{
	float fv;	// keep this "float"

	if (sizeof(float) == Read(reinterpret_cast<uint8*>(&fv), sizeof(float)))	// keep this "float"
	{
		*pVal = fv;
		return true;
	}
	return false;
}

inline bool 
FileStream::Read(int32* pVal)
{
	return sizeof(int32) == Read(reinterpret_cast<uint8*>(pVal), sizeof(int32));
}

inline bool 
FileStream::Read(uint32* pVal)
{
	return sizeof(uint32) == Read(reinterpret_cast<uint8*>(pVal), sizeof(uint32));
}

inline bool 
FileStream::Read(int16* pVal)
{
	return sizeof(int16) == Read(reinterpret_cast<uint8*>(pVal), sizeof(int16));
}

inline bool 
FileStream::Read(uint16* pVal)
{
	return sizeof(uint16) == Read(reinterpret_cast<uint8*>(pVal), sizeof(uint16));
}

inline bool 
FileStream::Read(uint8* pVal)
{
	return sizeof(uint8) == Read(pVal, sizeof(uint8));
}

inline bool 
FileStream::Read(int8* pVal)
{
	return sizeof(int8) == Read(reinterpret_cast<uint8*>(pVal), sizeof(int8));
}

inline bool  
FileStream::Write(real val)
{
	float fv = (float)val;	// keep this float
	return sizeof(float) == Write(reinterpret_cast<uint8*>(&fv), sizeof(float));	// keep this float
}

inline bool  
FileStream::Write(int32 val)
{
	return sizeof(int32) == Write(reinterpret_cast<uint8*>(&val), sizeof(int32));
}

inline bool  
FileStream::Write(uint32 val)
{
	return sizeof(uint32) == Write(reinterpret_cast<uint8*>(&val), sizeof(uint32));
}

inline bool  
FileStream::Write(int16 val)
{
	return sizeof(int16) == Write(reinterpret_cast<uint8*>(&val), sizeof(int16));
}

inline bool  
FileStream::Write(uint16 val)
{
	return sizeof(uint16) == Write(reinterpret_cast<uint8*>(&val), sizeof(uint16));
}

inline bool  
FileStream::Write(uint8 val)
{
	return sizeof(uint8) == Write(&val, sizeof(uint8));
}

inline bool  
FileStream::Write(int8 val)
{
	return sizeof(int8) == Write(reinterpret_cast<uint8*>(&val), sizeof(int8));
}

}
