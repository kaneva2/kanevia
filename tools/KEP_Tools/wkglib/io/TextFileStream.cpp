/******************************************************************************
 TextFileStream.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "TextFileStream.h"

namespace wkg {

TextFileStream::TextFileStream()
	: bufSize(2048), buf(0), start(0), end(0), nRead(0)
{
}

TextFileStream::~TextFileStream()
{
	delete [] buf;
	buf = 0;
	start = end = 0;
	nRead = bufSize = 0;
}

void 
TextFileStream::SetBufSize(uint32 bufSz)
{
	if (!buf)
		bufSize = bufSz;
}

bool 
TextFileStream::Open(const char* filename, OpenMode mode)
{
	if (!buf)
	{
		buf = new char[bufSize];
		if (!buf)
			return false;
	}

	return FileStream::Open(filename, mode);
}

char* 
TextFileStream::GetNextLine(uint32* pLineLen)
{
	char* result = 0;

	*pLineLen = 0;

	if (buf)
	{
		if (start >= buf && start < buf + nRead)
		{
			if (FindLineEnd())
				result = start;
			else
			{
				uint32 remain = nRead - (start - buf);

				memcpy(buf, start, remain);	// moving left; no overlap in memory movement
				nRead = Read((uint8*)(buf + remain), bufSize - remain);
				nRead += remain;
				start = buf;

				if (FindLineEnd())
					result = start;
			}
		}
		else
		{
			nRead = Read((uint8*)buf, bufSize);
			if (nRead)
			{
				start = buf;

				if (FindLineEnd())
					result = start;
			}
			else
				start = 0;
		}

		if (result)
		{
			ASSERT(0 != end);
			*end = '\0';
			*pLineLen = (int32)(end - start + 1);
			start = end + 1;
		}
	}

	return result;
}

bool 
TextFileStream::FindLineEnd()
{
	ASSERT(start >= buf && start < buf + nRead);

	end = (char*)memchr(start, '\n', nRead - (start - buf));

	return 0 != end;
}

}
