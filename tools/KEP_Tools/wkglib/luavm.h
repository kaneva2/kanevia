/******************************************************************************
 luavm.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

struct lua_State;

namespace wkg
{

class LuaVM
{
public:
	// Initializes a lua vm
	LuaVM();
	~LuaVM();

	// loads in a lua file, and then calls the main() function
	//  same as LoadFile(filename); CallFunction("main");
	bool RunFile(const std::string &filename);

	// loads in a lua file, thus enabling the access to any funcitons in
	//  this file to be called.
	bool LoadFile(const std::string &filename);
	bool CallFunction(const std::string &func);

	// returns the current lua state
	lua_State *GetState() { return m_state; }

	// returns a lua object as a string from the global namespace in 
	//  the current lua state
	bool GetGlobalString(const char *global_name, char *out_val);

protected:
	lua_State *m_state;

};

}

