/******************************************************************************
 BMP.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "BMP.h"

namespace wkg 
{

BMP::BMP()
{
	rgb = 0;
	wid = hei = 0;
}

BMP::~BMP()
{
	delete [] rgb;
	rgb = 0;
}

bool 
BMP::Create(int32 w, int32 h)
{
	delete [] rgb;
	rgb = new uint8[w * h * 3];
	if (!rgb)
		w = h = 0;

	wid = w;
	hei = h;

	return rgb != 0;
}

bool 
BMP::Save(const TCHAR* filename) const
{
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bih;
	HANDLE hFile;
	int32 dwordAlignedWidth, padBytes;
	int32 palSize;
	int32 y;
#	define WIDTHBYTES(bits)    (((bits) + 31) / 32 * 4)

	if (!rgb)
		return false;

	hFile = CreateFile(filename, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (INVALID_HANDLE_VALUE == hFile)
		return false;

	dwordAlignedWidth = WIDTHBYTES(wid * 3 * 8);
	padBytes = dwordAlignedWidth - (wid * 3);
	palSize = 0;	// no palette for 24-bit RGB

	bfh.bfType = ((WORD)('M' << 8) | 'B');
	bfh.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + palSize + (dwordAlignedWidth * hei);
	bfh.bfReserved1 = 0;
	bfh.bfReserved2 = 0;
	bfh.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + palSize;

	bih.biSize = sizeof(BITMAPINFOHEADER);
	bih.biWidth = wid;
	bih.biHeight = hei;		// BMPs are *always* stored upside-down
	bih.biPlanes = 1;
	bih.biBitCount = 24;
	bih.biCompression = BI_RGB;
	bih.biSizeImage = dwordAlignedWidth * hei; 
	bih.biXPelsPerMeter = 0;
	bih.biYPelsPerMeter = 0;
	bih.biClrUsed = 0;
	bih.biClrImportant = 0;

	if (!Write(hFile, (uint8*)&bfh, sizeof(BITMAPFILEHEADER)) ||
		!Write(hFile, (uint8*)&bih, sizeof(BITMAPINFOHEADER)))
	{
		goto abort;
	}

	for (y = hei - 1; y >= 0; y--)
	{
		if (!Write(hFile, rgb + (y * wid * 3), wid * 3) ||
			!Write(hFile, (uint8*)&padBytes, padBytes))
		{
			goto abort;
		}
	}

	CloseHandle(hFile);

	return true;

abort:
	DBPRINTLN(("BMP::Save: file error: %s:  0x%08x (%ld)",	filename, GetLastError(), GetLastError()));
	CloseHandle(hFile);
	DeleteFile(filename);
	return false;
}

bool
BMP::Write(HANDLE hFile, uint8* src, uint32 amt) const
{
	uint32 toWrite, written;

	while (amt > 0)
	{
		toWrite = MIN<uint32>(amt, 64 * 1024);

		if (!WriteFile(hFile, src, toWrite, &written, 0) ||
			written != toWrite)
		{
			return false;
		}

		src += toWrite;
		amt -= toWrite;
	}

	return true;
}

bool 
BMP::Load(const TCHAR* filename)
{
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bih;
	HANDLE hFile;
	int32 dwordAlignedWidth, padBytes, padDummy;
	int32 y;
#	define WIDTHBYTES(bits)    (((bits) + 31) / 32 * 4)

	hFile = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, 0);
	if (INVALID_HANDLE_VALUE == hFile)
		return false;

	if (!Read(hFile, (uint8*)&bfh, sizeof(BITMAPFILEHEADER)) ||
		bfh.bfType != ((WORD)('M' << 8) | 'B') ||
		bfh.bfOffBits != (DWORD)sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) ||		// only interested in non-palettized BMPs
		!Read(hFile, (uint8*)&bih, sizeof(BITMAPINFOHEADER)) ||
		bih.biBitCount != 24 ||																// only interested in 24-bit RGB BMPs
		bih.biCompression != BI_RGB)
	{
		goto abort;
	}

	ASSERT(bih.biWidth > 0 && bih.biHeight > 0);
	wid = bih.biWidth;
	hei = bih.biHeight;
	dwordAlignedWidth = WIDTHBYTES(wid * 3 * 8);
	padBytes = dwordAlignedWidth - (wid * 3);

	delete [] rgb;
	rgb = new uint8[wid * hei * 3];
	if (!rgb)
	{
		goto abort;
	}

	// BMPs are *always* stored upside-down
	//
	for (y = hei - 1; y >= 0; y--)
	{
		if (!Read(hFile, rgb + (y * wid * 3), wid * 3) ||
			!Read(hFile, (uint8*)&padDummy, padBytes))
		{
			goto abort;
		}
	}

	CloseHandle(hFile);

	return true;

abort:
	DBPRINTLN(("BMP::Load: file error: %s:  0x%08x (%ld)",	filename, GetLastError(), GetLastError()));
	CloseHandle(hFile);
	delete [] rgb;
	rgb = 0;
	wid = hei = 0;
	return false;
}

bool
BMP::Read(HANDLE hFile, uint8* dest, uint32 amt)
{
	uint32 toRead, nRead;

	while (amt > 0)
	{
		toRead = MIN<uint32>(amt, 64 * 1024);

		if (!ReadFile(hFile, dest, toRead, &nRead, 0) ||
			nRead != toRead)
		{
			return false;
		}

		dest += toRead;
		amt -= toRead;
	}

	return true;
}

}
