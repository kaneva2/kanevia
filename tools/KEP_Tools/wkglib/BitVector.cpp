/******************************************************************************
 BitVector.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "BitVector.h"

namespace wkg {

uint32 
BitVector::EnumSet(uint32 prev) const
{
	if (++prev < maxBits)
	{
		uint32 gi, bi, chkbit;

		gi = grp(prev);
		bi = bit(prev);
		if (bits[gi] & (0xffffffffL << bi))
			chkbit = 1 << bi;
		else
		{
			++gi;
			bi = 0;
			chkbit = 1;
		}

		for (; gi < numGrp(); gi++)
		{
			if (bits[gi])
			{
				while (chkbit)
				{
					if (bits[gi] & chkbit)
						return ndx(gi, bi);

					++bi;
					chkbit <<= 1;
				}
				ASSERT(0);
			}
			bi = 0;
			chkbit = 1;
		}
	}
	return 0xffffffffL;
}

#ifdef USE_SERIALIZATION
}
#include "Stream.h"
namespace wkg {

bool 
BitVector::Load(Stream* stream)
{
	uint32 maxBitCount;

	if (!stream->Read(&maxBitCount))
		return false;

	if (!Create(maxBitCount))
		return false;

	for (uint32 i = 0; i < numGrp(); i++)
	{
		if (!stream->Read(&bits[i]))
		{
			delete [] bits;
			bits = 0;
			maxBits = 0;
			return false;
		}
	}

	return true;
}

bool 
BitVector::Save(Stream* stream) const
{
	if (!stream->Write(maxBits))
		return false;

	for (uint32 i = 0; i < numGrp(); i++)
	{
		if (!stream->Write(bits[i]))
			return false;
	}

	return true;
}

#endif

}