/******************************************************************************
 BitVector.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg 
{

class Stream;

class BitVector
{
	public:
		// Initialized constructor
		BitVector();
		~BitVector();

		bool Create(uint32 maxBits);

		void Set(uint32 ndx);

		bool IsSet(uint32 ndx) const;

		// returns 0xffffffff when done
		uint32 EnumSet(uint32 prevSetNdx = 0xffffffffL) const;	

		SERIALIZABLE

	protected:
		uint32 grp(uint32) const;
		uint32 bit(uint32) const;
		uint32 ndx(uint32, uint32) const;
		uint32 numGrp() const;

	protected:
		uint32 maxBits;
		uint32* bits;
};

inline
BitVector::BitVector()
{
	maxBits = 0;
	bits = 0;
}

inline
BitVector::~BitVector()
{
	delete [] bits;
	maxBits = 0;
	bits = 0;
}

inline uint32 
BitVector::grp(uint32 ndx) const
{
	return ndx >> 5;
}

inline uint32 
BitVector::bit(uint32 ndx) const
{
	return ndx & 0x1fL;
}

inline uint32 
BitVector::ndx(uint32 groupIndex, uint32 bitIndex) const
{
	return (groupIndex << 5) | bitIndex;
}

inline uint32
BitVector::numGrp() const
{
	return grp(maxBits + 31);
}

inline bool 
BitVector::Create(uint32 maxBits)
{
	this->maxBits = maxBits;
	bits = new uint32[numGrp()];
	if (!bits)
		this->maxBits = 0;
	else
		memset(bits, 0, numGrp() * sizeof(uint32));
	return bits != 0;
}

inline void
BitVector::Set(uint32 ndx)
{
	ASSERT(ndx < maxBits);
	bits[grp(ndx)] |= (1 << bit(ndx));
}

inline bool 
BitVector::IsSet(uint32 ndx) const
{
	ASSERT(ndx < maxBits);
	return (bits[grp(ndx)] & (1 << bit(ndx))) != 0;
}

}
