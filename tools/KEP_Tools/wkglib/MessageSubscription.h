/******************************************************************************
 MessageSubscription.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

// Simply represents a subscription of a messenger to another messenger's
//  generic messages

#pragma once

namespace wkg
{

class MessageSubscription
{
public:
	MessageSubscription(int32 to, int32 from);
	~MessageSubscription();

	int32 GetIDTo()						{	return m_to;	}
	int32 GetIDFrom()					{	return m_from;	}
	void SetIDTo(int32 to)				{	m_to = to;		}
	void SetIDFrom(int32 from)			{	m_from = from;	}

protected:
	int32 m_to;
	int32 m_from;
};

}
