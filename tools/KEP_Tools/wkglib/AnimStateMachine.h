/******************************************************************************
 AnimStateMachine.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "StateMachine.h"

namespace wkg
{

namespace engine
{
class AnimatedMesh;
}

using namespace engine;

namespace state_machine
{

class AnimStateMachine : public StateMachine
{
public:
	AnimStateMachine(AnimatedMesh *mesh);
	virtual ~AnimStateMachine();

	void AddAnimations();
	AnimatedMesh* GetAnimatedMesh()				{ return m_animated_mesh;	}
	void SetAnimatedMesh(AnimatedMesh *am)		{ m_animated_mesh = am;		}

	void SetOwnerUID(int32 uid) { m_owner_uid = uid; }
	int32 GetOwnerUID() { return m_owner_uid; }

protected:
	int32 m_owner_uid;

	AnimatedMesh* m_animated_mesh;
};

}

}

