/******************************************************************************
 KeyFrameAnimState.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "AnimState.h"

namespace wkg
{

namespace state_machine
{
class KeyFrameMessage;

class KeyFrameAnimState : public AnimState
{
public:
	KeyFrameAnimState(std::string name, StateMachine* parent_sm);
	virtual ~KeyFrameAnimState();

	virtual void OnEnter();
	virtual void OnExit();
	virtual void OnUpdate(uint32 ms);
	void AddKeyFrameMessage(std::string msg, int32 frame);


protected:
	int32 GetKeyFrameInRange(int32 lo, int32 hi);
	void DoKeyFrameAction(int32 idx);



protected:
	int32									m_lastKeyFrame;
	int32									m_currKeyFrame;
	std::vector<KeyFrameMessage*>			m_keyFrameList;
};

}

}

