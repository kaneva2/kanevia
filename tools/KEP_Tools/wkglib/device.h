/******************************************************************************
 device.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once
#include <d3d.h>
#include "singleton.h"
#include "texture.h"
#include "renderstate.h"

namespace wkg
{

class Matrix44;
class Vector4;

namespace engine
{

class Device : public Singleton<Device>, public IDirect3DDevice9
{
public:
	static RenderState *m_rs;
	static D3DCAPS9 m_caps;
	static LPDIRECT3D9 m_d3d;

	Device()
	{
		m_d3d = 0;
		m_rs = new RenderState();
	}

	~Device()
	{
		if(m_d3d)
			m_d3d->Release();
		m_d3d = 0;

		delete m_rs;
		m_rs = 0;
	}

	void ForceDefaultRenderState();
	const RenderState& GetRenderState() const { return *m_rs; }

	void SetRenderState(const RenderState& rs);
	void RestoreRenderState(const RenderState &rs);
	void CaptureRenderState(RenderState &rs);

	static bool Init(Device **dev, HWND hwnd, bool windowed, int32 w = 0, int32 h = 0);
	static bool Init(Device **dev, HWND hwnd, bool windowed, int32 w, int32 h, IDirect3DDevice9 *d);

	HRESULT SetTexture(int32 stage, IDirect3DTexture9 *t);
	HRESULT SetTexture(int32 stage, const Texture *t);

	HRESULT SetTransform(D3DTRANSFORMSTATETYPE state, const Matrix44 *m);
	HRESULT SetTransform(D3DTRANSFORMSTATETYPE state, const D3DXMATRIX *m);

	HRESULT GetTransform(D3DTRANSFORMSTATETYPE state, Matrix44 *m);

	HRESULT SetVertexShaderConstantF(uint32 idx, const Matrix44 *m, uint32 n)
	{
		return ((IDirect3DDevice9 *)this)->SetVertexShaderConstantF(idx, (float *)m, n);
	}

	HRESULT SetVertexShaderConstantF(uint32 idx, const Vector4 *v, uint32 n)
	{
		return ((IDirect3DDevice9 *)this)->SetVertexShaderConstantF(idx, (float *)v, n);
	}

	HRESULT SetVertexShaderConstantF(uint32 idx, const float *v, uint32 n)
	{
		return ((IDirect3DDevice9 *)this)->SetVertexShaderConstantF(idx, v, n);
	}

};

}

}


