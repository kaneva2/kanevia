/******************************************************************************
 vertexshader.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

namespace engine
{

class VertexShader
{
	public:
		VertexShader();
		virtual ~VertexShader();

		virtual bool Init(const char *vsoFilename, D3DVERTEXELEMENT9 *e = 0);
		virtual bool Init(uint32 fvf);

		virtual bool CreateShader();
		virtual bool LoadFunction(const char *vsoFilename);

		virtual bool CreateDeclaration(D3DVERTEXELEMENT9 *e);

		virtual void LoadConstants() {};

		virtual bool Activate();

	protected:
		uint32 *m_function;

		bool m_is_fvf;
		uint32 m_fvf;

		IDirect3DVertexShader9 *m_handle;
		IDirect3DVertexDeclaration9 *m_declaration;

};

}

}


