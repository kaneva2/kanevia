/******************************************************************************
 wkgd3dwrap.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg 
{
	class Matrix44;
	class Vector3;
};

inline wkg::Matrix44* D3DXMatrixPerspectiveFovLH(wkg::Matrix44* pOut, real fovy, real aspect, real zn, real zf)								{ return (wkg::Matrix44*)D3DXMatrixPerspectiveFovLH((D3DXMATRIX*)pOut, (float)fovy, (float)aspect, (float)zn, (float)zf); }
inline wkg::Matrix44* D3DXMatrixLookAtLH(wkg::Matrix44* pOut, const wkg::Vector3* pEye, const wkg::Vector3* pAt, const wkg::Vector3* pUp)	{ return (wkg::Matrix44*)D3DXMatrixLookAtLH((D3DXMATRIX*)pOut, (D3DXVECTOR3*)pEye, (D3DXVECTOR3*)pAt, (D3DXVECTOR3*)pUp); }


