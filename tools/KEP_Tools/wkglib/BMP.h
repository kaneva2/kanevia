/******************************************************************************
 BMP.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg 
{

class BMP  
{
	public:
		BMP();
		virtual ~BMP();

		// creates an uninitialized bitmap of the given width and height
		bool Create(int32 width, int32 height);

		int32 GetWidth() const;
		int32 GetHeight() const;
		uint8 *GetBits();
		uint8 *GetBits() const;

		bool Load(const TCHAR* filename);
		bool Save(const TCHAR* filename) const;

	private:
		bool Read(HANDLE, uint8*, uint32);
		bool Write(HANDLE, uint8*, uint32) const;

	protected:
		uint8* rgb;
		int32 wid, hei;
};

inline int32 
BMP::GetWidth() const
{
	return wid;
}

inline int32 
BMP::GetHeight() const
{
	return hei;
}

inline uint8* 
BMP::GetBits()
{
	return rgb;
}

inline uint8* 
BMP::GetBits() const
{
	return rgb;
}

}
