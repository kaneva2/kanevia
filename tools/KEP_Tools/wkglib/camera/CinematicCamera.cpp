/******************************************************************************
 CinematicCamera.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "CinematicCamera.h"
#include "CoordFrame.h"

namespace wkg {

CoordFrame& CinematicCamera::GetCoordFrame(CoordFrame* frame) const
{
	real cx, sx, cy, sy;

	cx = cos(DEG_TO_RAD(tiltDegrees));
	cy = cos(DEG_TO_RAD(panDegrees));
	sx = sin(DEG_TO_RAD(tiltDegrees));
	sy = sin(DEG_TO_RAD(panDegrees));

	// unrotated (starting) direction points toward positive z axis <0, 0, 1>
	// rotation matrices are applied in RotX*RotY order (i.e. tilt, pan)
	// instead of actually building & concatenating the matrices just to pull out the 3rd row (since x & y of the starting direction is 0)
	// ...just calculate the result directly
	//	similarly for the 'up' direction (starts pointing <0, 1, 0>...will pull out the 2nd row)
	//
	frame->SetLookDir(pos, Vector3(cx*sy, -sx, cx*cy), Vector3(sx*sy, cx, sx*cy));

	return *frame;
}

void CinematicCamera::Truck(real distanceUnits)
{
	CoordFrameLH frame;

	pos += GetCoordFrame(&frame).GetXAxis() * distanceUnits;
}

void CinematicCamera::Dolly(real distanceUnits)
{
	CoordFrameLH frame;

	pos += GetCoordFrame(&frame).GetZAxis() * distanceUnits;
}

}
