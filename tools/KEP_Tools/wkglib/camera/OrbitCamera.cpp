/******************************************************************************
 OrbitCamera.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "OrbitCamera.h"
#include "CoordFrame.h"
#include "Units.h"

namespace wkg {

OrbitCamera::OrbitCamera()
	: camDist(Units::FromFeet(10.0f)), camRotX(0.0f), camRotY(0.0f),
	camZoomSpeed(0.0f), camRotXSpeed(0.0f), camRotYSpeed(0.0f)
{
	SetAngularSpeed(5.0f, 180.0f, 5.0f);
	SetZoomSpeed(0.5f, 18.0f, 0.5f);
	SetZoomLimits(0.5f, 100.0f);

	target.Set(0.0f, 0.0f, 0.0f);
	pos.Set(target.x, target.y, target.z - camDist);
	up.Set(0.0f, 1.0f, 0.0f);
}

void 
OrbitCamera::SetDistance(real units)
{
	camDist = units;

	if (camDist < Units::FromFeet(zoomMin))
		camDist = Units::FromFeet(zoomMin);
	else if (camDist > Units::FromFeet(zoomMax))
		camDist = Units::FromFeet(zoomMax);

	pos.z = target.z - camDist;
}

void 
OrbitCamera::Update(int32 msElapsed, uint8 motion)
{
#	define UPDATE_SPEED(var, pos, neg, amt, range)		\
		if (motion & (pos | neg))						\
		{												\
			if (motion & pos)							\
				var += amt;								\
			if (motion & neg)							\
				var -= amt;								\
			var = CLAMP(var, -range, range);			\
		}
#	define UPDATE_CAM(var, pos, neg, vel, amt, drag)	\
		if (vel)										\
		{												\
			var += amt;									\
			if (!(motion & (pos | neg)))				\
			{											\
				if (vel > 0.0f)							\
					vel = MAX(0.0f, vel - drag);		\
				else									\
					vel = MIN(0.0f, vel + drag);		\
			}											\
		}
	real cx, cy, sx, sy;

	UPDATE_SPEED(camRotXSpeed, Up, Down, angSpeed, maxAngSpeed);
	UPDATE_SPEED(camRotYSpeed, Left, Right, angSpeed, maxAngSpeed);
	UPDATE_SPEED(camZoomSpeed, ZoomOut, ZoomIn, zoomSpeed, maxZoomSpeed);

	UPDATE_CAM(camRotX, Up, Down, camRotXSpeed, DEG_TO_RAD(camRotXSpeed * msElapsed / 1000.0f), angDrag);
	UPDATE_CAM(camRotY, Left, Right, camRotYSpeed, DEG_TO_RAD(camRotYSpeed * msElapsed / 1000.0f), angDrag);
	UPDATE_CAM(camDist, ZoomOut, ZoomIn, camZoomSpeed, Units::FromFeet(camZoomSpeed * msElapsed / 1000.0f), zoomDrag);

	if (camDist < Units::FromFeet(zoomMin))
	{
		camDist = Units::FromFeet(zoomMin);
		camZoomSpeed = 0.0f;
	}
	else if (camDist > Units::FromFeet(zoomMax))
	{
		camDist = Units::FromFeet(zoomMax);
		camZoomSpeed = 0.0f;
	}

	// unrotated (starting) position is [0,0,-camDist]
	// rotation matrices are applied in RotX*RotY order
	// instead of actually building & concatenating the matrices just to pull out the 3rd row (since x & y of the starting position is 0)
	// ...just calculate the result directly
	//	similarly for the 'up' direction (starts pointing <0, 1, 0>...will pull out the 2nd row)
	//
	cx = cos(camRotX);
	cy = cos(camRotY);
	sx = sin(camRotX);
	sy = sin(camRotY);
	pos.Set(-camDist * cx*sy, -camDist * -sx, -camDist * cx*cy);
	pos += target;
	up.Set(sx*sy, cx, sx*cy);

#	undef UPDATE_SPEED
#	undef UPDATE_CAM
}

CoordFrame& 
OrbitCamera::GetCoordFrame(CoordFrame* frame) const
{
	frame->SetLookAt(pos, target, up);
	return *frame;
}

}