/******************************************************************************
 CinematicCamera.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Camera.h"

namespace wkg 
{

class CinematicCamera : public Camera
{
	friend class CameraConvert;

	public:
		CinematicCamera();					// starts level facing toward world's z-axis

		void Pan(real degrees);				// rotate direction the camera points about y-axis...rotates clockwise when sitting at <0, 10, 0> looking at the origin (looking top-down)
		void Tilt(real degrees);			// rotate direction the camera points about x-axis...rotates clockwise when sitting at <10, 0, 0> looking at the origin (looking at the right side of an object)
		void Truck(real distanceUnits);		// move perpendicular to the direction the camera points (along the camera's x-axis)...pos moves to the camera's right
		void Dolly(real distanceUnits);		// move in the direction the camera points (along the camera's z-axis)...pos moves forward
		void Crane(real distanceUnits);		// direction remains same, move the camera along the WORLD's y-axis...pos moves up
		void Zoom(real fovXDegrees);		// camera remains stationary; more (pos amt) or less (neg amt) of the scene becomes visible

		void PanLeft(real degrees);			// amt always positive...aim camera toward the left (position unchanged...rotate about y-axis)
		void PanRight(real degrees);		// amt always positive...aim camera toward the right (position unchanged...rotate about y-axis)
		void TiltUp(real degrees);			// amt always positive...aim camera toward the sky (position unchanged...rotate about z-axis)
		void TiltDown(real degrees);		// amt always positive...aim camera toward the ground (position unchanged...rotate about z-axis)
		void TruckLeft(real distUnits);		// amt always positive...move to the left (direction unchanged)
		void TruckRight(real distUnits);	// amt always positive...move to the right (direction unchanged)
		void DollyIn(real distUnits);		// amt always positive...move toward the subject
		void DollyOut(real distUnits);		// amt always positive...move away from the subject
		void CraneUp(real distUnits);		// amt always positive...direction remains unchanged
		void CraneDown(real distUnits);		// amt always positive...direction remains unchanged
		void ZoomIn(real fovXDegrees);		// amt always positive...less of the scene becomes visible (the subject fills more of the frame)
		void ZoomOut(real fovXDegrees);		// amt always positive...more of the scene becomes visible (the subject fills less of the frame)

		CoordFrame& GetCoordFrame(CoordFrame*) const;

	protected:
		real panDegrees, tiltDegrees;
};

inline CinematicCamera::CinematicCamera()
	: panDegrees(0.0f), tiltDegrees(0.0f)
{
}

inline void CinematicCamera::Pan(real degrees)
{
	panDegrees += degrees;
}

inline void CinematicCamera::Tilt(real degrees)
{
	tiltDegrees += degrees;
}

inline void CinematicCamera::Crane(real distanceUnits)
{
	pos.y += distanceUnits;
}

inline void CinematicCamera::Zoom(real FOVXDegrees)
{
	SetFOVXDegrees(GetFOVXDegrees() + FOVXDegrees); 
}

inline void CinematicCamera::PanLeft(real degrees)
{
	if (degrees > 0.0f)
		Pan(-degrees);
}

inline void CinematicCamera::PanRight(real degrees)
{
	if (degrees > 0.0f)
		Pan(degrees);
}

inline void CinematicCamera::TiltUp(real degrees)
{
	if (degrees > 0.0f)
		Tilt(-degrees);
}

inline void CinematicCamera::TiltDown(real degrees)
{
	if (degrees > 0.0f)
		Tilt(degrees);
}

inline void CinematicCamera::TruckLeft(real distUnits)
{
	if (distUnits > 0.0f)
		Truck(-distUnits);
}

inline void CinematicCamera::TruckRight(real distUnits)
{
	if (distUnits > 0.0f)
		Truck(distUnits);
}

inline void CinematicCamera::DollyIn(real distUnits)
{
	if (distUnits > 0.0f)
		Dolly(distUnits);
}

inline void CinematicCamera::DollyOut(real distUnits)
{
	if (distUnits > 0.0f)
		Dolly(-distUnits);
}

inline void CinematicCamera::CraneUp(real distUnits)
{
	if (distUnits > 0.0f)
		Crane(distUnits);
}

inline void CinematicCamera::CraneDown(real distUnits)
{
	if (distUnits > 0.0f)
		Crane(-distUnits);
}

inline void CinematicCamera::ZoomIn(real fovXDegrees)
{
	if (fovXDegrees > 0.0f)
		Zoom(-fovXDegrees);
}

inline void CinematicCamera::ZoomOut(real fovXDegrees)
{
	if (fovXDegrees > 0.0f)
		Zoom(fovXDegrees);
}

}