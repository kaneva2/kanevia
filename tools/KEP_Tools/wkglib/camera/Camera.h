/******************************************************************************
 Camera.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg 
{

class CoordFrame;

class Camera
{
	public:
		Camera();
		virtual ~Camera();

		virtual void Set(real fovXDegrees, real aspect, real nearUnits, real farUnits);
		virtual void SetFOVXDegrees(real fovXDegrees);		// also modifies fovY based upon aspect
		virtual void SetFOVYDegrees(real fovYDegrees);		// also modifies fovY based upon aspect
		virtual void SetNearDistance(real nearUnits);
		virtual void SetFarDistance(real farUnits);
		virtual void SetPosition(const Vector3&);	// in wkg::Units

		virtual real GetFOVXDegrees() const;
		virtual real GetFOVYDegrees() const;
		virtual real GetAspectRatio() const;
		virtual real GetNearDistance() const;			// in wkg::Units
		virtual real GetFarDistance() const;			// in wkg::Units
		virtual const Vector3& GetPosition() const;		// in wkg::Units

		virtual void OffsetPosition(const Vector3& offsetUnits);	// direction remains same, move camera a relative amount in an arbitrary direction (intended for special-purpose effects...like tracking along a spline or moving "forward" with the camera panned to the side)

		virtual CoordFrame& GetCoordFrame(CoordFrame*) const = 0;

	protected:
		// orientation is not stored in the base class to allow freedom in the subclasses
		//
		Vector3 pos;
		real fovXDeg, fovYDeg, aspect;
		real nearUnits, farUnits;
};

inline Camera::Camera()
	: pos(0.0f, 0.0f, 0.0f),
	fovXDeg(45.0f), fovYDeg(45.0f), aspect(1.0f),
	nearUnits(10.0f), farUnits(100.0f)
{
}

inline Camera::~Camera()
{
}

inline void Camera::SetFOVXDegrees(real FOVXDegrees)
{
	ASSERT(!EQUAL(aspect, 0.0f));
	fovXDeg = FOVXDegrees;
	fovYDeg = RAD_TO_DEG(real(2.0f * atan(tan(DEG_TO_RAD(fovXDeg) * 0.5f) / aspect)));
}

inline void Camera::SetFOVYDegrees(real FOVYDegrees)
{
	ASSERT(!EQUAL(aspect, 0.0f));
	fovYDeg = FOVYDegrees;
	fovXDeg = RAD_TO_DEG(real(2.0f * atan(tan(DEG_TO_RAD(fovYDeg) * 0.5f) * aspect)));
}

inline void Camera::SetNearDistance(real NearUnits)
{
	nearUnits = NearUnits;
}

inline void Camera::SetFarDistance(real FarUnits)
{
	farUnits = FarUnits;
}

inline void Camera::Set(real FOVXDegrees, real Aspect, real NearUnits, real FarUnits)
{
	aspect = Aspect;
	SetFOVXDegrees(FOVXDegrees);
	SetNearDistance(NearUnits);
	SetFarDistance(FarUnits);
}

inline void Camera::SetPosition(const Vector3& Pos)
{
	pos = Pos;
}

inline real Camera::GetFOVXDegrees() const
{
	return fovXDeg;
}

inline real Camera::GetFOVYDegrees() const
{
	return fovYDeg;
}

inline real Camera::GetAspectRatio() const
{
	return aspect;
}

inline real Camera::GetNearDistance() const
{
	return nearUnits;
}

inline real Camera::GetFarDistance() const
{
	return farUnits;
}

inline const Vector3& Camera::GetPosition() const
{
	return pos;
}

inline void Camera::OffsetPosition(const Vector3& offsetUnits)
{
	pos += offsetUnits;
}

}