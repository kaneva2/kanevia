/******************************************************************************
 CameraConvert.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// Just used to convert between Orbit to Cinematic

namespace wkg 
{

class CinematicCamera;
class OrbitCamera;

class CameraConvert
{
	public:
		static void OrbitToCinematic(const OrbitCamera&, CinematicCamera&);
		// sets Camera members and OrbitCamera rotX,rotY (assumes the remaining OrbitCamera members are valid or will be set after this call, as necessary...this call doesn't have the extra target, speed, zoom, etc info)
		static void CinematicToOrbit(const CinematicCamera&, OrbitCamera&);
};

}
