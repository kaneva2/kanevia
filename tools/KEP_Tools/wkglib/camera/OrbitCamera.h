/******************************************************************************
 OrbitCamera.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "TargetCamera.h"

namespace wkg 
{

class OrbitCamera : public TargetCamera
{
	friend class CameraConvert;

	public:
		OrbitCamera();

		void SetAngularSpeed(real degPerSec, real maxDegPerSec = 180.0f, real dragDegPerSec = 5.0f);
		void SetZoomSpeed(real feetPerSec, real maxFeetPerSec = 18.0f, real dragFeetPerSec = 0.5f);
		void SetZoomLimits(real minFeet, real maxFeet);

		void OrbitAround(const Vector3& target);
		void OrbitAround(real targX, real targY, real targZ);

		real GetDistance() const;			// wkg::Units
		void SetDistance(real units);		// wkg::Units

		enum Motion
		{
			Unchanged	= 0x00,
			Right		= 0x01,
			Left		= 0x02,
			Up			= 0x04,
			Down		= 0x08,
			ZoomIn		= 0x10,
			ZoomOut		= 0x20
		};

		void Update(int32 msElapsed, uint8 motion /* flags from above */);

		CoordFrame& GetCoordFrame(CoordFrame*) const;

	protected:
		real camDist, camRotX, camRotY;
		real camZoomSpeed, camRotXSpeed, camRotYSpeed;
		real angSpeed, maxAngSpeed, angDrag;
		real zoomSpeed, maxZoomSpeed, zoomDrag;
		real zoomMin, zoomMax;
		Vector3 up;
};

inline void 
OrbitCamera::SetAngularSpeed(real degPerSec, real maxDegPerSec, real dragDegPerSec)
{
	ASSERT(degPerSec >= 0.0f && maxDegPerSec > degPerSec && dragDegPerSec >= 0.0f);
	angSpeed = degPerSec;
	maxAngSpeed = maxDegPerSec;
	angDrag = dragDegPerSec;
}

inline void 
OrbitCamera::SetZoomSpeed(real feetPerSec, real maxFeetPerSec, real dragFeetPerSec)
{
	ASSERT(feetPerSec >= 0.0f && maxFeetPerSec > feetPerSec && dragFeetPerSec >= 0.0f);
	zoomSpeed = feetPerSec;
	maxZoomSpeed = maxFeetPerSec;
	zoomDrag = dragFeetPerSec;
}

inline void 
OrbitCamera::SetZoomLimits(real minFeet, real maxFeet)
{
	ASSERT(minFeet >= 0.0f && maxFeet > minFeet);
	zoomMin = minFeet;
	zoomMax = maxFeet;
}

inline void 
OrbitCamera::OrbitAround(const Vector3& Target)
{
	SetTarget(Target);
}

inline void 
OrbitCamera::OrbitAround(real targX, real targY, real targZ)
{
	target.Set(targX, targY, targZ);
}

inline real 
OrbitCamera::GetDistance() const
{
	return camDist;
}

}