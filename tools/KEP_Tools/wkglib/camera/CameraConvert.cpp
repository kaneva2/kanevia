/******************************************************************************
 CameraConvert.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "CameraConvert.h"
#include "OrbitCamera.h"
#include "CinematicCamera.h"
#include "CoordFrame.h"

namespace wkg {

void CameraConvert::OrbitToCinematic(const OrbitCamera& src, CinematicCamera& dest)
{
	*(Camera*)&dest = *(Camera*)&src;

	dest.tiltDegrees = src.camRotX;
	dest.panDegrees = src.camRotY;
}

void CameraConvert::CinematicToOrbit(const CinematicCamera& src, OrbitCamera& dest)
{
	CoordFrameLH frame;

	*(Camera*)&dest = *(Camera*)&src;

	dest.camRotX = src.tiltDegrees;
	dest.camRotY = src.panDegrees;

	dest.GetCoordFrame(&frame);
	dest.target = dest.pos + frame.GetZAxis() * dest.camDist;
}

}
