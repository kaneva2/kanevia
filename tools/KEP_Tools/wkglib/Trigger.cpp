/******************************************************************************
 Trigger.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include <string>
#include "Trigger.h"

namespace wkg
{

namespace state_machine
{

Trigger::Trigger(std::string name)
{
	m_name = name;
}

Trigger::~Trigger()
{

}

std::string
Trigger::GetName()
{
	return m_name;
}

}

}