/******************************************************************************
 Message.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

// Totally generic messages that are passed between objects;
//  usually, higher level game objects
// Derive objects from Messengers in order to send and receive messages

#pragma once

#include <string>

namespace wkg
{

class Message
{

public:
	Message(std::string name, int32 id_from, int32 id_to);
	virtual ~Message();

	std::string GetName()				{	return m_name;			}
	int32 GetIDFrom()					{	return m_id_from;		}
	int32 GetIDTo()						{	return m_id_to;			}

	void SetName(std::string name)		{	m_name = name;			}
	void SetIDFrom(int32 id_from)		{	m_id_from = id_from;	}
	void SetIDTo(int32 id_to)			{	m_id_to = id_to;		}

protected:
	std::string m_name;
	int32 m_id_from;
	int32 m_id_to;
};

}

