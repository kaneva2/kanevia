/******************************************************************************
 units.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "units.h"

namespace wkg 
{

const real Units::UNITS_TO_FOOT = 1.0f;

}