/******************************************************************************
 timedupdatable.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "timedupdatable.h"

namespace wkg
{

TimedUpdatable::TimedUpdatableNotifyVector TimedUpdatable::m_map;
bool TimedUpdatable::m_changed = false;

TimedUpdatable::TimedUpdatable() :
	m_running(false), m_ms(0), m_accumulator(0)
{
}

TimedUpdatable::~TimedUpdatable()
{
 	if(m_running)
		StopUpdates();
}

void
TimedUpdatable::StartUpdates()
{
	if(!m_running)
	{
		m_map.push_back(this);
		m_running = true;
	}
}

void
TimedUpdatable::StopUpdates()
{
	TimedUpdatableNotifyIterator ti = m_map.begin();
	TimedUpdatableNotifyIterator te = m_map.end();

	for(; ti < te; ti++)
	{
		if((*ti) == this)
		{
			m_map.erase(ti);
			break;
		}
	}

	m_changed = true;
	m_running = false;
}

void
TimedUpdatable::Update(int32 ms)
{
	TimedUpdate(ms);
}

void
TimedUpdatable::CallAllTimers(int32 ms)
{
	for(uint32 i = 0; i < m_map.size();)
	{
		m_changed = false;

		m_map[i]->Update(ms);

		if(!m_changed) 
			i++;
	}
}

}

