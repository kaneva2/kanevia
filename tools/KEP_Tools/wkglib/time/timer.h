/******************************************************************************
 timer.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Singleton.h"

namespace wkg 
{

class Timer : public Singleton<Timer>
{
	public:
		Timer();

		void Reset();

		// since last Reset() (milliseconds)
		uint32 GetElapsed() const;

		// since birth (milliseconds)
		uint32 GetAge() const;

		void Pause();

		// returns time elapsed since pause (milliseconds)
		uint32 Unpause();

		bool IsPaused() const;
		uint32 GetElapsedSincePause() const;

	protected:
		__int64 m_timer_frequency;
		__int64 m_last_refresh;

		uint32 m_elapsed;
		uint32 m_age;
		bool   m_paused;
};

inline uint32 
Timer::GetElapsed() const
{
	return m_elapsed;
}

inline uint32 
Timer::GetAge() const
{
	return m_age;
}

inline bool 
Timer::IsPaused() const
{
	return m_paused;
}

}