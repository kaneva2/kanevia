/******************************************************************************
 timedupdatable.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <vector>

namespace wkg
{

class TimedUpdatable
{
public:
	TimedUpdatable();
	virtual ~TimedUpdatable();

	void StartUpdates();
	void StopUpdates();

	virtual void TimedUpdate(int32 mspf) = 0;

	static void CallAllTimers(int32 ms);

	bool IsRunning() { return m_running; }
	
protected:
	void Update(int32 ms);

protected:
	typedef std::vector<TimedUpdatable *> TimedUpdatableNotifyVector;
	typedef TimedUpdatableNotifyVector::iterator TimedUpdatableNotifyIterator;

protected:
	static TimedUpdatableNotifyVector m_map;
	static bool m_changed;

	int32 m_ms;
	int32 m_accumulator;
	bool m_running;

};


}

