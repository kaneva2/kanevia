/******************************************************************************
 FrameRate.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Singleton.h"

namespace wkg {

class FrameRate : public Singleton<FrameRate>
{
	public:
		FrameRate();

		real GetFPS();
		void Update();
		
	protected:
		uint32 mFrameCount;
		uint32 mElapsed;
		real mFPS;
};

inline real 
FrameRate::GetFPS()
{
	return mFPS;
}


}