/******************************************************************************
 wkgfunc.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#undef MIN
#undef MAX
#undef ABS
#undef SQR
#undef CLAMP
#undef LTERP
#undef EQUAL
#undef DEG_TO_RAD
#undef RAD_TO_DEG

namespace wkg 
{

	template<class T> inline T MIN(T a, T b)					{ return a < b ? a : b; };
	template<class T> inline T MIN3(T a, T b, T c)				{ return MIN(MIN(a, b), c); }
	template<class T> inline T MAX(T a, T b)					{ return a > b ? a : b; };
	template<class T> inline T MAX3(T a, T b, T c)				{ return MAX(MAX(a, b), c); }
	template<class T> inline T ABS(T a)							{ return a >= 0 ? a : -a; };
	template<class T> inline T SQR(T x)							{ return x * x; };
	template<class T> inline T CLAMP(T x, T lo, T hi)			{ return MIN(MAX(x, lo), hi); }
	template<class T> inline T LTERP(T f1, T f2, real t)		{ return f1 + (f2 - f1) * t; }
	template<class T> inline T DEG_TO_RAD(T x)					{ return (x * WKG_PI) / 180.0f; }
	template<class T> inline T RAD_TO_DEG(T x)					{ return (x * 180.0f) / WKG_PI; }

#ifdef SINGLEPRECISION
	template<> inline real ABS(real r)							{ union rl { real f; uint32 ul; }; ((rl*)&r)->ul &= 0x7fffffffUL; return r;	}
#else
	template<> inline real ABS(real r)							{ union rl { real d; struct _ul { uint32 lo, hi; } ul; }; ((rl*)&r)->ul.hi &= 0x7fffffffUL; return r; }
#endif

	// Helper function to stuff a 32-bit float value into a 32-bit uint32 argument w/o conversion
	//
	inline uint32 FtoDW(float f)								{ return *((uint32*)&f); }

	// this must come after any specializations for ABS
	//
	template<class T> inline bool EQUAL(T f1, T f2, real tolerance = TOLERANCEf) { return ABS(f1 - f2) <= tolerance; }

	void memcpy(void* dest, const void* src, uint32 amt);
	void memset(void* dest, int32 val, uint32 amt);
	void memmove(void*, const void*, uint32);	// allows overlap
	void memzero(void*, uint32);

#ifndef NO_WKG_STRING
	int32 strlen(const char*);
	char* strcpy(char*, const char*);
	int32 strcmp(const char*, const char*);
	int32 stricmp(const char*, const char*);
#endif

	int32 atoi(const char*);
	real atof(const char*);

	real cos(real);
	real acos(real);
	real sin(real);
	real asin(real);
	real tan(real);
	real atan(real);
	real sqrt(real);
	double sqrt(double);

#ifndef SINGLEPRECISION
	// lots of code uses *float* constants...resolve the template ambiguities
	//
	inline real MIN(float a, real b)							{ return MIN(real(a), b); }
	inline real MAX(float a, real b)							{ return MAX(real(a), b); }
	inline real CLAMP(real x, float lo, float hi)				{ return CLAMP(x, real(lo), real(hi)); }
	inline bool EQUAL(real f1, float f2, real tolerance = TOLERANCEf) { return EQUAL(f1, real(f2), tolerance); }
#endif

	inline void MessageBox(const char *message)
	{
		::MessageBox(0, message, "Message", MB_OK);
	}

}
