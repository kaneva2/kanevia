@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=nvidia-texture-tools
SET SolutionToBuild="%ThisDir%project\vc15\nvtt.sln"
SET DebugConfiguration="debug (no cuda)"
SET ReleaseConfiguration="release (no cuda)"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (

	call %ScriptsDir%\xcopy.cmd %ThisDir%gnuwin32\lib\*.lib %LibDir%\3rdParty\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%gnuwin32\bin %BinDir%\3rdParty\ /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\nvconfig.h %IncludeDir%\nvconfig.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%src\nvcore\*.h %IncludeDir%\nvcore\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%src\nvcore\poshlib\*.h %IncludeDir%\nvcore\poshlib\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%src\nvimage\*.h %IncludeDir%\nvimage\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%src\nvmath\*.h %IncludeDir%\nvmath\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%src\nvtt\*.h %IncludeDir%\nvtt\ /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\nvcore.lib %LibDir%\Debug\nvcore.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\nvcore.pdb %LibDir%\Debug\nvcore.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\nvimage.lib %LibDir%\Debug\nvimage.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\nvimage.pdb %LibDir%\Debug\nvimage.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\nvmath.lib %LibDir%\Debug\nvmath.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\nvmath.pdb %LibDir%\Debug\nvmath.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\squish.lib %LibDir%\Debug\squish.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\squish.pdb %LibDir%\Debug\squish.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\lib\nvtt.lib %LibDir%\Debug\nvtt.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\bin\nvtt-d.dll %BinDir%\nvtt-d.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Debug.Win32\bin\nvtt-d.pdb %BinDir%\nvtt-d.pdb* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\nvcore.lib %LibDir%\Release\nvcore.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\nvcore.pdb %LibDir%\Release\nvcore.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\nvimage.lib %LibDir%\Release\nvimage.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\nvimage.pdb %LibDir%\Release\nvimage.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\nvmath.lib %LibDir%\Release\nvmath.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\nvmath.pdb %LibDir%\Release\nvmath.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\squish.lib %LibDir%\Release\squish.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\squish.pdb %LibDir%\Release\squish.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\lib\nvtt.lib %LibDir%\Release\nvtt.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\bin\nvtt.dll %BinDir%\nvtt.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%project\vc15\Release.Win32\bin\nvtt.pdb %BinDir%\nvtt.pdb* /Y
)
