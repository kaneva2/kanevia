The older release of NSIS 198 should be paired with the makensis.exe.old contained within this folder. It has logging turned on.

The new NSIS 2.2 replaces the 198 build. It has vista support. Install the 2.2 release and overlay the contents of nsis-2.22-log.zip to obtain install logging.
