@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=mysql++-3.0.8
SET SolutionToBuild="%ThisDir%vc2015\mysql++_vs2015.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"
SET PlatformToUse=win32

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\*.h %IncludeDir%\mysql++\ /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%vc2015\Debug\mysqlpp_d.lib %LibDir%\mysqlpp_d.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%vc2015\Debug\mysqlpp_d.dll %BinDir%\mysqlpp_d.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%vc2015\Debug\mysqlpp_d.pdb %BinDir%\mysqlpp_d.pdb* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%vc2015\Release\mysqlpp.lib %LibDir%\mysqlpp.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%vc2015\Release\mysqlpp.dll %BinDir%\mysqlpp.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%vc2015\Release\mysqlpp.pdb %BinDir%\mysqlpp.pdb* /Y
)
