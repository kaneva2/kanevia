@ECHO OFF
SETLOCAL
IF %1.==. GOTO Syntax
IF %2.==. GOTO Syntax

SET WorkFoldTmp=~push.WORKFOLD.TMP
SET WorkFoldTmp2=~push.WORKFOLD.TMP2
SET LuaVer=%1
SET BuildOutputDir=redist
SET TfsMainlineToolsCJSON=$/Mainline/Mainline/Source/Platform/Tools/%2
IF %3.==/deploy. SET TfsMainlineDeploy=$/Mainline/Mainline/Deploy
SET WORKDIR=%CD%

SET MasterList=(cjson_%LuaVer%.dll, cjson_%LuaVer%.pdb, cjson_%LuaVer%.lib, cjson_%LuaVer%d.dll, cjson_%LuaVer%d.pdb, cjson_%LuaVer%d.lib)

IF NOT EXIST %BuildOutputDir% GOTO MissingBuildDir

FOR %%a IN %MasterList% DO (
	IF NOT EXIST %WORKDIR%\%BuildOutputDir%\%%a SET MissingBuildOutput=%%a && GOTO MissingBuilds
)

SET TfsServerPath=%TfsMainlineToolsCJSON%
tf workfold %TfsServerPath% > %WorkFoldTmp%
IF ERRORLEVEL 1 GOTO NoWorkFold
tail -n 1 %WorkFoldTmp% | sed "s/.*: //" > %WorkFoldTmp2%
DEL %WorkFoldTmp%
SET /P MainlineToolsCJSON=< %WorkFoldTmp2%
DEL %WorkFoldTmp2%

IF %TfsMainlineDeploy%.==. GOTO NoDeploy
SET TfsServerPath=%TfsMainlineDeploy%
tf workfold %TfsServerPath% > %WorkFoldTmp%
IF ERRORLEVEL 1 GOTO NoWorkFold
tail -n 1 %WorkFoldTmp% | sed "s/.*: //" > %WorkFoldTmp2%
DEL %WorkFoldTmp%
SET /P MainlineDeploy=< %WorkFoldTmp2%
DEL %WorkFoldTmp2%
:NoDeploy

ECHO ==================================================
ECHO === Build  : %BuildOutputDir%
ECHO === Push To: %TfsMainlineToolsCJSON%
IF NOT %TfsMainlineDeploy%.==. ECHO === Also Deploy To   : %MainlineDeploy%
ECHO ==================================================
ECHO.

ECHO === Push libraries to %MainlineToolsCJSON%

PUSHD %MainlineToolsCJSON%
FOR %%a IN %MasterList% DO CALL :PushOneFile "%WORKDIR%\%BuildOutputDir%" %%a
POPD

IF %TfsMainlineDeploy%.==. GOTO SkipDeployFolders

ECHO === Deploy to %MainlineDeploy%\GameContent\bin

PUSHD %MainlineDeploy%\GameContent\bin
CALL :PushOneFile "%WORKDIR%\%BuildOutputDir%" cjson_%LuaVer%.dll
POPD

ECHO === Deploy to %MainlineDeploy%\GameContent\bind

PUSHD %MainlineDeploy%\GameContent\bind
CALL :PushOneFile "%WORKDIR%\%BuildOutputDir%" cjson_%LuaVer%d.dll
POPD

ECHO === Deploy to %MainlineDeploy%\PDBs

PUSHD %MainlineDeploy%\PDBs
CALL :PushOneFile "%WORKDIR%\%BuildOutputDir%" cjson_%LuaVer%.pdb
CALL :PushOneFile "%WORKDIR%\%BuildOutputDir%" cjson_%LuaVer%d.pdb
POPD

ECHO === Deploy to %MainlineDeploy%\GameContent\templates\Shared

PUSHD %MainlineDeploy%\GameContent\templates\Shared
CALL :PushOneFile "%WORKDIR%\%BuildOutputDir%" cjson_%LuaVer%.dll
CALL :PushOneFile "%WORKDIR%\%BuildOutputDir%" cjson_%LuaVer%d.dll
POPD

:SkipDeployFolders

GOTO :EOF

:MissingBuildDir
ECHO *** CJSON build output folder %BuildOutputDir% does not exist in current directory
GOTO Abort

:MissingBuilds
ECHO *** CJSON build output [%MissingBuildOutput%] are not found under %BuildOutputDir%
GOTO Abort

:NoWorkFold
ECHO *** Working directory for %TfsServerPath% is not defined
DEL %WorkFoldTmp% 2>NUL
GOTO Abort

:CheckOutFailed
ECHO *** Error checking out file from TFS
GOTO Abort

:CopyFailed
ECHO *** Error copying file
GOTO Abort

:Syntax
ECHO pushCJSONBuilds ^<LuaVersion^> ^<LibName^> [/deploy]
ECHO - ^<LuaVersion^>   - Lua build version used in output file names. E.g. 515 or 515co
ECHO - ^<LibName^>      - The cjson library folder under Platform\Tools where builds will be pushed to. E.g. lua-cjson-5.1.5 or lua-cjson-5.1.5-coco
ECHO - /deploy        - If specified, also deploy Lua binaries and symbols to folders under Mainline\Deploy
ECHO - Destination files will be checked out automatically before getting overwritten.
ECHO.
GOTO :EOF

:Abort
ECHO *** Abort...
PAUSE
EXIT /B 1

:PushOneFile
ECHO ^>^>^> %2
tf checkout %2 >NUL
IF ERRORLEVEL 1 GOTO CheckOutFailed
copy %1\%2 >NUL
IF ERRORLEVEL 1 GOTO CopyFailed
GOTO :EOF

