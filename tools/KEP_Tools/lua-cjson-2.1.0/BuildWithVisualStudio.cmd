@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=lua-cjson-5.1.5-coco
SET SolutionToBuild="%ThisDir%cjson_vs2015.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%lua_cjson.h %IncludeDir%\lua-cjson\ /I /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\cjson_515co.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\cjson_515co.dll %BinDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\cjson_515co.pdb %BinDir%\ /I /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\cjson_515cod.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\cjson_515cod.dll %BinDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\cjson_515cod.pdb %BinDir%\ /I /Y
)