Attribute VB_Name = "Sample8"
Option Explicit

'Win32 API
Private Declare Function lstrcmpi Lib "kernel32" Alias "lstrcmpiA" _
(ByVal lpString1 As Long, ByVal lpString2 As String) As Long

Public Declare Function CreateFile Lib "kernel32" _
   Alias "CreateFileA" _
  (ByVal lpFileName As String, _
   ByVal dwDesiredAccess As Long, _
   ByVal dwShareMode As Long, _
   ByVal lpSecurityAttributes As Long, _
   ByVal dwCreationDisposition As Long, _
   ByVal dwFlagsAndAttributes As Long, _
   ByVal hTemplateFile As Long) As Long

Public Declare Function ReadFile Lib "kernel32" _
  (ByVal hFile As Long, _
   lpBuffer As Any, _
   ByVal nNumberOfBytesToRead As Long, _
   lpNumberOfBytesRead As Long, _
   ByVal lpOverlapped As Long) As Long
   
Public Declare Function WriteFile Lib "kernel32" _
  (ByVal hFile As Long, _
   lpBuffer As Any, _
   ByVal nNumberOfBytesToWrite As Long, _
   lpNumberOfBytesWritten As Long, _
   ByVal lpOverlapped As Long) As Long
   
Public Declare Function CloseHandle Lib "kernel32" _
  (ByVal hFile As Long) As Long

'Win32 constants
Public Const INVALID_HANDLE_VALUE As Long = -1
Public Const GENERIC_WRITE As Long = &H40000000
Public Const GENERIC_READ As Long = &H80000000
Public Const FILE_SHARE_WRITE As Long = &H2
Public Const FILE_SHARE_READ As Long = &H1
Public Const OPEN_EXISTING As Long = &H3
Public Const CREATE_ALWAYS As Long = &H2

Const E_FAIL As Long = &H80004005
Const S_OK As Long = &H0

Public Function GlobalOnLoadExternalResourceHandler(ByVal lpszURL As Long, ByRef pStream As Long, ByVal hFPC As Long, ByVal lParam As Long) As Long
    
    GlobalOnLoadExternalResourceHandler = E_FAIL
    
    If lstrcmpi(lpszURL, "http://FLV/FlashVideo.flv") = 0 Then
        MainForm.UnpackerRef.Run pStream, MainForm.FLVPath
        GlobalOnLoadExternalResourceHandler = S_OK
    End If
    
End Function



