VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MainForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sample 5 - Convert SWF to bitmap"
   ClientHeight    =   6030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8700
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   402
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   580
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton SaveAsBitmapBtn 
      Caption         =   "Save as bitmap..."
      Height          =   375
      Left            =   3960
      TabIndex        =   5
      Top             =   5040
      Width           =   1695
   End
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   8040
      Top             =   4920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton PlayEmbeddedSWFBtn 
      Caption         =   "Play embedded SWF"
      Height          =   375
      Left            =   2040
      TabIndex        =   4
      Top             =   5040
      Width           =   1815
   End
   Begin VB.CommandButton OrderBtn 
      Caption         =   "Buy license"
      Height          =   375
      Left            =   6600
      TabIndex        =   3
      Top             =   5520
      Width           =   1935
   End
   Begin VB.CommandButton MoreAboutBtn 
      Caption         =   "More about F-IN-BOX"
      Height          =   375
      Left            =   4080
      TabIndex        =   2
      Top             =   5520
      Width           =   2415
   End
   Begin VB.CommandButton PlaySWFBtn 
      Caption         =   "Play SWF from file..."
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   5040
      Width           =   1815
   End
   Begin VB.Label PlaceHolder 
      Height          =   4680
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   8340
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Win32 API
Private Declare Function ShellExecute Lib "shell32" _
   Alias "ShellExecuteA" _
  (ByVal hWnd As Long, _
   ByVal lpOperation As String, _
   ByVal lpFile As String, _
   ByVal lpParameters As String, _
   ByVal lpDirectory As String, _
   ByVal nShowCmd As Long) As Long
   
Private Declare Function FPC_CreateWindow Lib "f_in_box.dll" _
   Alias "FPC_CreateWindowA" _
  (ByVal hFPC As Long, _
   ByVal dwExStyle As Long, _
   ByVal lpWindowName As String, _
   ByVal dwStyle As Long, _
   ByVal x As Long, ByVal y As Long, _
   ByVal nWidth As Long, _
   ByVal nHeight As Long, _
   ByVal hWndParent As Long, _
   ByVal hMenu As Long, _
   ByVal hInstance As Long, _
   lpParam As Any) As Long
   
Private Declare Function SendMessage Lib "user32" _
   Alias "SendMessageA" _
  (ByVal hWnd As Long, _
   ByVal wMsg As Long, _
   ByVal wParam As Long, _
   lParam As Any) As Long
   
Private Declare Function CLSIDFromString Lib "ole32" _
    (ByVal OleStringCLSID As Long, _
     myGUID As Any) As Long
     
Private Declare Function OleCreatePictureIndirect Lib "olepro32.dll" _
 (lpPictDesc As PICTDESC, _
  riid As GUID, _
  ByVal fPictureOwnsHandle As Long, _
  ipic As IPicture) As Long
 
'Struct GUID
Private Type GUID
   Data1 As Long
   Data2 As Integer
   Data3 As Integer
   Data4(7) As Byte
End Type

'Struct PICTDESC
Private Type PICTDESC
   cbSizeofstruct As Long
   PICTYPE As Long
   hBitmap As Long
   hPal As Long
   Reserved As Long
End Type

'Win32 constants
Private Const IID_IPicture As String = "{7BF80980-BF32-101A-8BBB-00AA00300CAB}"

Private Const WS_VISIBLE As Long = &H10000000
Private Const WS_CHILD As Long = &H40000000
Private Const WS_CLIPSIBLINGS As Long = &h4000000
Private Const WM_USER As Long = &H400
Private Const SW_SHOW = 5

Private Const S_OK As Long = &H0

'FlashPlayerControl API
Private Declare Function FPCIsFlashInstalled Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_LoadRegisteredOCX Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_UnloadCode Lib "f_in_box.dll" _
    (ByVal hFPC As Long) As Long

Private Declare Function FPCPutMovieFromResource Lib "f_in_box.dll" _
    Alias "FPCPutMovieFromResourceA" _
    (ByVal hwndFlashPlayerControl As Long, _
    ByVal hInstance As Long, _
    ByVal lpName As String, _
    ByVal lpType As String) As Boolean

'FlashPlayerControl constants
Private Const FPCM_FIRST As Long = WM_USER + &H1000
Private Const FPCM_PUT_MOVIE As Long = FPCM_FIRST + 317
Private Const FPCM_GET_FRAME_BITMAP As Long = FPCM_FIRST + 4

'Struct SFPCPutMovieA
Private Type SFPCPutMovieA
    lpszBuffer As String
    hr As Long
End Type

'Struct SFPCGetFrameBitmap
Private Type SFPCGetFrameBitmap
    hBitmap As Long
End Type

Dim hFPC As Long
Dim FlashPlayerControlWnd As Long
Dim MediaDir As String

Private Sub Form_Load()
    Initialize
End Sub

Private Sub Initialize()
    
    'Call the FindMediaDir procedure
    FindMediaDir
    
    'Prepare FlashPlayerControl
    If FPCIsFlashInstalled = 0 Then
        MsgBox ("The application needs Flash" + Chr$(13) + Chr$(10) + "Flash is not installed")
    End If
    
    hFPC = FPC_LoadRegisteredOCX

    If hFPC = 0 Then
        MsgBox ("FPC_LoadRegisteredOCX() failed")
        End
    End If
    
    'Create flash player window
    FlashPlayerControlWnd = FPC_CreateWindow( _
                                        hFPC, _
                                        0, _
                                        vbNullString, _
                                        WS_CHILD Or WS_VISIBLE Or WS_CLIPSIBLINGS, _
                                        PlaceHolder.Left, PlaceHolder.Top, _
                                        PlaceHolder.Width, _
                                        PlaceHolder.Height, _
                                        hWnd, 0, _
                                        App.hInstance, _
                                        ByVal 0)
                                        
    Call FPCPutMovieFromResource(FlashPlayerControlWnd, 0, "MOVIE", "SWF")
End Sub

Private Sub FindMediaDir()
    MediaDir = App.Path
End Sub

Private Sub Form_Terminate()
   If hFPC <> 0 Then
    FPC_UnloadCode hFPC
   End If
End Sub

Private Sub MoreAboutBtn_Click()
    ShellExecute hWnd, vbNullString, "http://www.f-in-box.com/dll/", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub OrderBtn_Click()
    ShellExecute hWnd, vbNullString, "http://www.f-in-box.com/dll/order.html", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub PlayEmbeddedSWFBtn_Click()
    Call FPCPutMovieFromResource(FlashPlayerControlWnd, 0, "MOVIE", "SWF")
End Sub

Private Sub PlaySWFBtn_Click()
    CommonDialog.DefaultExt = "swf"
    CommonDialog.FileName = ""
    CommonDialog.Filter = "Flash movie files (*.swf)|*.swf|All Files (*.*)|*.*||"
    CommonDialog.ShowOpen
    If (Len(CommonDialog.FileName) > 0) Then
        Dim FPCPutMovie As SFPCPutMovieA
        FPCPutMovie.lpszBuffer = CommonDialog.FileName
        Call SendMessage(FlashPlayerControlWnd, FPCM_PUT_MOVIE, 0&, FPCPutMovie)
    End If
End Sub

Private Sub SaveAsBitmapBtn_Click()
    Dim FPCGetFrameBitmap As SFPCGetFrameBitmap
    Call SendMessage(FlashPlayerControlWnd, FPCM_GET_FRAME_BITMAP, 0, FPCGetFrameBitmap)
    
    Dim PictureDesc As PICTDESC
    With PictureDesc
        .cbSizeofstruct = Len(PictureDesc)
        .PICTYPE = vbPicTypeBitmap
        .hBitmap = FPCGetFrameBitmap.hBitmap
        .hPal = 0
    End With
    
    Dim pPicture As IPicture
    Dim IPictureGUID As GUID
    
    If CLSIDFromString(StrPtr(IID_IPicture), IPictureGUID) = S_OK Then
        If OleCreatePictureIndirect(PictureDesc, IPictureGUID, False, pPicture) = S_OK Then
            CommonDialog.DefaultExt = "bmp"
            CommonDialog.FileName = ""
            CommonDialog.Filter = "Bitmap (*.bmp)|*.bmp|All Files (*.*)|*.*||"
            CommonDialog.ShowSave
            If (Len(CommonDialog.FileName) > 0) Then
                SavePicture pPicture, CommonDialog.FileName
            End If
        End If
    End If
End Sub
