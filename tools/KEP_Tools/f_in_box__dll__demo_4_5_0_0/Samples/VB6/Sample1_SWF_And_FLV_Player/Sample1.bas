Attribute VB_Name = "Sample1"
Option Explicit

'Win32 API
Private Declare Function lstrcmpi Lib "kernel32" Alias "lstrcmpiA" _
(ByVal lpString1 As Long, ByVal lpString2 As String) As Long

Private Declare Function lstrcmpiW Lib "kernel32" _
(ByVal lpString1 As Long, ByVal lpString2 As String) As Long

Private Declare Function lstrlenW Lib "kernel32" _
(ByVal lpString1 As Long) As Long

Private Declare Function lstrcpyW Lib "kernel32" _
(ByVal lpString1 As String, ByVal lpString2 As Long) As Long

Private Declare Function CreateFile Lib "kernel32" _
   Alias "CreateFileA" _
  (ByVal lpFileName As String, _
   ByVal dwDesiredAccess As Long, _
   ByVal dwShareMode As Long, _
   ByVal lpSecurityAttributes As Long, _
   ByVal dwCreationDisposition As Long, _
   ByVal dwFlagsAndAttributes As Long, _
   ByVal hTemplateFile As Long) As Long

Private Declare Function ReadFile Lib "kernel32" _
  (ByVal hFile As Long, _
   lpBuffer As Any, _
   ByVal nNumberOfBytesToRead As Long, _
   lpNumberOfBytesRead As Long, _
   ByVal lpOverlapped As Long) As Long
   
Private Declare Function CloseHandle Lib "kernel32" _
  (ByVal hFile As Long) As Long
  
' IStream helpers

Private Declare Function FPC_IStream_AddRef Lib "f_in_box.dll" _
    (ByVal pStream As Long) As Long
    
Private Declare Function FPC_IStream_Release Lib "f_in_box.dll" _
    (ByVal pStream As Long) As Long
    
Private Declare Function FPC_IStream_Write Lib "f_in_box.dll" _
    (ByVal pStream As Long, _
    pData As Any, _
    ByVal nSize As Long, _
    ByRef nWritten As Long) As Long
  
'Win32 constants
Const GENERIC_READ As Long = &H80000000
Const INVALID_HANDLE_VALUE As Long = -1
Const OPEN_EXISTING As Long = 3
Const FILE_SHARE_READ As Long = &H1

Const E_FAIL As Long = &H80004005
Const S_OK As Long = &H0

Public sNewURL As String

Public Function GlobalOnLoadExternalResourceHandler(ByVal lpszURL As Long, ByRef pStream As Long, ByVal hFPC As Long, ByVal lParam As Long) As Long
    
    GlobalOnLoadExternalResourceHandler = E_FAIL
    
    If lstrcmpi(lpszURL, "http://FLV/FlashVideo.flv") = 0 Then
    
        Dim hFile As Long
        hFile = CreateFile(MainForm.FLVPath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0)
        
        If Not hFile = INVALID_HANDLE_VALUE Then
            Const nBufferSize As Long = 1024
            Dim pBuffer(nBufferSize) As Byte
            Dim dwNumberOfBytesRead As Long
            Dim dwNumberOfBytesWritten As Long
            Dim bRes As Boolean

            Do While True
                bRes = ReadFile(hFile, pBuffer(0), nBufferSize, dwNumberOfBytesRead, 0)
                If Not bRes Or dwNumberOfBytesRead = 0 Then
                    GoTo HCloseHandle
                End If
                
                FPC_IStream_Write pStream, pBuffer(0), dwNumberOfBytesRead, dwNumberOfBytesWritten
            Loop
           
HCloseHandle:
            CloseHandle hFile
            GlobalOnLoadExternalResourceHandler = S_OK
        End If
        
    End If
    
End Function

Public Function GlobalPreProcessURLHandler(ByVal hFPC As Long, ByVal lParam As Long, ByRef URL As Long, ByRef bContinue As Long) As Long
    
'    Dim s As String
'    s = StrConv("http://flajector.com", vbUnicode)
'
'    If lstrcmpiW(URL, s) = 0 Then
'
'        sNewURL = "http://softanics.com"
'
'        URL = StrPtr(sNewURL)
'
'    End If

End Function

