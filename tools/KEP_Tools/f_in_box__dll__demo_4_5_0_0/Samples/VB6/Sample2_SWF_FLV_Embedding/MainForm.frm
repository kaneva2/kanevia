VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MainForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sample2 - SWF Embedding and FLV Embedding"
   ClientHeight    =   6030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8700
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   402
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   580
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   8040
      Top             =   4920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton PlayEmbeddedFLVBtn 
      Caption         =   "Play embedded FLV"
      Height          =   375
      Left            =   2040
      TabIndex        =   6
      Top             =   5520
      Width           =   1815
   End
   Begin VB.CommandButton OrderBtn 
      Caption         =   "Buy license"
      Height          =   375
      Left            =   6600
      TabIndex        =   5
      Top             =   5520
      Width           =   1935
   End
   Begin VB.CommandButton MoreAboutBtn 
      Caption         =   "More about F-IN-BOX"
      Height          =   375
      Left            =   4080
      TabIndex        =   4
      Top             =   5520
      Width           =   2415
   End
   Begin VB.CheckBox EnableSoundsCheck 
      Caption         =   "Enable sounds"
      Height          =   375
      Left            =   3240
      TabIndex        =   3
      Top             =   5040
      Value           =   1  'Checked
      Width           =   3015
   End
   Begin VB.CheckBox StandardMenuCheck 
      Caption         =   "Enable standard context flash menu"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   5040
      Width           =   3015
   End
   Begin VB.CommandButton PlayEmbeddedSWFBtn 
      Caption         =   "Play embedded SWF"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   5520
      Width           =   1815
   End
   Begin VB.Label PlaceHolder 
      Height          =   4680
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   8340
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Win32 API
Private Declare Function ShellExecute Lib "shell32" _
   Alias "ShellExecuteA" _
  (ByVal hWnd As Long, _
   ByVal lpOperation As String, _
   ByVal lpFile As String, _
   ByVal lpParameters As String, _
   ByVal lpDirectory As String, _
   ByVal nShowCmd As Long) As Long
   
Private Declare Function FPC_CreateWindow Lib "f_in_box.dll" _
   Alias "FPC_CreateWindowA" _
  (ByVal hFPC As Long, _
   ByVal dwExStyle As Long, _
   ByVal lpWindowName As String, _
   ByVal dwStyle As Long, _
   ByVal x As Long, ByVal y As Long, _
   ByVal nWidth As Long, _
   ByVal nHeight As Long, _
   ByVal hWndParent As Long, _
   ByVal hMenu As Long, _
   ByVal hInstance As Long, _
   lpParam As Any) As Long
   
Private Declare Function SendMessage Lib "user32" _
   Alias "SendMessageA" _
  (ByVal hWnd As Long, _
   ByVal wMsg As Long, _
   ByVal wParam As Long, _
   lParam As Any) As Long
   
Private Declare Function GetModuleHandle Lib "kernel32" _
    Alias "GetModuleHandleA" _
   (ByVal ApplicationFileName As String) As Integer
   
Private Declare Function FindResource Lib "kernel32" _
    Alias "FindResourceA" _
   (ByVal hInstance As Long, _
    ByVal lpName As String, _
    ByVal lpType As String) As Long
   
Private Declare Function LoadResource Lib "kernel32" _
   (ByVal hInstance As Long, _
    ByVal hResInfo As Long) As Long
   
Private Declare Function LockResource Lib "kernel32" _
   (ByVal hResData As Long) As Long
   
Private Declare Function SizeofResource Lib "kernel32" _
   (ByVal hInstance As Long, _
    ByVal hResInfo As Long) As Long
   
'Win32 constants
Private Const WS_VISIBLE As Long = &H10000000
Private Const WS_CHILD As Long = &H40000000
Private Const WS_CLIPSIBLINGS As Long = &h4000000
Private Const WM_USER As Long = &H400
Private Const SW_SHOW = 5

'FlashPlayerControl API
Private Declare Function FPCIsFlashInstalled Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_LoadRegisteredOCX Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_UnloadCode Lib "f_in_box.dll" _
    (ByVal hFPC As Long) As Long

Private Declare Function FPC_AddOnLoadExternalResourceHandler Lib "f_in_box.dll" _
    Alias "FPC_AddOnLoadExternalResourceHandlerA" _
    (ByVal hFPC As Long, _
     ByVal dwPtr As Long, _
     ByVal lParam As Long) As Long

Private Declare Function FPC_RemoveOnLoadExternalResourceHandler Lib "f_in_box.dll" _
    (ByVal hFPC As Long, _
     ByVal dwCookie As Long) As Long
     
Private Declare Sub FPC_EnableSound Lib "f_in_box.dll" _
    (ByVal hFPC As Long, _
     ByVal bEnable As Boolean)

'FlashPlayerControl constants
Private Const FPCM_FIRST As Long = WM_USER + &H1000
Private Const FPCM_PUTMOVIEFROMMEMORY As Long = FPCM_FIRST + 2
Private Const FPCM_PUT_STANDARD_MENU As Long = FPCM_FIRST + 5

'Struct SFPCPutMovieFromMemory
Private Type SFPCPutMovieFromMemory
    lpData As Long
    dwSize As Long
End Type

'Struct SFPCPutStandardMenu
Private Type SFPCPutStandardMenu
    StandardMenu As Boolean
End Type

Dim hFPC As Long
Dim nHandlerCookie As Long
Dim FlashPlayerControlWnd As Long
Dim MediaDir As String

Private Sub Form_Load()
    Initialize
End Sub

Private Sub Initialize()
    'Call the FindMediaDir procedure
    FindMediaDir
    
    'Prepare FlashPlayerControl
   
    If FPCIsFlashInstalled = 0 Then
        MsgBox ("The application needs Flash" + Chr$(13) + Chr$(10) + "Flash is not installed")
        End
    End If
    
    hFPC = FPC_LoadRegisteredOCX

    If hFPC = 0 Then
        MsgBox ("FPC_LoadRegisteredOCX() failed")
        End
    End If
   
    'Create flash player window
    FlashPlayerControlWnd = FPC_CreateWindow( _
                                        hFPC, _
                                        0, _
                                        vbNullString, _
                                        WS_CHILD Or WS_VISIBLE Or WS_CLIPSIBLINGS, _
                                        PlaceHolder.Left, PlaceHolder.Top, _
                                        PlaceHolder.Width, _
                                        PlaceHolder.Height, _
                                        hWnd, 0, _
                                        App.hInstance, _
                                        ByVal 0)
    
    'Play the movie file from resource
    Call PlayEmbeddedSWFBtn_Click
    
    'Set callback to FlashPlayerControl
    nHandlerCookie = FPC_AddOnLoadExternalResourceHandler(hFPC, AddressOf GlobalOnLoadExternalResourceHandler, 0)
End Sub

Private Sub FindMediaDir()
    MediaDir = App.Path
End Sub

Private Sub Form_Terminate()
   If hFPC <> 0 Then
    FPC_RemoveOnLoadExternalResourceHandler hFPC, nHandlerCookie
    FPC_UnloadCode hFPC
   End If
End Sub

Private Sub MoreAboutBtn_Click()
    ShellExecute hWnd, vbNullString, "http://www.f-in-box.com/dll/", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub OrderBtn_Click()
    ShellExecute hWnd, vbNullString, "http://www.f-in-box.com/dll/order.html", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub PlayEmbeddedFLVBtn_Click()
    Dim hModule As Long
    Dim hResInfo As Long
    Dim hResData As Long
    Dim lpData As Long
    Dim dwSize As Long
    
    hModule = GetModuleHandle(vbNullString)
    hResInfo = FindResource(hModule, "FLVPlayer", "SWF")
    hResData = LoadResource(hModule, hResInfo)
    lpData = LockResource(hResData)
    dwSize = SizeofResource(hModule, hResInfo)
    
    Dim FPCPutMovieFromMemory As SFPCPutMovieFromMemory
    FPCPutMovieFromMemory.lpData = lpData
    FPCPutMovieFromMemory.dwSize = dwSize
    
    Call SendMessage(FlashPlayerControlWnd, FPCM_PUTMOVIEFROMMEMORY, 0&, FPCPutMovieFromMemory)
End Sub

Private Sub PlayEmbeddedSWFBtn_Click()
    Dim hModule As Long
    Dim hResInfo As Long
    Dim hResData As Long
    Dim lpData As Long
    Dim dwSize As Long
    
    hModule = GetModuleHandle(vbNullString)
    hResInfo = FindResource(hModule, "MOVIE", "SWF")
    hResData = LoadResource(hModule, hResInfo)
    lpData = LockResource(hResData)
    dwSize = SizeofResource(hModule, hResInfo)
    
    Dim FPCPutMovieFromMemory As SFPCPutMovieFromMemory
    FPCPutMovieFromMemory.lpData = lpData
    FPCPutMovieFromMemory.dwSize = dwSize
    
    Call SendMessage(FlashPlayerControlWnd, FPCM_PUTMOVIEFROMMEMORY, 0&, FPCPutMovieFromMemory)
End Sub

Private Sub StandardMenuCheck_Click()
    Dim FPCPutStandardMenu As SFPCPutStandardMenu
    FPCPutStandardMenu.StandardMenu = StandardMenuCheck.Value = 1
    Call SendMessage(FlashPlayerControlWnd, FPCM_PUT_STANDARD_MENU, 0&, FPCPutStandardMenu)
End Sub

Private Sub EnableSoundsCheck_Click()
    FPC_EnableSound hFPC, EnableSoundsCheck.Value = 1
End Sub
