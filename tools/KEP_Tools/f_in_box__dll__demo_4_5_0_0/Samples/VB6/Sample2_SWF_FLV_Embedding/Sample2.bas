Attribute VB_Name = "Sample2"
Option Explicit

'Win32 API
Private Declare Function lstrcmpi Lib "kernel32" Alias "lstrcmpiA" _
(ByVal lpString1 As Long, ByVal lpString2 As String) As Long

Private Declare Function GetModuleHandle Lib "kernel32" _
    Alias "GetModuleHandleA" _
   (ByVal ApplicationFileName As String) As Integer
   
Private Declare Function FindResource Lib "kernel32" _
    Alias "FindResourceA" _
   (ByVal hInstance As Long, _
    ByVal lpName As String, _
    ByVal lpType As String) As Long
   
Private Declare Function LoadResource Lib "kernel32" _
   (ByVal hInstance As Long, _
    ByVal hResInfo As Long) As Long
   
Private Declare Function LockResource Lib "kernel32" _
   (ByVal hResData As Long) As Long
   
Private Declare Function SizeofResource Lib "kernel32" _
   (ByVal hInstance As Long, _
    ByVal hResInfo As Long) As Long
  
' IStream helpers

Private Declare Function FPC_IStream_AddRef Lib "f_in_box.dll" _
    (ByVal pStream As Long) As Long
    
Private Declare Function FPC_IStream_Release Lib "f_in_box.dll" _
    (ByVal pStream As Long) As Long
    
Private Declare Function FPC_IStream_Write Lib "f_in_box.dll" _
    (ByVal pStream As Long, _
    pData As Any, _
    ByVal nSize As Long, _
    ByRef nWritten As Long) As Long
  
'Win32 constants
Const E_FAIL As Long = &H80004005
Const S_OK As Long = &H0

Public Function GlobalOnLoadExternalResourceHandler(ByVal lpszURL As Long, ByRef pStream As Long, ByVal hFPC As Long, ByVal lParam As Long) As Long
    GlobalOnLoadExternalResourceHandler = E_FAIL
    
    If lstrcmpi(lpszURL, "http://FLV/FlashVideo.flv") = 0 Then
    
        'Save flash video to the stream from the resource
        Dim hModule As Long
        Dim hResInfo As Long
        Dim hResData As Long
        Dim lpData As Long
        Dim dwSize As Long
        Dim nWrittenBytes As Long
    
        hModule = GetModuleHandle(vbNullString)
        hResInfo = FindResource(hModule, "EMBEDDED_FLV", "FLV")
        hResData = LoadResource(hModule, hResInfo)
        lpData = LockResource(hResData)
        dwSize = SizeofResource(hModule, hResInfo)
        
        FPC_IStream_Write pStream, ByVal lpData, dwSize, nWrittenBytes
        GlobalOnLoadExternalResourceHandler = S_OK

    End If
    
End Function
