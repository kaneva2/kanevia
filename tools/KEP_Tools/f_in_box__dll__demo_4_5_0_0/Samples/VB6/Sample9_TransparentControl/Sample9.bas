Attribute VB_Name = "Sample9"
Option Explicit

'Win32 API

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Declare Function FillRect Lib "user32" _
(ByVal hdc As Long, lprc As RECT, ByVal hbr As Long) As Long

Private Declare Function DrawState Lib "user32" Alias "DrawStateA" _
  (ByVal hdc As Long, _
  ByVal hbr As Long, _
  ByVal lpOutputFunc As Long, _
  ByVal lData As Long, _
  ByVal wData As Long, _
  ByVal x As Long, _
  ByVal y As Long, _
  ByVal cx As Long, _
  ByVal cy As Long, _
  ByVal fuFlags As Long) As Long

Private Declare Function Rectangle Lib "gdi32" _
(ByVal hdc As Long, ByVal Left As Long, ByVal Top As Long, ByVal Right As Long, ByVal Bottom As Long) As Long

Private Declare Function SelectObject Lib "gdi32" _
(ByVal hdc As Long, ByVal hObject As Long) As Long

Private Declare Function CreateSolidBrush Lib "gdi32" _
(ByVal color As Long) As Long

Private Declare Function GetSysColorBrush Lib "user32" _
(ByVal index As Long) As Long

Private Declare Function DeleteObject Lib "gdi32" _
(ByVal hObject As Long) As Long

Private Declare Function OleTranslateColor Lib "oleaut32.dll" (ByVal lOleColor _
 As Long, ByVal lHPalette As Long, lColorRef As Long) As Long
 
Private Declare Function lstrcmpi Lib "kernel32" Alias "lstrcmpiA" _
(ByVal lpString1 As Long, ByVal lpString2 As String) As Long

Private Declare Function CreateFile Lib "kernel32" _
   Alias "CreateFileA" _
  (ByVal lpFileName As String, _
   ByVal dwDesiredAccess As Long, _
   ByVal dwShareMode As Long, _
   ByVal lpSecurityAttributes As Long, _
   ByVal dwCreationDisposition As Long, _
   ByVal dwFlagsAndAttributes As Long, _
   ByVal hTemplateFile As Long) As Long

Private Declare Function ReadFile Lib "kernel32" _
  (ByVal hFile As Long, _
   lpBuffer As Any, _
   ByVal nNumberOfBytesToRead As Long, _
   lpNumberOfBytesRead As Long, _
   ByVal lpOverlapped As Long) As Long
   
Private Declare Function CloseHandle Lib "kernel32" _
  (ByVal hFile As Long) As Long
  
' IStream helpers

Private Declare Function FPC_IStream_AddRef Lib "f_in_box.dll" _
    (ByVal pStream As Long) As Long
    
Private Declare Function FPC_IStream_Release Lib "f_in_box.dll" _
    (ByVal pStream As Long) As Long
    
Private Declare Function FPC_IStream_Write Lib "f_in_box.dll" _
    (ByVal pStream As Long, _
    pData As Any, _
    ByVal nSize As Long, _
    ByRef nWritten As Long) As Long
  
'Win32 constants
Const GENERIC_READ As Long = &H80000000
Const INVALID_HANDLE_VALUE As Long = -1
Const OPEN_EXISTING As Long = 3
Const FILE_SHARE_READ As Long = &H1

Private Const WM_NOTIFY As Long = &H4E
Private Const WM_USER As Long = &H400

Private Const COLOR_BTNFACE As Long = 15

Private Const DST_BITMAP As Long = 4

Const E_FAIL As Long = &H80004005
Const S_OK As Long = &H0

'FlashPlayerControl constants
Private Const FPCM_FIRST As Long = WM_USER + &H1000
Private Const FPCN_FIRST As Long = (FPCM_FIRST - 1)
Private Const FPCN_PAINT_STAGE As Long = (FPCN_FIRST - 10)
Private Const DEF_F_IN_BOX__PREPAINT_STAGE As Long = 0

'Win32 API
Private Declare Function CallWindowProc Lib "user32" _
    Alias "CallWindowProcA" _
   (ByVal lpPrevWndFunc As Long, _
    ByVal hwnd As Long, _
    ByVal uMsg As Long, _
    ByVal wParam As Long, _
    ByVal lParam As Long) As Long
    
Public Declare Sub CopyMemory Lib "kernel32" _
    Alias "RtlMoveMemory" _
   (Destination As Any, _
    Source As Any, _
    ByVal Length As Long)
    
Private Declare Function lstrlen Lib "kernel32" _
    Alias "lstrlenA" _
    (ByVal lpString As Any) As Long

Private Declare Function lstrcpy Lib "kernel32" _
    Alias "lstrcpyA" _
    (ByVal lpString1 As Any, _
    ByVal lpString2 As Any) As Long
    
'Struct NMHDR
Public Type NMHDR
   hWndFrom As Long
   idfrom   As Long
   code     As Long
End Type

'Struct SFPCNPaintStage
Private Type SFPCNPaintStage
    hdr As NMHDR
    dwStage As Long
    hdc As Long
End Type

Public Function GlobalOnLoadExternalResourceHandler(ByVal lpszURL As Long, ByRef pStream As Long, ByVal hFPC As Long, ByVal lParam As Long) As Long
    
    GlobalOnLoadExternalResourceHandler = E_FAIL
    
    If lstrcmpi(lpszURL, "http://FLV/FlashVideo.flv") = 0 Then
    
        Dim hFile As Long
        hFile = CreateFile(MainForm.FLVPath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0)
        
        If Not hFile = INVALID_HANDLE_VALUE Then
            Const nBufferSize As Long = 1024
            Dim pBuffer(nBufferSize) As Byte
            Dim dwNumberOfBytesRead As Long
            Dim dwNumberOfBytesWritten As Long
            Dim bRes As Boolean

            Do While True
                bRes = ReadFile(hFile, pBuffer(0), nBufferSize, dwNumberOfBytesRead, 0)
                If Not bRes Or dwNumberOfBytesRead = 0 Then
                    GoTo HCloseHandle
                End If
                
                FPC_IStream_Write pStream, pBuffer(0), dwNumberOfBytesRead, dwNumberOfBytesWritten
            Loop
           
HCloseHandle:
            CloseHandle hFile
            GlobalOnLoadExternalResourceHandler = S_OK
        End If
        
    End If
    
End Function

Private Function TranslateColor(ByVal clr As OLE_COLOR, _
                        Optional hPal As Long = 0) As Long
    If OleTranslateColor(clr, hPal, TranslateColor) Then
        TranslateColor = -1
    End If
End Function

Public Function MainFormWindowProc(ByVal hwnd As Long, _
                    ByVal uMsg As Long, _
                    ByVal wParam As Long, _
                    ByVal lParam As Long) As Long
    If uMsg = WM_NOTIFY Then
        Dim nm As NMHDR
        Call CopyMemory(nm, ByVal lParam, Len(nm))
        
        If nm.hWndFrom = MainForm.FlashPlayerControlVal Then
            Select Case nm.code
                Case FPCN_PAINT_STAGE
                    Dim FPCNPaintStage As SFPCNPaintStage
                    Call CopyMemory(FPCNPaintStage, ByVal lParam, Len(FPCNPaintStage))
                    
                    If FPCNPaintStage.dwStage = DEF_F_IN_BOX__PREPAINT_STAGE Then
                        
                        Dim Width As Long
                        Width = MainForm.Image1.Width
                        
                        Dim Height As Long
                        Height = MainForm.Image1.Height
                        
                        Dim rc As RECT
                        
                        rc.Left = 0
                        rc.Top = 0
                        rc.Right = MainForm.PlaceHolder.Width
                        rc.Bottom = MainForm.PlaceHolder.Height
                        
                        Dim hBrush As Long
                        hBrush = CreateSolidBrush(TranslateColor(MainForm.BackColor))
                        Call FillRect(FPCNPaintStage.hdc, rc, hBrush)
                        DeleteObject hBrush
                        
                        Call DrawState(FPCNPaintStage.hdc, 0, 0, MainForm.Image1.Picture.Handle, 0, MainForm.Image1.Left - MainForm.PlaceHolder.Left, MainForm.Image1.Top - MainForm.PlaceHolder.Top, Width, Height, DST_BITMAP)
                       
                    End If
                    
                    Exit Function
            End Select
        End If
        
    End If
    MainFormWindowProc = CallWindowProc(MainForm.PrevWindowProcVal, hwnd, uMsg, wParam, lParam)
End Function


