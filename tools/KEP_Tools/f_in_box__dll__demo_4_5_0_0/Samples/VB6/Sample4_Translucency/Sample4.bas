Attribute VB_Name = "Sample4"
Option Explicit

'Win32 API
Private Declare Function CallWindowProc Lib "user32" _
    Alias "CallWindowProcA" _
   (ByVal lpPrevWndFunc As Long, _
    ByVal hwnd As Long, _
    ByVal uMsg As Long, _
    ByVal wParam As Long, _
    ByVal lParam As Long) As Long

Private Declare Function PostQuitMessage Lib "user32" _
   (ByVal nExitCode As Long) As Long

'Win32 constants
Private Const WM_LBUTTONDOWN As Long = &H201
Private Const WM_LBUTTONUP As Long = &H202
Private Const WM_RBUTTONUP As Long = &H205
Private Const WM_MOUSEMOVE As Long = &H200
Private Const WM_DESTROY As Long = &H2

Public Function TransparentFlashPlayerControlWindowProc(ByVal hwnd As Long, _
                    ByVal uMsg As Long, _
                    ByVal wParam As Long, _
                    ByVal lParam As Long) As Long
    Select Case uMsg
        Case WM_LBUTTONDOWN
            MainForm.FlashPlayerControlRef.OnLButtonDown
            Exit Function
        Case WM_LBUTTONUP
            MainForm.FlashPlayerControlRef.OnLButtonUp
            Exit Function
        Case WM_MOUSEMOVE
            MainForm.FlashPlayerControlRef.OnMouseMove
            Exit Function
        Case WM_RBUTTONUP
            MainForm.PopupMenu MainForm.ContextMenu
            Exit Function
        Case WM_DESTROY
            Call PostQuitMessage(0)
    End Select
    'Call previous window procedure
    TransparentFlashPlayerControlWindowProc = CallWindowProc(MainForm.FlashPlayerControlRef.PrevWindowProcVal, hwnd, uMsg, wParam, lParam)
End Function


