VERSION 5.00
Begin VB.Form MainForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sample7  - Flash 8 External API"
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8700
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   424
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   580
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CallActionScriptBtn 
      Caption         =   "Call ActionScript function"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   5880
      Width           =   2415
   End
   Begin VB.CommandButton OrderBtn 
      Caption         =   "Buy license"
      Height          =   375
      Left            =   6600
      TabIndex        =   2
      Top             =   5880
      Width           =   1935
   End
   Begin VB.CommandButton MoreAboutBtn 
      Caption         =   "More about F-IN-BOX"
      Height          =   375
      Left            =   4080
      TabIndex        =   1
      Top             =   5880
      Width           =   2415
   End
   Begin VB.Label PlaceHolder 
      Height          =   5580
      Left            =   120
      TabIndex        =   0
      Top             =   135
      Visible         =   0   'False
      Width           =   8460
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Win32 API
Private Declare Function ShellExecute Lib "shell32" _
   Alias "ShellExecuteA" _
  (ByVal hwnd As Long, _
   ByVal lpOperation As String, _
   ByVal lpFile As String, _
   ByVal lpParameters As String, _
   ByVal lpDirectory As String, _
   ByVal nShowCmd As Long) As Long
   
Private Declare Function FPC_CreateWindow Lib "f_in_box.dll" _
   Alias "FPC_CreateWindowA" _
  (ByVal hFPC As Long, _
   ByVal dwExStyle As Long, _
   ByVal lpWindowName As String, _
   ByVal dwStyle As Long, _
   ByVal x As Long, ByVal y As Long, _
   ByVal nWidth As Long, _
   ByVal nHeight As Long, _
   ByVal hWndParent As Long, _
   ByVal hMenu As Long, _
   ByVal hInstance As Long, _
   lpParam As Any) As Long
   
Private Declare Function SetWindowLong Lib "user32" _
    Alias "SetWindowLongA" _
   (ByVal hwnd As Long, _
    ByVal nIndex As Long, _
    ByVal wNewLong As Long) As Long
   
'Win32 constants
Private Const WS_VISIBLE As Long = &H10000000
Private Const WS_CHILD As Long = &H40000000
Private Const WS_CLIPSIBLINGS As Long = &h4000000
Private Const WM_USER As Long = &H400
Private Const SW_SHOW = 5

Private Const GWL_WNDPROC = (-4)
Private Const S_OK As Long = &H0

'FlashPlayerControl API
Private Declare Function FPCIsFlashInstalled Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_LoadRegisteredOCX Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_UnloadCode Lib "f_in_box.dll" _
    (ByVal hFPC As Long) As Long

Private Declare Function FPCPutMovieFromResource Lib "f_in_box.dll" _
    Alias "FPCPutMovieFromResourceA" _
    (ByVal hwndFlashPlayerControl As Long, _
    ByVal hInstance As Long, _
    ByVal lpName As String, _
    ByVal lpType As String) As Boolean
    
Private Declare Function FPCCallFunction Lib "f_in_box.dll" _
    Alias "FPCCallFunctionA" _
    (ByVal hwndFlashPlayerControl As Long, _
    ByVal lpszRequest As String, _
    lpszResponse As String, _
    pdwResponseLength As Long) As Long
    
'FlashPlayerControl constants

Private hFPC As Long
Private FlashPlayerControlWnd As Long
Private PrevWindowProc As Long
Private MediaDir As String

Public Property Get PrevWindowProcVal() As Long
    PrevWindowProcVal = PrevWindowProc
End Property

Public Property Get FlashPlayerControlVal() As Long
    FlashPlayerControlVal = FlashPlayerControlWnd
End Property

Private Sub Form_Load()
    Initialize
End Sub

Private Sub Initialize()
    
    'Call the FindMediaDir procedure
    FindMediaDir
    
    'Prepare FlashPlayerControl
    
    If FPCIsFlashInstalled = 0 Then
        MsgBox ("The application needs Flash" + Chr$(13) + Chr$(10) + "Flash is not installed")
        End
    End If
    
    hFPC = FPC_LoadRegisteredOCX

    If hFPC = 0 Then
        MsgBox ("FPC_LoadRegisteredOCX() failed")
        End
    End If
   
    'Create flash player window
    FlashPlayerControlWnd = FPC_CreateWindow( _
                                        hFPC, _
                                        0, _
                                        vbNullString, _
                                        WS_CHILD Or WS_VISIBLE Or WS_CLIPSIBLINGS, _
                                        PlaceHolder.Left, PlaceHolder.Top, _
                                        PlaceHolder.Width, _
                                        PlaceHolder.Height, _
                                        hwnd, 0, _
                                        App.hInstance, _
                                        ByVal 0)
    'Subclass window
    PrevWindowProc = SetWindowLong(hwnd, GWL_WNDPROC, AddressOf MainFormWindowProc)
    'Play movie from resource
    Call FPCPutMovieFromResource(FlashPlayerControlWnd, 0, "MOVIE", "SWF")
End Sub

Private Sub FindMediaDir()
    MediaDir = CurDir
End Sub

Private Sub CallActionScriptBtn_Click()
    Const request As String = "<invoke name=""CallMeFromApplication"" returntype=""xml""><arguments><string>Some text for FlashPlayerControl</string></arguments></invoke>"
    Const nBufferSize As Long = 1024
    Dim szResponse As String
    Dim dwLength As Long
    
    szResponse = String(nBufferSize, "*")
    dwLength = nBufferSize - 1
    
    Call FPCCallFunction(FlashPlayerControlWnd, request, ByVal szResponse, dwLength)
End Sub

Private Sub Form_Terminate()
    If hFPC <> 0 Then
        FPC_UnloadCode hFPC
    End If
End Sub

Private Sub MoreAboutBtn_Click()
    ShellExecute hwnd, vbNullString, "http://www.f-in-box.com/dll/", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub OrderBtn_Click()
    ShellExecute hwnd, vbNullString, "http://www.f-in-box.com/dll/order.html", vbNullString, vbNullString, SW_SHOW
End Sub


