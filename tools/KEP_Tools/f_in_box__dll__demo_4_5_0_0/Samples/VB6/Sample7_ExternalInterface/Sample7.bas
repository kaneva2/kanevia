Attribute VB_Name = "Sample7"
Option Explicit

'Win32 API
Private Declare Function CallWindowProc Lib "user32" _
    Alias "CallWindowProcA" _
   (ByVal lpPrevWndFunc As Long, _
    ByVal hwnd As Long, _
    ByVal uMsg As Long, _
    ByVal wParam As Long, _
    ByVal lParam As Long) As Long
    
Public Declare Sub CopyMemory Lib "kernel32" _
    Alias "RtlMoveMemory" _
   (Destination As Any, _
    Source As Any, _
    ByVal Length As Long)
    
Private Declare Function lstrlen Lib "kernel32" _
    Alias "lstrlenA" _
    (ByVal lpString As Any) As Long

Private Declare Function lstrcpy Lib "kernel32" _
    Alias "lstrcpyA" _
    (ByVal lpString1 As Any, _
    ByVal lpString2 As Any) As Long
    
'Struct NMHDR
Public Type NMHDR
   hWndFrom As Long
   idfrom   As Long
   code     As Long
End Type
    
'Win32 constants
Private Const WM_NOTIFY As Long = &H4E
Private Const WM_USER As Long = &H400

'FlashPlayerControl API
Private Declare Function FPCSetReturnValue Lib "f_in_box.dll" _
    Alias "FPCSetReturnValueA" _
   (ByVal hwndFlashPlayerControl As Long, _
    ByVal lpszReturnValue As String) As Long

'FlashPlayerControl constants
Private Const FPCM_FIRST As Long = WM_USER + &H1000
Private Const FPCN_FIRST As Long = (FPCM_FIRST - 1)
Private Const FPCN_FLASHCALL As Long = (FPCN_FIRST - 4)

'Struct SFPCFlashCallInfoStruct
Private Type SFPCFlashCallInfoStruct
    hdr As NMHDR
    request As Long ' LPCSTR
End Type

Public Function MainFormWindowProc(ByVal hwnd As Long, _
                    ByVal uMsg As Long, _
                    ByVal wParam As Long, _
                    ByVal lParam As Long) As Long
    If uMsg = WM_NOTIFY Then
        Dim nm As NMHDR
        Call CopyMemory(nm, ByVal lParam, Len(nm))
        
        If nm.hWndFrom = MainForm.FlashPlayerControlVal Then
            Select Case nm.code
                Case FPCN_FLASHCALL
                    Dim FPCFlashCallInfoStruct As SFPCFlashCallInfoStruct
                    Call CopyMemory(FPCFlashCallInfoStruct, ByVal lParam, Len(FPCFlashCallInfoStruct))
                    
                    MsgBox "The request is: '" & GetStringFromLongPtr(FPCFlashCallInfoStruct.request) & "'"

                    Dim msg As String
                    msg = "<string>Current time is: " & Format(Now(), "dd.mm.yyyy hh:mm:ss")
                    msg = msg & Chr$(13) & Chr$(10)
                    msg = msg & "This info is returned from the handler</string>"
                    
                    Call FPCSetReturnValue(MainForm.FlashPlayerControlVal, msg)
                    
                    Exit Function
            End Select
        End If
        
    End If
    MainFormWindowProc = CallWindowProc(MainForm.PrevWindowProcVal, hwnd, uMsg, wParam, lParam)
End Function

Private Function GetStringFromLongPtr(ByVal pStr As Long) As String
    GetStringFromLongPtr = String$(lstrlen(ByVal pStr), 0)
    Call lstrcpy(ByVal GetStringFromLongPtr, ByVal pStr)
End Function
