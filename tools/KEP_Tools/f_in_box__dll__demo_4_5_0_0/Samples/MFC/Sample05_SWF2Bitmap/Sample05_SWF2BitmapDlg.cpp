// Sample05_SWF2BitmapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Sample05_SWF2Bitmap.h"
#include "Sample05_SWF2BitmapDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample05_SWF2BitmapDlg dialog

CSample05_SWF2BitmapDlg::CSample05_SWF2BitmapDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample05_SWF2BitmapDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSample05_SWF2BitmapDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample05_SWF2BitmapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSample05_SWF2BitmapDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSample05_SWF2BitmapDlg, CDialog)
	//{{AFX_MSG_MAP(CSample05_SWF2BitmapDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_PLAY_SWF_FROM_FILE, OnPlaySwfFromFile)
	ON_BN_CLICKED(ID_PLAY_EMBEDDED_SWF, OnPlayEmbeddedSwf)
	ON_BN_CLICKED(ID_SAVE_AS_BITMAP, OnSaveAsBitmap)
	ON_BN_CLICKED(ID_SITE, OnSite)
	ON_BN_CLICKED(ID_ORDER, OnOrder)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample05_SWF2BitmapDlg message handlers

BOOL CSample05_SWF2BitmapDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	// FlashPlayerControl creating
	// NOTE: CALL FPC_LoadRegisteredOCX() or FPC_LoadOCXCodeFromMemory() before creating!
	// See InitInstance() implementation
	{
		RECT rc;
		GetDlgItem(IDC_STATIC_FPC_PLACE)->GetWindowRect(&rc);
		ScreenToClient(&rc);

		m_hwndFlashPlayerControl = 
			CreateWindow((LPCTSTR)FPC_GetClassAtom(theApp.m_hFPC), 
						 NULL, 
						 WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS, 
						 rc.left, 
						 rc.top, 
						 rc.right - rc.left, 
						 rc.bottom - rc.top, 
						 m_hWnd, 
						 NULL, 
						 NULL, 
						 NULL);
	}

	// Loading movie from resource
	FPCPutMovieFromResource(m_hwndFlashPlayerControl, 0, _T("MOVIE"), _T("SWF"));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSample05_SWF2BitmapDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSample05_SWF2BitmapDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSample05_SWF2BitmapDlg::OnPlaySwfFromFile() 
{
    CFileDialog dlg(TRUE, 
                    _T("swf"), 
                    _T(""), 
                    0, 
                    _T("Flash movie files (*.swf)|*.swf|All Files (*.*)|*.*||"), 
                    this);

    if (IDOK == dlg.DoModal())
    {
        //
        CString strFlashMoviePath = dlg.GetPathName();

        //
        TCHAR lpszFlashMoviePath[MAX_PATH + 1] = { 0 };
        lstrcpy(lpszFlashMoviePath, strFlashMoviePath);

        //
        SFPCPutMovie SFPCPutMovie;
        SFPCPutMovie.lpszBuffer = lpszFlashMoviePath;
        ::SendMessage(m_hwndFlashPlayerControl, FPCM_PUT_MOVIE, 0, (LPARAM)&SFPCPutMovie);
    }
}

void CSample05_SWF2BitmapDlg::OnPlayEmbeddedSwf() 
{
	// Loading movie from resource
	FPCPutMovieFromResource(m_hwndFlashPlayerControl, 0, _T("MOVIE"), _T("SWF"));
}

void CSample05_SWF2BitmapDlg::OnSaveAsBitmap() 
{
	SFPCGetFrameBitmap FPCGetFrameBitmap = { 0 };

	::SendMessage(m_hwndFlashPlayerControl, FPCM_GET_FRAME_BITMAP, 0, (LPARAM)&FPCGetFrameBitmap);

	PICTDESC pictdesc = { 0 };

	pictdesc.cbSizeofstruct = sizeof(pictdesc);
	pictdesc.picType = PICTYPE_BITMAP;
	pictdesc.bmp.hbitmap = FPCGetFrameBitmap.hBitmap;

	IPicture* pPicture = NULL;
	OleCreatePictureIndirect(&pictdesc, IID_IPicture, FALSE, (void**)&pPicture);

	if (pPicture)
	{
		IStream* pMemStream = NULL;
		CreateStreamOnHGlobal(0, TRUE, &pMemStream);

		if (pMemStream)
		{
			CFileDialog dlg(FALSE, 
							_T("bmp"), 
							_T(""), 
							0, 
							_T("Bitmap (*.bmp)|*.bmp|All Files (*.*)|*.*||"), 
							this);

			if (IDOK == dlg.DoModal())
			{
				HANDLE hFile = 
					CreateFile(dlg.GetPathName(), GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);

				if (INVALID_HANDLE_VALUE != hFile)
				{
					LONG nSize;
					pPicture->SaveAsFile(pMemStream, TRUE, &nSize);

					if (nSize > 0)
					{
						BYTE* pBuffer = new BYTE[nSize];

						LARGE_INTEGER liZero = { 0 };
						ULARGE_INTEGER liNewPosition;
						pMemStream->Seek(liZero, STREAM_SEEK_SET, &liNewPosition);
						
						ULONG nRead;
						pMemStream->Read(pBuffer, nSize, &nRead);

						DWORD nNumberOfBytesWritten;
						WriteFile(hFile, pBuffer, nSize, &nNumberOfBytesWritten, NULL);

						delete[] pBuffer;

						if (IDYES == 
							AfxMessageBox(_T("The frame saved\nWould you like to open picture?"), 
										  MB_OK | MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON1))
							ShellExecute(NULL, NULL, dlg.GetPathName(), NULL, NULL, SW_SHOW);
					}

					CloseHandle(hFile);
				}
			}

			pMemStream->Release();
		}

		pPicture->Release();
		pPicture = NULL;
	}

	DeleteObject(FPCGetFrameBitmap.hBitmap);
}

void CSample05_SWF2BitmapDlg::OnSite() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/"), NULL, NULL, SW_SHOW);
}

void CSample05_SWF2BitmapDlg::OnOrder() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/order.html"), NULL, NULL, SW_SHOW);
}
