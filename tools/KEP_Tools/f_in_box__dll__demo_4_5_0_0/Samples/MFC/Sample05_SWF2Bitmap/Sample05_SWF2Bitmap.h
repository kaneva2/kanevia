// Sample05_SWF2Bitmap.h : main header file for the Sample05_SWF2Bitmap application
//

#if !defined(AFX_Sample05_SWF2Bitmap_H__CFC6375A_4A78_4BCA_9C0B_8CF6CFCBDEA0__INCLUDED_)
#define AFX_Sample05_SWF2Bitmap_H__CFC6375A_4A78_4BCA_9C0B_8CF6CFCBDEA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample05_SWF2BitmapApp:
// See Sample05_SWF2Bitmap.cpp for the implementation of this class
//

class CSample05_SWF2BitmapApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CSample05_SWF2BitmapApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample05_SWF2BitmapApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample05_SWF2BitmapApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CSample05_SWF2BitmapApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample05_SWF2Bitmap_H__CFC6375A_4A78_4BCA_9C0B_8CF6CFCBDEA0__INCLUDED_)
