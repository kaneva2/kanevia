// Sample10_SoundRecorder.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Sample10_SoundRecorder.h"
#include "Sample10_SoundRecorderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample10_SoundRecorderApp

BEGIN_MESSAGE_MAP(CSample10_SoundRecorderApp, CWinApp)
	//{{AFX_MSG_MAP(CSample10_SoundRecorderApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample10_SoundRecorderApp construction

CSample10_SoundRecorderApp::CSample10_SoundRecorderApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSample10_SoundRecorderApp object

CSample10_SoundRecorderApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSample10_SoundRecorderApp initialization

BOOL CSample10_SoundRecorderApp::InitInstance()
{
	AfxEnableControlContainer();

	// Check if flash is installed on the machine
	if (!FPCIsFlashInstalled())
	{
		AfxMessageBox(_T("The application needs Flash\nFlash is not installed"));
		return FALSE;
	}

	// Load registered flash activex
	// Don't forget to unload it; see ExitInstance()
	m_hFPC = FPC_LoadRegisteredOCX();

	if (NULL == m_hFPC)
	{
		AfxMessageBox(_T("FPC_LoadRegisteredOCX() failed"));
		return FALSE;
	}

	CSample10_SoundRecorderDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CSample10_SoundRecorderApp::ExitInstance() 
{
	// Unload the flash ocx code
	if (NULL != m_hFPC)
	{
		FPC_UnloadCode(m_hFPC);
		m_hFPC = NULL;
	}
	
	return CWinApp::ExitInstance();
}
