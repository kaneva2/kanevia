// Sample10_SoundRecorderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Sample10_SoundRecorder.h"
#include "Sample10_SoundRecorderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample10_SoundRecorderDlg dialog

CSample10_SoundRecorderDlg::CSample10_SoundRecorderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample10_SoundRecorderDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSample10_SoundRecorderDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample10_SoundRecorderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSample10_SoundRecorderDlg)
	DDX_Control(pDX, IDC_BUTTON_SELECT_WAV, m_wndBrowseButton);
	DDX_Control(pDX, IDC_BUTTON_STOP, m_wndStopButton);
	DDX_Control(pDX, IDC_BUTTON_START, m_wndStartButton);
	DDX_Control(pDX, IDC_EDIT_WAV_PATH, m_wndWAVPath);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSample10_SoundRecorderDlg, CDialog)
	//{{AFX_MSG_MAP(CSample10_SoundRecorderDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_PLAY_SWF_FROM_FILE, OnPlaySwfFromFile)
	ON_BN_CLICKED(ID_PLAY_FLV_FROM_FILE, OnPlayFlvFromFile)
	ON_BN_CLICKED(ID_SITE, OnSite)
	ON_BN_CLICKED(ID_ORDER, OnOrder)
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_BUTTON_SELECT_WAV, OnButtonSelectWav)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample10_SoundRecorderDlg message handlers

BOOL CSample10_SoundRecorderDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	// f_in_box creating
	// NOTE: CALL FPC_LoadRegisteredOCX() or FPC_LoadOCXCodeFromMemory() before creating!
	// See InitInstance() implementation
	{
		RECT rc;
		GetDlgItem(IDC_STATIC_FPC_PLACE)->GetWindowRect(&rc);
		ScreenToClient(&rc);

		m_hwndFlashPlayerControl = 
			CreateWindow((LPCTSTR)FPC_GetClassAtom(theApp.m_hFPC), 
						 NULL, 
						 WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS, 
						 rc.left, 
						 rc.top, 
						 rc.right - rc.left, 
						 rc.bottom - rc.top, 
						 m_hWnd, 
						 NULL, 
						 NULL, 
						 NULL);
	}

	m_dwHandlerCookie = FPC_AddOnLoadExternalResourceHandler(theApp.m_hFPC, &StaticGlobalOnLoadExternalResourceHandler, (LPARAM)this);

	MessageBox(_T("If you use DEMO VERSION, Sound Capturing works in the demo mode\nIt means that you can record only first 10 seconds starting from click to Start Recording\nRestart to test capturing again"));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSample10_SoundRecorderDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSample10_SoundRecorderDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

HRESULT WINAPI CSample10_SoundRecorderDlg::GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC)
{
	HRESULT hr = E_FAIL;

	if (0 == lstrcmpi(lpszURL, _T("http://FLV/FlashVideo.flv")))
	{
		IStream* pMemStream = NULL;
		CreateStreamOnHGlobal(NULL, TRUE, &pMemStream);
		
		HANDLE hFile = CreateFile(m_strFLVPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		if (INVALID_HANDLE_VALUE != hFile)
		{
			const DWORD nBufferSize = 1024 * 1024;
			BYTE* pBuffer = new BYTE[nBufferSize];
			DWORD dwNumberOfBytesRead;
			ULONG nWritten;

			while (true)
			{
				BOOL bRes = ReadFile(hFile, pBuffer, nBufferSize, &dwNumberOfBytesRead, NULL);

				if (!bRes || 0 == dwNumberOfBytesRead)
					break;

				pMemStream->Write(pBuffer, dwNumberOfBytesRead, &nWritten);
			}

			delete[] pBuffer;

			CloseHandle(hFile);

			LARGE_INTEGER liZero = { 0 };
			ULARGE_INTEGER ulNewPosition;
			pMemStream->Seek(liZero, STREAM_SEEK_SET, &ulNewPosition);

			*ppStream = pMemStream;
			hr = S_OK;
		}
	}

	return hr;
}

void CSample10_SoundRecorderDlg::OnPlaySwfFromFile() 
{
	if (0 == m_wndWAVPath.GetWindowTextLength())
	{
		AfxMessageBox(_T("Please select WAV file"));
		return;
	}

    CFileDialog dlg(TRUE, 
                    _T("swf"), 
                    _T(""), 
                    0, 
                    _T("Flash movie files (*.swf)|*.swf|All Files (*.*)|*.*||"), 
                    this);

    if (IDOK == dlg.DoModal())
    {
        //
        CString strFlashMoviePath = dlg.GetPathName();

        //
        TCHAR lpszFlashMoviePath[MAX_PATH + 1] = { 0 };
        lstrcpy(lpszFlashMoviePath, strFlashMoviePath);

        //
        SFPCPutMovie SFPCPutMovie;
        SFPCPutMovie.lpszBuffer = lpszFlashMoviePath;
        ::SendMessage(m_hwndFlashPlayerControl, FPCM_PUT_MOVIE, 0, (LPARAM)&SFPCPutMovie);
    }

	if (IsDlgButtonChecked(IDC_CHECK_AUTOSTART))
		OnButtonStart();
}

void CSample10_SoundRecorderDlg::OnPlayFlvFromFile() 
{
	if (0 == m_wndWAVPath.GetWindowTextLength())
	{
		AfxMessageBox(_T("Please select WAV file"));
		return;
	}


	if (0 == m_wndWAVPath.GetWindowTextLength())
	{
		AfxMessageBox(_T("Please select WAV file"));
		return;
	}

    CFileDialog dlg(TRUE, 
                    _T("flv"), 
                    _T(""), 
                    0, 
                    _T("Flash video files (*.flv)|*.flv|All Files (*.*)|*.*||"), 
                    this);

    if (IDOK == dlg.DoModal())
	{
        //
		FPC_PutFlashVars(m_hwndFlashPlayerControl, CString(_T("FLVPath=")) + dlg.GetPathName());
		FPCPutMovieFromResource(m_hwndFlashPlayerControl, 0, _T("FLVPlayer"), _T("SWF"));
    }

	if (IsDlgButtonChecked(IDC_CHECK_AUTOSTART))
		OnButtonStart();
}

void CSample10_SoundRecorderDlg::PostNcDestroy() 
{
	CDialog::PostNcDestroy();

	FPC_RemoveOnLoadExternalResourceHandler(theApp.m_hFPC, m_dwHandlerCookie);
}

void CSample10_SoundRecorderDlg::OnSite() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/"), NULL, NULL, SW_SHOW);
}

void CSample10_SoundRecorderDlg::OnOrder() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/order.html"), NULL, NULL, SW_SHOW);
}

void CSample10_SoundRecorderDlg::OnButtonStart() 
{
	m_wndBrowseButton.EnableWindow(FALSE);
	m_wndWAVPath.EnableWindow(FALSE);
	m_wndStartButton.EnableWindow(FALSE);
	m_wndStopButton.EnableWindow(TRUE);

	CString strWAVPath;
	m_wndWAVPath.GetWindowText(strWAVPath);

	theApp.m_FlashSoundRecorder.Start(strWAVPath, theApp.m_hFPC);
}

void CSample10_SoundRecorderDlg::OnButtonStop() 
{
	m_wndBrowseButton.EnableWindow(TRUE);
	m_wndWAVPath.EnableWindow(TRUE);
	m_wndStartButton.EnableWindow(TRUE);
	m_wndStopButton.EnableWindow(FALSE);

	theApp.m_FlashSoundRecorder.Stop();

	if (IDYES == AfxMessageBox(_T("Would you like to launch saved WAV file?"), MB_YESNO | MB_DEFBUTTON1))
	{
		CString strPath;
		m_wndWAVPath.GetWindowText(strPath);

		ShellExecute(m_hWnd, NULL, strPath, NULL, NULL, SW_SHOW);
	}
}

void CSample10_SoundRecorderDlg::OnButtonSelectWav() 
{
    CFileDialog dlg(FALSE, 
                    _T("wav"), 
                    _T(""), 
                    OFN_OVERWRITEPROMPT, 
                    _T("WAV files (*.wav)|*.wav|All Files (*.*)|*.*||"), 
                    this);

    if (IDOK == dlg.DoModal())
		m_wndWAVPath.SetWindowText(dlg.GetPathName());
}
