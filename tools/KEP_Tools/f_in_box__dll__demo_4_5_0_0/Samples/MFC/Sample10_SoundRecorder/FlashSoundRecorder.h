#ifndef __FLASHSOUNDRECORDER_H__
#define __FLASHSOUNDRECORDER_H__

class CFlashSoundRecorder
{
private:
	CString m_strWAVPath;
	HFPC m_hFPC;
	HMMIO m_hmmio;
	MMCKINFO m_wave;
	MMCKINFO m_data;
	BOOL m_bFirstTime;

private:

	static HRESULT WINAPI StaticSoundListener(HFPC hFPC, LPARAM lParam, PWAVEFORMATEX pWaveFormat, LPWAVEHDR pWaveHeader, UINT nHeaderSize)
	{
		((CFlashSoundRecorder*)lParam)->SoundListener(pWaveFormat, pWaveHeader, nHeaderSize);

		return S_OK;
	}

	void SoundListener(PWAVEFORMATEX pWaveFormat, LPWAVEHDR pWaveHeader, UINT nHeaderSize);

public:

	CFlashSoundRecorder();
	~CFlashSoundRecorder();

	void Start(LPCTSTR szWAVPath, HFPC hFPC);
	void Stop();
};

#endif // !__FLASHSOUNDRECORDER_H__
