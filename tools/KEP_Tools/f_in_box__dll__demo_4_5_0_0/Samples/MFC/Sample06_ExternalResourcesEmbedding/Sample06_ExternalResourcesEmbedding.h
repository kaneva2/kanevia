// Sample06_ExternalResourcesEmbedding.h : main header file for the Sample06_ExternalResourcesEmbedding application
//

#if !defined(AFX_Sample06_ExternalResourcesEmbedding_H__BB29AF01_489F_4D18_9C70_C950BA0580D1__INCLUDED_)
#define AFX_Sample06_ExternalResourcesEmbedding_H__BB29AF01_489F_4D18_9C70_C950BA0580D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample06_ExternalResourcesEmbeddingApp:
// See Sample06_ExternalResourcesEmbedding.cpp for the implementation of this class
//

class CSample06_ExternalResourcesEmbeddingApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CSample06_ExternalResourcesEmbeddingApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample06_ExternalResourcesEmbeddingApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample06_ExternalResourcesEmbeddingApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CSample06_ExternalResourcesEmbeddingApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample06_ExternalResourcesEmbedding_H__BB29AF01_489F_4D18_9C70_C950BA0580D1__INCLUDED_)
