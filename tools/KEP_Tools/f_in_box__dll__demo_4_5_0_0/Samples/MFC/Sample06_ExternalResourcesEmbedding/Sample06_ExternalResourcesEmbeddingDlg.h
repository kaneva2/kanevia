// Sample06_ExternalResourcesEmbeddingDlg.h : header file
//

#if !defined(AFX_Sample06_ExternalResourcesEmbeddingDLG_H__CB5A78AE_5041_4325_A42B_68C16D3E0BE1__INCLUDED_)
#define AFX_Sample06_ExternalResourcesEmbeddingDLG_H__CB5A78AE_5041_4325_A42B_68C16D3E0BE1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample06_ExternalResourcesEmbeddingDlg dialog

class CSample06_ExternalResourcesEmbeddingDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

// Construction
public:
	CSample06_ExternalResourcesEmbeddingDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample06_ExternalResourcesEmbeddingDlg)
	enum { IDD = IDD_Sample06_ExternalResourcesEmbedding_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample06_ExternalResourcesEmbeddingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample06_ExternalResourcesEmbeddingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample06_ExternalResourcesEmbeddingDLG_H__CB5A78AE_5041_4325_A42B_68C16D3E0BE1__INCLUDED_)
