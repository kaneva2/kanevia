// Sample06_ExternalResourcesEmbeddingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Sample06_ExternalResourcesEmbedding.h"
#include "Sample06_ExternalResourcesEmbeddingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Load resource helper
void LoadResourceHelper( /* in */ LPCTSTR lpszName, 
                         /* in */ LPCTSTR lpszType, 
                         /* out */ LPVOID& lpData, 
                         /* out */ DWORD& dwSize);

/////////////////////////////////////////////////////////////////////////////
// CSample06_ExternalResourcesEmbeddingDlg dialog

CSample06_ExternalResourcesEmbeddingDlg::CSample06_ExternalResourcesEmbeddingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample06_ExternalResourcesEmbeddingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSample06_ExternalResourcesEmbeddingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample06_ExternalResourcesEmbeddingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSample06_ExternalResourcesEmbeddingDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSample06_ExternalResourcesEmbeddingDlg, CDialog)
	//{{AFX_MSG_MAP(CSample06_ExternalResourcesEmbeddingDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_SITE, OnSite)
	ON_BN_CLICKED(ID_ORDER, OnOrder)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample06_ExternalResourcesEmbeddingDlg message handlers

BOOL CSample06_ExternalResourcesEmbeddingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	// FlashPlayerControl creating
	// NOTE: CALL FPC_LoadRegisteredOCX() or FPC_LoadOCXCodeFromMemory() before creating!
	// See InitInstance() implementation
	{
		RECT rc;
		GetDlgItem(IDC_STATIC_FPC_PLACE)->GetWindowRect(&rc);
		ScreenToClient(&rc);

		m_hwndFlashPlayerControl = 
			CreateWindow((LPCTSTR)FPC_GetClassAtom(theApp.m_hFPC), 
						 NULL, 
						 WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS, 
						 rc.left, 
						 rc.top, 
						 rc.right - rc.left, 
						 rc.bottom - rc.top, 
						 m_hWnd, 
						 NULL, 
						 NULL, 
						 NULL);
	}

	// Load movie from resource
	FPCPutMovieFromResource(m_hwndFlashPlayerControl, 0, _T("MOVIE"), _T("SWF"));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSample06_ExternalResourcesEmbeddingDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSample06_ExternalResourcesEmbeddingDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LRESULT CSample06_ExternalResourcesEmbeddingDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
    if (WM_NOTIFY == message)
    {
        LPNMHDR lpNMHDR = (LPNMHDR)lParam;

        if (m_hwndFlashPlayerControl == lpNMHDR->hwndFrom)
            switch (lpNMHDR->code)
            {
                case FPCN_ONPROGRESS:
                {
                    SFPCOnProgressInfoStruct* pInfo = (SFPCOnProgressInfoStruct*)lParam;

                    TRACE(_T("FPCN_ONPROGRESS: percentDone(%d)\n"), pInfo->percentDone);

                    return 1;
                }

                case FPCN_FSCOMMAND:
                {
                    SFPCFSCommandInfoStruct* pInfo = (SFPCFSCommandInfoStruct*)lParam;

                    TRACE(_T("FPCN_FSCOMMAND: command(%s), args(%s)\n"), pInfo->command, pInfo->args);

                    return 1;
                }

                case FPCN_ONREADYSTATECHANGE:
                {
                    SFPCOnReadyStateChangeInfoStruct* pInfo = (SFPCOnReadyStateChangeInfoStruct*)lParam;

                    TRACE(_T("FPCN_ONREADYSTATECHANGE: newState(%d)\n"), pInfo->newState);

                    return 1;
                }

                case FPCN_LOADEXTERNALRESOURCE:
                {
                    LPVOID lpResourceData = NULL;
                    DWORD dwResourceSize = 0;

                    SFPCLoadExternalResource* pInfo = (SFPCLoadExternalResource*)lParam;

                    if (0 == lstrcmpi(pInfo->lpszRelativePath, _T("images/embedded_image1.jpg")))
                    {
                        LoadResourceHelper(_T("IMAGE1"), _T("IMAGE"), lpResourceData, dwResourceSize);

                        ULONG ulWritten;
                        pInfo->lpStream->Write(lpResourceData, dwResourceSize, &ulWritten);
                    }
                    else if (0 == lstrcmpi(pInfo->lpszRelativePath, _T("images/embedded_image2.jpg")))
                    {
                        LoadResourceHelper(_T("IMAGE2"), _T("IMAGE"), lpResourceData, dwResourceSize);

                        ULONG ulWritten;
                        pInfo->lpStream->Write(lpResourceData, dwResourceSize, &ulWritten);
                    }
                    else if (0 == lstrcmpi(pInfo->lpszRelativePath, _T("images/embedded_image3.jpg")))
                    {
                        LoadResourceHelper(_T("IMAGE3"), _T("IMAGE"), lpResourceData, dwResourceSize);

                        ULONG ulWritten;
                        pInfo->lpStream->Write(lpResourceData, dwResourceSize, &ulWritten);
                    }
                    else if (0 == lstrcmpi(pInfo->lpszRelativePath, _T("images/embedded_image4.jpg")))
                    {
                        LoadResourceHelper(_T("IMAGE4"), _T("IMAGE"), lpResourceData, dwResourceSize);

                        ULONG ulWritten;
                        pInfo->lpStream->Write(lpResourceData, dwResourceSize, &ulWritten);
                    }
                    else if (0 == lstrcmpi(pInfo->lpszRelativePath, _T("images/embedded_image5.jpg")))
                    {
                        LoadResourceHelper(_T("IMAGE5"), _T("IMAGE"), lpResourceData, dwResourceSize);

                        ULONG ulWritten;
                        pInfo->lpStream->Write(lpResourceData, dwResourceSize, &ulWritten);
                    }
                    else if (0 == lstrcmpi(pInfo->lpszRelativePath, _T("external_image.jpg")))
                    {
                        CFileDialog dlg(TRUE, 
                                        _T("jpg"), 
                                        _T(""), 
                                        0, 
                                        _T("JPEG (*.jpg, *.jpeg)|*.jpg;*.jpeg|All Files (*.*)|*.*||"), 
                                        this);

                        if (IDOK == dlg.DoModal())
                        {
                            HANDLE hFile = 
                                CreateFile(dlg.GetPathName(), 
                                           GENERIC_READ, 
                                           FILE_SHARE_READ, 
                                           NULL, 
                                           OPEN_EXISTING, 
                                           0, 
                                           NULL);

                            if (INVALID_HANDLE_VALUE != hFile)
                            {
                                HANDLE hFileMapping = 
                                    CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);

                                lpResourceData = 
                                    MapViewOfFile(hFileMapping, FILE_MAP_READ, 0, 0, 0);
                                dwResourceSize = 
                                    GetFileSize(hFile, NULL);

                                ULONG ulWritten;
                                pInfo->lpStream->Write(lpResourceData, dwResourceSize, &ulWritten);

                                UnmapViewOfFile(lpResourceData);

                                CloseHandle(hFileMapping);
                            }

                            CloseHandle(hFile);
                        }
                    }
                    else if (0 == lstrcmpi(pInfo->lpszRelativePath, _T("embedded_movie.swf")))
                    {
                        LoadResourceHelper(_T("MOVIE1"), _T("SWF"), lpResourceData, dwResourceSize);

                        ULONG ulWritten;
                        pInfo->lpStream->Write(lpResourceData, dwResourceSize, &ulWritten);
					}
                }
            }
    }

    return CDialog::WindowProc(message, wParam, lParam);
}

// Load resource helper
void LoadResourceHelper( /* in */ LPCTSTR lpszName, 
                         /* in */ LPCTSTR lpszType, 
                         /* out */ LPVOID& lpData, 
                         /* out */ DWORD& dwSize)
{
    HMODULE hModule = GetModuleHandle(NULL);
    HRSRC hResInfo = FindResource(hModule, lpszName, lpszType);
    HGLOBAL hResData = LoadResource(hModule, hResInfo);
    lpData = LockResource(hResData);
    dwSize = SizeofResource(hModule, hResInfo);
}

void CSample06_ExternalResourcesEmbeddingDlg::OnSite() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/"), NULL, NULL, SW_SHOW);
}

void CSample06_ExternalResourcesEmbeddingDlg::OnOrder() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/order.html"), NULL, NULL, SW_SHOW);
}
