//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Sample08_Streaming.rc
//
#define ID_PLAY_SWF_FROM_FILE           3
#define ID_PLAY                         3
#define ID_PLAY_FLV_FROM_FILE           4
#define ID_CRYPT                        5
#define ID_ORDER                        6
#define ID_SITE                         7
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_Sample08_Streaming_DIALOG    102
#define IDR_MAINFRAME                   128
#define IDR_SWF1                        129
#define IDC_STATIC_FPC_PLACE            1000
#define IDC_CHECK_STANDARD_MENU         1001
#define IDC_CHECK_SOUNDS                1002

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
