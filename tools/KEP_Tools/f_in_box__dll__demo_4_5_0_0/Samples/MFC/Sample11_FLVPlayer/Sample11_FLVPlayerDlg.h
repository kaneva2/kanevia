// Sample11_FLVPlayerDlg.h : header file
//

#if !defined(AFX_Sample11_FLVPlayerDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_)
#define AFX_Sample11_FLVPlayerDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample11_FLVPlayerDlg dialog

class CSample11_FLVPlayerDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

	DWORD m_dwHandlerCookie;

    UINT_PTR m_nTimerUpdatePos;

private:

	static HRESULT WINAPI StaticGlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC, LPARAM lParam)
	{
		CSample11_FLVPlayerDlg* pThis = (CSample11_FLVPlayerDlg*)lParam;

		return pThis->GlobalOnLoadExternalResourceHandler(lpszURL, ppStream, hFPC);
	}

	HRESULT WINAPI GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC);

// Construction
public:
	CSample11_FLVPlayerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample11_FLVPlayerDlg)
	enum { IDD = IDD_Sample11_FLVPlayer_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample11_FLVPlayerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual void PostNcDestroy();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample11_FLVPlayerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	afx_msg void OnCheckShowControls();
	afx_msg void OnPlay();
	afx_msg void OnPause();
	afx_msg void OnLoadFLV();
	afx_msg void OnPlayFromFile();
	afx_msg void OnPlayFromURL();
	afx_msg void OnPlayFromRTMP();
	afx_msg void OnPlayEmbedded();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnCheckEnableSounds();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample11_FLVPlayerDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_)
