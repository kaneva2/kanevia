// Sample04_Translucency.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Sample04_Translucency.h"
#include "TransparentFlashPlayerControlWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample04_TranslucencyApp

BEGIN_MESSAGE_MAP(CSample04_TranslucencyApp, CWinApp)
	//{{AFX_MSG_MAP(CSample04_TranslucencyApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample04_TranslucencyApp construction

CSample04_TranslucencyApp::CSample04_TranslucencyApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSample04_TranslucencyApp object

CSample04_TranslucencyApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSample04_TranslucencyApp initialization

BOOL CSample04_TranslucencyApp::InitInstance()
{
	AfxEnableControlContainer();

	//
	if (!FPCIsFlashInstalled())
	{
		AfxMessageBox(_T("The application needs Flash\nFlash is not installed"));
		return FALSE;
	}

	m_hFPC = FPC_LoadRegisteredOCX();

	if (NULL == m_hFPC)
	{
		AfxMessageBox(_T("FPC_LoadRegisteredOCX() failed"));
		return FALSE;
	}

	if (!FPCIsTransparentAvailable())
	{
		AfxMessageBox(_T("Flash transparent is not available"));
		return FALSE;
	}

	RECT rc = { 0, 0, 640, 480 };

	/*
	    Note about WS_EX_LAYERED:

		#if(_WIN32_WINNT >= 0x0500)
		#define WS_EX_LAYERED           0x00080000
	*/

#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED           0x00080000
#endif // WS_EX_LAYERED

	HWND hwndFlashPlayerControl = 
		CreateWindowEx(WS_EX_LAYERED, 
					   (LPCTSTR)FPC_GetClassAtom(m_hFPC), 
					   NULL, 
					   WS_POPUP | WS_VISIBLE, 
					   rc.left, 
					   rc.top, 
					   rc.right - rc.left, 
					   rc.bottom - rc.top, 
					   NULL, 
					   NULL, 
					   NULL, 
					   NULL);

	CTransparentFlashPlayerControlWnd* pFlashProjectorWnd = new CTransparentFlashPlayerControlWnd;
	pFlashProjectorWnd->SubclassWindow(hwndFlashPlayerControl);
	pFlashProjectorWnd->SetWindowText(_T("Sample 4 - Translucency"));
	pFlashProjectorWnd->CenterWindow();
	pFlashProjectorWnd->ShowWindow(SW_SHOW);
	pFlashProjectorWnd->UpdateWindow();

	// Also you can use FPCM_LOADMOVIEFROMMEMORY
	// FPCLoadMovieFromResource is useful for loading from resources, 
	// use FPCM_LOADMOVIEFROMMEMORY to load movie from any memory block
	FPCLoadMovieFromResource(hwndFlashPlayerControl, 0, 0, _T("MOVIE"), _T("SWF"));

	// Disable standard flash context menu
	FPC_PutStandardMenu(hwndFlashPlayerControl, FALSE);

	m_pMainWnd = pFlashProjectorWnd;

	return TRUE;
}

int CSample04_TranslucencyApp::ExitInstance() 
{
	// Unload the flash ocx code
	if (NULL != m_hFPC)
	{
		FPC_UnloadCode(m_hFPC);
		m_hFPC = NULL;
	}
	
	return CWinApp::ExitInstance();
}
