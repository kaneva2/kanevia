//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Sample04_Translucency.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDS_ABOUTBOX                    101
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDR_CONTEXT_MENU                129
#define ID_MINIMIZE                     32776
#define ID_CLOSE                        32777
#define ID_SITE                         32778
#define ID_ORDER                        32779
#define ID_FORUM                        32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
