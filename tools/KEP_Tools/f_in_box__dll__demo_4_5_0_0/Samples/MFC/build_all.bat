set PATH=%PATH%;%VS80COMNTOOLS%..\IDE\

REM TODO: when we will use newest version of VS let's return to "devenv mfc_samples.sln /rebuild ..." form
REM
REM In the past we just wrote:
REM
REM devenv mfc_samples.sln /rebuild "Release|Win32"
REM devenv mfc_samples.sln /rebuild "Release_UseStaticLib|Win32"
REM devenv mfc_samples.sln /rebuild "Release|x64"
REM devenv mfc_samples.sln /rebuild "Release_UseStaticLib|x64"
REM
REM But devenv hanged. It seems that the problem is related with paraller building
REM So let's build project by project

PUSHD

CD %~dp0

FOR /D %%P IN (*.*) DO (
	devenv mfc_samples.sln /project "%%P" /rebuild "Release|Win32"
	devenv mfc_samples.sln /project "%%P" /rebuild "Release_UseStaticLib|Win32"
	devenv mfc_samples.sln /project "%%P" /rebuild "Release|x64"
	devenv mfc_samples.sln /project "%%P" /rebuild "Release_UseStaticLib|x64"
)

POPD

