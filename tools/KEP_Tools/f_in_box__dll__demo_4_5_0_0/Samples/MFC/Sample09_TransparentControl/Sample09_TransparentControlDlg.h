// Sample09_TransparentControlDlg.h : header file
//

#if !defined(AFX_Sample09_TransparentControlDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_)
#define AFX_Sample09_TransparentControlDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample09_TransparentControlDlg dialog

class CSample09_TransparentControlDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

	DWORD m_dwHandlerCookie;

private:

	static HRESULT WINAPI StaticGlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC, LPARAM lParam)
	{
		CSample09_TransparentControlDlg* pThis = (CSample09_TransparentControlDlg*)lParam;

		return pThis->GlobalOnLoadExternalResourceHandler(lpszURL, ppStream, hFPC);
	}

	HRESULT WINAPI GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC);

// Construction
public:
	CSample09_TransparentControlDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample09_TransparentControlDlg)
	enum { IDD = IDD_Sample09_TransparentControl_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample09_TransparentControlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample09_TransparentControlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPlayEmbeddedSwf();
	afx_msg void OnPlayEmbeddedFlv();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	afx_msg void OnPlaySwfFlvFromFile();
	afx_msg void OnCheckStandardMenu();
	afx_msg void OnCheckSounds();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample09_TransparentControlDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_)
