// Sample07_ExternalInterfaceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Sample07_ExternalInterface.h"
#include "Sample07_ExternalInterfaceDlg.h"
#include <atlconv.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample07_ExternalInterfaceDlg dialog

CSample07_ExternalInterfaceDlg::CSample07_ExternalInterfaceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample07_ExternalInterfaceDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSample07_ExternalInterfaceDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample07_ExternalInterfaceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSample07_ExternalInterfaceDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSample07_ExternalInterfaceDlg, CDialog)
	//{{AFX_MSG_MAP(CSample07_ExternalInterfaceDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_CALL_ACTION_SCRIPT_FUNCTION, OnCallActionScriptFunction)
	ON_BN_CLICKED(ID_ORDER, OnOrder)
	ON_BN_CLICKED(ID_SITE, OnSite)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample07_ExternalInterfaceDlg message handlers

BOOL CSample07_ExternalInterfaceDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	// FlashPlayerControl creating
	// NOTE: CALL FPC_LoadRegisteredOCX() or FPC_LoadOCXCodeFromMemory() before creating!
	// See InitInstance() implementation
	{
		RECT rc;
		GetDlgItem(IDC_STATIC_FPC_PLACE)->GetWindowRect(&rc);
		ScreenToClient(&rc);

		m_hwndFlashPlayerControl = 
			CreateWindow((LPCTSTR)FPC_GetClassAtom(theApp.m_hFPC), 
						 NULL, 
						 WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | FPCS_NEED_ALL_KEYS,  // use FPCS_NEED_ALL_KEYS if you movie should handle all keyboard input (ENTER, TAB etc.)
						 rc.left, 
						 rc.top, 
						 rc.right - rc.left, 
						 rc.bottom - rc.top, 
						 m_hWnd, 
						 NULL, 
						 NULL, 
						 NULL);
	}

	// Load movie from resource
	FPCPutMovieFromResource(m_hwndFlashPlayerControl, 0, _T("MOVIE"), _T("SWF"));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSample07_ExternalInterfaceDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSample07_ExternalInterfaceDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSample07_ExternalInterfaceDlg::OnCallActionScriptFunction() 
{
	BSTR bstrRequest = SysAllocString(L"<invoke name=\"CallMeFromApplication\" returntype=\"xml\"><arguments><string>Some text for FlashPlayerControl</string></arguments></invoke>");
	BSTR bstrResponse;

	if (S_OK == 
		FPCCallFunctionBSTR(m_hwndFlashPlayerControl, 
					        bstrRequest, 
							&bstrResponse))
	{
		USES_CONVERSION;

		CString str = OLE2T(bstrResponse);
		AfxMessageBox(CString(_T("The function returned: ")) + str);

		SysFreeString(bstrResponse);
	}

	SysFreeString(bstrRequest);
}

LRESULT CSample07_ExternalInterfaceDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
    if (WM_NOTIFY == message)
    {
        LPNMHDR lpNMHDR = (LPNMHDR)lParam;

        if (m_hwndFlashPlayerControl == lpNMHDR->hwndFrom)
            switch (lpNMHDR->code)
            {
				case FPCN_FLASHCALL:
                {
					SFPCFlashCallInfoStruct* pInfo = (SFPCFlashCallInfoStruct*)lpNMHDR;

					CString str;
					str += _T("The request is: '");
					str += pInfo->request;
					str += _T("'");

					AfxMessageBox(str);

					COleDateTime now = COleDateTime::GetCurrentTime();
					FPCSetReturnValue(m_hwndFlashPlayerControl, 
									  _T("<string>Current time is: ") + 
									  now.Format() + 
									  _T("\r\nThis info is returned from the handler</string>"));

                    return 1;
                }
			}
	}

	return CDialog::WindowProc(message, wParam, lParam);
}

void CSample07_ExternalInterfaceDlg::OnSite() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/"), NULL, NULL, SW_SHOW);
}

void CSample07_ExternalInterfaceDlg::OnOrder() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/order.html"), NULL, NULL, SW_SHOW);
}
