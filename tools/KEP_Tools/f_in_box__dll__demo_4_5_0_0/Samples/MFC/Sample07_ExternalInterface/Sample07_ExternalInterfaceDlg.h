// Sample07_ExternalInterfaceDlg.h : header file
//

#if !defined(AFX_Sample07_ExternalInterfaceDLG_H__44C6CFEC_5CFC_41E9_97F9_165DF37C7A95__INCLUDED_)
#define AFX_Sample07_ExternalInterfaceDLG_H__44C6CFEC_5CFC_41E9_97F9_165DF37C7A95__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample07_ExternalInterfaceDlg dialog

class CSample07_ExternalInterfaceDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

// Construction
public:
	CSample07_ExternalInterfaceDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample07_ExternalInterfaceDlg)
	enum { IDD = IDD_Sample07_ExternalInterface_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample07_ExternalInterfaceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample07_ExternalInterfaceDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnCallActionScriptFunction();
	afx_msg void OnOrder();
	afx_msg void OnSite();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample07_ExternalInterfaceDLG_H__44C6CFEC_5CFC_41E9_97F9_165DF37C7A95__INCLUDED_)
