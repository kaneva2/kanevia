// Sample07_ExternalInterface.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Sample07_ExternalInterface.h"
#include "Sample07_ExternalInterfaceDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample07_ExternalInterfaceApp

BEGIN_MESSAGE_MAP(CSample07_ExternalInterfaceApp, CWinApp)
	//{{AFX_MSG_MAP(CSample07_ExternalInterfaceApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample07_ExternalInterfaceApp construction

CSample07_ExternalInterfaceApp::CSample07_ExternalInterfaceApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSample07_ExternalInterfaceApp object

CSample07_ExternalInterfaceApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSample07_ExternalInterfaceApp initialization

BOOL CSample07_ExternalInterfaceApp::InitInstance()
{
	AfxEnableControlContainer();

	// Check if flash is installed on the machine
	if (!FPCIsFlashInstalled())
	{
		AfxMessageBox(_T("The application needs Flash\nFlash is not installed"));
		return FALSE;
	}

	// Load registered flash activex
	// Don't forget to unload it; see ExitInstance()
	m_hFPC = FPC_LoadRegisteredOCX();

	if (NULL == m_hFPC)
	{
		AfxMessageBox(_T("FPC_LoadRegisteredOCX() failed"));
		return FALSE;
	}


	// Get flash version        
	SFPCVersion FlashVersion;
	FPC_GetVersionEx(m_hFPC, &FlashVersion);

	// Compare major version
	if (FlashVersion.v[3] < 8)
	{
		TCHAR szMsg[1024];
		wsprintf(szMsg, 
				 _T("External API is supported only by Flash 8 and higher\nCurrent version: %d.%d.%d.%d"), 
				 FlashVersion.v[3], 
				 FlashVersion.v[2], 
				 FlashVersion.v[1], 
				 FlashVersion.v[0]);

		AfxMessageBox(szMsg);

		return FALSE;
	}

	CSample07_ExternalInterfaceDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CSample07_ExternalInterfaceApp::ExitInstance() 
{
	// Unload the flash ocx code
	if (NULL != m_hFPC)
	{
		FPC_UnloadCode(m_hFPC);
		m_hFPC = NULL;
	}
	
	return CWinApp::ExitInstance();
}
