// Sample07_ExternalInterface.h : main header file for the Sample07_ExternalInterface application
//

#if !defined(AFX_Sample07_ExternalInterface_H__ECAA90C1_55D3_4387_B22C_0BBB78F2066E__INCLUDED_)
#define AFX_Sample07_ExternalInterface_H__ECAA90C1_55D3_4387_B22C_0BBB78F2066E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample07_ExternalInterfaceApp:
// See Sample07_ExternalInterface.cpp for the implementation of this class
//

class CSample07_ExternalInterfaceApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CSample07_ExternalInterfaceApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample07_ExternalInterfaceApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample07_ExternalInterfaceApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CSample07_ExternalInterfaceApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample07_ExternalInterface_H__ECAA90C1_55D3_4387_B22C_0BBB78F2066E__INCLUDED_)
