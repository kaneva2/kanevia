@echo off
rem ###################################################################################################################
rem 	Need to copy to MyToolsBuilt and ToolsBuilt to keep Visual Studio and git in sync with the files
rem 	Copying files to just MyToolsBuilt allows the Visual Studio solution to determine the build status of KEPPhysics.
rem 	Copying files to just ToolsBuilt allows git to determine the status of the KEPPhysics files.
rem ###################################################################################################################
rem Update include files
echo Copy bullet include files to ToolsBuilt
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\src\*.h" %PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\include\bullet\" /s /y /i /f /exclude:exclude_files.txt

echo Copy .lib and .pdb files to ToolsBuilt

xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletCollision.lib" 			%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletDynamics.lib" 			%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\LinearMath.lib" 				%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\LinearMath.pdb" 				%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletCollision.pdb"			%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletDynamics.pdb" 			%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletCollision_debug.lib" 	%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletDynamics_debug.lib" 	%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\LinearMath_debug.lib" 		%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\LinearMath_debug.pdb" 		%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletCollision_debug.pdb" 	%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletDynamics_debug.pdb" 	%PLATFORM_SOURCE_DIR%"\ToolsBuilt\bullet3-2.83.6\lib\" /y

echo Copy bullet include files to MyToolsBuilt
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\src\*.h" "%PLATFORM_SOURCE_DIR%\MyToolsBuilt\bullet3-2.83.6\include\bullet\" /s /y /i /f /exclude:exclude_files.txt

echo Copy .lib and .pdb files to MyToolsBuilt

xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletCollision.lib" 			%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletDynamics.lib" 			%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\LinearMath.lib" 				%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\LinearMath.pdb" 				%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletCollision.pdb"			%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletDynamics.pdb" 			%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletCollision_debug.lib" 	%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletDynamics_debug.lib" 	%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y 
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\LinearMath_debug.lib" 		%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\LinearMath_debug.pdb" 		%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletCollision_debug.pdb" 	%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y
xcopy "C:\Projects\Tools\KEP_Tools\bullet3-2.83.6\bin\BulletDynamics_debug.pdb" 	%PLATFORM_SOURCE_DIR%"\MyToolsBuilt\bullet3-2.83.6\lib\" /y

pause   																  
