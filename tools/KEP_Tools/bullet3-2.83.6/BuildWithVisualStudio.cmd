@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=bullet3-2.83.6
SET SolutionToBuild="%ThisDir%build3\vs2010\0_Bullet3Solution.sln"
rem SET DebugConfiguration="Debug"
rem SET ReleaseConfiguration="Release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%src\*.h %IncludeDir%\bullet\ /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\BulletCollision.lib %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\BulletDynamics.lib %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\LinearMath.lib %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\LinearMath.pdb %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\BulletCollision.pdb %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\BulletDynamics.pdb %LibDir%\ /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\BulletCollision_debug.lib %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\BulletDynamics_debug.lib %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\LinearMath_debug.lib %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\LinearMath_debug.pdb %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\BulletCollision_debug.pdb %LibDir%\ /E /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\BulletDynamics_debug.pdb %LibDir%\ /E /Y
)
