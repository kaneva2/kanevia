#pragma once
#define Kaneva_Split_Large_Triangles

#if defined(Kaneva_Split_Large_Triangles)
#include "BulletCollision/CollisionShapes/btOptimizedBvh.h"
#include <algorithm>


#if 0 // Helper function for testing.
inline bool IsPointInPoly(const btVector3& ptTest, const btVector3 apts[], size_t nPoints, btScalar tolerance)
{
	btVector3 avEdges[100];
	for( size_t i = 0; i < nPoints; ++i )
		avEdges[i] = apts[(i+1)%nPoints] - apts[i];
	btVector3 vProjDir(0,-1,0);
	btVector3 avNormals[100];
	btScalar afPlaneDots[100];
	for( size_t i = 0; i < nPoints; ++i ) {
		avNormals[i] = avEdges[i].cross(vProjDir);
		if( avNormals[i].length() < .001 )
			return false;
		avNormals[i].normalize();
		afPlaneDots[i] = avNormals[i].dot(apts[i]);
	}
	bool bAllPositive = true;
	bool bAllNegative = true;
	for( size_t i = 0; i < nPoints; ++i ) {
		float fDist = avNormals[i].dot(ptTest) - afPlaneDots[i];
		if( fDist < -tolerance )
			bAllPositive = false;
		if( fDist > tolerance )
			bAllNegative = false;
	}

	bool bContained = false;
	if( bAllPositive || bAllNegative )
		bContained = true;
	else
		bContained = false;
	return bContained;
}
#endif

inline size_t ClipConvexPolyByPlane(const btVector3& planeNormal, btScalar planeD, btVector3 aptsPoly[], size_t nPolyPoints, btVector3 aptsClipped[])
{
	size_t nClippedPoints = 0;
	size_t iPrevPolyPoint = nPolyPoints-1;
	btScalar dotPrev = aptsPoly[iPrevPolyPoint].dot(planeNormal) + planeD;
	for( size_t iPolyPoint = 0; iPolyPoint < nPolyPoints; ++iPolyPoint ) {
		btScalar t_begin = -1;
		btScalar t_end = 2;

		btScalar dotCurrent = aptsPoly[iPolyPoint].dot(planeNormal) + planeD;
		if( dotPrev >= 0 ) {
			if( dotCurrent < 0 ) {
				btScalar t = dotPrev / (dotPrev - dotCurrent);
				if( t > t_begin )
					t_begin = t;
			} else {
				t_begin = 2;
			}
		} else {
			if( dotCurrent > 0 ) {
				btScalar t = dotPrev / (dotPrev - dotCurrent);
				if( t < t_end )
					t_end = t;
			}
		}

		if( t_begin < t_end ) {
			if( t_begin >= 0 ) {
				if( t_begin < 1 )
					aptsClipped[nClippedPoints++] = aptsPoly[iPrevPolyPoint] + (aptsPoly[iPolyPoint] - aptsPoly[iPrevPolyPoint]) * t_begin;
				aptsClipped[nClippedPoints++] = aptsPoly[iPolyPoint];
			} else {
				aptsClipped[nClippedPoints++] = t_end == 2 ? aptsPoly[iPolyPoint] : aptsPoly[iPrevPolyPoint] + (aptsPoly[iPolyPoint] - aptsPoly[iPrevPolyPoint]) * t_end;
			}
		}
		iPrevPolyPoint = iPolyPoint;
		dotPrev = dotCurrent;
	}
	return nClippedPoints;
}

struct	KanevaConvexCastNodeOverlapCallback : public btNodeOverlapCallback
{
	btStridingMeshInterface*	m_meshInterface;
	btTriangleCallback* m_callback;

	btVector3 m_aabbMin;
	btVector3 m_aabbMax;
	btVector3 m_aabbSize;
	btVector3 m_raySource;
	btVector3 m_rayTarget;
	btVector3 m_totalScaling;

	KanevaConvexCastNodeOverlapCallback(btTriangleCallback* callback,btStridingMeshInterface* meshInterface, const btVector3& raySource, const btVector3& rayTarget, const btVector3& aabbMin, const btVector3& aabbMax, const btVector3& localScaling = btVector3(1,1,1))
		:m_meshInterface(meshInterface),
		m_callback(callback),
		m_aabbMin(aabbMin),
		m_aabbMax(aabbMax),
		m_raySource(raySource),
		m_rayTarget(rayTarget)
	{
		m_aabbSize = aabbMax - aabbMin;
		m_totalScaling = m_meshInterface->getScaling() * localScaling;
	}

	virtual void processNode(int nodeSubPart, int nodeTriangleIndex)
	{
		btVector3 m_triangle[3];
		const unsigned char *vertexbase;
		int numverts;
		PHY_ScalarType type;
		int stride;
		const unsigned char *indexbase;
		int indexstride;
		int numfaces;
		PHY_ScalarType indicestype;

		m_meshInterface->getLockedReadOnlyVertexIndexBase(
			&vertexbase,
			numverts,
			type,
			stride,
			&indexbase,
			indexstride,
			numfaces,
			indicestype,
			nodeSubPart);

		unsigned int* gfxbase = (unsigned int*)(indexbase+nodeTriangleIndex*indexstride);
		btAssert(indicestype==PHY_INTEGER||indicestype==PHY_SHORT);

		for (int j=2;j>=0;j--)
		{
			int graphicsindex = indicestype==PHY_SHORT?((unsigned short*)gfxbase)[j]:gfxbase[j];

			if (type == PHY_FLOAT)
			{
				float* graphicsbase = (float*)(vertexbase+graphicsindex*stride);

				m_triangle[j] = btVector3(graphicsbase[0]*m_totalScaling.getX(),graphicsbase[1]*m_totalScaling.getY(),graphicsbase[2]*m_totalScaling.getZ());		
			}
			else
			{
				double* graphicsbase = (double*)(vertexbase+graphicsindex*stride);
				
				m_triangle[j] = btVector3(btScalar(graphicsbase[0])*m_totalScaling.getX(),btScalar(graphicsbase[1])*m_totalScaling.getY(),btScalar(graphicsbase[2])*m_totalScaling.getZ());		
			}
		}

		btVector3 avEdges[3] = {
			m_triangle[1] - m_triangle[0],
			m_triangle[2] - m_triangle[1],
			m_triangle[0] - m_triangle[2]
		};
		btScalar aLenSq[3];
		for( int i = 0; i < 3; ++i )
			aLenSq[i] = avEdges[i].length2();
		unsigned int aSortedSides[3] = {0, 1, 2};
		if( aLenSq[aSortedSides[0]] > aLenSq[aSortedSides[1]] )
			std::swap(aSortedSides[0], aSortedSides[1]);
		if( aLenSq[aSortedSides[0]] > aLenSq[aSortedSides[2]] )
			std::swap(aSortedSides[0], aSortedSides[2]);
		if( aLenSq[aSortedSides[1]] > aLenSq[aSortedSides[2]] )
			std::swap(aSortedSides[1], aSortedSides[2]);

		// Split the triangle if its largest edge is at least length 10 and it is
		// greater than 10 times the size of the object being test against.
		btScalar bboxMinLength = BT_LARGE_FLOAT;
		for( size_t i = 0; i < 3; ++i )
			bboxMinLength = std::min(bboxMinLength, m_aabbSize[i]);
		static const btScalar minSizeSquaredToSplit = 25;
		bool bNeedsSplit = aLenSq[aSortedSides[2]] >= minSizeSquaredToSplit * bboxMinLength * bboxMinLength; // Split based on relative size of triangle to bounding box.

#if 0 // Calculate angle of smallest side to determine need to split. If disabled, split based only on size of triangle.
		// C^2 = A^2 + B^2 - 2AB cos(c)
		// (A^2 + B^2 - C^2) / 2AB = cos(c)
		// Require angle c < x to split
		// cos(c) > cos(x)
		// (A^2 + B^2 - C^2) / 2AB > cos(x)
		// (A^2 + B^2 - C^2) > 2AB / cos(x)
		// (A^2 + B^2 - C^2)^2 > 4*A^2*B^2 / cos^2(x)
		// Let x = 15 degrees. cos(x) = 0.9659258262890682867497431997289. 1 / cos^2(x) = 1.0717967697244908258902146339765
		// (A^2 + B^2 - C^2)^2 > 4*A^2*B^2 * 1.0717967697244908258902146339765
		// let D = (A^2 + B^2 - C^2)
		// D^2 > 4*A^2*B^2*1.0717967697244908258902146339765
		btScalar A2 = aLenSq[1];
		btScalar B2 = aLenSq[2];
		btScalar C2 = aLenSq[0]; // We sorted by edge length, so the shortest side (across from the min angle) is side 0.
		btScalar D = A2 * B2 - C2;
#if 0
		// Prevent overflow by scaling down
		auto RoughRecipSqrt = [](btScalar d) { return scalbn(btScalar(1),-ilogb(d) / 2); };
		//auto RoughRecip = [](btScalar d) { return scalbn(btScalar(1),-ilogb(d)); };
		btScalar scaleFactor = RoughRecipSqrt(aLenSq[1]);
		D *= scaleFactor;
		A2 *= scaleFactor;
#endif
		bool bSmallAngle = D * D > 4 * btScalar(1.0717967697244908258902146339765) * A2 * B2;
		if( bSmallAngle )
			bNeedsSplit = true;
#endif

		bool bTriangleWasSplit = false;
		if( bNeedsSplit ) {
			unsigned int iBasePoint = (aSortedSides[1] + 2) % 3;
			unsigned int iEdge0 = aSortedSides[0]; // Shortest edge.
			unsigned int iEdge1 = aSortedSides[2]; // Longest edge.
			unsigned int iEdge2 = 3 - iEdge0 - iEdge1;
			const btVector3& ptBase = m_triangle[iBasePoint];
			btVector3 vEdge0 = avEdges[iEdge0];
			btVector3 vEdge1 = avEdges[iEdge1];
			if( iEdge0 == iBasePoint )
				vEdge1 = -vEdge1;
			else
				vEdge0 = -vEdge0;

			btScalar dot_00 = aLenSq[iEdge0];
			btScalar dot_11 = aLenSq[iEdge1];
			btScalar dot_01 = vEdge0.dot(vEdge1); // negative to account for opposite direction of vEdge1.
			btScalar denom = dot_00 * dot_11 - dot_01 * dot_01;
			if( denom > 0 ) {
				btScalar inv_denom = 1 / denom;
				// Coefficients for perpendicular lines perpendicular to bisectors
				// of angles at vertex 0, 1, and 2
				btScalar len_0 = sqrt(dot_00);
				btScalar len_1 = sqrt(dot_11);
				btScalar len_2 = sqrt(aLenSq[2]);

				btVector3 avEdgesNormalized[3]; // These edges are sorted by size.
				for( size_t i = 0; i < 3; ++i ) {
					//avEdgesNormalized[i] = avEdges[aSortedSides[i]];
					avEdgesNormalized[i] = avEdges[i];
					avEdgesNormalized[i].safeNormalize();
				}
				btVector3 aBisectors[3];
				static const btScalar cosineClipAngle = btScalar(0.70710678118654752440084436210485); // 45 degrees
				size_t nClipPlanes = 0;
				for( size_t iEdge = 0, iPrevEdge = 2; iEdge < 3; iPrevEdge=iEdge++ ) {
					if( -avEdgesNormalized[iEdge].dot(avEdgesNormalized[iPrevEdge]) > cosineClipAngle )
						aBisectors[nClipPlanes++] = avEdgesNormalized[iEdge] - avEdgesNormalized[iPrevEdge];
				}
//Limited testing shows brute-force method gives more accurate worst-case results, although it could be an artifact of the specific test set.
#if 1 // Brute force processing of 16 vertex positions.
				btScalar aMinDotBisector[3] = {
					BT_LARGE_FLOAT,
					BT_LARGE_FLOAT,
					BT_LARGE_FLOAT,
				};
				btScalar minU = BT_LARGE_FLOAT;
				btScalar minV = BT_LARGE_FLOAT;
				btScalar maxUplusV = -BT_LARGE_FLOAT;
				for( unsigned int i = 0; i < 8; ++i ) {
					btVector3 ptBox;
					for( unsigned int j = 0; j < 3; ++j ) {
						if( i & (1 << j) )
							ptBox[j] = m_aabbMax[j];
						else
							ptBox[j] = m_aabbMin[j];
					}
					for( int k = 0; k < 2; ++k ) {
						btVector3 pt = ptBox;
						if( k == 0 )
							pt += m_raySource;
						else
							pt += m_rayTarget;
						for( size_t i = 0; i < nClipPlanes; ++i ) {
							btScalar dot = pt.dot(aBisectors[i]);
							if( dot < aMinDotBisector[i] )
								aMinDotBisector[i] = dot;
						}
						btVector3 ptRelative = pt - ptBase;
						btScalar dot_0p = vEdge0.dot(ptRelative);
						btScalar dot_1p = vEdge1.dot(ptRelative);
						btScalar numerU = dot_0p * dot_11 - dot_1p * dot_01;
						btScalar numerV = dot_1p * dot_00 - dot_0p * dot_01;
						btScalar numerUplusV = numerU + numerV;
						if( numerU < minU )
							minU = numerU;
						if( numerV < minV )
							minV = numerV;
						if( numerUplusV > maxUplusV )
							maxUplusV = numerUplusV;
					}
				}
#else // Calculation of minimum/maximum distances based on box symmetry.
				btScalar aMinDotBisector[3];
				btScalar bboxSizeMaxDot0 = std::abs(m_aabbHalfSize[0] * vEdge0[0]) + std::abs(m_aabbHalfSize[1] * vEdge0[1]) + std::abs(m_aabbHalfSize[2] * vEdge0[2]);
				btScalar bboxSizeMaxDot1 = std::abs(m_aabbHalfSize[0] * vEdge1[0]) + std::abs(m_aabbHalfSize[1] * vEdge1[1]) + std::abs(m_aabbHalfSize[2] * vEdge1[2]);
				btScalar rayHalfAbsDot0 = std::abs(m_rayHalf.dot(vEdge0));
				btScalar rayHalfAbsDot1 = std::abs(m_rayHalf.dot(vEdge1));
				btVector3 ptCenter = m_rayCenter + m_aabbCenter;
				btVector3 ptRelative = ptCenter - ptBase;
				btScalar ptRel_dot_0 = vEdge0.dot(ptRelative);
				btScalar ptRel_dot_1 = vEdge1.dot(ptRelative);
				btScalar minU = ptRel_dot_0 * dot_11 - ptRel_dot_1 * dot_01 - (rayHalfAbsDot0 + bboxSizeMaxDot0) * dot_11 - (rayHalfAbsDot1 + bboxSizeMaxDot1) * std::abs(dot_01);
				btScalar minV = ptRel_dot_1 * dot_00 - ptRel_dot_0 * dot_01 - (rayHalfAbsDot1 + bboxSizeMaxDot1) * dot_00 - (rayHalfAbsDot0 + bboxSizeMaxDot0) * std::abs(dot_01);
				btScalar maxUplusV =
					+ (ptRel_dot_0 + rayHalfAbsDot0 + bboxSizeMaxDot0) * (dot_11 - dot_01)
					+ (ptRel_dot_1 + rayHalfAbsDot1 + bboxSizeMaxDot1) * (dot_00 - dot_01);
				for( size_t i = 0; i < nClipPlanes; ++i ) {
					btScalar dot = ptCenter.dot(aBisectors[i]);
					dot -= std::abs(m_rayHalf.dot(aBisectors[i]));
					dot -= std::abs(m_aabbHalfSize[0] * aBisectors[i][0]) + std::abs(m_aabbHalfSize[1] * aBisectors[i][1]) + std::abs(m_aabbHalfSize[2] * aBisectors[i][2]);
					aMinDotBisector[i] = dot;
				}
#endif
				minU *= inv_denom;
				minV *= inv_denom;
				maxUplusV *= inv_denom;
				if( minU < 1 && minV < 1 && maxUplusV > 0 ) {
					minU = std::max<btScalar>(minU, 0);
					minV = std::max<btScalar>(minV, 0);
					maxUplusV = std::min<btScalar>(maxUplusV, 1);

					std::pair<btScalar,btScalar> aTriUV[3] = {
						std::make_pair(minU, minV),
						std::make_pair(maxUplusV - minV, minV),
						std::make_pair(minU, maxUplusV - minU),
					};
					static const size_t nMaxPlanes = 3;
					btVector3 aptsClipped[nMaxPlanes+1][6];
					for( size_t i = 0; i < 3; ++i )
						aptsClipped[0][i] = ptBase + vEdge0 * aTriUV[i].first + vEdge1 * aTriUV[i].second;

					size_t nClippedPoints = 3;
					for( size_t i = 0; i < nClipPlanes; ++i ) {
						size_t nNewClippedPoints = ClipConvexPolyByPlane(-aBisectors[i], aMinDotBisector[i], aptsClipped[i], nClippedPoints, aptsClipped[i+1]);
						nClippedPoints = nNewClippedPoints;
					}

					// Make sure our polygon split is at a non-acute vertex.
					size_t iBasePoint = 0;
					if( nClippedPoints > 3 ) {
						btVector3 avPolyEdges[6];
						for( size_t iPolyPoint = 0, iPrevPolyPoint=2+nClippedPoints; iPolyPoint < 3+nClipPlanes; ++iPolyPoint ) {
							avPolyEdges[iPolyPoint] = aptsClipped[nClipPlanes][iPolyPoint] - aptsClipped[nClipPlanes][iPrevPolyPoint];
						}
						// If the edges at any point besides point zero is > 90 degrees, use that as the base point.
						for( size_t iPolyEdge = 0, iPrevPolyEdge = 2+nClippedPoints; iPolyEdge < 2+nClipPlanes; ++iPolyEdge ) {
							if( avPolyEdges[iPolyEdge].dot(avPolyEdges[iPrevPolyEdge]) < 0 ) {
								iBasePoint = iPolyEdge + 1;
								break;
							}
						}
					}
					for( size_t i = 0; i + 2 < nClippedPoints; ++i ) {
						size_t idx0 = iBasePoint;
						size_t idx1 = iBasePoint + i + 1;
						size_t idx2 = idx1 + 1;
						if( idx1 >= nClippedPoints )
							idx1 -= nClippedPoints;
						if( idx2 >= nClippedPoints )
							idx2 -= nClippedPoints;
						btVector3 tri[3] = { aptsClipped[nClipPlanes][idx0], aptsClipped[nClipPlanes][idx1], aptsClipped[nClipPlanes][idx2] };
#if 0 // Test code
						for( size_t j = 0; j < 3; ++j ) {
							btScalar len = (tri[j] - tri[(j+1)%3]).length();
							static btScalar max_len = 0;
							if( len > max_len ) {
								max_len = len;
								printf("max_len: %f\n", max_len);
							}
						}
#endif
						m_callback->processTriangle(tri,nodeSubPart,nodeTriangleIndex);
					}
					bTriangleWasSplit = true;
				}
			}
		}
		if( !bTriangleWasSplit )
			m_callback->processTriangle(m_triangle,nodeSubPart,nodeTriangleIndex);

		m_meshInterface->unLockReadOnlyVertexBase(nodeSubPart);
	}
};

#endif //defined(Kaneva_Split_Large_Triangles)
