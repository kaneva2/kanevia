#include "btWorldStats.h"

#ifndef BT_NO_PROFILE
void btWorldStats::resetStats()
{
	if (m_world && m_world->getWorldStats()) {

		btWorldStats& worldStats(*m_world->getWorldStats());
		for (size_t i = 0; i<ACTIVATION_STATES_COUNT; ++i ) {
			worldStats.m_activationStates[i] = 0;
		}
		worldStats.m_numberOfCollisionObjectsInWorld = 0;
	}
}

void btWorldStats::trackCollisionObjectsActivationState(const btCollisionObject* co) 
{
	if (m_world && m_world->getWorldStats()) {

		btWorldStats& worldStats(*m_world->getWorldStats());
		int activationState = co->getActivationState();
		switch (activationState) 
		{
			case ACTIVE_TAG:
			case ISLAND_SLEEPING:
			case WANTS_DEACTIVATION:
			case DISABLE_DEACTIVATION:
			case DISABLE_SIMULATION:
				worldStats.m_activationStates[activationState]++;
				break;
			default:
				worldStats.m_activationStates[INVALID_ACTIVATION_TAG]++;
		}
	}
}

void btWorldStats::trackCollisonObjectsInWorld()
{
	if (m_world && m_world->getWorldStats()) {

		btWorldStats& worldStats(*m_world->getWorldStats());
		worldStats.m_numberOfCollisionObjectsInWorld = m_world->getCollisionObjectArray().size();
	}
}
#endif // BT_NO_PROFILE
