#pragma once

#include <memory>
#include <string>
#include <deque>
#include <vector>
#include <set>
#include <map>

#include <string.h>

#ifndef __AFX_H__
class CStringA;
#endif

namespace KEP {

	class IMemSizeGadget {
	public:
		IMemSizeGadget()=default;
		IMemSizeGadget(const IMemSizeGadget&)=delete;
		virtual ~IMemSizeGadget()=default;

		virtual size_t GetTotalSizeInBytes() = 0;
		virtual size_t GetTotalObjects() = 0;
		virtual void Release() = 0;
		virtual void Reset() = 0;

		template<typename T>
		bool AddObjectSizeInBytes(const T* pObj, size_t sizeInBytes)
		{
			if (pObj && sizeInBytes) {

				return this->AddMemSize(reinterpret_cast<size_t>(pObj), sizeInBytes);
			}
			return false;
		}

		template<typename T>
		bool AddObjectSizeof(const T* pObj, size_t count=1)
		{
			if (pObj) {

				return this->AddMemSize(reinterpret_cast<size_t>(pObj), sizeof(T) * count);
			} 
			return false;
		}

		template<typename T>
		bool AddObjectSizeof(const T& rObj, size_t count=1)
		{
			return this->AddMemSize(reinterpret_cast<size_t>(&rObj), sizeof(T) * count);
		}

		template<typename T>
		void AddObjectSize(T* pObj)
		{
			if (pObj) {

				if (this->AddObjectSizeof(*pObj)) {

					this->AddObject(*pObj);
				}
			}
		}

		template<typename T>
		void AddObject(T* pObj)
		{
			if (pObj) {

				this->AddObject(*pObj);
			}
		}

		template<typename T>
		void AddObject(const T& rObj)
		{
			::KEP::AddMemSize(this, rObj);
		}

		/// POD Types
		template<>
		void AddObject(const wchar_t& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(wchar_t));
		}

		template<>
		void AddObject(const char& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(char));
		}

		template<>
		void AddObject(const unsigned char& param)
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(unsigned char));
		}

		template<>
		void AddObject(const signed char& param)
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(signed char));
		}

		template<>
		void AddObject(const short& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(short));
		}

		template<>
		void AddObject(const unsigned short& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(unsigned short));
		}

		template<>
		void AddObject(const int& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(int));
		}

		template<>
		void AddObject(const unsigned int& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(unsigned int));
		}

		template<>
		void AddObject(const long& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(long));
		}

		template<>
		void AddObject(const unsigned long& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(unsigned long));
		}

		template<>
		void AddObject(const float& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(float));
		}

		template<>
		void AddObject(const bool& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(bool));
		}

		template<>
		void AddObject(const unsigned long long& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(unsigned long long));
		}

		template<>
		void AddObject(const long long& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(long long));
		}

		template<>
		void AddObject(const double& param) 
		{
			this->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(double));
		} 

		template<>
		void AddObject(const char* pObj)
		{
			if (pObj) {
				this->AddMemSize(reinterpret_cast<size_t>(pObj), strnlen(pObj, MaxStringSize));
			}
		}

		/// std::stl smart pointers
		template<typename T>
		void AddObject(const std::unique_ptr<T>& rObj) 
		{ 
			this->AddObject(rObj.get());
		}

		template<typename T>
		void AddObject(const std::shared_ptr<T>& rObj) 
		{ 
			this->AddObject(rObj.get());
		}

		/// std::stl string and pair
		template<>
		void AddObject(const std::string& str)
		{
			if (!str.empty()) {
				this->AddObjectSizeof(str.c_str(), str.capacity()); 
			}
		}

		template<>
		void AddObject(std::string* pStr)
		{
			if (pStr) {
				this->AddObjectSizeof(*pStr);
				this->AddObject(*pStr);
			}
		}

		/// std::stl containers vector and deque
		template<typename T, typename U>
		void AddObject(const std::pair<T, U>& rPair)
		{
			this->AddObject(rPair.first);
			this->AddObject(rPair.second);
		}

		template<typename T>
		void AddObject(const std::pair<T, std::string>& rPair)
		{
			if (this->AddMemSize(reinterpret_cast<size_t>(&rPair.second)+1,sizeof(std::string)))  {

				this->AddObject(rPair.first);
				this->AddObject(rPair.second);
			}
		}

		template<typename U>
		void AddObject(const std::pair<std::string, U>& rPair)
		{
			if(this->AddMemSize(reinterpret_cast<size_t>(&rPair.first)+1,sizeof(std::string))) {

				this->AddObject(rPair.first);
				this->AddObject(rPair.second);
			}
		}

		/// std::vector
		template<typename T, typename Alloc>
		void AddObject(const std::vector<T, Alloc>& rVector)
		{
			if (rVector.empty()) {

				if (rVector.capacity() > 0)
					this->AddMemSize(reinterpret_cast<size_t>(&rVector), rVector.capacity() * sizeof(T));

			} else {

				// Add each node and any dependent child allocations
				for (auto it = rVector.begin(); it != rVector.end(); ++it) {
					this->AddObject(*it);
				}

				// Add any capacity that is not used
				if (rVector.size() < rVector.capacity()) {
					this->AddMemSize(reinterpret_cast<size_t>(&rVector)+rVector.size()+1, (rVector.capacity()-rVector.size()) * sizeof(T));
				}
			}
		}

		template<typename T, typename Alloc>
		void AddObject(const std::vector<T*, Alloc>& rVector)
		{
			if (rVector.empty()) {

				if (rVector.capacity() > 0)
					this->AddMemSize(reinterpret_cast<size_t>(&rVector), rVector.capacity() * sizeof(T*));

			} else {

				// Includes any excess capacity that is not used
				if (this->AddMemSize(reinterpret_cast<size_t>(&rVector)+1, rVector.capacity() * sizeof(T*))) {

					for (auto it = rVector.begin(); it != rVector.end(); ++it) {
						this->AddObject(*it);
					}
				}
			}
		}

		template<typename Alloc>
		void AddObject(const std::vector<std::string, Alloc>& rVector)
		{
			if (rVector.empty()) {

				if (rVector.capacity() > 0)
					this->AddMemSize(reinterpret_cast<size_t>(&rVector), rVector.capacity() * sizeof(std::string));

			} else {

				if (this->AddMemSize(reinterpret_cast<size_t>(&rVector)+1, rVector.size() * sizeof(std::string)))
				{
					// Add each node and any dependent child allocations
					for (auto it = rVector.begin(); it != rVector.end(); ++it) {
						this->AddObject(*it);
					}
				}

				// Add any capacity that is not used
				if (rVector.size() < rVector.capacity()) {
					this->AddMemSize(reinterpret_cast<size_t>(&rVector)+rVector.size(), (rVector.capacity()-rVector.size()) * sizeof(std::string));
				}
			}
		}

		template<typename Alloc>
		void AddObject(const std::vector<void*, Alloc>& rVector)
		{
			if (rVector.capacity() > 0)
				this->AddMemSize(reinterpret_cast<size_t>(&rVector), rVector.capacity() * sizeof(void*));
		}

		/// std::deque
		template<typename T, typename Alloc>
		void AddObject(const std::deque<T, Alloc>& rDeque)
		{		
			for (auto it = rDeque.begin(); it != rDeque.end(); ++it) {			
					this->AddObject(*it);		
			}
		}

		template<typename T, typename Alloc>
		void AddObject(const std::deque<T*, Alloc>& rDeque)
		{		
			if (this->AddMemSize(reinterpret_cast<size_t>(&rDeque)+1, rDeque.size() * sizeof(T*))) {
				for (auto it = rDeque.begin(); it != rDeque.end(); ++it) {			
						this->AddObject(*it);		
				}
			}
		}

		template<typename Alloc>
		void AddObject(const std::deque<std::string, Alloc>& rDeque)
		{		
			if (this->AddMemSize(reinterpret_cast<size_t>(&rDeque)+1, rDeque.size() * sizeof(std::string)))
			{
				for (auto it = rDeque.begin(); it != rDeque.end(); ++it) {			
						this->AddObject(*it);		
				}
			}
		}

		//From stl file <xtree>
		//
		//	template<class _Value_type,
		//		class _Voidptr>
		//		struct _Tree_node
		//	{	// tree node
		//		_Voidptr _Left;	// left subtree, or smallest element if head
		//		_Voidptr _Parent;	// parent, or root of tree if head
		//		_Voidptr _Right;	// right subtree, or largest element if head
		//		char _Color;	// _Red or _Black, _Black if head
		//		char _Isnil;	// true only if head (also nil) node
		//		_Value_type _Myval;	// the stored value, unused if head
		//	 
		//	private:
		//		_Tree_node& operator=(const _Tree_node&);
		//	};

		/// std::stl associative containers map and set
		template<typename K, typename T, typename Comp, typename Alloc>
		void AddObject( const std::map<K, T, Comp, Alloc>& rMap)
		{
			// _Tree_node struct to get correct element size
			typedef std::pair<const K, T> value_type;
			struct _Tree_node { void* _Left; void* _Parent; void* _Right; char _Color; char _Isnil; value_type _MyVal; };
			// Use the address of the _Tree_node's _Left ptr as a unique identifier.
			size_t offsetTo_Left = sizeof(_Tree_node) - sizeof(value_type);

			for(auto it = rMap.begin(); it != rMap.end(); ++it )
			{			
				if( this->AddMemSize(reinterpret_cast<size_t>(&(*it)) - offsetTo_Left, sizeof(_Tree_node) - sizeof(value_type)))
				{
					this->AddObject(*it);
				}
			}
		}

		template<typename K, typename T, typename Comp, typename Alloc>
		void AddObject(const std::map<K, T*, Comp, Alloc>& rMap)
		{
			// _Tree_node struct to get correct node size
			typedef std::pair<const K, T*> value_type;
			struct _Tree_node { void* _Left; void* _Parent; void* _Right; char _Color; char _Isnil; value_type _MyVal; };
			// Use the address of the _Tree_node's _Left ptr as a unique identifier.
			size_t offsetTo_Left = sizeof(_Tree_node) - sizeof(value_type);

			for (auto it = rMap.begin(); it != rMap.end(); ++it)
			{
				if (this->AddMemSize(reinterpret_cast<size_t>(&(*it)) - offsetTo_Left, sizeof(_Tree_node) - sizeof(value_type) + sizeof(T*)))
				{
					this->AddObject(*it);
				}
			}
		}

		template<typename T,typename Comp, typename Alloc>
		void AddObject( const std::set<T, Comp, Alloc>& rSet)
		{
			// _Tree_node struct to get correct node size
			typedef T value_type;
			struct _Tree_node { void* _Left; void* _Parent; void* _Right; char _Color; char _Isnil; value_type _MyVal; };
			// Use the address of the _Tree_node's _Left ptr as a unique identifier.
			size_t offsetTo_Left = sizeof(_Tree_node) - sizeof(value_type);

			for( auto it = rSet.begin(); it != rSet.end(); ++it )
			{	
				// When each element is stored by value, the corresponding AddObject function will add the size of the element,
				// so we need to add the sizeof the node less the size of the element.
				if (this->AddMemSize(reinterpret_cast<size_t>(&(*it)) - offsetTo_Left, sizeof(_Tree_node)-sizeof(value_type)))
				{
					this->AddObject(*it);		
				}
			}
		}

		template<typename T, typename Comp, typename Alloc>
		void AddObject(const std::set<T*, Comp, Alloc>& rSet)
		{
			// _Tree_node struct to get correct node size
			typedef T* value_type;
			struct _Tree_node { void* _Left; void* _Parent; void* _Right; char _Color; char _Isnil; value_type _MyVal; };
			// Use the address of the _Tree_node's _Left ptr as a unique identifier.
			size_t offsetTo_Left = sizeof(_Tree_node) - sizeof(value_type);

			for (auto it = rSet.begin(); it != rSet.end(); ++it)
			{
				// When each element is stored as a pointer, the corresponding AddObject function will add the size of element,
				// but not the size of the pointer. Consequently we first add the size of the node which includes the size of
				// the pointer.
				if (this->AddMemSize(reinterpret_cast<size_t>(&(*it)) - offsetTo_Left, sizeof(_Tree_node)))
				{
					this->AddObject(*it);
				}
			}
		}

		template<typename Comp, typename Alloc>
		void AddObject( const std::set<std::string, Comp, Alloc>& rSet)
		{
			// _Tree_node struct to get correct node size
			typedef std::string value_type;
			struct _Tree_node { void* _Left; void* _Parent; void* _Right; char _Color; char _Isnil; value_type _MyVal; };
			// Use the address of the _Tree_node's _Left ptr as a unique identifier.
			size_t offsetTo_Left = sizeof(_Tree_node) - sizeof(value_type);

			for( auto it = rSet.begin(); it != rSet.end(); ++it )
			{			
				if (this->AddMemSize(reinterpret_cast<size_t>(&(*it)) - offsetTo_Left, sizeof(_Tree_node)))
				{
					this->AddObject(*it);		
				}
			}
		}

		// For MFC CStringA classes
		const size_t MaxStringSize = 80;
		virtual void AddObject(const CStringA* pCString) = 0;
		virtual void AddObject(const CStringA& aCString) = 0;
		virtual void AddObject(const std::vector<CStringA*>& rVector) = 0;

		// Core function responsible for adding the size of an object
		virtual bool AddMemSize(size_t objectId, size_t sizeInBytes) = 0;
	};

	// Make AddMemSize in the KEP namespace instead of the global namespace
	template<typename T>
	void AddMemSize(IMemSizeGadget* const memSizeGadget, const T& rObj)
	{
		rObj.GetMemSize(memSizeGadget);
	}

} // namespace KEP


