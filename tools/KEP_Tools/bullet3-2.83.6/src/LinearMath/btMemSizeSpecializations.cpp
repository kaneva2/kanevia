#include "IMemSizeGadget.h"
#include "btMemSizeSpecializations.h"
#include "btPoolAllocator.h"
#include "BulletCollision/BroadphaseCollision/btBroadphaseProxy.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"
#include "BulletDynamics/ConstraintSolver/btSolverBody.h"  	
#include "BulletDynamics/ConstraintSolver/btSolverConstraint.h"  	

void btSolverBody::GetMemSize(KEP::IMemSizeGadget* pMemSizeGadget) const
{
	if (pMemSizeGadget->AddObjectSizeof(this)) {

		pMemSizeGadget->AddObject(m_originalBody);
	}
}

void btSolverConstraint::GetMemSize(KEP::IMemSizeGadget* pMemSizeGadget) const
{
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void btBroadphasePair::GetMemSize(KEP::IMemSizeGadget* pMemSizeGadget) const
{
	if (pMemSizeGadget->AddObjectSizeof(this)) {

		pMemSizeGadget->AddObject(m_pProxy0);
		pMemSizeGadget->AddObject(m_pProxy1);
	}
}

void btPoolAllocator::GetMemSize(KEP::IMemSizeGadget* pMemSizeGadget) const
{
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObjectSizeInBytes(m_pool, m_maxElements * m_elemSize);
	}
}

