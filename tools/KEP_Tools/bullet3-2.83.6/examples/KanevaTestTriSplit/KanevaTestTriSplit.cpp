/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2015 Google Inc. http://bulletphysics.org

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/



#include "KanevaTestTriSplit.h"

#include "btBulletDynamicsCommon.h"
#define ARRAY_SIZE_Y 5
#define ARRAY_SIZE_X 5
#define ARRAY_SIZE_Z 5

#include "LinearMath/btVector3.h"
#include "LinearMath/btAlignedObjectArray.h"

#include "../CommonInterfaces/CommonRigidBodyBase.h"
#include "BulletCollision/NarrowPhaseCollision/btContinuousConvexCollision.h"
#include "BulletCollision/CollisionShapes/btTriangleShape.h"
#include "BulletCollision/NarrowPhaseCollision/btGjkEpaPenetrationDepthSolver.h"
#include "BulletCollision/NarrowPhaseCollision/btGjkPairDetector.h"
#include "BulletCollision/NarrowPhaseCollision/btPointCollector.h"
#include <iostream>
#include <thread>
#include <algorithm>

static void PrintTransform(const btTransform& transform) {
	std::cout 
		<< transform.getBasis()[0][0] << ", " << transform.getBasis()[0][1] << ", " << transform.getBasis()[0][2] << "\n"
		<< transform.getBasis()[1][0] << ", " << transform.getBasis()[1][1] << ", " << transform.getBasis()[1][2] << "\n"
		<< transform.getBasis()[2][0] << ", " << transform.getBasis()[2][1] << ", " << transform.getBasis()[2][2] << "\n"
		<< transform.getOrigin()[0] << ", " << transform.getOrigin()[1] << ", " << transform.getOrigin()[2] << "\n";
};
static void PrintInput(const btGjkPairDetector::ClosestPointInput& input) {
	std::cout << "transformA:\n";
	PrintTransform(input.m_transformA);
	std::cout << "transformB:\n";
	PrintTransform(input.m_transformB);
};
static void DiffInput(const btGjkPairDetector::ClosestPointInput& left, const btGjkPairDetector::ClosestPointInput& right) {
	for( int a = 0; a < 2; ++a ) {
		std::cout << "Diff " << (a==0 ? "A" : "B") << "\n";
		const btTransform& l = a == 0 ? left.m_transformA : left.m_transformB;
		const btTransform& r = a == 0 ? right.m_transformA : right.m_transformB;
		for( int i = 0; i < 3; ++i ) {
			for( int j = 0; j < 3; ++j ) {
				std::cout << l.getBasis()[i][j] - r.getBasis()[i][j] << " ";
			}
			std::cout << l.getOrigin()[i] - r.getOrigin()[i];
			std::cout << "\n";
		}
	}
};

btScalar Cast (btCapsuleShape* pCapsuleShape, btTriangleShape* pTriangleShape, const btVector3& capsulePositionFrom, const btVector3& capsulePositionTo) {
	btVoronoiSimplexSolver	simplexSolver;
	btGjkEpaPenetrationDepthSolver	gjkEpaPenetrationSolver;
	btContinuousConvexCollision convexCaster(pCapsuleShape, pTriangleShape, &simplexSolver, &gjkEpaPenetrationSolver);
	btConvexCast::CastResult castResult;
	castResult.m_fraction = btScalar(1.);
	castResult.m_allowedPenetration = 0;
	btTransform triangleToWorld = btTransform(btMatrix3x3(0.999999940, 0, 0, 0, 1, 0, 0, 0, 0.999999940), btVector3(0,0,0));
	//btTransform triangleToWorld = btTransform::getIdentity();
	btTransform convexShapeFrom1(btMatrix3x3::getIdentity(), capsulePositionFrom);
	btTransform convexShapeTo(btMatrix3x3::getIdentity(), capsulePositionTo);
	bool b1 = convexCaster.calcTimeOfImpact(convexShapeFrom1, convexShapeTo, triangleToWorld, triangleToWorld, castResult);

	btScalar pos = convexShapeFrom1.getOrigin()[1] + castResult.m_fraction * (convexShapeTo.getOrigin()[1] - convexShapeFrom1.getOrigin()[1]);
	return pos;
};


void DoTestTriangle(btDynamicsWorld* pWorld, const btVector3 aptTri[3]);
void DoTestTriangle(btDynamicsWorld* pWorld, const double afLegLengths[3], double fRot);

void DoTest(btDynamicsWorld* pWorld)
{
	/*
	btVector3 aptTri[3] = {
#if 1
		btVector3(0.000000,0, 0.000000),
		btVector3(1000.000000,0, 0.000000),
		//btVector3(1004.09625,0,42.6110077)
		//btVector3(984.399902,0,34.3774948)
		btVector3(710.642273,0,710.642273)
#else
		btVector3(0.000000,0, 0.000000),
		btVector3(1000.000000,0, 0.000000),
		btVector3(0, 0, 1000)
#endif
	};

	DoTestTriangle(pWorld, aptTri);
	return;
	//*/

	static const double pi = 3.14159265358979323846f;
	size_t nLengthSteps = 100;
	size_t nRotSteps = 1;
	size_t nAngleSteps = 100;
	double fMinAngle = (pi / 180) * 2;
	double fMaxAngle = (pi / 180) * 45;
	for( size_t iAngleStep = 0; iAngleStep <= nAngleSteps; ++iAngleStep ) {
		double angle = fMinAngle + (fMaxAngle - fMinAngle) * iAngleStep / nAngleSteps;
		printf("angle: %f\n", angle);
		for( size_t iLengthStep = 0; iLengthStep <= nLengthSteps; ++iLengthStep ) {
			double fLegLength1 = 1000;
			double fLegLength2 = (iLengthStep + 0.5) * fLegLength1 / nLengthSteps;
			printf("LegLength2: %f\n", fLegLength2);
			for( size_t iRotStep = 0; iRotStep < nRotSteps; ++iRotStep ) {
				double fRot = 2 * pi * iRotStep / nRotSteps;
				double fLegLength3 = sqrt(fLegLength1 * fLegLength1 + fLegLength2 * fLegLength2 - 2 * fLegLength1 * fLegLength2 * cos(angle));
				double afLegLengths[3] = {
					fLegLength1,
					fLegLength2,
					fLegLength3
				};
				DoTestTriangle(pWorld, afLegLengths, fRot);
				fflush(stdout);
			}
		}
	}
}

void DoTestTriangle(btDynamicsWorld* pWorld, const double afLegLengths[3], double fRot)
{
	btVector3 aptTri[3] = {
		{0, 0, 0},
		{0, 0, 0},
		{0, 0, 0},
	};
	float fLeg1Length = float(afLegLengths[0]);
	float fLeg2Length = float(afLegLengths[1]);
	float fLeg3Length = float(afLegLengths[2]);
	float fCosAngle = (fLeg1Length * fLeg1Length + fLeg2Length * fLeg2Length - fLeg3Length * fLeg3Length) / (2 * fLeg1Length * fLeg2Length);
	//float fCosAngle = cos((pi/180) * 0);
	if( fabs(fCosAngle) > 1 ) {
		printf("Invalid triangle");
		return;
	}
	float fSinAngle = sqrt(1 - fCosAngle * fCosAngle);
	//float pi = 3.14159265358979323846f;
	//float fTriRotRad = (pi/180) * 0;
	float fTriRotRad = float(fRot);
	aptTri[1][0] = fLeg1Length;
	aptTri[2][0] = fLeg2Length * fCosAngle;
	aptTri[2][2] = fLeg2Length * fSinAngle;
	for( size_t i = 0; i < 3; ++i ) {
		float c = std::cos(fTriRotRad);
		float s = std::sin(fTriRotRad);
		aptTri[i].setValue(
			aptTri[i][0] * c - aptTri[i][2] * s,
			aptTri[i][1],
			aptTri[i][0] * s + aptTri[i][2] * c
			);
	}

	float fOffsetX = 0;
	float fOffsetY = 0;
	float fOffsetZ = 0;
	btVector3 vTriOffset(fOffsetX, fOffsetY, fOffsetZ);
	for( size_t i = 0; i < 3; ++i ) {
		aptTri[i] += vTriOffset;
	}
	DoTestTriangle(pWorld, aptTri);
}

void DoTestTriangle(btDynamicsWorld* pWorld, const btVector3 aptTri[3])
{
	btVector3 aTriEdges[3] = {
		aptTri[1] - aptTri[0],
		aptTri[2] - aptTri[1],
		aptTri[0] - aptTri[2],
	};

	printf("Triangle:\n");
	printf("Side lengths: %f, %f, %f\n", aTriEdges[0].length(), aTriEdges[1].length(), aTriEdges[2].length());
	for( size_t i = 0; i < 3; ++i )
		printf("(%.9g,%.9g)\n", aptTri[i][0], aptTri[i][2]);

	// Add triangle to world as a bvhTriangleMeshShape
	btTriangleMesh* mesh = new btTriangleMesh();
	mesh->addTriangle(aptTri[0], aptTri[1], aptTri[2], true);
	btBvhTriangleMeshShape* trimesh = new btBvhTriangleMeshShape(mesh,true,true);
	btVector3 vScale(0.5,0.25,1.5);
	btScaledBvhTriangleMeshShape* trimeshScaled = new btScaledBvhTriangleMeshShape(trimesh, vScale);
	//btRigidBody* pRigidBody = new btRigidBody(0, nullptr, trimesh);
	btRigidBody* pRigidBody = new btRigidBody(0, nullptr, trimeshScaled);
	pRigidBody->setWorldTransform(btTransform::getIdentity());
	pWorld->addRigidBody(pRigidBody);

	btScalar fCapsuleRadius = 0.5f;
	btScalar fCapsuleHeight = 1.0f;
	btCapsuleShape* pCapsuleShape = new btCapsuleShape(fCapsuleRadius, fCapsuleHeight);
	btTriangleShape* pTriangleShape = new btTriangleShape(aptTri[0], aptTri[1], aptTri[2]);
	pTriangleShape->setMargin(0);

	btVector4 aPlanes[3];
	for( size_t i = 0; i < 3; ++i ) {
		btVector3 normal = aTriEdges[i].cross(btVector3(0,-1,0));
		normal.normalize();
		btScalar d = normal.dot(aptTri[i]);
		aPlanes[i].setValue(normal[0], normal[1], normal[2], -d);
	}
	btScalar aMaxError[2] = { 0, 0 };
	size_t anErrors[2] = {0};
	size_t anSucceeded[2] = {0};
	btScalar afOffsets[2] = { 0, btScalar(.01) };
	size_t nStepsU = 100;
	size_t nStepsV = 100;
	for( size_t iu = 0; iu < nStepsU; ++iu ) {
		btScalar u = btScalar(iu + 0.5) / nStepsU;
		for( size_t iv = 0; iv < nStepsV; ++iv ) {
			btScalar v = btScalar(iv + 0.5) / nStepsV;
			if( u + v > 1 )
				break;

			btVector3 pt = (1-u-v) * aptTri[0] + u * aptTri[1] + v * aptTri[2];
			pt *= vScale;
			btScalar fCapsuleY = pt[1] + fCapsuleHeight * 0.5f + fCapsuleRadius;
			btVector3 ptTo(pt[0], fCapsuleY - 10, pt[2]);
			for( size_t i = 0; i < 2; ++i ) {
				btVector3 ptFrom{pt[0], fCapsuleY + afOffsets[i], pt[2]};
#if 0
				btScalar y = Cast(pCapsuleShape, pTriangleShape, ptFrom, ptTo);
#else
				btCollisionWorld::ClosestConvexResultCallback resultCallback(ptFrom, ptTo);
				pWorld->convexSweepTest(pCapsuleShape, btTransform(btMatrix3x3::getIdentity(), ptFrom), btTransform(btMatrix3x3::getIdentity(), ptTo), resultCallback);
				btScalar y = ptFrom[1] + resultCallback.m_closestHitFraction * (ptTo[1] - ptFrom[1]);
#endif

				btScalar diff = y - fCapsuleY;
				if( abs(diff) > aMaxError[i] )
					aMaxError[i] = abs(diff);
				if( abs(diff) > .01 ) {
					++anErrors[i];
				} else {
					++anSucceeded[i];
				}
			}
		}
	}
	for( size_t i = 0; i < 2; ++i ) {
		//printf("Offset: %f\n", afOffsets[i]);
		if( anErrors[i] == 0 )
			printf("Succeeded.\n");
		else
			printf("%u errors.\n", anErrors[i]);
		//printf("%u errors. %u succeeded.\n", anErrors[i], anSucceeded[i]);
		printf("Max error: %f\n", aMaxError[i]);
	}
	pWorld->removeRigidBody(pRigidBody);
}

struct BasicExample : public CommonRigidBodyBase
{
	BasicExample(struct GUIHelperInterface* helper)
		:CommonRigidBodyBase(helper)
	{
	}
	virtual ~BasicExample(){}
	virtual void initPhysics();
	virtual void renderScene();
	void resetCamera()
	{
		float dist = 41;
		float pitch = 52;
		float yaw = 35;
		float targetPos[3]={0,0.46,0};
		m_guiHelper->resetCamera(dist,pitch,yaw,targetPos[0],targetPos[1],targetPos[2]);
	}
};

void BasicExample::initPhysics()
{
	m_guiHelper->setUpAxis(1);

	createEmptyDynamicsWorld();
	
	m_guiHelper->createPhysicsDebugDrawer(m_dynamicsWorld);

	if (m_dynamicsWorld->getDebugDrawer())
		m_dynamicsWorld->getDebugDrawer()->setDebugMode(btIDebugDraw::DBG_DrawWireframe+btIDebugDraw::DBG_DrawContactPoints);


	// DoTest() tests the modified code to see that it solves the problem in a number of cases.
	DoTest(m_dynamicsWorld); return;

	// This is code that was used to isolate the problem.

#if 0 // Original error
	float fCapsuleRadius = 0.400659353;
	float fCapsuleHeight = 2*0.458888859;
	btVector3 capsulePositionFrom1(325.221649, 42.3123322, -365.150391);

	btVector3 triVerts[] = {
		{ 603.492310, 41.4527969, -644.420654},
		{ 2.77213961e-13, 41.4527969, 0.000000000},
		{ 600.955566, 41.4527969, -691.739868}
	};
#elif 0 // edited
	float fCapsuleRadius = 0.5f;
	float fCapsuleHeight = 1;
	//btVector3 capsulePositionFrom1(325, 2, -365);
	btVector3 capsulePositionFrom1(500, 0.5f, -550);

	float fTriHeight = capsulePositionFrom1[1] - (fCapsuleRadius + 0.5f*fCapsuleHeight);
	btVector3 triVerts[] = {
		/*
		btVector3{ 600, fTriHeight, -650},
		btVector3{ 0, fTriHeight, 0},
		btVector3{ 600, fTriHeight, -700},
		*/
		btVector3{ 1000, fTriHeight, -1000},
		btVector3{ 0, fTriHeight, 0},
		btVector3{ 1000, fTriHeight, -1200},
	};
#else // error with new code
	float fCapsuleRadius = 0.400659353;
	float fCapsuleHeight = 2*0.458888859;
	//btVector3 capsulePositionFrom1(152.400009, 42.8282928, -182.880005);
	//btVector3 capsulePositionFrom1(152.400009, 42.2238121, -182.880005);
	btVector3 capsulePositionFrom1(152.400009, 0, -182.880005);

	//float fTriHeight = capsulePositionFrom1[1] - (fCapsuleRadius + 0.5f*fCapsuleHeight);
	float fTriHeight = 41.4527969;
	btVector3 triVerts[] = {
/*
		btVector3{ 600.955566, fTriHeight, -691.739868},
		btVector3{ 2.77213961e-13, fTriHeight, 0},
		btVector3{ 620.503967, fTriHeight, -717.443298},
*/
		btVector3{ 2.77213961e-13, fTriHeight, 0},
		btVector3{ 2.77213961e-13, fTriHeight, -972.921509},
		btVector3{ 620.503967, fTriHeight, -717.443298},
	};
	capsulePositionFrom1[1] = fTriHeight + fCapsuleRadius + 0.5f * fCapsuleHeight;
#endif

	btVector3 v = capsulePositionFrom1;
	v[1] = fTriHeight;
	v -= triVerts[1];
	btVector3 v1 = triVerts[2] - triVerts[1];
	btVector3 v2 = triVerts[0] - triVerts[1];
	btScalar denom = v1[0] * v2[2] - v1[2] * v2[0];
	btScalar numer1 = v[0] * v2[2] - v[2] * v2[0];
	btScalar numer2 = v1[0] * v[2] - v1[2] * v[0];
	btScalar f1 = numer1 / denom;
	btScalar f2 = numer2 / denom;
	btVector3 vR = f1 * v1 + f2 * v2;
	btCapsuleShape* capsuleShape = new btCapsuleShape(fCapsuleRadius, fCapsuleHeight);
	//capsuleShape->setMargin(0);
	btVector3 capsulePositionTo = capsulePositionFrom1 - btVector3(0,1.0f,0);
	//btVector3 capsulePositionTo(325.221649, 41.1962738, -365.150391); // Data from original error
	btVector3 capsulePositionFrom2 = capsulePositionFrom1 + btVector3(0,.01f,0);

	btVector3 capsulePosition = capsulePositionFrom1;
	auto calcCapsuleBottom = [capsuleShape](const btVector3& capsulePosition) -> btScalar {
		return capsulePosition[1] - (capsuleShape->getHalfHeight() + capsuleShape->getRadius());
	};
	btScalar capsuleBottom = calcCapsuleBottom(capsulePositionFrom1);

	btVector3 edges[3];
	for( int i = 0; i < 3; ++i )
		edges[i] = triVerts[(i+1)%3] - triVerts[i];
	btVector3 edgeNormals[3];
	btScalar dists[3];
	for( int i = 0; i < 3; ++i ) {
		edgeNormals[i] = edges[i].cross(btVector3(0,-1,0));
		edgeNormals[i].normalize();
		dists[i] = edgeNormals[i].dot(triVerts[i]);
	}
	btVector3 triNormal = edges[1].cross(edges[0]);
	triNormal.normalize();
	btScalar triPlaneDist = triNormal.dot(triVerts[0]);
	btScalar distToPlanes[3];
	for( int i = 0; i < 3; ++i ) {
		distToPlanes[i] = edgeNormals[i].dot(capsulePosition) - dists[i];
	}
	btScalar distToTri = triNormal.dot(capsulePosition) - triPlaneDist;

	btTriangleShape triangleShape(triVerts[0], triVerts[1], triVerts[2]);
	triangleShape.setMargin(0);

	{
		btTriangleMesh* mesh = new btTriangleMesh();
		mesh->addTriangle(triVerts[0], triVerts[1], triVerts[2], true);
		btBvhTriangleMeshShape* trimesh = new btBvhTriangleMeshShape(mesh,true,true);
		btCollisionWorld::ClosestConvexResultCallback resultCallback(capsulePositionFrom2, capsulePositionFrom1-btVector3(0,10,0));
		btRigidBody* pRigidBody = new btRigidBody(0, nullptr, trimesh);
		pRigidBody->setWorldTransform(btTransform::getIdentity());
		m_dynamicsWorld->addRigidBody(pRigidBody);
		m_dynamicsWorld->convexSweepTest(capsuleShape, btTransform(btMatrix3x3::getIdentity(), capsulePositionFrom2), btTransform(btMatrix3x3::getIdentity(), capsulePositionFrom1-btVector3(0,10,0)), resultCallback);
	}

	auto CalcClosestPoints = [capsuleShape,triangleShape](float fOffsetY) -> float {
		btVoronoiSimplexSolver	simplexSolver;
		btGjkEpaPenetrationDepthSolver	gjkEpaPenetrationSolver;
		btPointCollector pointCollector;
		//btTransform transA(btMatrix3x3::getIdentity(), btVector3(500,0.499997199,-550));
		btTransform transA(btMatrix3x3::getIdentity(), btVector3(152.4, 42.8283, -182.88));
		//btTransform transA(btMatrix3x3::getIdentity(), btVector3(500,fOffsetY,-550));
		btTransform transB = btTransform::getIdentity();
		btGjkPairDetector::ClosestPointInput input;
		input.m_transformA = transA;
		input.m_transformB = transB;
		btGjkPairDetector gjk(capsuleShape,&triangleShape,capsuleShape->getShapeType(),triangleShape.getShapeType(),capsuleShape->getMargin(),triangleShape.getMargin(),&simplexSolver,&gjkEpaPenetrationSolver);
		//PrintInput(input);
		pointCollector.m_distance = BT_LARGE_FLOAT;
		gjk.getClosestPoints(input, pointCollector, 0);
		printf("dist: %f\n", pointCollector.m_distance);
		return pointCollector.m_distance;
	};
	//CalcClosestPoints(0.5);
	//float fErrorHeight = 0.501112998;
	//float fNoErrorHeight = 0.501113057;
	//CalcClosestPoints(fErrorHeight);
	//CalcClosestPoints(fNoErrorHeight);

	btVoronoiSimplexSolver	simplexSolver;
	btGjkEpaPenetrationDepthSolver	gjkEpaPenetrationSolver;
	btContinuousConvexCollision convexCaster(capsuleShape,&triangleShape,&simplexSolver,&gjkEpaPenetrationSolver);
	btConvexCast::CastResult castResult1, castResult2;
	castResult1.m_fraction = castResult2.m_fraction = btScalar(1.);
	castResult1.m_allowedPenetration = castResult2.m_allowedPenetration = 0;
	btTransform triangleToWorld = btTransform(btMatrix3x3(0.999999940, 0, 0, 0, 1, 0, 0, 0, 0.999999940), btVector3(0,0,0));
	//btTransform triangleToWorld = btTransform::getIdentity();
	btTransform convexShapeFrom1(btMatrix3x3::getIdentity(), capsulePositionFrom1);
	btTransform convexShapeTo(btMatrix3x3::getIdentity(), capsulePositionTo);
	bool b1 = convexCaster.calcTimeOfImpact(convexShapeFrom1, convexShapeTo, triangleToWorld, triangleToWorld, castResult1);

	btTransform convexShapeFrom2(btMatrix3x3::getIdentity(), capsulePositionFrom2);
	bool b2 = convexCaster.calcTimeOfImpact(convexShapeFrom2, convexShapeTo, triangleToWorld, triangleToWorld, castResult2);

	btScalar pos1 = convexShapeFrom1.getOrigin()[1] + castResult1.m_fraction * (convexShapeTo.getOrigin()[1] - convexShapeFrom1.getOrigin()[1]);
	btScalar pos2 = convexShapeFrom2.getOrigin()[1] + castResult2.m_fraction * (convexShapeTo.getOrigin()[1] - convexShapeFrom2.getOrigin()[1]);
	if( abs(pos1 - pos2) > .001 ) {
		printf("Error!\n");
	} else {
		printf("No error\n");
	}


	btPointCollector pointCollectors[2];
	for( int i = 0; i < 2; ++i ) {
		btGjkPairDetector gjk(capsuleShape,&triangleShape,capsuleShape->getShapeType(),triangleShape.getShapeType(),capsuleShape->getMargin(),triangleShape.getMargin(),&simplexSolver,&gjkEpaPenetrationSolver);
		btGjkPairDetector::ClosestPointInput input;
		input.m_transformB = triangleToWorld;
		if( i == 0 )
			input.m_transformA = convexShapeFrom1;
		else
			input.m_transformA = convexShapeFrom2;
		PrintInput(input);
		gjk.getClosestPoints(input, pointCollectors[i], 0);
		printf("dist: %f\n", pointCollectors[i].m_distance);
	}
}


void BasicExample::renderScene()
{
	CommonRigidBodyBase::renderScene();
	
}







CommonExampleInterface*    BasicExampleCreateFunc(CommonExampleOptions& options)
{
	return new BasicExample(options.m_guiHelper);
}



