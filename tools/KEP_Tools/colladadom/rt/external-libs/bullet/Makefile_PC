#!/usr/bin/make
# Copyright 2006 Sony Computer Entertainment Inc.
#
# Licensed under the SCEA Shared Source License, Version 1.0 (the "License"); you may not use this 
# file except in compliance with the License. You may obtain a copy of the License at:
# http://research.scea.com/scea_shared_source_license.html
#
# Unless required by applicable law or agreed to in writing, software distributed under the License 
# is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
# implied. See the License for the specific language governing permissions and limitations under the 
# License. 

# target file(s)
ROOT_DIR         :=	.
LIBRARY          := bullet
OUTPUT_DIR       := $(ROOT_DIR)/lib

# definitions
include         $(ROOT_DIR)/build/makedefs.txt

SRC := \
src/BulletCollision/BroadphaseCollision/btAxisSweep3.cpp \
src/BulletCollision/BroadphaseCollision/btBroadphaseProxy.cpp \
src/BulletCollision/BroadphaseCollision/btCollisionAlgorithm.cpp \
src/BulletCollision/BroadphaseCollision/btDispatcher.cpp \
src/BulletCollision/BroadphaseCollision/btOverlappingPairCache.cpp \
src/BulletCollision/BroadphaseCollision/btSimpleBroadphase.cpp \
src/BulletCollision/CollisionDispatch/SphereTriangleDetector.cpp \
src/BulletCollision/CollisionDispatch/btCollisionDispatcher.cpp \
src/BulletCollision/CollisionDispatch/btCollisionObject.cpp \
src/BulletCollision/CollisionDispatch/btCollisionWorld.cpp \
src/BulletCollision/CollisionDispatch/btCompoundCollisionAlgorithm.cpp \
src/BulletCollision/CollisionDispatch/btConvexConcaveCollisionAlgorithm.cpp \
src/BulletCollision/CollisionDispatch/btConvexConvexAlgorithm.cpp \
src/BulletCollision/CollisionDispatch/btEmptyCollisionAlgorithm.cpp \
src/BulletCollision/CollisionDispatch/btManifoldResult.cpp \
src/BulletCollision/CollisionDispatch/btSimulationIslandManager.cpp \
src/BulletCollision/CollisionDispatch/btSphereBoxCollisionAlgorithm.cpp \
src/BulletCollision/CollisionDispatch/btSphereSphereCollisionAlgorithm.cpp \
src/BulletCollision/CollisionDispatch/btSphereTriangleCollisionAlgorithm.cpp \
src/BulletCollision/CollisionDispatch/btUnionFind.cpp \
src/BulletCollision/CollisionShapes/btBoxShape.cpp \
src/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.cpp \
src/BulletCollision/CollisionShapes/btCapsuleShape.cpp \
src/BulletCollision/CollisionShapes/btCollisionShape.cpp \
src/BulletCollision/CollisionShapes/btCompoundShape.cpp \
src/BulletCollision/CollisionShapes/btConcaveShape.cpp \
src/BulletCollision/CollisionShapes/btConeShape.cpp \
src/BulletCollision/CollisionShapes/btConvexHullShape.cpp \
src/BulletCollision/CollisionShapes/btConvexShape.cpp \
src/BulletCollision/CollisionShapes/btConvexTriangleMeshShape.cpp \
src/BulletCollision/CollisionShapes/btCylinderShape.cpp \
src/BulletCollision/CollisionShapes/btEmptyShape.cpp \
src/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.cpp \
src/BulletCollision/CollisionShapes/btMinkowskiSumShape.cpp \
src/BulletCollision/CollisionShapes/btMultiSphereShape.cpp \
src/BulletCollision/CollisionShapes/btOptimizedBvh.cpp \
src/BulletCollision/CollisionShapes/btPolyhedralConvexShape.cpp \
src/BulletCollision/CollisionShapes/btSphereShape.cpp \
src/BulletCollision/CollisionShapes/btStaticPlaneShape.cpp \
src/BulletCollision/CollisionShapes/btStridingMeshInterface.cpp \
src/BulletCollision/CollisionShapes/btTetrahedronShape.cpp \
src/BulletCollision/CollisionShapes/btTriangleBuffer.cpp \
src/BulletCollision/CollisionShapes/btTriangleCallback.cpp \
src/BulletCollision/CollisionShapes/btTriangleIndexVertexArray.cpp \
src/BulletCollision/CollisionShapes/btTriangleMesh.cpp \
src/BulletCollision/CollisionShapes/btTriangleMeshShape.cpp \
src/BulletCollision/NarrowPhaseCollision/btContinuousConvexCollision.cpp \
src/BulletCollision/NarrowPhaseCollision/btConvexCast.cpp \
src/BulletCollision/NarrowPhaseCollision/btGjkConvexCast.cpp \
src/BulletCollision/NarrowPhaseCollision/btGjkEpa.cpp \
src/BulletCollision/NarrowPhaseCollision/btGjkEpaPenetrationDepthSolver.cpp \
src/BulletCollision/NarrowPhaseCollision/btGjkPairDetector.cpp \
src/BulletCollision/NarrowPhaseCollision/btMinkowskiPenetrationDepthSolver.cpp \
src/BulletCollision/NarrowPhaseCollision/btPersistentManifold.cpp \
src/BulletCollision/NarrowPhaseCollision/btRaycastCallback.cpp \
src/BulletCollision/NarrowPhaseCollision/btSubSimplexConvexCast.cpp \
src/BulletCollision/NarrowPhaseCollision/btVoronoiSimplexSolver.cpp \
src/BulletDynamics/ConstraintSolver/btContactConstraint.cpp \
src/BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.cpp \
src/BulletDynamics/ConstraintSolver/btHingeConstraint.cpp \
src/BulletDynamics/ConstraintSolver/btPoint2PointConstraint.cpp \
src/BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.cpp \
src/BulletDynamics/ConstraintSolver/btSolve2LinearConstraint.cpp \
src/BulletDynamics/ConstraintSolver/btTypedConstraint.cpp \
src/BulletDynamics/Dynamics/btDiscreteDynamicsWorld.cpp \
src/BulletDynamics/Dynamics/btRigidBody.cpp \
src/BulletDynamics/Dynamics/btSimpleDynamicsWorld.cpp \
src/BulletDynamics/Vehicle/btRaycastVehicle.cpp \
src/BulletDynamics/Vehicle/btWheelInfo.cpp \
src/LinearMath/btAlignedAllocator.cpp \
src/LinearMath/btGeometryUtil.cpp \
src/LinearMath/btQuickprof.cpp \

HEADERS := \
src/btBulletCollisionCommon.h \
src/btBulletDynamicsCommon.h \
src/BulletCollision/BroadphaseCollision/btAxisSweep3.h \
src/BulletCollision/BroadphaseCollision/btBroadphaseInterface.h \
src/BulletCollision/BroadphaseCollision/btBroadphaseProxy.h \
src/BulletCollision/BroadphaseCollision/btCollisionAlgorithm.h \
src/BulletCollision/BroadphaseCollision/btDispatcher.h \
src/BulletCollision/BroadphaseCollision/btOverlappingPairCache.h \
src/BulletCollision/BroadphaseCollision/btSimpleBroadphase.h \
src/BulletCollision/CollisionDispatch/SphereTriangleDetector.h \
src/BulletCollision/CollisionDispatch/btCollisionCreateFunc.h \
src/BulletCollision/CollisionDispatch/btCollisionDispatcher.h \
src/BulletCollision/CollisionDispatch/btCollisionObject.h \
src/BulletCollision/CollisionDispatch/btCollisionWorld.h \
src/BulletCollision/CollisionDispatch/btCompoundCollisionAlgorithm.h \
src/BulletCollision/CollisionDispatch/btConvexConcaveCollisionAlgorithm.h \
src/BulletCollision/CollisionDispatch/btConvexConvexAlgorithm.h \
src/BulletCollision/CollisionDispatch/btEmptyCollisionAlgorithm.h \
src/BulletCollision/CollisionDispatch/btManifoldResult.h \
src/BulletCollision/CollisionDispatch/btSimulationIslandManager.h \
src/BulletCollision/CollisionDispatch/btSphereBoxCollisionAlgorithm.h \
src/BulletCollision/CollisionDispatch/btSphereSphereCollisionAlgorithm.h \
src/BulletCollision/CollisionDispatch/btSphereTriangleCollisionAlgorithm.h \
src/BulletCollision/CollisionDispatch/btUnionFind.h \
src/BulletCollision/CollisionShapes/btBoxShape.h \
src/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.h \
src/BulletCollision/CollisionShapes/btCapsuleShape.h \
src/BulletCollision/CollisionShapes/btCollisionMargin.h \
src/BulletCollision/CollisionShapes/btCollisionShape.h \
src/BulletCollision/CollisionShapes/btCompoundShape.h \
src/BulletCollision/CollisionShapes/btConcaveShape.h \
src/BulletCollision/CollisionShapes/btConeShape.h \
src/BulletCollision/CollisionShapes/btConvexHullShape.h \
src/BulletCollision/CollisionShapes/btConvexShape.h \
src/BulletCollision/CollisionShapes/btConvexTriangleMeshShape.h \
src/BulletCollision/CollisionShapes/btCylinderShape.h \
src/BulletCollision/CollisionShapes/btEmptyShape.h \
src/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h \
src/BulletCollision/CollisionShapes/btMinkowskiSumShape.h \
src/BulletCollision/CollisionShapes/btMultiSphereShape.h \
src/BulletCollision/CollisionShapes/btOptimizedBvh.h \
src/BulletCollision/CollisionShapes/btPolyhedralConvexShape.h \
src/BulletCollision/CollisionShapes/btSphereShape.h \
src/BulletCollision/CollisionShapes/btStaticPlaneShape.h \
src/BulletCollision/CollisionShapes/btStridingMeshInterface.h \
src/BulletCollision/CollisionShapes/btTetrahedronShape.h \
src/BulletCollision/CollisionShapes/btTriangleBuffer.h \
src/BulletCollision/CollisionShapes/btTriangleCallback.h \
src/BulletCollision/CollisionShapes/btTriangleIndexVertexArray.h \
src/BulletCollision/CollisionShapes/btTriangleMesh.h \
src/BulletCollision/CollisionShapes/btTriangleMeshShape.h \
src/BulletCollision/CollisionShapes/btTriangleShape.h \
src/BulletCollision/NarrowPhaseCollision/btContinuousConvexCollision.h \
src/BulletCollision/NarrowPhaseCollision/btConvexCast.h \
src/BulletCollision/NarrowPhaseCollision/btConvexPenetrationDepthSolver.h \
src/BulletCollision/NarrowPhaseCollision/btDiscreteCollisionDetectorInterface.h \
src/BulletCollision/NarrowPhaseCollision/btGjkConvexCast.h \
src/BulletCollision/NarrowPhaseCollision/btGjkEpa.h \
src/BulletCollision/NarrowPhaseCollision/btGjkEpaPenetrationDepthSolver.h \
src/BulletCollision/NarrowPhaseCollision/btGjkPairDetector.h \
src/BulletCollision/NarrowPhaseCollision/btManifoldPoint.h \
src/BulletCollision/NarrowPhaseCollision/btMinkowskiPenetrationDepthSolver.h \
src/BulletCollision/NarrowPhaseCollision/btPersistentManifold.h \
src/BulletCollision/NarrowPhaseCollision/btPointCollector.h \
src/BulletCollision/NarrowPhaseCollision/btRaycastCallback.h \
src/BulletCollision/NarrowPhaseCollision/btSimplexSolverInterface.h \
src/BulletCollision/NarrowPhaseCollision/btSubSimplexConvexCast.h \
src/BulletCollision/NarrowPhaseCollision/btVoronoiSimplexSolver.h \
src/BulletDynamics/ConstraintSolver/btConstraintSolver.h \
src/BulletDynamics/ConstraintSolver/btContactConstraint.h \
src/BulletDynamics/ConstraintSolver/btContactSolverInfo.h \
src/BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.h \
src/BulletDynamics/ConstraintSolver/btHingeConstraint.h \
src/BulletDynamics/ConstraintSolver/btJacobianEntry.h \
src/BulletDynamics/ConstraintSolver/btPoint2PointConstraint.h \
src/BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.h \
src/BulletDynamics/ConstraintSolver/btSolve2LinearConstraint.h \
src/BulletDynamics/ConstraintSolver/btSolverBody.h \
src/BulletDynamics/ConstraintSolver/btSolverConstraint.h \
src/BulletDynamics/ConstraintSolver/btTypedConstraint.h \
src/src/BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h \
src/BulletDynamics/Dynamics/btDynamicsWorld.h \
src/BulletDynamics/Dynamics/btRigidBody.h \
src/BulletDynamics/Dynamics/btSimpleDynamicsWorld.h \
src/BulletDynamics/Vehicle/btRaycastVehicle.h \
src/BulletDynamics/Vehicle/btVehicleRaycaster.h \
src/BulletDynamics/Vehicle/btWheelInfo.h \
src/LinearMath/btAabbUtil2.h \
src/LinearMath/btAlignedAllocator.h \
src/LinearMath/btAlignedObjectArray.h \
src/LinearMath/btDefaultMotionState.h \
src/LinearMath/btGeometryUtil.h \
src/LinearMath/btIDebugDraw.h \
src/LinearMath/btList.h \
src/LinearMath/btMatrix3x3.h \
src/LinearMath/btMinMax.h \
src/LinearMath/btMotionState.h \
src/LinearMath/btPoint3.h \
src/LinearMath/btQuadWord.h \
src/LinearMath/btQuaternion.h \
src/LinearMath/btQuickprof.h \
src/LinearMath/btRandom.h \
src/LinearMath/btScalar.h \
src/LinearMath/btSimdMinMax.h \
src/LinearMath/btStackAlloc.h \
src/LinearMath/btTransform.h \
src/LinearMath/btTransformUtil.h \
src/LinearMath/btVector3.h \

INCLUDE_DIR		 += $(ROOT_DIR)/src

# rules
include          $(ROOT_DIR)/build/makerules.txt
