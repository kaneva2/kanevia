#-*-makefile-*-
# Copyright 2006 Sony Computer Entertainment Inc.
#
# Licensed under the SCEA Shared Source License, Version 1.0 (the "License"); you may not use this 
# file except in compliance with the License. You may obtain a copy of the License at:
# http://research.scea.com/scea_shared_source_license.html
#
# Unless required by applicable law or agreed to in writing, software distributed under the License 
# is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
# implied. See the License for the specific language governing permissions and limitations under the 
# License. 
#
# 
#
# Common Makefile definitions
#
#


#########################################################
# detect platform
ifneq ($(WIN32),)
PLATFORM_NAME       := win32
else
PLATFORM_NAME       := linux
endif

#########################################################
# COLLADA version
ifeq ($(COLLADA_DOM_VERSION),)
export COLLADA_DOM_VERSION = 1.4
endif

#########################################################
# configuration - This is a unique string that identifies the build config. It's used
#                 to generate build output paths.
CONF_NAME := $(PLATFORM_NAME)_$(COLLADA_DOM_VERSION)
ifneq ($(RELEASE),1)
CONF_NAME := $(CONF_NAME)_debug
endif

#########################################################
# target name
ifneq ($(EXECUTABLE),)
OUT_NAME            := $(EXECUTABLE)
else
OUT_NAME            := $(LIBRARY)
endif

#########################################################
# create paths
INTERMEDIATE_DIR = $(ROOT_DIR)/tmp/$(CONF_NAME)/$(OUT_NAME)/

#########################################################
# platform specific definitions
ifneq ($(PLATFORM_NAME),win32)
include         $(ROOT_DIR)/build/linuxdefs.txt
else
include         $(ROOT_DIR)/build/win32defs.txt
endif

#########################################################
# common settings
#QUIET           := @

ifeq ($(RELEASE),1)
CCOPT			+= $(RELEASE_CCOPT)
else
CCOPT			+= $(DEBUG_CCOPT)
endif

TMPDIR			= $(OUTPUT_DIR)/$(CONF_NAME)
CREATE_DIR		+= $(INTERMEDIATE_DIR) $(TMPDIR)

OUTDIR			:= $(TMPDIR)/

#########################################################
#build type specific settings

ifneq ($(LIBRARY),)

# library build settings
TARGET          := $(OUTDIR)$(LIB_PREFIX)$(LIBRARY)$(LIB_SUFFIX)
CCOPT			+= $(LIB_CCOPT)
else

ifneq ($(EXECUTABLE),)

# executable build settings
TARGET          := $(OUTDIR)$(EXECUTABLE)$(EXE_SUFFIX)
LCOPT     += $(EXE_LCOPT)
endif
endif
