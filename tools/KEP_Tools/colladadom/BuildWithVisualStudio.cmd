@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=colladadom
SET SolutionToBuild="%ThisDir%dom\projects\vc15\dom.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%dom\include %IncludeDir%\colladadom /S /I /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%dom\build\vc15-1.4\libcollada14dom21.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%dom\build\vc15-1.4\libcollada14dom21.dll %BinDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%dom\build\vc15-1.4\libcollada14dom21.pdb %BinDir%\ /I /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%dom\build\vc15-1.4-d\libcollada14dom21-d.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%dom\build\vc15-1.4-d\libcollada14dom21-d.dll %BinDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%dom\build\vc15-1.4-d\libcollada14dom21-d.pdb %BinDir%\ /I /Y
)