<?php
/*
 * Copyright 2007 Sony Computer Entertainment Inc.
 *
 * Licensed under the SCEA Shared Source License, Version 1.0 (the "License"); you may not use this 
 * file except in compliance with the License. You may obtain a copy of the License at:
 * http://research.scea.com/scea_shared_source_license.html
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions and limitations under the 
 * License. 
 */

require_once( 'om/base-types.php' );

require_once( 'om/xsAll.php' );
require_once( 'om/xsAnnotation.php' );
require_once( 'om/xsAny.php' );
require_once( 'om/xsAttribute.php' );
require_once( 'om/xsChoice.php' );
require_once( 'om/xsComplexContent.php' );
require_once( 'om/xsComplexType.php' );
require_once( 'om/xsAll.php' );
require_once( 'om/xsDocumentation.php' );
require_once( 'om/xsAppinfo.php' );
require_once( 'om/xsElement.php' );
require_once( 'om/xsEnumeration.php' );
require_once( 'om/xsExtension.php' );
require_once( 'om/xsGroup.php' );
require_once( 'om/xsList.php' );
require_once( 'om/xsMaxLength.php' );
require_once( 'om/xsMaxExclusive.php' );
require_once( 'om/xsMaxInclusive.php' );
require_once( 'om/xsMinLength.php' );
require_once( 'om/xsMinExclusive.php' );
require_once( 'om/xsMinInclusive.php' );
require_once( 'om/xsPattern.php' );
require_once( 'om/xsRestriction.php' );
require_once( 'om/xsSchema.php' );
require_once( 'om/xsSequence.php' );
require_once( 'om/xsSimpleContent.php' );
require_once( 'om/xsSimpleType.php' );
require_once( 'om/xsUnion.php' );
require_once( 'om/xsWhiteSpace.php' );
require_once( 'om/xsImport.php' );

?>