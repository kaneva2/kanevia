/******************************************************************************
 enumobject.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#ifndef Py_ENUMOBJECT_H
#define Py_ENUMOBJECT_H

/* Enumerate Object */

#ifdef __cplusplus
extern "C" {
#endif

PyAPI_DATA(PyTypeObject) PyEnum_Type;
PyAPI_DATA(PyTypeObject) PyReversed_Type;

#ifdef __cplusplus
}
#endif

#endif /* !Py_ENUMOBJECT_H */
