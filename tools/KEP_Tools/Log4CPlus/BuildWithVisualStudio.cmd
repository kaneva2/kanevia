@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=Log4CPlus-1.1.3-rc2
SET SolutionToBuild="%ThisDir%1.1.3-rc2\msvc10and15\log4cplus_vs2015.sln"
SET DebugConfiguration="debug_unicode"
SET ReleaseConfiguration="release_unicode"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\include\log4cplus %IncludeDir%\Log4CPlus\ /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Debug_Unicode\log4cplusSUD.lib %LibDir%\log4cplusSUD.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Debug_Unicode\log4cplusSUD.pdb %LibDir%\log4cplusSUD.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Debug_Unicode\log4cplusUD.lib %LibDir%\log4cplusUD.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Debug_Unicode\log4cplusUD.dll %BinDir%\log4cplusUD.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Debug_Unicode\log4cplusUD.pdb %BinDir%\log4cplusUD.pdb* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Release_Unicode\log4cplusSU.lib %LibDir%\log4cplusSU.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Release_Unicode\log4cplusSU.pdb %LibDir%\log4cplusSU.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Release_Unicode\log4cplusU.lib %LibDir%\log4cplusU.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Release_Unicode\log4cplusU.dll %BinDir%\log4cplusU.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%1.1.3-rc2\msvc10and15\Win32\bin.Release_Unicode\log4cplusU.pdb %BinDir%\log4cplusU.pdb* /Y
)
