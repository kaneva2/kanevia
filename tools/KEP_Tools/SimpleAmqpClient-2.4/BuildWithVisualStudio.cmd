@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=SimpleAmqpClient-2.4
SET SolutionToBuild="%ThisDir%simpleamqpclient-build\SimpleAmqpClient.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%src\SimpleAmqpClient %IncludeDir%\SimpleAmqpClient /S /E /I /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%simpleamqpclient-build\Release\SimpleAmqpClient.2.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%simpleamqpclient-build\Release\SimpleAmqpClient.2.dll %BinDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%simpleamqpclient-build\Release\SimpleAmqpClient.2.pdb %BinDir%\ /I /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%simpleamqpclient-build\Debug\SimpleAmqpClient.2d.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%simpleamqpclient-build\Debug\SimpleAmqpClient.2d.dll %BinDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%simpleamqpclient-build\Debug\SimpleAmqpClient.2d.pdb %BinDir%\ /I /Y
)
