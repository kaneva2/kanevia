@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=libcpuid
SET SolutionToBuild="%ThisDir%libcpuid_vc14.sln"
rem SET DebugConfiguration="Debug"
rem SET ReleaseConfiguration="Release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%libcpuid\*.h %IncludeDir%\libcpuid\ /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%libcpuid\Debug\libcpuid.lib %LibDir%\Debug\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%libcpuid\Debug\libcpuid.pdb %LibDir%\Debug\ /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%libcpuid\Release\libcpuid.lib %LibDir%\Release\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%libcpuid\Release\libcpuid.pdb %LibDir%\Release\ /Y
)
