@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=libjpeg-turbo-1.4.2
SET SolutionToBuild="%ThisDir%libjpeg-turbo_vs2015.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%jconfig.h %IncludeDir%\libjpeg-turbo\jconfig.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jerror.h %IncludeDir%\libjpeg-turbo\jerror.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jmorecfg.h %IncludeDir%\libjpeg-turbo\jmorecfg.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jpeglib.h %IncludeDir%\libjpeg-turbo\jpeglib.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%turbojpeg.h %IncludeDir%\libjpeg-turbo\turbojpeg.h* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jpeg-static.lib %LibDir%\jpeg-static.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jpeg-static.pdb %LibDir%\jpeg-static.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jpeg62.lib %LibDir%\jpeg62.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\jpeg62.dll %BinDir%\jpeg62.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\jpeg62.pdb %BinDir%\jpeg62.pdb* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%\lib\jpeg-staticd.lib %LibDir%\jpeg-staticd.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jpeg-staticd.pdb %LibDir%\jpeg-staticd.pdb* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jpeg62d.lib %LibDir%\jpeg62d.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\jpeg62d.dll %BinDir%\jpeg62d.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\jpeg62d.pdb %BinDir%\jpeg62d.pdb* /Y
)
