@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=lua-5.1.5-coco
SET SolutionToBuild="%ThisDir%KanevaBuild\alllua.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%lua-5.1.5-coco\src\lauxlib.h %IncludeDir%\lua-coco\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lua-5.1.5-coco\src\lcoco.h %IncludeDir%\lua-coco\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lua-5.1.5-coco\src\lua.h %IncludeDir%\lua-coco\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lua-5.1.5-coco\src\luaconf.h %IncludeDir%\lua-coco\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lua-5.1.5-coco\src\lualib.h %IncludeDir%\lua-coco\ /I /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\lualib_515co.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\lualib_515co.pdb %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\luadll_515co.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\luadll_515co.dll %BinDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\luadll_515co.pdb %BinDir%\ /I /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\lualib_515cod.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\lualib_515cod.pdb %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\luadll_515cod.lib %LibDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\luadll_515cod.dll %BinDir%\ /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%redist\luadll_515cod.pdb %BinDir%\ /I /Y
)