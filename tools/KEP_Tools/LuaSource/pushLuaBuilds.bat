@ECHO OFF
SETLOCAL
IF %1.==. GOTO Syntax
IF %2.==. GOTO Syntax

SET WorkFoldTmp=~push.WORKFOLD.TMP
SET WorkFoldTmp2=~push.WORKFOLD.TMP2
SET LuaVer=%1
SET LibDir=%2
SET TfsMainlineToolsLua=$/Mainline/Mainline/Source/Platform/Tools/%LibDir%
IF %3.==/deploy. SET TfsMainlineDeploy=$/Mainline/Mainline/Deploy
SET WORKDIR=%CD%

SET LUA_COMPILER=luac_%LuaVer%
SET LUA_DLL=luadll_%LuaVer%
SET LUA_DLLd=luadll_%LuaVer%d
SET LUA_LIB=lualib_%LuaVer%
SET LUA_LIBd=lualib_%LuaVer%d
SET MasterList=(%LUA_DLL%.dll, %LUA_DLL%.pdb, %LUA_DLL%.lib, %LUA_DLLd%.dll, %LUA_DLLd%.pdb, %LUA_DLLd%.lib, %LUA_LIB%.lib, %LUA_LIB%.pdb, %LUA_LIBd%.lib, %LUA_LIBd%.pdb)
SET HeaderList=(lua.h, luaconf.h, lualib.h, lauxlib.h)

IF NOT EXIST redist GOTO MissingRedist

FOR %%a IN %MasterList% DO (
	IF NOT EXIST %WORKDIR%\redist\%%a SET MissingBuildOutput=%%a && GOTO MissingBuilds
)

SET TfsServerPath=%TfsMainlineToolsLua%
tf workfold %TfsServerPath% > %WorkFoldTmp%
IF ERRORLEVEL 1 GOTO NoWorkFold
tail -n 1 %WorkFoldTmp% | sed "s/.*: //" > %WorkFoldTmp2%
DEL %WorkFoldTmp%
SET /P MainlineToolsLua=< %WorkFoldTmp2%
DEL %WorkFoldTmp2%

IF %TfsMainlineDeploy%.==. GOTO NoDeploy
SET TfsServerPath=%TfsMainlineDeploy%
tf workfold %TfsServerPath% > %WorkFoldTmp%
IF ERRORLEVEL 1 GOTO NoWorkFold
tail -n 1 %WorkFoldTmp% | sed "s/.*: //" > %WorkFoldTmp2%
DEL %WorkFoldTmp%
SET /P MainlineDeploy=< %WorkFoldTmp2%
DEL %WorkFoldTmp2%
:NoDeploy

ECHO ==================================================
ECHO === Build Version    : %LuaVer%
ECHO === Push Destination : %TfsMainlineToolsLua%
IF NOT %TfsMainlineDeploy%.==. ECHO === Also Deploy To   : %MainlineDeploy%
ECHO ==================================================
ECHO.

ECHO === Push header files to %MainlineToolsLua%\include

PUSHD %MainlineToolsLua%\include
FOR %%a IN %HeaderList% DO CALL :PushOneFile "%WORKDIR%\%LibDir%\src" %%a
POPD

ECHO === Push libraries to to %MainlineToolsLua%\lib

PUSHD %MainlineToolsLua%\lib
FOR %%a IN %MasterList% DO CALL :PushOneFile "%WORKDIR%\redist" %%a
POPD

IF %TfsMainlineDeploy%.==. GOTO SkipDeployFolders

ECHO === Deploy to %MainlineDeploy%\GameContent\bin

PUSHD %MainlineDeploy%\GameContent\bin
CALL :PushOneFile "%WORKDIR%\redist" %LUA_DLL%.dll
CALL :PushOneFile "%WORKDIR%\redist" %LUA_COMPILER%.exe
POPD

ECHO === Deploy to %MainlineDeploy%\GameContent\bind

PUSHD %MainlineDeploy%\GameContent\bind
CALL :PushOneFile "%WORKDIR%\redist" %LUA_DLLd%.dll
CALL :PushOneFile "%WORKDIR%\redist" %LUA_COMPILER%d.exe
POPD

ECHO === Deploy to %MainlineDeploy%\PDBs

PUSHD %MainlineDeploy%\PDBs
CALL :PushOneFile "%WORKDIR%\redist" %LUA_DLL%.pdb
CALL :PushOneFile "%WORKDIR%\redist" %LUA_DLLd%.pdb
POPD

ECHO === Deploy to %MainlineDeploy%\GameContent\templates\Shared

PUSHD %MainlineDeploy%\GameContent\templates\Shared
CALL :PushOneFile "%WORKDIR%\redist" %LUA_DLL%.dll
CALL :PushOneFile "%WORKDIR%\redist" %LUA_DLLd%.dll
CALL :PushOneFile "%WORKDIR%\redist" %LUA_COMPILER%.exe
CALL :PushOneFile "%WORKDIR%\redist" %LUA_COMPILER%d.exe
POPD

:SkipDeployFolders

GOTO :EOF

:MissingRedist
ECHO *** Lua build output folder "redist" does not exist in current directory
GOTO Abort

:MissingBuilds
ECHO *** Lua build output [%MissingBuildOutput%] are not found under redist\
GOTO Abort

:NoWorkFold
ECHO *** Working directory for %TfsServerPath% is not defined
DEL %WorkFoldTmp% 2>NUL
GOTO Abort

:CheckOutFailed
ECHO *** Error checking out file from TFS
GOTO Abort

:CopyFailed
ECHO *** Error copying file
GOTO Abort

:Syntax
ECHO pushLuaBuilds ^<BuildVersion^> ^<LibName^> [/deploy]
ECHO - ^<BuildVersion^> - Build version used in output file names. E.g. 515
ECHO - ^<LibName^>      - The Lua library folder under Platform\Tools where builds will be pushed to. E.g. lua-5.1.5 or lua-5.1.5-coco
ECHO - /deploy          - If specified, also deploy Lua binaries and symbols to folders under Mainline\Deploy
ECHO - Destination files will be checked out automatically before getting overwritten.
ECHO.
GOTO :EOF

:Abort
ECHO *** Abort...
PAUSE
EXIT /B 1

:PushOneFile
ECHO ^>^>^> %2
tf checkout %2 >NUL
IF ERRORLEVEL 1 GOTO CheckOutFailed
copy %1\%2 >NUL
IF ERRORLEVEL 1 GOTO CopyFailed
GOTO :EOF
