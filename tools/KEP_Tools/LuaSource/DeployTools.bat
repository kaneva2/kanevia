cls
@echo off
echo.========================================================
echo.================= lua build move =======================
echo.========================================================
echo.
echo.
echo. 1) 5.0.2
echo. 2) 5.1.4
echo. 3) 5.2.2
echo. 4) all
echo. 5) Exit 
echo.

choice /c:12345 /N /D 5 /T:10

if errorlevel 5 goto End
if errorlevel 4 goto LuaAll
if errorlevel 3 goto Lua522
if errorlevel 2 goto Lua514
if errorlevel 1 goto Lua502


REM 
REM srcRoot: lua root folder 
REM srcDir:  lua subfolder where libraries and exe are
REM dstRoot: destination folder
REM dstDir:  destination subfolder
REM 



:LuaAll
SET srcRoot=C:\dev\Tools_2010\KEP_Tools\LuaSource
SET srcDir=redist
SET dstRoot=C:\KanevaTools
SET dstDir=Lua

echo f | xcopy /f /Y "%srcRoot%\%srcDir%\lua_522.exe"	"%dstRoot%\%dstDir%\lua_522.exe"
echo f | xcopy /f /Y "%srcRoot%\%srcDir%\luac_522.exe"	"%dstRoot%\%dstDir%\luac_522.exe"
echo f | xcopy /f /Y "%srcRoot%\%srcDir%\lua_514.exe"	"%dstRoot%\%dstDir%\lua_514.exe"
echo f | xcopy /f /Y "%srcRoot%\%srcDir%\luac_514.exe"	"%dstRoot%\%dstDir%\luac_514.exe"
echo f | xcopy /f /Y "%srcRoot%\%srcDir%\lua_502.exe"	"%dstRoot%\%dstDir%\lua_502.exe"
echo f | xcopy /f /Y "%srcRoot%\%srcDir%\luac_502.exe"	"%dstRoot%\%dstDir%\luac_502.exe"
goto End


:Lua522
SET srcRoot=C:\dev\Tools_2010\KEP_Tools\LuaSource
SET srcDir=redist
SET dstRoot=C:\KanevaTools
SET dstDir=Lua

echo f | xcopy /f /Y "%srcRoot%\%srcDir%\lua_522.exe"	"%dstRoot%\%dstDir%\lua_522.exe"
echo f | xcopy /f /Y "%srcRoot%\%srcDir%\luac_522.exe"	"%dstRoot%\%dstDir%\luac_522.exe"
goto End


:Lua514
SET srcRoot=C:\dev\Tools_2010\KEP_Tools\LuaSource
SET srcDir=redist
SET dstRoot=C:\KanevaTools
SET dstDir=Lua

echo f | xcopy /f /Y "%srcRoot%\%srcDir%\lua_514.exe"	"%dstRoot%\%dstDir%\lua_514.exe"
echo f | xcopy /f /Y "%srcRoot%\%srcDir%\luac_514.exe"	"%dstRoot%\%dstDir%\luac_514.exe"
goto End


:Lua502
SET srcRoot=C:\dev\Tools_2010\KEP_Tools\LuaSource\
SET srcDir=redist
SET dstRoot=C:\KanevaTools
SET dstDir=Lua

echo f | xcopy /f /Y "%srcRoot%\%srcDir%\lua_502.exe"	"%dstRoot%\%dstDir%\lua_502.exe"
echo f | xcopy /f /Y "%srcRoot%\%srcDir%\luac_502.exe"	"%dstRoot%\%dstDir%\luac_502.exe"
goto End


:End
PAUSE
