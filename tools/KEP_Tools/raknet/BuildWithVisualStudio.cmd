@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=RakNet-4.081
SET SolutionToBuild="%ThisDir%RakNet_vs2015.sln"
rem SET DebugConfiguration="Debug"
rem SET ReleaseConfiguration="Release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (

	call %ScriptsDir%\xcopy.cmd %ThisDir%Source\*.h %IncludeDir%\RakNet\ /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_LibStatic_Debug_Win32.lib %LibDir%\RakNet_VS2015_LibStatic_Debug_Win32.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_LibStatic_Debug_Win32.pdb %LibDir%\RakNet_VS2015_LibStatic_Debug_Win32.pdb* /Y
	rem call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_DLL_Debug_Win32.lib %LibDir%\RakNet_VS2015_DLL_Debug_Win32.lib* /Y
	rem call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_DLL_Debug_Win32.dll %BinDir%\RakNet_VS2015_DLL_Debug_Win32.dll* /Y
	rem call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_DLL_Debug_Win32.pdb %BinDir%\RakNet_VS2015_DLL_Debug_Win32.pdb* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_LibStatic_Release_Win32.lib %LibDir%\RakNet_VS2015_LibStatic_Release_Win32.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_LibStatic_Release_Win32.pdb %LibDir%\RakNet_VS2015_LibStatic_Release_Win32.pdb* /Y
	rem call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_DLL_Release_Win32.lib %LibDir%\RakNet_VS2015_DLL_Release_Win32.lib* /Y
	rem call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_DLL_Release_Win32.dll %BinDir%\RakNet_VS2015_DLL_Release_Win32.dll* /Y
	rem call %ScriptsDir%\xcopy.cmd %ThisDir%Lib\RakNet_VS2015_DLL_Release_Win32.pdb %BinDir%\RakNet_VS2015_DLL_Release_Win32.pdb* /Y
)
