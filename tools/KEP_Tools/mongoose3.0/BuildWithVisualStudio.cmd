@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=mongoose3.0
SET SolutionToBuild="%ThisDir%Mongoose_vs2015.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"
SET PlatformToUse=win32

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%mongoose.h %IncludeDir%\mongoose3.0\mongoose.h* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%Debug\Mongoose3.0.lib %LibDir%\Debug\Mongoose3.0.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%Debug\Mongoose3.0.pdb %LibDir%\Debug\Mongoose3.0.pdb* /Y
	
	call %ScriptsDir%\xcopy.cmd %ThisDir%Release\Mongoose3.0.lib %LibDir%\Release\Mongoose3.0.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%Release\Mongoose3.0.pdb %LibDir%\Release\Mongoose3.0.pdb* /Y
)
